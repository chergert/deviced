/* devd-app-info.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "devd-app-info"

#include <json-glib/json-glib.h>
#include <jsonrpc-glib.h>

#include "devd-app-info.h"
#include "devd-util.h"

typedef struct
{
#define _PROPERTY(_1, name, field_type, _3, _pname, _4, _5, _6, _7) \
  field_type name;
#include "devd-app-info.defs"
#undef _PROPERTY
} DevdAppInfoPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (DevdAppInfo, devd_app_info, G_TYPE_OBJECT)

enum {
  PROP_0,
#define _PROPERTY(NAME, _1, _2, _3, _pname, _4, _5, _6, _7) \
  PROP_##NAME,
#include "devd-app-info.defs"
#undef _PROPERTY
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

DevdAppInfo *
devd_app_info_new (void)
{
  return g_object_new (DEVD_TYPE_APP_INFO, NULL);
}

static GVariant *
devd_app_info_real_to_variant (DevdAppInfo *self)
{
  return devd_to_gvariant (G_OBJECT (self));
}

GVariant *
devd_app_info_to_variant (DevdAppInfo *self)
{
  g_return_val_if_fail (DEVD_IS_APP_INFO (self), NULL);

  return DEVD_APP_INFO_GET_CLASS (self)->to_variant (self);
}

static void
devd_app_info_finalize (GObject *object)
{
  DevdAppInfo *self = (DevdAppInfo *)object;
  DevdAppInfoPrivate *priv = devd_app_info_get_instance_private (self);

#define _PROPERTY(NAME, name, _2, _3, _4, _5, _6, free_clause, _7) \
  G_STMT_START { free_clause } G_STMT_END;
# include "devd-app-info.defs"
#undef _PROPERTY

  G_OBJECT_CLASS (devd_app_info_parent_class)->finalize (object);
}

static void
devd_app_info_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  DevdAppInfo *self = DEVD_APP_INFO (object);

  switch (prop_id)
    {
#define _PROPERTY(NAME, name, _2, _3, _4, _5, _6, _7, value_type) \
    case PROP_##NAME: \
      g_value_set_##value_type (value, devd_app_info_get_##name (self)); \
      break;
# include "devd-app-info.defs"
#undef _PROPERTY

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
devd_app_info_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  DevdAppInfo *self = DEVD_APP_INFO (object);

  switch (prop_id)
    {
#define _PROPERTY(NAME, name, _2, _3, _4, _5, _6, _7, value_type) \
    case PROP_##NAME: \
      devd_app_info_set_##name (self, g_value_get_##value_type (value)); \
      break;
# include "devd-app-info.defs"
#undef _PROPERTY

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
devd_app_info_class_init (DevdAppInfoClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = devd_app_info_finalize;
  object_class->get_property = devd_app_info_get_property;
  object_class->set_property = devd_app_info_set_property;

  klass->to_variant = devd_app_info_real_to_variant;

#define _PROPERTY(NAME, name, _1, _2, _pname, pspec, _3, _4, _5) \
  properties [PROP_##NAME] = pspec;
# include "devd-app-info.defs"
#undef _PROPERTY

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
devd_app_info_init (DevdAppInfo *self)
{
}

#define _PROPERTY(_1, name, _2, ret_type, _pname, _3, _4, _5, _6) \
ret_type devd_app_info_get_##name (DevdAppInfo *self) \
{ \
  DevdAppInfoPrivate *priv = devd_app_info_get_instance_private (self); \
  g_return_val_if_fail (DEVD_IS_APP_INFO (self), (ret_type)0); \
  return priv->name; \
}
# include "devd-app-info.defs"
#undef _PROPERTY

#define _PROPERTY(NAME, name, _1, ret_type, _pname, _3, assign_stmt, _4, _5) \
void devd_app_info_set_##name (DevdAppInfo *self, \
                              ret_type      name) \
{ \
  DevdAppInfoPrivate *priv = devd_app_info_get_instance_private (self); \
  g_return_if_fail (DEVD_IS_APP_INFO (self)); \
  if (priv->name != name) { \
    G_STMT_START { assign_stmt } G_STMT_END; \
  } \
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_##NAME]); \
}
# include "devd-app-info.defs"
#undef _PROPERTY
