/* devd-flatpak-app-provider.c
 *
 * Copyright 2018 Patrick Griffis <tingping@tingping.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "devd-flatpak-app-provider"

#include "config.h"

#include <appstream-glib.h>
#include <errno.h>
#include <flatpak/flatpak.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>

#include "devd-app-info.h"
#include "devd-application.h"
#include "devd-app-provider.h"
#include "devd-flatpak-app-provider.h"
#include "devd-trace.h"
#include "process/devd-process-service.h"

typedef struct
{
  gchar *app_id;
  gint   pty_slave_fd;
} RunAppTaskData;

static void
run_app_task_data_free (gpointer data)
{
  RunAppTaskData *task_data = data;

  if (task_data->pty_slave_fd != -1)
    {
      close (task_data->pty_slave_fd);
      task_data->pty_slave_fd = -1;
    }

  g_clear_pointer (&task_data->app_id, g_free);
  g_slice_free (RunAppTaskData, task_data);
}

static gchar *
devd_appstream_name_from_ref (FlatpakInstalledRef *ref,
                              GCancellable        *cancellable)
{
  g_autofree gchar *appstream_path = NULL;
  g_autoptr(GFile) appstream_file = NULL;
  g_autoptr(AsStore) store = NULL;
  g_autofree gchar *app_id_with_suffix = NULL;
  const gchar *app_id;
  const gchar *name;
  AsApp *app = NULL;

  DEVD_ENTRY;

  g_assert (FLATPAK_IS_INSTALLED_REF (ref));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  app_id = flatpak_ref_get_name (FLATPAK_REF (ref));
  app_id_with_suffix = g_strconcat (app_id, ".desktop", NULL);

  appstream_path = g_strdup_printf ("%s/files/share/app-info/xmls/%s.xml.gz",
                                    flatpak_installed_ref_get_deploy_dir (ref),
                                    app_id);

  appstream_file = g_file_new_for_path (appstream_path);
  store = as_store_new ();

  if (!as_store_from_file (store, appstream_file, NULL, cancellable, NULL))
    DEVD_GOTO (failure);

  if (!(app = as_store_get_app_by_id (store, app_id)) &&
      !(app = as_store_get_app_by_id (store, app_id_with_suffix)))
    DEVD_GOTO (failure);

  // TODO: Get locale from client to translate
  if ((name = as_app_get_name (app, NULL)))
    DEVD_RETURN (g_strdup (name));

failure:
  DEVD_RETURN (g_strdup (app_id));
}


static DevdAppInfo *
devd_flatpak_info_from_ref (FlatpakRef   *ref,
                            GCancellable *cancellable)
{
  g_autofree gchar *id = NULL;
  g_autofree gchar *name = NULL;
  const gchar *commit_id = NULL;
  DevdAppInfo *info;
  guint64 size = 0;

  g_assert (FLATPAK_IS_REF (ref));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  info = devd_app_info_new ();

  id = g_strdup_printf ("%s/%s/%s",
                        flatpak_ref_get_name (ref),
                        flatpak_ref_get_arch (ref),
                        flatpak_ref_get_branch (ref));

  devd_app_info_set_id (info, id);
  devd_app_info_set_provider (info, "flatpak");

  if (FLATPAK_IS_INSTALLED_REF (ref))
    {
      commit_id = flatpak_installed_ref_get_latest_commit (FLATPAK_INSTALLED_REF (ref));
      size = flatpak_installed_ref_get_installed_size (FLATPAK_INSTALLED_REF (ref));
      name = devd_appstream_name_from_ref (FLATPAK_INSTALLED_REF (ref), cancellable);
    }
  else
    name = g_strdup (flatpak_ref_get_name (ref));

  devd_app_info_set_name (info, name);
  devd_app_info_set_installed_size (info, size);
  devd_app_info_set_commit_id (info, commit_id);

  return g_steal_pointer (&info);
}

static GPtrArray *
devd_flatpak_get_installations (GCancellable *cancellable,
                                GError       **error)
{
  g_autoptr(GPtrArray) installations = NULL;
  g_autoptr(FlatpakInstallation) user_install = NULL;

  if (!(installations = flatpak_get_system_installations (cancellable, error)))
    return NULL;

  if (!(user_install = flatpak_installation_new_user (cancellable, error)))
    return NULL;

  g_ptr_array_add (installations, g_steal_pointer (&user_install));

  return g_steal_pointer (&installations);
}

static void
devd_flatpak_list_apps_worker (GTask        *task,
                               gpointer      source_object,
                               gpointer      task_data,
                               GCancellable *cancellable)
{
  g_autoptr(GPtrArray) installations = NULL;
  g_autoptr(GPtrArray) installed_apps = NULL;
  g_autoptr(GError) error = NULL;
  gboolean list_runtimes = GPOINTER_TO_INT (task_data);
  FlatpakRefKind kind = list_runtimes ? FLATPAK_REF_KIND_RUNTIME : FLATPAK_REF_KIND_APP;

  DEVD_ENTRY;

  g_assert (G_IS_TASK (task));
  g_assert (DEVD_IS_FLATPAK_APP_PROVIDER (source_object));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  if (!(installations = devd_flatpak_get_installations (cancellable, &error)))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  installed_apps = g_ptr_array_new_full (0, g_object_unref);

  for (guint i = 0; i < installations->len; ++i)
    {
      g_autoptr(GPtrArray) refs = NULL;

      refs = flatpak_installation_list_installed_refs_by_kind (installations->pdata[i],
                                                               kind,
                                                               cancellable,
                                                               &error);

      if (refs == NULL)
        {
          g_task_return_error (task, g_steal_pointer (&error));
          DEVD_EXIT;
        }

      for (guint j = 0; j < refs->len; j++)
        {
          g_autoptr(DevdAppInfo) info = NULL;

          info = devd_flatpak_info_from_ref (refs->pdata[j], cancellable);
          g_ptr_array_add (installed_apps, g_steal_pointer (&info));
        }
    }

  if (installed_apps->len == 0)
    g_task_return_new_error (task,
                             G_IO_ERROR,
                             G_IO_ERROR_FAILED,
                             "No installed Flatapk apps found");
  else
    g_task_return_pointer (task,
                           g_ptr_array_ref (installed_apps),
                           (GDestroyNotify)g_ptr_array_unref);

  DEVD_EXIT;
}

static void
devd_flatpak_app_provider_list_apps_async (DevdAppProvider     *self,
                                           gboolean             list_runtimes,
                                           GCancellable        *cancellable,
                                           GAsyncReadyCallback  callback,
                                           gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_FLATPAK_APP_PROVIDER (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_flatpak_app_provider_list_apps_async);
  g_task_set_task_data (task, GINT_TO_POINTER(list_runtimes), NULL);
  g_task_run_in_thread (task, devd_flatpak_list_apps_worker);

  DEVD_EXIT;
}

static gboolean
parse_flatpak_id (const char  *full_id,
                  gchar      **app,
                  gchar      **arch,
                  gchar      **branch)
{
  g_auto(GStrv) parts = NULL;

  g_assert (full_id != NULL);
  g_assert (app != NULL);
  g_assert (arch != NULL);
  g_assert (branch != NULL);

  *arch = NULL;
  *branch = NULL;

  if (full_id[0] == '\0')
    return FALSE;

  // Just the name
  if (strchr (full_id, '/') == NULL)
    {
      *app = g_strdup (full_id);
      return TRUE;
    }

  parts = g_strsplit (full_id, "/", 0);
  if (g_strv_length (parts) != 3)
      return FALSE;

  *app = g_strdup (parts[0]);
  if (strlen (parts[1]))
    *arch = g_strdup (parts[1]);

  if (strlen (parts[2]))
    *branch = g_strdup (parts[2]);

  return TRUE;
}

static void
child_setup_cb (gpointer user_data)
{
  setsid ();
  setpgid (0, 0);

  if (isatty (STDIN_FILENO))
    {
      if (ioctl (STDIN_FILENO, TIOCSCTTY, 0) != 0)
        g_warning ("Failed to setup TIOCSCTTY on stdin: %s",
                   g_strerror (errno));
    }
}

static void
devd_flatpak_app_provider_run_app_worker (GTask        *task,
                                          gpointer      source_object,
                                          gpointer      task_data,
                                          GCancellable *cancellable)
{
  RunAppTaskData *run_app = task_data;
  g_autoptr(GSubprocessLauncher) launcher = NULL;
  g_autoptr(GSubprocess) subprocess = NULL;
  g_autoptr(GPtrArray) args = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *name = NULL;
  g_autofree gchar *arch = NULL;
  g_autofree gchar *branch = NULL;
  const gchar *identifier;
  DevdService *processes;

  DEVD_ENTRY;

  g_assert (G_IS_TASK (task));
  g_assert (DEVD_IS_FLATPAK_APP_PROVIDER (source_object));
  g_assert (run_app != NULL);
  g_assert (run_app->app_id != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  if (!parse_flatpak_id (run_app->app_id, &name, &arch, &branch))
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_FAILED,
                               "Given app-id was not valid: %s",
                               run_app->app_id);
      DEVD_EXIT;
    }

  /*
   * We need to launch using "flatpak run " so that we can eventually add
   * more options like --devel, or spawning a debugger, and setting file
   * descriptors for a PTY.
   */

  args = g_ptr_array_new_with_free_func (g_free);
  g_ptr_array_add (args, g_strdup ("flatpak"));
  g_ptr_array_add (args, g_strdup ("run"));
  g_ptr_array_add (args, g_strdup_printf ("%s/%s/%s", name, arch ?: "", branch ?: ""));
  g_ptr_array_add (args, NULL);

  launcher = g_subprocess_launcher_new (0);
  g_subprocess_launcher_set_cwd (launcher, g_get_home_dir ());
  g_subprocess_launcher_set_child_setup (launcher, child_setup_cb, NULL, NULL);

  if (run_app->pty_slave_fd != -1)
    {
      g_subprocess_launcher_take_stdin_fd (launcher, dup (run_app->pty_slave_fd));
      g_subprocess_launcher_take_stdout_fd (launcher, dup (run_app->pty_slave_fd));
      g_subprocess_launcher_take_stderr_fd (launcher, run_app->pty_slave_fd);
      run_app->pty_slave_fd = -1;
    }

  subprocess = g_subprocess_launcher_spawnv (launcher,
                                             (const gchar * const *)args->pdata,
                                             &error);

  if (subprocess == NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  /*
   * Let the DevdApplication know about the subprocess so that the client
   * can send-signal or force-exit. It will also monitor the lifetime of
   * the child so we don't need to wait_async() on it here.
   */
  processes = devd_application_get_service (DEVD_APPLICATION_DEFAULT, DEVD_PROCESS_SERVICE_NAME);
  devd_process_service_monitor_subprocess (DEVD_PROCESS_SERVICE (processes), subprocess);

  /*
   * Know let the client know about the process identifier so they can send
   * signals or force exit using the identifier as the key.
   */
  identifier = g_subprocess_get_identifier (subprocess);
  g_task_return_pointer (task, g_strdup (identifier), g_free);

  DEVD_EXIT;
}

static void
devd_flatpak_app_provider_run_app_async (DevdAppProvider     *provider,
                                         const gchar         *app_id,
                                         DevdPty             *pty,
                                         GCancellable        *cancellable,
                                         GAsyncReadyCallback  callback,
                                         gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  RunAppTaskData *task_data;
  gint slave_fd = -1;

  DEVD_ENTRY;

  g_assert (DEVD_IS_FLATPAK_APP_PROVIDER (provider));
  g_assert (app_id != NULL);
  g_assert (!pty || DEVD_IS_PTY (pty));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (provider, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_flatpak_app_provider_run_app_async);

  if (pty != NULL)
    {
      g_autoptr(GError) error = NULL;

      if (-1 == (slave_fd = devd_pty_create_slave (pty, TRUE, TRUE, &error)))
        {
          g_task_return_error (task, g_steal_pointer (&error));
          DEVD_EXIT;
        }
    }

  task_data = g_slice_new0 (RunAppTaskData);
  task_data->app_id = g_strdup (app_id);
  task_data->pty_slave_fd = slave_fd;

  g_task_set_task_data (task, task_data, run_app_task_data_free);
  g_task_run_in_thread (task, devd_flatpak_app_provider_run_app_worker);

  DEVD_EXIT;
}

static void
devd_flatpak_install (GTask        *task,
                      gpointer      source_object,
                      gpointer      task_data,
                      GCancellable *cancellable)
{
  const gchar *path = task_data;
  g_autoptr(FlatpakInstallation) user_install = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GFile) file = NULL;

  DEVD_ENTRY;

  g_assert (path != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));
  g_assert (DEVD_IS_FLATPAK_APP_PROVIDER (source_object));

  if (!(user_install = flatpak_installation_new_user (cancellable, &error)))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  file = g_file_new_for_path (path);
  flatpak_installation_install_bundle (user_install,
                                       file,
                                       NULL, NULL, /* TODO: Track progress */
                                       cancellable,
                                       &error);

  if (error != NULL)
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);

  DEVD_EXIT;
}

void
devd_flatpak_app_provider_install_bundle_async (DevdFlatpakAppProvider *self,
                                                const gchar            *path,
                                                GCancellable           *cancellable,
                                                GAsyncReadyCallback     callback,
                                                gpointer                user_data)
{
  g_autoptr(GTask) task = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_FLATPAK_APP_PROVIDER (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));
  g_assert (path != NULL);

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_task_data (task, g_strdup (path), g_free);
  g_task_set_source_tag (task, devd_flatpak_app_provider_install_bundle_async);
  g_task_run_in_thread (task, devd_flatpak_install);

  DEVD_EXIT;
}

gboolean
devd_flatpak_app_provider_install_bundle_finish (DevdFlatpakAppProvider  *self,
                                                 GAsyncResult            *result,
                                                 GError                 **error)
{
  gboolean ret;

  DEVD_ENTRY;

  g_assert (DEVD_IS_FLATPAK_APP_PROVIDER (self));
  g_assert (G_IS_TASK (result));

  ret = g_task_propagate_boolean (G_TASK (result), error);

  DEVD_RETURN (ret);
}

DevdFlatpakAppProvider *
devd_flatpak_app_provider_new (void)
{
  return g_object_new (DEVD_TYPE_FLATPAK_APP_PROVIDER, NULL);
}

static void
devd_flatpak_app_provider_iface_init (DevdAppProviderInterface *iface)
{
  iface->list_apps_async = devd_flatpak_app_provider_list_apps_async;
  iface->run_app_async = devd_flatpak_app_provider_run_app_async;
}

struct _DevdFlatpakAppProvider { GObject parent_instance; };

G_DEFINE_TYPE_WITH_CODE (DevdFlatpakAppProvider, devd_flatpak_app_provider, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (DEVD_TYPE_APP_PROVIDER,
                                                devd_flatpak_app_provider_iface_init))

static void
devd_flatpak_app_provider_class_init (DevdFlatpakAppProviderClass *klass)
{
}

static void
devd_flatpak_app_provider_init (DevdFlatpakAppProvider *self)
{
}
