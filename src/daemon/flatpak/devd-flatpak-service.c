/* devd-flatpak-service.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "devd-flatpak-service"

#include <jsonrpc-glib.h>

#include "devd-application.h"
#include "devd-flatpak-app-provider.h"
#include "devd-flatpak-service.h"
#include "devd-trace.h"

static void
devd_flatpak_service_install_bundle_cb (GObject      *object,
                                        GAsyncResult *result,
                                        gpointer      user_data)
{
  DevdFlatpakAppProvider *provider = (DevdFlatpakAppProvider *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;

  DEVD_ENTRY;

  g_assert (DEVD_IS_FLATPAK_APP_PROVIDER (provider));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!devd_flatpak_app_provider_install_bundle_finish (provider, result, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_pointer (task,
                           g_variant_take_ref (g_variant_new_boolean (TRUE)),
                           (GDestroyNotify)g_variant_unref);

  DEVD_EXIT;
}

static void
devd_flatpak_service_install_bundle (DevdService         *service,
                                     const gchar         *method,
                                     GVariant            *params,
                                     GCancellable        *cancellable,
                                     GAsyncReadyCallback  callback,
                                     gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  g_autofree gchar *absolute = NULL;
  DevdAppProvider *provider;
  const gchar *path = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_SERVICE (service));
  g_assert (g_str_equal (method, "install-bundle"));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  provider = devd_application_get_provider_by_kind (DEVD_APPLICATION_DEFAULT, "flatpak");

  task = g_task_new (service, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_flatpak_service_install_bundle);

  if (!JSONRPC_MESSAGE_PARSE (params, "path", JSONRPC_MESSAGE_GET_STRING (&path)))
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_NOT_FOUND,
                               "install-bundle requires a path");
      DEVD_EXIT;
    }

  if (!g_path_is_absolute (path))
    path = absolute = g_build_filename (g_get_home_dir (), path, NULL);

  if (!g_file_test (path, G_FILE_TEST_IS_REGULAR))
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_NOT_FOUND,
                               "Failed to locate bundle to install");
      DEVD_EXIT;
    }

  devd_flatpak_app_provider_install_bundle_async (DEVD_FLATPAK_APP_PROVIDER (provider),
                                                  path,
                                                  cancellable,
                                                  devd_flatpak_service_install_bundle_cb,
                                                  g_steal_pointer (&task));

  DEVD_EXIT;
}

static const DevdServiceMethods methods[] = {
  { "install-bundle", devd_flatpak_service_install_bundle },
};

struct _DevdFlatpakService { GObject parent_instance; };

G_DEFINE_TYPE (DevdFlatpakService, devd_flatpak_service, DEVD_TYPE_SERVICE)

static void
devd_flatpak_service_class_init (DevdFlatpakServiceClass *klass)
{
}

static void
devd_flatpak_service_init (DevdFlatpakService *self)
{
  devd_service_add_methods (DEVD_SERVICE (self), methods, G_N_ELEMENTS (methods));
}

DevdFlatpakService *
devd_flatpak_service_new (void)
{
  return g_object_new (DEVD_TYPE_FLATPAK_SERVICE, NULL);
}
