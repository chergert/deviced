/* devd-network-listener.h
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <NetworkManager.h>

G_BEGIN_DECLS

#define DEVD_TYPE_NETWORK_LISTENER (devd_network_listener_get_type())

G_DECLARE_FINAL_TYPE (DevdNetworkListener, devd_network_listener, DEVD, NETWORK_LISTENER, GSocketService)

DevdNetworkListener *devd_network_listener_new             (NMActiveConnection   *connection,
                                                            GTlsCertificate      *certificate,
                                                            GCancellable         *cancellable,
                                                            GError              **error);
GTlsCertificate     *devd_network_listener_get_certificate (DevdNetworkListener  *seelf);
void                 devd_network_listener_set_certificate (DevdNetworkListener  *self,
                                                            GTlsCertificate      *certificate);
void                 devd_network_listener_start           (DevdNetworkListener  *self);
void                 devd_network_listener_stop            (DevdNetworkListener  *self);

G_END_DECLS
