/* devd-application.h
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

#include "devd-app-provider.h"
#include "devd-service.h"

G_BEGIN_DECLS

#define DEVD_TYPE_APPLICATION    (devd_application_get_type())
#define DEVD_APPLICATION_DEFAULT (DEVD_APPLICATION(g_application_get_default()))

G_DECLARE_FINAL_TYPE (DevdApplication, devd_application, DEVD, APPLICATION, GApplication)

DevdApplication *devd_application_new                    (void);
DevdAppProvider *devd_application_get_provider_by_kind   (DevdApplication      *self,
                                                          const gchar          *kind);
gchar          **devd_application_get_services           (DevdApplication      *self);
DevdService     *devd_application_get_service            (DevdApplication      *self,
                                                          const gchar          *name);
void             devd_application_list_apps_async        (DevdApplication      *self,
                                                          gboolean              list_runtimes,
                                                          GCancellable         *cancellable,
                                                          GAsyncReadyCallback   callback,
                                                          gpointer              user_data);
GPtrArray       *devd_application_list_apps_finish       (DevdApplication      *self,
                                                          GAsyncResult         *result,
                                                          GError              **error);
void             devd_application_list_files_async       (DevdApplication      *self,
                                                          const gchar          *path,
                                                          const gchar          *attributes,
                                                          GCancellable         *cancellable,
                                                          GAsyncReadyCallback   callback,
                                                          gpointer              user_data);
GPtrArray       *devd_application_list_files_finish      (DevdApplication      *self,
                                                          GAsyncResult         *result,
                                                          GError              **error);
void             devd_application_run_app_async          (DevdApplication      *self,
                                                          const gchar          *provider,
                                                          const gchar          *app_id,
                                                          const gchar          *pty_id,
                                                          GCancellable         *cancellable,
                                                          GAsyncReadyCallback   callback,
                                                          gpointer              user_data);
gchar           *devd_application_run_app_finish         (DevdApplication      *self,
                                                          GAsyncResult         *result,
                                                          GError              **error);
void             devd_application_syncfs_async           (DevdApplication      *self,
                                                          const gchar          *devices,
                                                          GCancellable         *cancellable,
                                                          GAsyncReadyCallback   callback,
                                                          gpointer              user_data);
gboolean         devd_application_syncfs_finish          (DevdApplication      *self,
                                                          GAsyncResult         *result,
                                                          GError              **error);

G_END_DECLS
