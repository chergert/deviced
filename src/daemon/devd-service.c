/* devd-service.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "devd-service"

#include <stdlib.h>

#include "devd-service.h"

typedef struct
{
  const gchar            *name;
  DevdServiceAsyncMethod  async;
  DevdServiceSyncMethod   sync;
} MethodInfo;

typedef struct
{
  GArray *methods;
} DevdServicePrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (DevdService, devd_service, G_TYPE_OBJECT)

enum {
  NOTIFICATION,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

static void
devd_service_finalize (GObject *object)
{
  DevdService *self = (DevdService *)object;
  DevdServicePrivate *priv = devd_service_get_instance_private (self);

  g_clear_pointer (&priv->methods, g_array_unref);

  G_OBJECT_CLASS (devd_service_parent_class)->finalize (object);
}

static void
devd_service_class_init (DevdServiceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = devd_service_finalize;

  signals [NOTIFICATION] =
    g_signal_new ("notification",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DevdServiceClass, notification),
                  NULL, NULL, NULL,
                  G_TYPE_NONE,
                  2,
                  G_TYPE_STRING | G_SIGNAL_TYPE_STATIC_SCOPE,
                  G_TYPE_VARIANT);
}

static void
devd_service_init (DevdService *self)
{
  DevdServicePrivate *priv = devd_service_get_instance_private (self);

  priv->methods = g_array_new (FALSE, FALSE, sizeof (MethodInfo));
}

static gint
method_info_compare (gconstpointer a,
                     gconstpointer b)
{
  const MethodInfo *a_info = a;
  const MethodInfo *b_info = b;

  if (a_info->name < b_info->name)
    return -1;
  else if (a_info->name > b_info->name)
    return 1;
  else
    return 0;
}

static MethodInfo *
devd_service_find_method (DevdService *self,
                          const gchar *method)
{
  DevdServicePrivate *priv = devd_service_get_instance_private (self);
  MethodInfo key;

  g_assert (DEVD_IS_SERVICE (self));
  g_assert (method != NULL);

  key.name = g_intern_string (method);

  return bsearch (&key, (gpointer)priv->methods->data,
                  priv->methods->len, sizeof (MethodInfo),
                  method_info_compare);
}

void
devd_service_add_async_method (DevdService            *self,
                               const gchar            *method,
                               DevdServiceAsyncMethod  func)
{
  DevdServicePrivate *priv = devd_service_get_instance_private (self);
  MethodInfo info;

  g_return_if_fail (DEVD_IS_SERVICE (self));
  g_return_if_fail (method != NULL);
  g_return_if_fail (func != NULL);

  info.name = g_intern_string (method);
  info.async = func;
  info.sync = NULL;

  g_array_append_val (priv->methods, info);
  g_array_sort (priv->methods, method_info_compare);
}

void
devd_service_add_sync_method (DevdService           *self,
                              const gchar           *method,
                              DevdServiceSyncMethod  func)
{
  DevdServicePrivate *priv = devd_service_get_instance_private (self);
  MethodInfo info;

  g_return_if_fail (DEVD_IS_SERVICE (self));
  g_return_if_fail (method != NULL);
  g_return_if_fail (func != NULL);

  info.name = g_intern_string (method);
  info.async = NULL;
  info.sync = func;

  g_array_append_val (priv->methods, info);
  g_array_sort (priv->methods, method_info_compare);
}

void
devd_service_add_methods (DevdService              *self,
                          const DevdServiceMethods *methods,
                          guint                     n_methods)
{
  DevdServicePrivate *priv = devd_service_get_instance_private (self);

  g_return_if_fail (DEVD_IS_SERVICE (self));
  g_return_if_fail (methods != NULL);

  for (guint i = 0; i < n_methods; i++)
    {
      MethodInfo info;

      info.name = g_intern_string (methods[i].name);
      info.async = methods[i].async;
      info.sync = methods[i].sync;

      g_array_append_val (priv->methods, info);
    }

  g_array_sort (priv->methods, method_info_compare);
}

void
devd_service_remove_method (DevdService *self,
                            const gchar *method)
{
  DevdServicePrivate *priv = devd_service_get_instance_private (self);

  g_return_if_fail (DEVD_IS_SERVICE (self));
  g_return_if_fail (method != NULL);

  for (guint i = priv->methods->len; i > 0; i--)
    {
      const MethodInfo *info = &g_array_index (priv->methods, MethodInfo, i - 1);

      if (g_strcmp0 (method, info->name) == 0)
        g_array_remove_index (priv->methods, i - 1);
    }
}

gboolean
devd_service_has_method (DevdService *self,
                         const gchar *method)
{
  g_return_val_if_fail (DEVD_IS_SERVICE (self), FALSE);
  g_return_val_if_fail (method != NULL, FALSE);

  return !!devd_service_find_method (self, method);
}

static void
devd_service_call_cb (GObject      *object,
                      GAsyncResult *result,
                      gpointer      user_data)
{
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GVariant) ret = NULL;

  g_assert (DEVD_IS_SERVICE (object));
  g_assert (G_IS_TASK (result));
  g_assert (G_IS_TASK (task));

  ret = g_task_propagate_pointer (G_TASK (result), &error);

  if (error != NULL)
    g_task_return_error (task, g_steal_pointer (&error));
  else if (ret != NULL)
    g_task_return_pointer (task,
                           g_steal_pointer (&ret),
                           (GDestroyNotify)g_variant_unref);
  else
    g_task_return_pointer (task, NULL, NULL);
}

void
devd_service_call_async (DevdService         *self,
                         const gchar         *method,
                         GVariant            *params,
                         GCancellable        *cancellable,
                         GAsyncReadyCallback  callback,
                         gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  const MethodInfo *info;

  g_return_if_fail (DEVD_IS_SERVICE (self));
  g_return_if_fail (method != NULL);
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_service_call_async);

  info = devd_service_find_method (self, method);

  if (info == NULL)
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_NOT_FOUND,
                               "No such method %s", method);
      return;
    }

  g_assert (info != NULL);
  g_assert (info->async || info->sync);

  if (info->async != NULL)
    {
      info->async (self,
                   method,
                   params,
                   cancellable,
                   devd_service_call_cb,
                   g_steal_pointer (&task));
    }
  else
    {
      g_autoptr(GVariant) reply = NULL;
      g_autoptr(GError) error = NULL;

      if (!info->sync (self, method, params, cancellable, &reply, &error))
        g_task_return_error (task, g_steal_pointer (&error));
      else if (reply == NULL)
        g_task_return_pointer (task, NULL, NULL);
      else
        g_task_return_pointer (task,
                               g_steal_pointer (&reply),
                               (GDestroyNotify)g_variant_unref);
    }
}

gboolean
devd_service_call_finish (DevdService   *self,
                          GAsyncResult  *result,
                          GVariant     **reply,
                          GError       **error)
{
  g_autoptr(GError) local_error = NULL;
  g_autoptr(GVariant) local_reply = NULL;
  gboolean ret;

  g_return_val_if_fail (DEVD_IS_SERVICE (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  local_reply = g_task_propagate_pointer (G_TASK (result), &local_error);
  ret = local_error == NULL;
  if (local_error != NULL)
    g_propagate_error (error, g_steal_pointer (&local_error));
  if (reply != NULL)
    *reply = g_steal_pointer (&local_reply);

  return ret;
}

void
devd_service_emit_notification (DevdService *self,
                                const gchar *method,
                                GVariant    *params)
{
  g_return_if_fail (DEVD_IS_SERVICE (self));
  g_return_if_fail (method != NULL);

  g_signal_emit (self, signals [NOTIFICATION], 0, method, params);
}
