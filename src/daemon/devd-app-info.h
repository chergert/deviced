/* devd-app-info.h
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

#define DEVD_TYPE_APP_INFO (devd_app_info_get_type())

G_DECLARE_DERIVABLE_TYPE (DevdAppInfo, devd_app_info, DEVD, APP_INFO, GObject)

struct _DevdAppInfoClass
{
  GObjectClass parent;

  GVariant *(*to_variant) (DevdAppInfo *self);
};

DevdAppInfo *devd_app_info_new        (void);
GVariant    *devd_app_info_to_variant (DevdAppInfo *self);

#define _PROPERTY(_1, name, _2, ret_type, _3, _4, _5, _6, _7) \
  ret_type devd_app_info_get_##name (DevdAppInfo *self);
# include "devd-app-info.defs"
#undef _PROPERTY

#define _PROPERTY(_1, name, _2, ret_type, _3, _4, _5, _6, _7) \
  void devd_app_info_set_##name (DevdAppInfo *self,          \
                                 ret_type     name);
# include "devd-app-info.defs"
#undef _PROPERTY

G_END_DECLS
