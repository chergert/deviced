/* devd-network-manager.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "devd-network-manager"

#include <NetworkManager.h>

#include "devd-network-listener.h"
#include "devd-network-manager.h"
#include "devd-trace.h"
#include "devd-tls-certificate.h"

struct _DevdNetworkManager
{
  GObject          parent_instance;
  GTlsCertificate *certificate;
  NMClient        *nm_client;
  GSettings       *settings;
  GHashTable      *listener_by_uuid;
};

G_DEFINE_TYPE (DevdNetworkManager, devd_network_manager, G_TYPE_OBJECT)

static DevdNetworkListener *
create_service_for_active_connection (DevdNetworkManager  *self,
                                      NMActiveConnection  *connection,
                                      GCancellable        *cancellable,
                                      GError             **error)
{
  DevdNetworkListener *ret;

  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_MANAGER (self));
  g_assert (NM_IS_ACTIVE_CONNECTION (connection));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));
  g_assert (G_IS_TLS_CERTIFICATE (self->certificate));

  ret = devd_network_listener_new (connection, self->certificate, cancellable, error);

  DEVD_RETURN (ret);
}

static gboolean
active_connections_contains (const GPtrArray *active_connections,
                             const gchar     *uuid)
{
  g_assert (active_connections != NULL);
  g_assert (uuid != NULL);

  for (guint i = 0; i < active_connections->len; i++)
    {
      NMActiveConnection *conn = g_ptr_array_index (active_connections, i);

      if (g_strcmp0 (uuid, nm_active_connection_get_uuid (conn)) == 0)
        return TRUE;
    }

  return FALSE;
}

static void
devd_network_manager_reset (DevdNetworkManager *self)
{
  g_auto(GStrv) enabled_connections = NULL;
  const GPtrArray *active_connections;
  GHashTableIter iter;
  gpointer key, value;
  gboolean enable_network;

  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_MANAGER (self));
  g_assert (G_IS_SETTINGS (self->settings));

  /* Get the new list of connections we can listen on. */
  enable_network = g_settings_get_boolean (self->settings, "enable-network");
  enabled_connections = g_settings_get_strv (self->settings, "enabled-connections");
  active_connections = nm_client_get_active_connections (self->nm_client);

  /* Give some information about enabled network connections */
  g_debug ("%d enabled network connections", g_strv_length (enabled_connections));
  for (guint i = 0; enabled_connections[i]; i++)
    g_debug (" [%u] = %s", i, enabled_connections[i]);

  /*
   * Remove any connections that have been revoked or we are no
   * longer actively connected to.
   */
  g_hash_table_iter_init (&iter, self->listener_by_uuid);
  while (g_hash_table_iter_next (&iter, &key, &value))
    {
      DevdNetworkListener *listener = value;
      const gchar *uuid = key;

      if (!enable_network ||
          !g_strv_contains ((const gchar * const *)enabled_connections, uuid) ||
          !active_connections_contains (active_connections, uuid))
        {
          g_socket_service_stop (G_SOCKET_SERVICE (listener));
          g_hash_table_iter_remove (&iter);
        }
    }

  /* Nothing more to do if network is disabled. */
  if (!enable_network)
    {
      g_debug ("network disabled");
      DEVD_EXIT;
    }

  /*
   * Any active connection that is in our enabled_connections list needs to
   * have a listener created (unless it already has one).
   */
  for (guint i = 0; i < active_connections->len; i++)
    {
      NMActiveConnection *conn = g_ptr_array_index (active_connections, i);
      const gchar *uuid = nm_active_connection_get_uuid (conn);

      if (g_strv_contains ((const gchar * const *)enabled_connections, uuid) &&
          !g_hash_table_contains (self->listener_by_uuid, uuid))
        {
          g_autoptr(DevdNetworkListener) listener = NULL;
          g_autoptr(GError) error = NULL;

          if (!(listener = create_service_for_active_connection (self, conn, NULL, &error)))
            {
              const gchar *name = nm_active_connection_get_id (conn);
              g_warning ("Failed to create socket service for connection %s", name);
              continue;
            }

          g_hash_table_insert (self->listener_by_uuid,
                               g_strdup (uuid),
                               g_object_ref (listener));

          g_socket_service_start (G_SOCKET_SERVICE (listener));
        }
    }

  DEVD_EXIT;
}

static void
on_network_settings_changed_cb (DevdNetworkManager *self,
                                const gchar        *key,
                                GSettings          *settings)
{
  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_MANAGER (self));

  devd_network_manager_reset (self);

  DEVD_EXIT;
}

static void
devd_network_manager_dispose (GObject *object)
{
  DevdNetworkManager *self = (DevdNetworkManager *)object;

  DEVD_ENTRY;

  devd_network_manager_stop (self);

  G_OBJECT_CLASS (devd_network_manager_parent_class)->dispose (object);

  DEVD_EXIT;
}

static void
devd_network_manager_finalize (GObject *object)
{
  DevdNetworkManager *self = (DevdNetworkManager *)object;

  DEVD_ENTRY;

  g_clear_object (&self->certificate);
  g_clear_object (&self->nm_client);
  g_clear_object (&self->settings);
  g_clear_pointer (&self->listener_by_uuid, g_hash_table_unref);

  G_OBJECT_CLASS (devd_network_manager_parent_class)->finalize (object);

  DEVD_EXIT;
}

static void
devd_network_manager_class_init (DevdNetworkManagerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = devd_network_manager_dispose;
  object_class->finalize = devd_network_manager_finalize;
}

static void
devd_network_manager_init (DevdNetworkManager *self)
{
  self->settings = g_settings_new ("org.gnome.deviced");
  self->listener_by_uuid = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);
}

DevdNetworkManager *
devd_network_manager_new (void)
{
  return g_object_new (DEVD_TYPE_NETWORK_MANAGER, NULL);
}

gboolean
devd_network_manager_start (DevdNetworkManager  *self,
                            GCancellable        *cancellable,
                            GError             **error)
{
  g_autoptr(GTlsCertificate) cert = NULL;
  g_autofree gchar *private_key = NULL;
  g_autofree gchar *public_key = NULL;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_NETWORK_MANAGER (self), FALSE);

  if (!(self->nm_client = nm_client_new (NULL, error)))
    DEVD_RETURN (FALSE);

  private_key = g_build_filename (g_get_user_config_dir (), "deviced", "private.pem", NULL);
  public_key = g_build_filename (g_get_user_config_dir (), "deviced", "public.pem", NULL);

  if (!g_file_test (public_key, G_FILE_TEST_IS_REGULAR) ||
      !g_file_test (private_key, G_FILE_TEST_IS_REGULAR))
    cert = devd_tls_certificate_new_generate (public_key, private_key,
                                              "US", "GNOME:deviced",
                                              NULL, error);
  else
    cert = g_tls_certificate_new_from_files (public_key, private_key, error);

  if (cert == NULL)
    DEVD_RETURN (FALSE);

  g_set_object (&self->certificate, cert);

  g_signal_connect_object (self->settings,
                           "changed::enable-network",
                           G_CALLBACK (on_network_settings_changed_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_signal_connect_object (self->settings,
                           "changed::enabled-connections",
                           G_CALLBACK (on_network_settings_changed_cb),
                           self,
                           G_CONNECT_SWAPPED);

  devd_network_manager_reset (self);

  DEVD_RETURN (TRUE);
}

void
devd_network_manager_stop (DevdNetworkManager *self)
{
  GHashTableIter iter;
  gpointer value;

  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_NETWORK_MANAGER (self));

  /* Stop and remove all socket services */
  g_hash_table_iter_init (&iter, self->listener_by_uuid);
  while (g_hash_table_iter_next (&iter, NULL, &value))
    {
      DevdNetworkListener *listener = value;

      g_socket_service_stop (G_SOCKET_SERVICE (listener));
      g_hash_table_iter_remove (&iter);
    }

  g_signal_handlers_disconnect_by_func (self->settings,
                                        G_CALLBACK (on_network_settings_changed_cb),
                                        self);

  g_clear_object (&self->nm_client);

  DEVD_EXIT;
}
