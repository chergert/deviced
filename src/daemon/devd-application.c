/* devd-application.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "devd-application"

#include "config.h"

#include <errno.h>
#include <fcntl.h>
#include <glib/gi18n.h>
#include <stdlib.h>
#include <unistd.h>

#include "devd-application.h"
#include "devd-app-info.h"
#include "devd-app-provider.h"
#include "devd-macros.h"
#include "devd-network-manager.h"
#include "devd-pty.h"
#include "devd-trace.h"
#include "devd-util.h"

#ifdef HAVE_FLATPAK
# include "flatpak/devd-flatpak-app-provider.h"
# include "flatpak/devd-flatpak-service.h"
#endif

#include "process/devd-process-service.h"
#include "transfers/devd-transfer-service.h"

struct _DevdApplication
{
  GApplication parent_instance;

  /*
   * The DevdNetworkManager watches for changes to the network settings
   * and updates network listeners as necessary. Additionally, it tracks
   * network connection changes (like changning Wi-Fi networks) and updates
   * what socket addresses are listening.
   */
  DevdNetworkManager *network_manager;

  /*
   * This contains our supported app providers. We don't use plugins, so this
   * is populated with compiled-time supported providres in
   * devd_application_startup().
   */
  GPtrArray *app_providers;

  /*
   * A hashtable of services that are registered. This allows for us to add
   * new features to deviced that are self-contained and notify the clients
   * of the available services. This means that the client library can have
   * helper service objects only when the service is known to exist.
   */
  GHashTable *services;
};

typedef struct
{
  GPtrArray *app_infos;
  guint      n_active;
} ListAppsState;

G_DEFINE_TYPE (DevdApplication, devd_application, G_TYPE_APPLICATION)

static const GOptionEntry cmd_options[] = {
  { "kill", 'k', G_OPTION_FLAG_NONE, G_OPTION_ARG_NONE, NULL, N_("Kill a running daemon") },
  { NULL }
};


static void
list_apps_state_free (gpointer data)
{
  ListAppsState *state = data;

  g_clear_pointer (&state->app_infos, g_ptr_array_unref);
  g_slice_free (ListAppsState, state);
}


/**
 * devd_application_get_provider_by_kind:
 * @self: a #DevdApplication
 *
 * Gets the provider by name.
 *
 * Returns: (transfer none): a #DevdAppProvider or %NULL
 */
DevdAppProvider *
devd_application_get_provider_by_kind (DevdApplication *self,
                                       const gchar     *kind)
{
  GType expected_type = G_TYPE_INVALID;

  g_assert (DEVD_IS_APPLICATION (self));

  if (FALSE) {}
#ifdef HAVE_FLATPAK
  else if (devd_str_equal0 (kind, "flatpak"))
    expected_type = DEVD_TYPE_FLATPAK_APP_PROVIDER;
#endif

  if (expected_type == G_TYPE_INVALID)
    return NULL;

  for (guint i = 0; i < self->app_providers->len; i++)
    {
      DevdAppProvider *provider = g_ptr_array_index (self->app_providers, i);

      if (g_type_is_a (G_OBJECT_TYPE (provider), expected_type))
        return provider;
    }

  return NULL;
}

static void
devd_application_activate (GApplication *app)
{
  DEVD_ENTRY;

  g_assert (DEVD_IS_APPLICATION (app));

  DEVD_EXIT;
}

static void
devd_application_startup (GApplication *app)
{
  DevdApplication *self = (DevdApplication *)app;
  g_autoptr(GError) error = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_APPLICATION (self));

  G_APPLICATION_CLASS (devd_application_parent_class)->startup (app);

  g_hash_table_insert (self->services,
                       g_strdup (DEVD_PROCESS_SERVICE_NAME),
                       devd_process_service_new ());
  g_hash_table_insert (self->services,
                       g_strdup (DEVD_TRANSFER_SERVICE_NAME),
                       devd_transfer_service_new ());

#ifdef HAVE_FLATPAK
  g_ptr_array_add (self->app_providers, devd_flatpak_app_provider_new ());
  g_hash_table_insert (self->services,
                       g_strdup (DEVD_FLATPAK_SERVICE_NAME),
                       devd_flatpak_service_new ());
#endif

  self->network_manager = devd_network_manager_new ();

  if (!devd_network_manager_start (self->network_manager, NULL, &error))
    g_error ("Failed to start network subsystem: %s", error->message);

  g_application_hold (app);

  DEVD_EXIT;
}


static void
devd_application_shutdown (GApplication *app)
{
  DevdApplication *self = (DevdApplication *)app;

  DEVD_ENTRY;

  g_assert (DEVD_IS_APPLICATION (self));

  devd_network_manager_stop (self->network_manager);
  g_clear_object (&self->network_manager);

  if (self->app_providers->len > 0)
    g_ptr_array_remove_range (self->app_providers, 0, self->app_providers->len);

  G_APPLICATION_CLASS (devd_application_parent_class)->shutdown (app);

  DEVD_EXIT;
}

static gint
devd_application_command_line (GApplication            *application,
                               GApplicationCommandLine *command_line)
{
  GVariantDict *options;

  options = g_application_command_line_get_options_dict (command_line);

  if (g_variant_dict_contains (options, "kill"))
    {
      g_application_quit (application);
      return 0;
    }

  g_application_activate (application);

  return 0;
}

static void
devd_application_finalize (GObject *object)
{
  DevdApplication *self = (DevdApplication *)object;

  DEVD_ENTRY;

  g_clear_pointer (&self->app_providers, g_ptr_array_unref);
  g_clear_pointer (&self->services, g_hash_table_unref);

  G_OBJECT_CLASS (devd_application_parent_class)->finalize (object);

  DEVD_EXIT;
}

static void
devd_application_class_init (DevdApplicationClass *klass)
{
  GApplicationClass *application_class = G_APPLICATION_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = devd_application_finalize;

  application_class->activate = devd_application_activate;
  application_class->startup = devd_application_startup;
  application_class->shutdown = devd_application_shutdown;
  application_class->command_line = devd_application_command_line;
}

static void
devd_application_init (DevdApplication *self)
{
  DEVD_ENTRY;

  g_application_add_main_option_entries (G_APPLICATION (self), cmd_options);
  self->app_providers = g_ptr_array_new_with_free_func (g_object_unref);
  self->services = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);

  DEVD_EXIT;
}

/**
 * devd_application_new:
 *
 * Creates a new #DevdApplication. This is the central object controlling
 * the daemon. It handles command-line arguments, handleing requests from
 * external applications, and managing socket listeners such as over Wi-Fi.
 *
 * Returns: a new #DevdApplication
 */
DevdApplication *
devd_application_new (void)
{
  return g_object_new (DEVD_TYPE_APPLICATION,
                       "application-id", "org.gnome.Deviced",
                       "flags", G_APPLICATION_HANDLES_COMMAND_LINE,
                       NULL);
}

/**
 * devd_application_get_service:
 * @self: a #DevdApplication
 * @name: the name of the service, like "org.gnome.deviced.foo"
 *
 * Gets the service by it's registered name.
 *
 * Returns: (transfer none) (nullable): the service or %NULL
 */
DevdService *
devd_application_get_service (DevdApplication *self,
                              const gchar     *name)
{
  g_return_val_if_fail (DEVD_IS_APPLICATION (self), NULL);

  return g_hash_table_lookup (self->services, name);
}

/**
 * devd_application_get_services:
 * @self: a #DevdApplication
 *
 * Gets a string list containing the list of available services.
 *
 * Returns: (transfer full) (element-type utf8) (array null-terminated=1): An
 *   array of service identifiers that are available to the service.
 */
gchar **
devd_application_get_services (DevdApplication *self)
{
  g_autoptr(GPtrArray) names = NULL;
  GHashTableIter iter;
  const gchar *name;

  g_assert (DEVD_IS_APPLICATION (self));

  names = g_ptr_array_new_with_free_func (g_free);
  g_hash_table_iter_init (&iter, self->services);
  while (g_hash_table_iter_next (&iter, (gpointer *)&name, NULL))
    g_ptr_array_add (names, g_strdup (name));
  g_ptr_array_add (names, NULL);

  return (gchar **)g_ptr_array_free (g_steal_pointer (&names), FALSE);
}

static void
devd_application_list_apps_cb (GObject      *object,
                               GAsyncResult *result,
                               gpointer      user_data)
{
  DevdAppProvider *app_provider = (DevdAppProvider *)object;
  g_autoptr(GPtrArray) app_infos = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  ListAppsState *state;

  DEVD_ENTRY;

  g_assert (DEVD_IS_APP_PROVIDER (app_provider));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  state = g_task_get_task_data (task);

  app_infos = devd_app_provider_list_apps_finish (app_provider, result, &error);

  if (app_infos != NULL)
    {
      for (guint i = 0; i < app_infos->len; i++)
        {
          DevdAppInfo *app_info = g_ptr_array_index (app_infos, i);

          g_ptr_array_add (state->app_infos, g_object_ref (app_info));
        }
    }

  state->n_active--;

  if (state->n_active == 0)
    g_task_return_pointer (task,
                           g_steal_pointer (&state->app_infos),
                           (GDestroyNotify)g_ptr_array_unref);

  DEVD_EXIT;
}

/**
 * devd_application_list_apps_async:
 * @self: a #DevdApplication
 * @list_runtimes: if %TRUE list runtimes instead of apps
 *
 * Lists the known applications to the #DevdApplication.
 *
 * The goal here is to get the available apps from a number of providers,
 * such as a flatpak provider, etc.
 */
void
devd_application_list_apps_async (DevdApplication     *self,
                                  gboolean             list_runtimes,
                                  GCancellable        *cancellable,
                                  GAsyncReadyCallback  callback,
                                  gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  ListAppsState *state;

  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_APPLICATION (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_application_list_apps_async);

  state = g_slice_new0 (ListAppsState);
  state->n_active = self->app_providers->len;
  state->app_infos = g_ptr_array_new_with_free_func (g_object_unref);
  g_task_set_task_data (task, state, list_apps_state_free);

  for (guint i = 0; i < self->app_providers->len; i++)
    {
      DevdAppProvider *app_provider = g_ptr_array_index (self->app_providers, i);

      devd_app_provider_list_apps_async (app_provider,
                                         list_runtimes,
                                         cancellable,
                                         devd_application_list_apps_cb,
                                         g_object_ref (task));
    }

  if (state->n_active == 0)
    g_task_return_new_error (task,
                             G_IO_ERROR,
                             G_IO_ERROR_NOT_SUPPORTED,
                             "No app providers are loaded");

  DEVD_EXIT;
}

/**
 * devd_application_list_apps_finish:
 * @self: a #DevdApplication
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError or %NULL
 *
 * Completes an asynchronous request to list the apps.
 *
 * ** The return type for array has not yet been determined **
 *
 * Returns: (transfer container) (elment-type Devd.AppInfo): A #GPtrArray of
 *   apps or %NULL upon failure and @error is set.
 */
GPtrArray *
devd_application_list_apps_finish (DevdApplication  *self,
                                   GAsyncResult     *result,
                                   GError          **error)
{
  g_autoptr(GPtrArray) ret = NULL;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (G_IS_TASK (result), NULL);

  ret = g_task_propagate_pointer (G_TASK (result), error);

  DEVD_RETURN (g_steal_pointer (&ret));
}

static void
devd_application_list_files_next_cb (GObject      *object,
                                     GAsyncResult *result,
                                     gpointer      user_data)
{
  GFileEnumerator *enumerator = (GFileEnumerator *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  GCancellable *cancellable;
  GPtrArray *array;
  GList *files;

  DEVD_ENTRY;

  g_assert (G_IS_FILE_ENUMERATOR (enumerator));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  files = g_file_enumerator_next_files_finish (enumerator, result, &error);

  if (error != NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  array = g_task_get_task_data (task);

  if (files == NULL)
    {
      g_task_return_pointer (task,
                             g_ptr_array_ref (array),
                             (GDestroyNotify)g_ptr_array_unref);
      DEVD_EXIT;
    }

  cancellable = g_task_get_cancellable (task);

  for (GList *iter = files; iter != NULL; iter = iter->next)
    g_ptr_array_add (array, g_steal_pointer (&iter->data));
  g_list_free (files);

  g_file_enumerator_next_files_async (enumerator,
                                      100,
                                      G_PRIORITY_LOW,
                                      cancellable,
                                      devd_application_list_files_next_cb,
                                      g_steal_pointer (&task));

  DEVD_EXIT;
}

static void
devd_application_list_files_enumerate_cb (GObject      *object,
                                          GAsyncResult *result,
                                          gpointer      user_data)
{
  GFile *file = (GFile *)object;
  g_autoptr(GFileEnumerator) enumerator = NULL;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  GCancellable *cancellable;

  DEVD_ENTRY;

  g_assert (G_IS_FILE (file));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!(enumerator = g_file_enumerate_children_finish (file, result, &error)))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  cancellable = g_task_get_cancellable (task);

  g_file_enumerator_next_files_async (enumerator,
                                      100,
                                      G_PRIORITY_LOW,
                                      cancellable,
                                      devd_application_list_files_next_cb,
                                      g_steal_pointer (&task));

  DEVD_EXIT;
}


void
devd_application_list_files_async (DevdApplication     *self,
                                   const gchar         *path,
                                   const gchar         *attributes,
                                   GCancellable        *cancellable,
                                   GAsyncReadyCallback  callback,
                                   gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  g_autoptr(GFile) file = NULL;

  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_APPLICATION (self));
  g_return_if_fail (path != NULL);
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  file = g_file_new_for_path (path);

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_application_list_files_async);
  g_task_set_task_data (task,
                        g_ptr_array_new_with_free_func (g_object_unref),
                        (GDestroyNotify)g_ptr_array_unref);

  g_file_enumerate_children_async (file,
                                   attributes,
                                   G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS,
                                   G_PRIORITY_LOW,
                                   cancellable,
                                   devd_application_list_files_enumerate_cb,
                                   g_steal_pointer (&task));

  DEVD_EXIT;
}

/**
 * devd_application_list_files_async:
 * @self: a #DevdApplication
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous request to list files in a directory.
 *
 * Returns: (transfer container) (element-type Gio.FileInfo): A #GPtrArray of
 *   #GFileInfo or %NULL and @error is set.
 */
GPtrArray *
devd_application_list_files_finish (DevdApplication  *self,
                                    GAsyncResult     *result,
                                    GError          **error)
{
  g_autoptr(GPtrArray) ret = NULL;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (G_IS_TASK (result), NULL);

  ret = g_task_propagate_pointer (G_TASK (result), error);

  DEVD_RETURN (g_steal_pointer (&ret));
}

static void
devd_application_run_app_cb (GObject      *object,
                             GAsyncResult *result,
                             gpointer      user_data)
{
  DevdAppProvider *provider = DEVD_APP_PROVIDER (object);
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *identifier = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_APP_PROVIDER (provider));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  identifier = devd_app_provider_run_app_finish (provider, result, &error);

  if (error != NULL)
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_pointer (task, g_steal_pointer (&identifier), g_free);

  DEVD_EXIT;
}

void
devd_application_run_app_async (DevdApplication     *self,
                                const gchar         *provider,
                                const gchar         *app_id,
                                const gchar         *pty_id,
                                GCancellable        *cancellable,
                                GAsyncReadyCallback  callback,
                                gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  DevdAppProvider *app_provider;
  DevdProcessService *process_service;
  DevdPty *pty = NULL;

  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_APPLICATION (self));
  g_return_if_fail (provider != NULL);
  g_return_if_fail (app_id != NULL);
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_application_run_app_async);

  if (pty_id != NULL)
    {
      process_service = DEVD_PROCESS_SERVICE (devd_application_get_service (self, DEVD_PROCESS_SERVICE_NAME));
      pty = devd_process_service_get_pty_by_id (process_service, pty_id);

      if (pty == NULL)
        {
          g_task_report_new_error (task, callback, user_data,
                                   devd_application_run_app_async,
                                   G_IO_ERROR,
                                   G_IO_ERROR_FAILED,
                                   "No pty \"%s\" found", pty_id);
          DEVD_EXIT;
        }
    }

  if ((app_provider = devd_application_get_provider_by_kind (self, provider)))
    devd_app_provider_run_app_async (app_provider,
                                     app_id,
                                     pty,
                                     cancellable,
                                     devd_application_run_app_cb,
                                     g_steal_pointer (&task));
  else
    g_task_report_new_error (task, callback, user_data,
                             devd_application_run_app_async,
                             G_IO_ERROR,
                             G_IO_ERROR_FAILED,
                             "No provider \"%s\" found", provider);
  DEVD_EXIT;
}

gchar *
devd_application_run_app_finish (DevdApplication  *self,
                                 GAsyncResult     *result,
                                 GError          **error)
{
  gchar *ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_APPLICATION (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  ret = g_task_propagate_pointer (G_TASK (result), error);

  DEVD_RETURN (ret);
}

static void
devd_application_syncfs_worker (GTask        *task,
                                gpointer      source_object,
                                gpointer      task_data,
                                GCancellable *cancellable)
{
  sync ();
  g_task_return_boolean (task, TRUE);
}

void
devd_application_syncfs_async (DevdApplication     *self,
                               const gchar         *devices,
                               GCancellable        *cancellable,
                               GAsyncReadyCallback  callback,
                               gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_APPLICATION (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  /* TODO: Support devices and device classes with something like:
   *       "emmc,sda,nvm0". I would expect emmc to match all emmc
   *       devices.
   */

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_application_syncfs_async);
  g_task_run_in_thread (task, devd_application_syncfs_worker);

  DEVD_EXIT;
}

gboolean
devd_application_syncfs_finish (DevdApplication  *self,
                                GAsyncResult     *result,
                                GError          **error)
{
  gboolean ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_APPLICATION (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  ret = g_task_propagate_boolean (G_TASK (result), error);

  DEVD_RETURN (ret);
}
