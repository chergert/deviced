/* devd-service.h
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

#define DEVD_TYPE_SERVICE (devd_service_get_type())

G_DECLARE_DERIVABLE_TYPE (DevdService, devd_service, DEVD, SERVICE, GObject)

typedef gboolean (*DevdServiceSyncMethod)  (DevdService          *self,
                                            const gchar          *method,
                                            GVariant             *params,
                                            GCancellable         *cancellable,
                                            GVariant            **reply,
                                            GError              **error);
typedef void     (*DevdServiceAsyncMethod) (DevdService          *self,
                                            const gchar          *method,
                                            GVariant             *params,
                                            GCancellable         *cancellable,
                                            GAsyncReadyCallback   callback,
                                            gpointer              user_data);

typedef struct
{
  const gchar            *name;
  DevdServiceAsyncMethod  async;
  DevdServiceSyncMethod   sync;
} DevdServiceMethods;

struct _DevdServiceClass
{
  GObjectClass parent_class;

  void     (*notification) (DevdService          *self,
                            const gchar          *method,
                            GVariant             *params);
  gboolean (*has_method)   (DevdService          *self,
                            const gchar          *method);
  void     (*call_async)   (DevdService          *self,
                            const gchar          *method,
                            GVariant             *params,
                            GCancellable         *cancellable,
                            GAsyncReadyCallback   callback,
                            gpointer              user_data);
  gboolean (*call_finish)  (DevdService          *self,
                            GAsyncResult         *result,
                            GVariant            **reply,
                            GError              **error);

  /*< private >*/
  gpointer _reserved[8];
};

void     devd_service_emit_notification (DevdService               *self,
                                         const gchar               *method,
                                         GVariant                  *params);
void     devd_service_add_async_method  (DevdService               *self,
                                         const gchar               *method,
                                         DevdServiceAsyncMethod     func);
void     devd_service_add_sync_method   (DevdService               *self,
                                         const gchar               *method,
                                         DevdServiceSyncMethod      func);
void     devd_service_add_methods       (DevdService               *self,
                                         const DevdServiceMethods  *methods,
                                         guint                      n_methods);
void     devd_service_remove_method     (DevdService               *self,
                                         const gchar               *method);
gboolean devd_service_has_method        (DevdService               *self,
                                         const gchar               *method);
void     devd_service_call_async        (DevdService               *self,
                                         const gchar               *method,
                                         GVariant                  *params,
                                         GCancellable              *cancellable,
                                         GAsyncReadyCallback        callback,
                                         gpointer                   user_data);
gboolean devd_service_call_finish       (DevdService               *self,
                                         GAsyncResult              *result,
                                         GVariant                 **reply,
                                         GError                   **error);

G_END_DECLS
