/* devd-transfer-service.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "devd-transfer-service"

#include <errno.h>
#include <fcntl.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <jsonrpc-glib.h>

#include "devd-macros.h"
#include "devd-trace.h"
#include "devd-transfer-service.h"
#include "devd-util.h"

#define XFER_FILE_TIMEOUT_SECONDS 60
#define XFER_READ_SIZE_BYTES      (4096*16)

struct _DevdTransferService
{
  GObject parent_instance;

  /*
   * A hashtable of all the active uploads, indexed by the token that we give
   * back to the client after starting an upload.
   */
  GHashTable *uploads;

  /*
   * A hashtable of all the active downloads, indexed by the token that we give
   * back to the client after they requested a download (and we opened a stream
   * to the file).
   */
  GHashTable *downloads;

  /*
   * We have a simple source that periodically looks through uploads and
   * downloads that are stalled and cancels them and removes the temporary
   * files and open handles.
   */
  guint gc_source;
};

typedef struct
{
  volatile gint  ref_count;
  gchar         *path;
  gchar         *token;
  gchar         *tmp_path;
  gsize          size;
  gint64         last_op_time;
  gint           upload_fd;
} PutFileState;

typedef struct
{
  GBytes  *bytes;
  goffset  offset;
  gint     fd;
} WriteOp;

typedef struct
{
  gchar        *token;
  GFileInfo    *info;
  GInputStream *stream;
  gint64        last_op_time;
} GetFileState;

G_DEFINE_TYPE (DevdTransferService, devd_transfer_service, DEVD_TYPE_SERVICE)

static void
get_file_state_free (GetFileState *state)
{
  g_clear_pointer (&state->token, g_free);
  g_clear_object (&state->info);
  g_clear_object (&state->stream);
  g_slice_free (GetFileState, state);
}

static void
write_op_free (WriteOp *op)
{
  g_bytes_unref (op->bytes);
  g_slice_free (WriteOp, op);
}

static PutFileState *
put_file_state_ref (PutFileState *state)
{
  g_assert (state != NULL);
  g_assert (state->ref_count > 0);

  g_atomic_int_inc (&state->ref_count);

  return state;
}

static void
put_file_state_unref (PutFileState *state)
{
  g_assert (state != NULL);
  g_assert (state->ref_count > 0);

  if (g_atomic_int_dec_and_test (&state->ref_count))
    {
      if (state->upload_fd != -1)
        {
          close (state->upload_fd);
          state->upload_fd = -1;
        }

      g_clear_pointer (&state->path, g_free);
      g_clear_pointer (&state->token, g_free);

      if (state->tmp_path != NULL)
        {
          if (g_file_test (state->tmp_path, G_FILE_TEST_IS_REGULAR))
            g_unlink (state->tmp_path);
          g_clear_pointer (&state->tmp_path, g_free);
        }

      g_slice_free (PutFileState, state);
    }
}

static gboolean
devd_transfer_service_gc_source_cb (gpointer data)
{
  DevdTransferService *self = data;
  GHashTableIter iter;
  gpointer value;
  gint64 deadline;

  DEVD_ENTRY;

  g_assert (DEVD_IS_TRANSFER_SERVICE (self));

  deadline = g_get_monotonic_time () - (G_TIME_SPAN_SECOND * XFER_FILE_TIMEOUT_SECONDS);

  g_hash_table_iter_init (&iter, self->uploads);
  while (g_hash_table_iter_next (&iter, NULL, &value))
    {
      PutFileState *state = value;

      g_assert (state != NULL);
      g_assert (state->ref_count > 0);
      g_assert (state->path != NULL);

      if (state->last_op_time <= deadline)
        g_hash_table_iter_remove (&iter);
    }

  g_hash_table_iter_init (&iter, self->downloads);
  while (g_hash_table_iter_next (&iter, NULL, &value))
    {
      GetFileState *state = value;

      g_assert (state != NULL);
      g_assert (G_IS_FILE_INFO (state->info));
      g_assert (!state->stream || G_IS_INPUT_STREAM (state->stream));

      if (state->last_op_time <= deadline)
        g_hash_table_iter_remove (&iter);
    }

  if (g_hash_table_size (self->uploads) > 0 || g_hash_table_size (self->downloads) > 0)
    DEVD_RETURN (G_SOURCE_CONTINUE);

  self->gc_source = 0;

  DEVD_RETURN (G_SOURCE_REMOVE);
}

static void
handle_put_file_begin_cb (GObject      *object,
                          GAsyncResult *result,
                          gpointer      user_data)
{
  DevdTransferService *self = (DevdTransferService *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  g_autofree gchar *token = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_TRANSFER_SERVICE (self));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!devd_transfer_service_put_file_begin_finish (self, result, &token, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_pointer (task,
                           JSONRPC_MESSAGE_NEW ("token", JSONRPC_MESSAGE_PUT_STRING (token)),
                           (GDestroyNotify)g_variant_unref);

  DEVD_EXIT;
}

static void
handle_put_file_begin (DevdService         *service,
                       const gchar         *method,
                       GVariant            *params,
                       GCancellable        *cancellable,
                       GAsyncReadyCallback  callback,
                       gpointer             user_data)
{
  DevdTransferService *self = (DevdTransferService *)service;
  g_autoptr(GTask) task = NULL;
  g_autofree gchar *absolute = NULL;
  const gchar *path = NULL;
  gint64 size = 0;

  DEVD_ENTRY;

  g_assert (DEVD_IS_TRANSFER_SERVICE (self));
  g_assert (method != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, handle_put_file_begin);

  if (!JSONRPC_MESSAGE_PARSE (params,
                              "path", JSONRPC_MESSAGE_GET_STRING (&path),
                              "size", JSONRPC_MESSAGE_GET_INT64 (&size)) ||
      path == NULL || *path == 0)
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_INVAL,
                               "put-file-begin requires a destination path");
      DEVD_EXIT;
    }

  /* resolve paths from $HOME */
  if (!g_path_is_absolute (path))
    path = absolute = g_build_filename (g_get_home_dir (), path, NULL);

  devd_transfer_service_put_file_begin_async (self,
                                              path,
                                              size,
                                              cancellable,
                                              handle_put_file_begin_cb,
                                              g_steal_pointer (&task));

  DEVD_EXIT;
}

static void
handle_put_file_data_cb (GObject      *object,
                         GAsyncResult *result,
                         gpointer      user_data)
{
  DevdTransferService *self = (DevdTransferService *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;

  DEVD_ENTRY;

  g_assert (DEVD_IS_TRANSFER_SERVICE (self));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!devd_transfer_service_put_file_data_finish (self, result, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_pointer (task, NULL, NULL);

  DEVD_EXIT;
}

static void
handle_put_file_data (DevdService         *service,
                      const gchar         *method,
                      GVariant            *params,
                      GCancellable        *cancellable,
                      GAsyncReadyCallback  callback,
                      gpointer             user_data)
{
  DevdTransferService *self = (DevdTransferService *)service;
  g_autoptr(GBytes) bytes = NULL;
  g_autoptr(GTask) task = NULL;
  g_autofree guchar *decoded = NULL;
  const gchar *token = NULL;
  const gchar *encoded = NULL;
  gint64 offset = 0;
  gsize len = 0;

  DEVD_ENTRY;

  g_assert (DEVD_IS_TRANSFER_SERVICE (self));
  g_assert (method != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, handle_put_file_data);

  JSONRPC_MESSAGE_PARSE (params,
    "token", JSONRPC_MESSAGE_GET_STRING (&token),
    "offset", JSONRPC_MESSAGE_GET_INT64 (&offset),
    "data", JSONRPC_MESSAGE_GET_STRING (&encoded)
  );

  if (offset < 0 || encoded == NULL)
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_INVAL,
                               "Invalid parameters provided");
      return;
    }

  if (encoded != NULL)
    decoded = g_base64_decode (encoded, &len);
  bytes = g_bytes_new_take (g_steal_pointer (&decoded), len);

  devd_transfer_service_put_file_data_async (self,
                                             token,
                                             offset,
                                             bytes,
                                             cancellable,
                                             handle_put_file_data_cb,
                                             g_steal_pointer (&task));

  DEVD_EXIT;
}

static void
handle_put_file_finish_cb (GObject      *object,
                           GAsyncResult *result,
                           gpointer      user_data)
{
  DevdTransferService *self = (DevdTransferService *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;

  DEVD_ENTRY;

  g_assert (DEVD_IS_TRANSFER_SERVICE (self));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!devd_transfer_service_put_file_finish_finish (self, result, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_pointer (task,
                           g_variant_take_ref (g_variant_new_boolean (TRUE)),
                           (GDestroyNotify)g_variant_unref);

  DEVD_EXIT;
}

static void
handle_put_file_finish (DevdService         *service,
                        const gchar         *method,
                        GVariant            *params,
                        GCancellable        *cancellable,
                        GAsyncReadyCallback  callback,
                        gpointer             user_data)
{
  DevdTransferService *self = (DevdTransferService *)service;
  g_autoptr(GTask) task = NULL;
  const gchar *token = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_TRANSFER_SERVICE (self));
  g_assert (method != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, handle_put_file_finish);

  JSONRPC_MESSAGE_PARSE (params, "token", JSONRPC_MESSAGE_GET_STRING (&token));

  devd_transfer_service_put_file_finish_async (self,
                                               token,
                                               cancellable,
                                               handle_put_file_finish_cb,
                                               g_steal_pointer (&task));

  DEVD_EXIT;
}

static void
handle_get_file_finish_cb (GObject      *object,
                           GAsyncResult *result,
                           gpointer      user_data)
{
  DevdTransferService *self = (DevdTransferService *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  g_autofree gchar *token = NULL;
  gsize size = 0;

  DEVD_ENTRY;

  g_assert (DEVD_IS_TRANSFER_SERVICE (self));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!devd_transfer_service_get_file_begin_finish (self, result, &token, &size, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_pointer (task,
                           JSONRPC_MESSAGE_NEW ("token", JSONRPC_MESSAGE_PUT_STRING (token),
                                                "size", JSONRPC_MESSAGE_PUT_INT64 (size)),
                           (GDestroyNotify)g_variant_unref);

  DEVD_EXIT;
}

static void
handle_get_file_begin (DevdService         *service,
                       const gchar         *method,
                       GVariant            *params,
                       GCancellable        *cancellable,
                       GAsyncReadyCallback  callback,
                       gpointer             user_data)
{
  DevdTransferService *self = (DevdTransferService *)service;
  g_autoptr(GTask) task = NULL;
  const gchar *path = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_TRANSFER_SERVICE (self));
  g_assert (method != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, handle_get_file_begin);

  JSONRPC_MESSAGE_PARSE (params, "path", JSONRPC_MESSAGE_GET_STRING (&path));

  devd_transfer_service_get_file_begin_async (self,
                                              path,
                                              cancellable,
                                              handle_get_file_finish_cb,
                                              g_steal_pointer (&task));

  DEVD_EXIT;
}

static void
handle_get_file_data_cb (GObject      *object,
                         GAsyncResult *result,
                         gpointer      user_data)
{
  DevdTransferService *self = (DevdTransferService *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GBytes) bytes = NULL;
  g_autoptr(GVariant) reply = NULL;
  g_autofree gchar *encoded = NULL;
  const guchar *data;
  gsize len = 0;

  DEVD_ENTRY;

  g_assert (DEVD_IS_TRANSFER_SERVICE (self));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!(bytes = devd_transfer_service_get_file_data_finish (self, result, &error)))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  data = g_bytes_get_data (bytes, &len);
  encoded = g_base64_encode (data, len);
  g_assert (encoded != NULL);

  reply = JSONRPC_MESSAGE_NEW (
    "data", JSONRPC_MESSAGE_PUT_STRING (encoded)
  );

  g_task_return_pointer (task,
                         g_steal_pointer (&reply),
                         (GDestroyNotify)g_variant_unref);

  DEVD_EXIT;
}

static void
handle_get_file_data (DevdService         *service,
                      const gchar         *method,
                      GVariant            *params,
                      GCancellable        *cancellable,
                      GAsyncReadyCallback  callback,
                      gpointer             user_data)
{
  DevdTransferService *self = (DevdTransferService *)service;
  g_autoptr(GTask) task = NULL;
  const gchar *token = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_TRANSFER_SERVICE (self));
  g_assert (method != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, handle_get_file_data);

  JSONRPC_MESSAGE_PARSE (params, "token", JSONRPC_MESSAGE_GET_STRING (&token));

  devd_transfer_service_get_file_data_async (self,
                                             token,
                                             cancellable,
                                             handle_get_file_data_cb,
                                             g_steal_pointer (&task));

  DEVD_EXIT;
}

static const DevdServiceMethods methods[] = {
  { "get-file-begin",  handle_get_file_begin },
  { "get-file-data",   handle_get_file_data },
  { "put-file-begin",  handle_put_file_begin },
  { "put-file-data",   handle_put_file_data },
  { "put-file-finish", handle_put_file_finish },
};

static void
devd_transfer_service_finalize (GObject *object)
{
  DevdTransferService *self = (DevdTransferService *)object;

  DEVD_ENTRY;

  devd_clear_source (&self->gc_source);
  g_clear_pointer (&self->downloads, g_hash_table_unref);
  g_clear_pointer (&self->uploads, g_hash_table_unref);

  G_OBJECT_CLASS (devd_transfer_service_parent_class)->finalize (object);

  DEVD_EXIT;
}

static void
devd_transfer_service_class_init (DevdTransferServiceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = devd_transfer_service_finalize;
}

static void
devd_transfer_service_init (DevdTransferService *self)
{
  DEVD_ENTRY;

  self->downloads =
    g_hash_table_new_full (g_str_hash,
                           g_str_equal,
                           NULL,
                           (GDestroyNotify)get_file_state_free);

  self->uploads =
    g_hash_table_new_full (g_str_hash,
                           g_str_equal,
                           NULL,
                           (GDestroyNotify)put_file_state_unref);

  devd_service_add_methods (DEVD_SERVICE (self), methods, G_N_ELEMENTS (methods));

  DEVD_EXIT;
}

static gboolean
devd_transfer_service_get_put_file_state (DevdTransferService  *self,
                                          const gchar          *token,
                                          PutFileState        **state,
                                          GError              **error)
{
  g_assert (DEVD_IS_TRANSFER_SERVICE (self));
  g_assert (self->uploads != NULL);
  g_assert (state != NULL);

  *state = NULL;

  /*
   * Locate the upload state by the token we've been provided. We gave the
   * client a token when it started the upload, and it is responsible for
   * passing it back to us.
   */
  if (token == NULL || NULL == (*state = g_hash_table_lookup (self->uploads, token)))
    g_set_error (error,
                 G_IO_ERROR,
                 G_IO_ERROR_NOT_FOUND,
                 "Failed to locate upload for token");

  return *state != NULL;
}

static void
devd_transfer_service_put_file_begin_worker (GTask        *task,
                                             gpointer      source_object,
                                             gpointer      task_data,
                                             GCancellable *cancellable)
{
  PutFileState *state = task_data;
  g_autofree gchar *directory = NULL;
  g_autofree gchar *template = NULL;
  int fret;

  DEVD_ENTRY;

  g_assert (G_IS_TASK (task));
  g_assert (DEVD_IS_TRANSFER_SERVICE (source_object));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));
  g_assert (state != NULL);
  g_assert (state->path != NULL);
  g_assert (state->token == NULL);
  g_assert (state->upload_fd == -1);

  /*
   * Make sure the directory that will contain our file is created, so that
   * we can place the temporary upload file in the same directory (so that
   * we can use rename() for the atomic rename).
   */

  directory = g_path_get_dirname (state->path);

  if (g_mkdir_with_parents (directory, 0750) != 0)
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               g_io_error_from_errno (errno),
                               "%s",
                               g_strerror (errno));
      DEVD_EXIT;
    }

  /*
   * Create a temporary file in the target directory to store upload contents
   * from the peer. @template has "XXXXXX" transformed into random characters
   * that we can use later on to rename/unlink the temporary file.
   */
  g_assert (state->upload_fd == -1);

  template = g_build_filename (directory, ".deviced-upload-XXXXXX", NULL);
  state->upload_fd = g_mkstemp_full (template, O_RDWR, 0640);

  if (state->upload_fd == -1)
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               g_io_error_from_errno (errno),
                               "%s",
                               g_strerror (errno));
      DEVD_EXIT;
    }

  g_assert (state->upload_fd != -1);

  /*
   * If we can't fallocate() enough space for the file, then fail the operation
   * immediately instead of allowing the peer to continue processing the
   * transfer (only to fail later).
   */
  if (state->size > 0 &&
      0 != (fret = posix_fallocate (state->upload_fd, 0, state->size)))
    {
      g_autofree gchar *formatted = g_format_size (state->size);

      g_unlink (template);
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_NO_SPACE,
                               "Not enough space for %s",
                               formatted);

      DEVD_EXIT;
    }

  /*
   * Save the temporary file path so that we can do a rename() from the
   * put-file-finish operation.
   */
  state->tmp_path = g_steal_pointer (&template);

  /*
   * Generate a token that the client can use to write more data to the file
   * by streaming it in a series of put-file-data calls.
   */
  state->token = g_uuid_string_random ();

  /*
   * Keep track of the time we exit this function, so that we wont
   * GC things until the request has timed out.
   */
  state->last_op_time = g_get_monotonic_time ();

  /*
   * Because we are in a thread, we don't stash the PutFileState here and
   * instead wait until we round-trip back to the main thread. That is
   * better anyway because it means we discard state immediately if the
   * consumer didn't call devd_transfer_service_put_file_begin_finish().
   */
  g_task_return_boolean (task, TRUE);

  DEVD_EXIT;
}

void
devd_transfer_service_put_file_begin_async (DevdTransferService *self,
                                            const gchar         *path,
                                            gsize                size,
                                            GCancellable        *cancellable,
                                            GAsyncReadyCallback  callback,
                                            gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  PutFileState *state;

  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_TRANSFER_SERVICE (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  state = g_slice_new0 (PutFileState);
  state->ref_count = 1;
  state->path = g_strdup (path);
  state->size = size;
  state->token = NULL;
  state->upload_fd = -1;
  state->last_op_time = 0;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_transfer_service_put_file_begin_async);
  g_task_set_task_data (task, g_steal_pointer (&state), (GDestroyNotify)put_file_state_unref);
  g_task_run_in_thread (task, devd_transfer_service_put_file_begin_worker);

  DEVD_EXIT;
}

gboolean
devd_transfer_service_put_file_begin_finish (DevdTransferService  *self,
                                             GAsyncResult         *result,
                                             gchar               **token,
                                             GError              **error)
{
  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_TRANSFER_SERVICE (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  if (g_task_propagate_boolean (G_TASK (result), error))
    {
      PutFileState *state = g_task_get_task_data (G_TASK (result));

      if (token != NULL)
        *token = g_strdup (state->token);


      g_hash_table_insert (self->uploads,
                           state->token,
                           put_file_state_ref (state));

      if (self->gc_source == 0)
        self->gc_source = g_timeout_add_seconds_full (G_PRIORITY_DEFAULT,
                                                      XFER_FILE_TIMEOUT_SECONDS,
                                                      devd_transfer_service_gc_source_cb,
                                                      self,
                                                      NULL);

      DEVD_RETURN (TRUE);
    }

  DEVD_RETURN (FALSE);
}

static void
devd_transfer_service_put_file_data_worker (GTask        *task,
                                            gpointer      source_object,
                                            gpointer      task_data,
                                            GCancellable *cancellable)
{
  WriteOp *op = task_data;
  const gchar *data;
  goffset offset;
  gssize n_written;
  gsize len;

  DEVD_ENTRY;

  g_assert (G_IS_TASK (task));
  g_assert (DEVD_IS_TRANSFER_SERVICE (source_object));
  g_assert (op != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  offset = op->offset;
  data = g_bytes_get_data (op->bytes, &len);

  while (len > 0)
    {
      n_written = pwrite (op->fd, data, len, offset);

      if (n_written > 0)
        {
          len -= n_written;
          offset += offset;
          data += offset;

          continue;
        }

      if (errno == EAGAIN)
        continue;

      g_task_return_new_error (task,
                               G_IO_ERROR,
                               g_io_error_from_errno (errno),
                               "%s",
                               g_strerror (errno));

      DEVD_EXIT;
    }

  g_task_return_boolean (task, TRUE);

  DEVD_EXIT;
}

void
devd_transfer_service_put_file_data_async (DevdTransferService *self,
                                           const gchar         *token,
                                           goffset              offset,
                                           GBytes              *bytes,
                                           GCancellable        *cancellable,
                                           GAsyncReadyCallback  callback,
                                           gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  g_autoptr(GError) error = NULL;
  PutFileState *state = NULL;
  WriteOp *op;

  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_TRANSFER_SERVICE (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_transfer_service_put_file_data_async);

  if (!devd_transfer_service_get_put_file_state (self, token, &state, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  state->last_op_time = g_get_monotonic_time ();

  op = g_slice_new0 (WriteOp);
  op->fd = state->upload_fd;
  op->offset = offset;
  op->bytes = g_bytes_ref (bytes);

  g_task_set_task_data (task, op, (GDestroyNotify)write_op_free);
  g_task_run_in_thread (task, devd_transfer_service_put_file_data_worker);

  DEVD_EXIT;
}

gboolean
devd_transfer_service_put_file_data_finish (DevdTransferService  *self,
                                            GAsyncResult         *result,
                                            GError              **error)
{
  gboolean ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_TRANSFER_SERVICE (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  ret = g_task_propagate_boolean (G_TASK (result), error);

  DEVD_RETURN (ret);
}

void
devd_transfer_service_put_file_finish_async (DevdTransferService *self,
                                             const gchar         *token,
                                             GCancellable        *cancellable,
                                             GAsyncReadyCallback  callback,
                                             gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  g_autoptr(GError) error = NULL;
  PutFileState *state = NULL;

  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_TRANSFER_SERVICE (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_transfer_service_put_file_finish_async);

  if (!devd_transfer_service_get_put_file_state (self, token, &state, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  if (rename (state->tmp_path, state->path) != 0)
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               g_io_error_from_errno (errno),
                               "%s",
                               g_strerror (errno));
      DEVD_EXIT;
    }

  g_hash_table_remove (self->uploads, state->token);

  g_task_return_boolean (task, TRUE);

  DEVD_EXIT;
}

gboolean
devd_transfer_service_put_file_finish_finish (DevdTransferService  *self,
                                              GAsyncResult         *result,
                                              GError              **error)
{
  gboolean ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_TRANSFER_SERVICE (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  ret = g_task_propagate_boolean (G_TASK (result), error);

  DEVD_RETURN (ret);
}

static void
devd_transfer_service_get_file_open_cb (GObject      *object,
                                        GAsyncResult *result,
                                        gpointer      user_data)
{
  GFile *file = (GFile *)object;
  g_autoptr(GInputStream) stream = NULL;
  g_autoptr(GFileInfo) info = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  DevdTransferService *self;
  GetFileState *state;

  DEVD_ENTRY;

  g_assert (G_IS_FILE (file));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!(stream = devd_file_read_with_info_finish (file, result, &info, &error)))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  self = g_task_get_source_object (task);

  g_assert (G_IS_INPUT_STREAM (stream));
  g_assert (G_IS_FILE_INFO (info));
  g_assert (DEVD_IS_TRANSFER_SERVICE (self));

  state = g_slice_new0 (GetFileState);
  state->stream = g_object_ref (stream);
  state->info = g_object_ref (info);
  state->token = g_uuid_string_random ();
  state->last_op_time = g_get_monotonic_time ();
  g_hash_table_insert (self->downloads, state->token, state);

  if (self->gc_source == 0)
    self->gc_source = g_timeout_add_seconds_full (G_PRIORITY_DEFAULT,
                                                  XFER_FILE_TIMEOUT_SECONDS,
                                                  devd_transfer_service_gc_source_cb,
                                                  self,
                                                  NULL);

  g_task_set_task_data (task, g_strdup (state->token), g_free);
  g_task_return_boolean (task, TRUE);

  DEVD_EXIT;
}

void
devd_transfer_service_get_file_begin_async (DevdTransferService *self,
                                            const gchar         *path,
                                            GCancellable        *cancellable,
                                            GAsyncReadyCallback  callback,
                                            gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  g_autoptr(GFile) file = NULL;
  g_autofree gchar *absolute = NULL;

  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_TRANSFER_SERVICE (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_transfer_service_get_file_begin_async);

  if (path == NULL)
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_NOT_FOUND,
                               "Path must be provided");
      DEVD_EXIT;
    }

  /* resolve paths from $HOME */
  if (!g_path_is_absolute (path))
    path = absolute = g_build_filename (g_get_home_dir (), path, NULL);

  file = g_file_new_for_path (path);
  devd_file_read_with_info_async (file,
                                  G_FILE_ATTRIBUTE_STANDARD_NAME","
                                  G_FILE_ATTRIBUTE_STANDARD_SIZE,
                                  cancellable,
                                  devd_transfer_service_get_file_open_cb,
                                  g_steal_pointer (&task));

  DEVD_EXIT;
}

gboolean
devd_transfer_service_get_file_begin_finish (DevdTransferService  *self,
                                             GAsyncResult         *result,
                                             gchar               **token,
                                             gsize                *size,
                                             GError              **error)
{
  const gchar *local_token;
  GetFileState *state;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_TRANSFER_SERVICE (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);
  g_return_val_if_fail (g_task_is_valid (G_TASK (result), self), FALSE);

  if (token != NULL)
    *token = NULL;

  if (size != NULL)
    *size = 0;

  if (!g_task_propagate_boolean (G_TASK (result), error))
    return FALSE;

  if (!(local_token = g_task_get_task_data (G_TASK (result))) ||
      !(state = g_hash_table_lookup (self->downloads, local_token)))
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_FAILED,
                   "Unexpected failure, daemon might be in shutdown");
      return FALSE;
    }

  g_assert (state != NULL);
  g_assert (G_IS_FILE_INFO (state->info));
  g_assert (G_IS_INPUT_STREAM (state->stream));

  if (token)
    *token = g_strdup (state->token);

  if (size != NULL)
    *size = g_file_info_get_size (state->info);

  DEVD_RETURN (TRUE);
}

static void
devd_transfer_service_get_file_read_cb (GObject      *object,
                                        GAsyncResult *result,
                                        gpointer      user_data)
{
  GInputStream *stream = (GInputStream *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_autoptr(GBytes) bytes = NULL;
  DevdTransferService *self;
  const gchar *token;

  DEVD_ENTRY;

  g_assert (G_IS_INPUT_STREAM (stream));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!(bytes = g_input_stream_read_bytes_finish (stream, result, &error)))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_pointer (task, g_bytes_ref (bytes), (GDestroyNotify)g_bytes_unref);

  token = g_task_get_task_data (task);
  self = g_task_get_source_object (task);

  /* If it looks like we finished the read, remove state */
  if (token != NULL && bytes != NULL && g_bytes_get_size (bytes) == 0)
    g_hash_table_remove (self->downloads, token);

  DEVD_EXIT;
}

void
devd_transfer_service_get_file_data_async (DevdTransferService *self,
                                           const gchar         *token,
                                           GCancellable        *cancellable,
                                           GAsyncReadyCallback  callback,
                                           gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  GetFileState *state;

  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_TRANSFER_SERVICE (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_transfer_service_get_file_data_async);
  g_task_set_task_data (task, g_strdup (token), g_free);

  if (token == NULL || !(state = g_hash_table_lookup (self->downloads, token)))
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_NOT_FOUND,
                               "No such token to continue download");
      DEVD_EXIT;
    }

  g_assert (state != NULL);
  g_assert (G_IS_INPUT_STREAM (state->stream));

  state->last_op_time = g_get_monotonic_time ();

  g_input_stream_read_bytes_async (state->stream,
                                   XFER_READ_SIZE_BYTES,
                                   G_PRIORITY_DEFAULT,
                                   cancellable,
                                   devd_transfer_service_get_file_read_cb,
                                   g_steal_pointer (&task));

  DEVD_EXIT;
}

GBytes *
devd_transfer_service_get_file_data_finish (DevdTransferService  *self,
                                            GAsyncResult         *result,
                                            GError              **error)
{
  g_autoptr(GBytes) ret = NULL;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_TRANSFER_SERVICE (self), NULL);
  g_return_val_if_fail (G_IS_TASK (result), NULL);
  g_return_val_if_fail (g_task_is_valid (G_TASK (result), self), NULL);

  ret = g_task_propagate_pointer (G_TASK (result), error);

  DEVD_RETURN (g_steal_pointer (&ret));
}

DevdTransferService *
devd_transfer_service_new (void)
{
  return g_object_new (DEVD_TYPE_TRANSFER_SERVICE, NULL);
}
