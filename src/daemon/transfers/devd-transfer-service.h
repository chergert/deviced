/* devd-transfer-service.h
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "devd-service.h"

G_BEGIN_DECLS

#define DEVD_TRANSFER_SERVICE_NAME "org.gnome.deviced.transfers"
#define DEVD_TYPE_TRANSFER_SERVICE (devd_transfer_service_get_type())

G_DECLARE_FINAL_TYPE (DevdTransferService, devd_transfer_service, DEVD, TRANSFER_SERVICE, DevdService)

DevdTransferService *devd_transfer_service_new                    (void);
void                 devd_transfer_service_put_file_begin_async   (DevdTransferService  *self,
                                                                   const gchar          *path,
                                                                   gsize                 size,
                                                                   GCancellable         *cancellable,
                                                                   GAsyncReadyCallback   callback,
                                                                   gpointer              user_data);
gboolean             devd_transfer_service_put_file_begin_finish  (DevdTransferService  *self,
                                                                   GAsyncResult         *result,
                                                                   gchar               **token,
                                                                   GError              **error);
void                 devd_transfer_service_put_file_data_async    (DevdTransferService  *self,
                                                                   const gchar          *token,
                                                                   goffset               offset,
                                                                   GBytes               *bytes,
                                                                   GCancellable         *cancellable,
                                                                   GAsyncReadyCallback   callback,
                                                                   gpointer              user_data);
gboolean             devd_transfer_service_put_file_data_finish   (DevdTransferService  *self,
                                                                   GAsyncResult         *result,
                                                                   GError              **error);
void                 devd_transfer_service_put_file_finish_async  (DevdTransferService  *self,
                                                                   const gchar          *token,
                                                                   GCancellable         *cancellable,
                                                                   GAsyncReadyCallback   callback,
                                                                   gpointer              user_data);
gboolean             devd_transfer_service_put_file_finish_finish (DevdTransferService  *self,
                                                                   GAsyncResult         *result,
                                                                   GError              **error);
void                 devd_transfer_service_get_file_begin_async   (DevdTransferService  *self,
                                                                   const gchar          *path,
                                                                   GCancellable         *cancellable,
                                                                   GAsyncReadyCallback   callback,
                                                                   gpointer              user_data);
gboolean             devd_transfer_service_get_file_begin_finish  (DevdTransferService  *self,
                                                                   GAsyncResult         *result,
                                                                   gchar               **token,
                                                                   gsize                *size,
                                                                   GError              **error);
void                 devd_transfer_service_get_file_data_async    (DevdTransferService  *self,
                                                                   const gchar          *token,
                                                                   GCancellable         *cancellable,
                                                                   GAsyncReadyCallback   callback,
                                                                   gpointer              user_data);
GBytes              *devd_transfer_service_get_file_data_finish   (DevdTransferService  *self,
                                                                   GAsyncResult         *result,
                                                                   GError              **error);

G_END_DECLS
