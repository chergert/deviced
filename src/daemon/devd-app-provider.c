/* devd-app-provider.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "devd-app-provider"

#include "devd-app-provider.h"
#include "devd-trace.h"

G_DEFINE_INTERFACE (DevdAppProvider, devd_app_provider, G_TYPE_OBJECT)

static void
devd_app_provider_real_list_apps_async (DevdAppProvider     *self,
                                        gboolean             list_runtimes,
                                        GCancellable        *cancellable,
                                        GAsyncReadyCallback  callback,
                                        gpointer             user_data)
{
  g_task_report_new_error (self, callback, user_data,
                           devd_app_provider_real_list_apps_async,
                           G_IO_ERROR,
                           G_IO_ERROR_NOT_SUPPORTED,
                           "Listing apps is not supported on this backend");
}

static GPtrArray *
devd_app_provider_real_list_apps_finish (DevdAppProvider  *self,
                                         GAsyncResult     *result,
                                         GError          **error)
{
  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
devd_app_provider_real_run_app_async (DevdAppProvider     *self,
                                      const gchar         *app_id,
                                      DevdPty             *pty,
                                      GCancellable        *cancellable,
                                      GAsyncReadyCallback  callback,
                                      gpointer             user_data)
{
  g_task_report_new_error (self, callback, user_data,
                           devd_app_provider_real_run_app_async,
                           G_IO_ERROR,
                           G_IO_ERROR_NOT_SUPPORTED,
                           "Launching apps is not supported on this backend");
}

static gchar *
devd_app_provider_real_run_app_finish (DevdAppProvider  *self,
                                       GAsyncResult     *result,
                                       GError          **error)
{
  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
devd_app_provider_default_init (DevdAppProviderInterface *iface)
{
  iface->list_apps_async = devd_app_provider_real_list_apps_async;
  iface->list_apps_finish = devd_app_provider_real_list_apps_finish;
  iface->run_app_async = devd_app_provider_real_run_app_async;
  iface->run_app_finish = devd_app_provider_real_run_app_finish;
}

/**
 * devd_app_provider_list_apps_async:
 * @self: a #DevdAppProvider
 * @cancellable: (nullable): a #GCancellable or %NULL
 * @callback: a callback to execute upon completion
 * @user_data: user data for @callback
 *
 * Asynchronously requests a list of apps known to the provider.
 *
 * See devd_app_provider_list_apps_finish() to get the result.
 */
void
devd_app_provider_list_apps_async (DevdAppProvider     *self,
                                   gboolean             list_runtimes,
                                   GCancellable        *cancellable,
                                   GAsyncReadyCallback  callback,
                                   gpointer             user_data)
{
  g_return_if_fail (DEVD_IS_APP_PROVIDER (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  DEVD_APP_PROVIDER_GET_IFACE (self)->list_apps_async (self, list_runtimes, cancellable, callback, user_data);
}

/**
 * devd_app_provider_list_apps_finish:
 * @self: a #DevdAppProvider
 * @result: a #GAsyncResult
 * @error: a location for a #GError, or %NULL
 *
 * Completes a request to list the applications known to this provider.
 *
 * Returns: (transfer container) (element-type Devd.AppInfo): an array of app info
 *   or %NULL and @error is set.
 */
GPtrArray *
devd_app_provider_list_apps_finish (DevdAppProvider  *self,
                                    GAsyncResult     *result,
                                    GError          **error)
{
  GPtrArray *ret;

  g_return_val_if_fail (DEVD_IS_APP_PROVIDER (self), NULL);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), NULL);

  ret = DEVD_APP_PROVIDER_GET_IFACE (self)->list_apps_finish (self, result, error);

  DEVD_RETURN (ret);
}


/**
 * devd_app_provider_run_app_async:
 * @self: a #DevdAppProvider
 * @app_id: an identifier for the application
 * @pty: (nullable): a #DevdPty or %NULL
 * @cancellable: (nullable): a #GCancellable or %NULL
 * @callback: a callback to execute upon completion
 * @user_data: user data for @callback
 *
 * Asynchronously launches an application known to the provider.
 *
 * See devd_app_provider_run_app_finish() to get the result.
 */
void
devd_app_provider_run_app_async (DevdAppProvider     *self,
                                 const gchar         *app_id,
                                 DevdPty             *pty,
                                 GCancellable        *cancellable,
                                 GAsyncReadyCallback  callback,
                                 gpointer             user_data)
{
  g_return_if_fail (DEVD_IS_APP_PROVIDER (self));
  g_return_if_fail (app_id != NULL);
  g_return_if_fail (!pty || DEVD_IS_PTY (pty));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  DEVD_APP_PROVIDER_GET_IFACE (self)->run_app_async (self, app_id, pty, cancellable, callback, user_data);
}

/**
 * devd_app_provider_run_app_finish:
 * @self: a #DevdAppProvider
 * @result: a #GAsyncResult
 * @error: a location for a #GError, or %NULL
 *
 * Completes a request to launch the applications known to this provider.
 *
 * Returns: the process identifier if successful; otherwise %NULL and @error is set.
 */
gchar *
devd_app_provider_run_app_finish (DevdAppProvider  *self,
                                  GAsyncResult     *result,
                                  GError          **error)
{
  gchar *ret;

  g_return_val_if_fail (DEVD_IS_APP_PROVIDER (self), FALSE);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), FALSE);

  ret = DEVD_APP_PROVIDER_GET_IFACE (self)->run_app_finish (self, result, error);

  DEVD_RETURN (ret);
}
