/* devd-network-listener.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "devd-network-listener"

#include "config.h"

#include <avahi-gobject/ga-client.h>
#include <avahi-gobject/ga-entry-group.h>
#include <glib/gi18n.h>
#include <jsonrpc-glib.h>
#include <sys/utsname.h>

#include "devd-application.h"
#include "devd-app-info.h"
#include "devd-network-listener.h"
#include "devd-pty.h"
#include "devd-service.h"
#include "devd-tls-certificate.h"
#include "devd-trace.h"
#include "devd-util.h"

#define RPC_TIMEOUT_SECONDS 60

/**
 * SECTION:devd-network-listener
 * @title: DevdNetworkListener
 * @short_description: A listener that accepts network connections
 *
 * The #DevdNetworkListener is responsible for setting up socket listeners
 * based on the allowed-networks settings. Additionally, it managed the
 * mDNS broadcast (avahi) to allow clients to find this process.
 */

struct _DevdNetworkListener
{
  GSocketService parent_instance;

  /*
   * This cancellable is used so that we can cancel any inflight
   * operation when shutdown has been requested. We chain it to
   * every async operation we do so that things cleanup imediately.
   */
  GCancellable *cancellable;

  /*
   * Our JsonrpcServer manages our JSON-RPC API. We accept connections
   * from socket listeners and then use the JsonrpcServer to implement
   * the server side of the connection. We connect to various signals
   * to handle incoming calls and notifications.
   */
  JsonrpcServer *server;

  /*
   * For each of our method handlers we add to @server, we track the
   * handler-id so that we can remove it during shutdown. This ensures
   * that no callback can be executed should the lifetime of @server
   * outlive our object instance.
   */
  GArray *handlers;

  /*
   * This is our certificate to use to perform the server side of TLS.
   * If set, we require TLS when accepting connections and the client
   * will choose to trust our certificate or not.
   *
   * It's only really useful to not use a certificate when troubleshooting
   * this program or clients to it.
   */
  GTlsCertificate *certificate;

  /*
   * This is our connection to the Avahi daemon. We use it to broadcast
   * the address for peers on the network to find us. That allows programs
   * such as IDEs to display a useful name and connection info for the
   * developer to connect to this device over the local Ethernet segment.
   */
  GaClient *avahi_client;

  /*
   * This contains a collection of information that we are broadcasting
   * on the local Ethernet segment using Avahi. Address info is added
   * to this as we bind to new TCP socket addresses.
   */
  GaEntryGroup *avahi_entry_group;

  /*
   * This is a connection that has been created but not yet been
   * accepted by the user as trusted.
   */
  GIOStream *awaiting_connection;

  /* Application settings */
  GSettings       *settings;
};

typedef struct
{
  JsonrpcClient   *client;
  GVariant        *id;
  GCancellable    *cancellable;
  guint            timeout_source;
} ClientOp;

G_DEFINE_TYPE (DevdNetworkListener, devd_network_listener, G_TYPE_SOCKET_SERVICE)

enum {
  PROP_0,
  PROP_CERTIFICATE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
client_op_free (gpointer data)
{
  ClientOp *op = data;

  if (op->timeout_source != 0)
    {
      g_source_remove (op->timeout_source);
      op->timeout_source = 0;
    }

  g_clear_pointer (&op->id, g_variant_unref);
  g_clear_object (&op->client);
  g_clear_object (&op->cancellable);
  g_slice_free (ClientOp, op);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (ClientOp, client_op_free)

static gboolean
client_op_timed_out_cb (gpointer data)
{
  ClientOp *op = data;

  op->timeout_source = 0;
  g_cancellable_cancel (op->cancellable);
  return G_SOURCE_REMOVE;
}

static ClientOp *
client_op_new (JsonrpcClient *client,
               GVariant      *id)
{
  ClientOp *op;

  op = g_slice_new0 (ClientOp);
  op->client = g_object_ref (client);
  op->id = g_variant_ref (id);
  op->cancellable = g_cancellable_new ();
  op->timeout_source = g_timeout_add_seconds_full (G_PRIORITY_HIGH,
                                                   RPC_TIMEOUT_SECONDS,
                                                   client_op_timed_out_cb,
                                                   op,
                                                   NULL);

  return op;
}

static void
log_reply_cb (GObject      *object,
              GAsyncResult *result,
              gpointer      user_data)
{
  g_autoptr(GError) error = NULL;

  if (!jsonrpc_client_reply_finish (JSONRPC_CLIENT (object), result, &error))
    g_warning ("%s", error->message);
}

static void
client_reply_error (ClientOp     *op,
                    const GError *error)
{
  g_assert (op != NULL);

  if (error != NULL)
    jsonrpc_client_reply_error_async (op->client, op->id, error->code, error->message,
                                      NULL, log_reply_cb, NULL);
  else
    jsonrpc_client_reply_error_async (op->client, op->id,
                                      JSONRPC_CLIENT_ERROR_INVALID_PARAMS,
                                      "Incorrect parameters to operation",
                                      NULL, log_reply_cb, NULL);
}

static void
devd_network_listener_service_notification_cb (JsonrpcClient       *client,
                                               const gchar         *method,
                                               GVariant            *params,
                                               DevdService         *service)
{
  g_assert (JSONRPC_IS_CLIENT (client));
  g_assert (method != NULL);

  jsonrpc_client_send_notification_async (client, method, params, NULL, NULL, NULL);
}

static void
handle_initialize (JsonrpcServer *server,
                   JsonrpcClient *client,
                   const gchar   *method,
                   GVariant      *id,
                   GVariant      *params,
                   gpointer       user_data)
{
  DevdNetworkListener *self = user_data;
  g_autoptr(GVariant) res = NULL;
  g_auto(GStrv) services = NULL;
  const gchar *arch = "unknown";
  const gchar *kernel = "unknown";
  const gchar *system = "gnu";
  struct utsname u;

  g_assert (JSONRPC_IS_SERVER (server));
  g_assert (JSONRPC_IS_CLIENT (client));
  g_assert (method != NULL);
  g_assert (id != NULL);
  g_assert (DEVD_IS_NETWORK_LISTENER (self));

  if (uname (&u) >= 0)
    {
      arch = u.machine;
      kernel = u.sysname;
    }

  res = JSONRPC_MESSAGE_NEW (
    "version", JSONRPC_MESSAGE_PUT_STRING (PACKAGE_VERSION),
    "host", "{",
      "arch", JSONRPC_MESSAGE_PUT_STRING (arch),
      "system", JSONRPC_MESSAGE_PUT_STRING (system),
      "kernel", JSONRPC_MESSAGE_PUT_STRING (kernel),
    "}",
    "name", JSONRPC_MESSAGE_PUT_STRING (g_get_host_name ()),
    "capabilities", "{",
#ifdef HAVE_FLATPAK
      "flatpak", JSONRPC_MESSAGE_PUT_BOOLEAN (TRUE),
#endif
    "}"
  );

  /* Notify the client of available services, before we return from
   * initialize so it has all answers when the RPC finishes.
   */
  services = devd_application_get_services (DEVD_APPLICATION_DEFAULT);
  for (guint i = 0; services[i]; i++)
    {
      const gchar *name = services[i];
      DevdService *service = devd_application_get_service (DEVD_APPLICATION_DEFAULT, name);

      g_signal_connect_object (service,
                               "notification",
                               G_CALLBACK (devd_network_listener_service_notification_cb),
                               client,
                               G_CONNECT_SWAPPED);

      jsonrpc_client_send_notification_async (client,
                                              "service-added",
                                              g_variant_new_string (services[i]),
                                              NULL, NULL, NULL);
    }

  jsonrpc_client_reply_async (client, id, res, NULL, NULL, NULL);
}

static void
handle_keep_alive (JsonrpcServer *server,
                   JsonrpcClient *client,
                   const gchar   *method,
                   GVariant      *id,
                   GVariant      *params,
                   gpointer       user_data)
{
  g_assert (JSONRPC_IS_SERVER (server));
  g_assert (JSONRPC_IS_CLIENT (client));
  g_assert (method != NULL);
  g_assert (id != NULL);
  g_assert (DEVD_IS_NETWORK_LISTENER (user_data));

  jsonrpc_client_reply_async (client, id, NULL, NULL, NULL, NULL);
}

static void
handle_list_apps_cb (GObject      *object,
                     GAsyncResult *result,
                     gpointer      user_data)
{
  DevdApplication *app = (DevdApplication *)object;
  g_autoptr(ClientOp) op = user_data;
  g_autoptr(GPtrArray) app_infos = NULL;
  g_autoptr(GError) error = NULL;
  GVariantBuilder builder;

  g_assert (DEVD_IS_APPLICATION (app));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (op != NULL);

  if (!(app_infos = devd_application_list_apps_finish (app, result, &error)))
    {
      client_reply_error (op, error);
      return;
    }

  g_variant_builder_init (&builder, G_VARIANT_TYPE ("aa{sv}"));

  for (guint i = 0; i < app_infos->len; i++)
    {
      DevdAppInfo *app_info = g_ptr_array_index (app_infos, i);
      g_autoptr(GVariant) doc = devd_app_info_to_variant (app_info);

      g_variant_builder_add_value (&builder, doc);
    }

  jsonrpc_client_reply_async (op->client,
                              op->id,
                              g_variant_builder_end (&builder),
                              op->cancellable,
                              log_reply_cb,
                              NULL);
}

static void
handle_list_apps (JsonrpcServer *server,
                  JsonrpcClient *client,
                  const gchar   *method,
                  GVariant      *id,
                  GVariant      *params,
                  gpointer       user_data)
{
  gboolean list_runtimes;
  ClientOp *op;

  g_assert (JSONRPC_IS_SERVER (server));
  g_assert (JSONRPC_IS_CLIENT (client));
  g_assert (method != NULL);
  g_assert (id != NULL);
  g_assert (DEVD_IS_NETWORK_LISTENER (user_data));

  /* Share one codepath for both since roughly equal */
  list_runtimes = g_strcmp0 (method, "list-runtimes") == 0;

  op = client_op_new (client, id);

  devd_application_list_apps_async (DEVD_APPLICATION_DEFAULT,
                                    list_runtimes,
                                    op->cancellable,
                                    handle_list_apps_cb,
                                    op);
}

static void
handle_list_files_cb (GObject      *object,
                      GAsyncResult *result,
                      gpointer      user_data)
{
  DevdApplication *app = (DevdApplication *)object;
  g_autoptr(ClientOp) op = user_data;
  g_autoptr(GPtrArray) file_infos = NULL;
  g_autoptr(GError) error = NULL;
  GVariantBuilder builder;

  g_assert (DEVD_IS_APPLICATION (app));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (op != NULL);

  if (!(file_infos = devd_application_list_files_finish (app, result, &error)))
    {
      client_reply_error (op, error);
      return;
    }

  g_variant_builder_init (&builder, G_VARIANT_TYPE ("aa{sv}"));

  for (guint i = 0; i < file_infos->len; i++)
    {
      GFileInfo *file_info = g_ptr_array_index (file_infos, i);
      g_autoptr(GVariant) doc = NULL;

      g_assert (G_IS_FILE_INFO (file_info));

      if ((doc = devd_file_info_to_gvariant (file_info)))
        g_variant_builder_add_value (&builder, doc);
    }

  jsonrpc_client_reply_async (op->client,
                              op->id,
                              g_variant_builder_end (&builder),
                              op->cancellable,
                              log_reply_cb,
                              NULL);
}

static void
handle_list_files (JsonrpcServer *server,
                   JsonrpcClient *client,
                   const gchar   *method,
                   GVariant      *id,
                   GVariant      *params,
                   gpointer       user_data)
{
  const gchar *path = NULL;
  const gchar *attributes = NULL;
  ClientOp *op;

  g_assert (JSONRPC_IS_SERVER (server));
  g_assert (JSONRPC_IS_CLIENT (client));
  g_assert (method != NULL);
  g_assert (id != NULL);
  g_assert (DEVD_IS_NETWORK_LISTENER (user_data));

  op = client_op_new (client, id);

  if (params != NULL)
    {
      JSONRPC_MESSAGE_PARSE (params, "path", JSONRPC_MESSAGE_GET_STRING (&path));
      JSONRPC_MESSAGE_PARSE (params, "attributes", JSONRPC_MESSAGE_GET_STRING (&attributes));
    }

  if (path == NULL)
    path = g_get_home_dir ();

  if (attributes == NULL)
    attributes = G_FILE_ATTRIBUTE_STANDARD_NAME","
                 G_FILE_ATTRIBUTE_STANDARD_TYPE","
                 G_FILE_ATTRIBUTE_STANDARD_SIZE","
                 G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE","
                 G_FILE_ATTRIBUTE_STANDARD_ICON;

  devd_application_list_files_async (DEVD_APPLICATION_DEFAULT,
                                     path,
                                     attributes,
                                     op->cancellable,
                                     handle_list_files_cb,
                                     op);
}

static void
handle_run_app_cb (GObject      *object,
                   GAsyncResult *result,
                   gpointer      user_data)
{
  DevdApplication *app = (DevdApplication *)object;
  g_autoptr(ClientOp) op = user_data;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *identifier = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_APPLICATION (app));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (op != NULL);

  identifier = devd_application_run_app_finish (app, result, &error);

  if (error != NULL)
    {
      client_reply_error (op, error);
      DEVD_EXIT;
    }

  jsonrpc_client_reply_async (op->client,
                              op->id,
                              g_variant_new_string (identifier),
                              op->cancellable,
                              log_reply_cb,
                              NULL);

  DEVD_EXIT;
}

static void
handle_run_app (JsonrpcServer *server,
                JsonrpcClient *client,
                const gchar   *method,
                GVariant      *id,
                GVariant      *params,
                gpointer       user_data)
{
  const gchar *provider = NULL;
  const gchar *app_id = NULL;
  const gchar *pty_id = NULL;
  ClientOp *op;

  DEVD_ENTRY;

  g_assert (JSONRPC_IS_SERVER (server));
  g_assert (JSONRPC_IS_CLIENT (client));
  g_assert (method != NULL);
  g_assert (id != NULL);
  g_assert (DEVD_IS_NETWORK_LISTENER (user_data));

  op = client_op_new (client, id);

  if (params == NULL)
    {
      g_autoptr(GError) error = NULL;

      error = g_error_new (G_IO_ERROR,
                           G_IO_ERROR_FAILED,
                           "No parameters passed to run-app");
      client_reply_error (op, error);
      client_op_free (op);
      return;
    }

  JSONRPC_MESSAGE_PARSE (params, "provider", JSONRPC_MESSAGE_GET_STRING (&provider));
  JSONRPC_MESSAGE_PARSE (params, "app-id", JSONRPC_MESSAGE_GET_STRING (&app_id));
  JSONRPC_MESSAGE_PARSE (params, "pty", JSONRPC_MESSAGE_GET_STRING (&pty_id));

  devd_application_run_app_async (DEVD_APPLICATION_DEFAULT,
                                  provider,
                                  app_id,
                                  pty_id,
                                  op->cancellable,
                                  handle_run_app_cb,
                                  op);

  DEVD_EXIT;
}

static void
handle_syncfs_cb (GObject      *object,
                  GAsyncResult *result,
                  gpointer      user_data)
{
  DevdApplication *app = (DevdApplication *)object;
  g_autoptr(ClientOp) op = user_data;
  g_autoptr(GError) error = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_APPLICATION (app));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (op != NULL);

  if (!devd_application_syncfs_finish (app, result, &error))
    client_reply_error (op, error);
  else
    jsonrpc_client_reply_async (op->client,
                                op->id,
                                g_variant_new_boolean (TRUE),
                                op->cancellable,
                                log_reply_cb,
                                NULL);

  DEVD_EXIT;
}

static void
handle_syncfs (JsonrpcServer *server,
               JsonrpcClient *client,
               const gchar   *method,
               GVariant      *id,
               GVariant      *params,
               gpointer       user_data)
{
  const gchar *devices = NULL;
  ClientOp *op;

  DEVD_ENTRY;

  g_assert (JSONRPC_IS_SERVER (server));
  g_assert (JSONRPC_IS_CLIENT (client));
  g_assert (method != NULL);
  g_assert (id != NULL);
  g_assert (DEVD_IS_NETWORK_LISTENER (user_data));

  op = client_op_new (client, id);

  JSONRPC_MESSAGE_PARSE (params, "devices", JSONRPC_MESSAGE_GET_STRING (&devices));

  devd_application_syncfs_async (DEVD_APPLICATION_DEFAULT,
                                 devices,
                                 op->cancellable,
                                 handle_syncfs_cb,
                                 op);

  DEVD_EXIT;
}

static struct {
  const gchar          *method;
  JsonrpcServerHandler  handler;
} handlers[] = {
  { "initialize",      handle_initialize },
  { "keep-alive",      handle_keep_alive },
  { "list-apps",       handle_list_apps },
  { "list-files",      handle_list_files },
  { "list-runtimes",   handle_list_apps },
  { "run-app",         handle_run_app },
  { "syncfs",          handle_syncfs },
};

static void
devd_network_listener_call_cb (GObject      *object,
                               GAsyncResult *result,
                               gpointer      user_data)
{
  DevdService *service = (DevdService *)object;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(ClientOp) op = user_data;
  g_autoptr(GError) error = NULL;

  g_assert (DEVD_IS_SERVICE (service));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (op != NULL);

  if (!devd_service_call_finish (service, result, &reply, &error))
    {
      client_reply_error (op, error);
      return;
    }

  if (reply != NULL && g_variant_is_floating (reply))
    g_variant_take_ref (reply);

  jsonrpc_client_reply_async (op->client,
                              op->id,
                              reply,
                              op->cancellable,
                              log_reply_cb,
                              NULL);
}

static gboolean
devd_network_listener_handle_call (JsonrpcServer       *server,
                                   JsonrpcClient       *client,
                                   const gchar         *method,
                                   GVariant            *id,
                                   GVariant            *params,
                                   DevdNetworkListener *self)
{
  g_autofree gchar *service_name = NULL;
  const gchar *dot;

  g_assert (JSONRPC_IS_SERVER (server));
  g_assert (JSONRPC_IS_CLIENT (client));
  g_assert (method != NULL);
  g_assert (id != NULL);
  g_assert (DEVD_IS_NETWORK_LISTENER (self));

  if ((dot = strrchr (method, '.')))
    {
      DevdService *service;
      ClientOp *op;

      service_name = g_strndup (method, dot - method);
      service = devd_application_get_service (DEVD_APPLICATION_DEFAULT, service_name);
      method = dot + 1;

      if (service == NULL)
        return FALSE;

      op = client_op_new (client, id);
      devd_service_call_async (service,
                               method,
                               params,
                               op->cancellable,
                               devd_network_listener_call_cb,
                               op);
      return TRUE;
    }

  for (guint i = 0; i < G_N_ELEMENTS (handlers); i++)
    {
      if (g_strcmp0 (handlers[i].method, method) == 0)
        {
          handlers[i].handler (server, client, method, id, params, self);
          return TRUE;
        }
    }

  return FALSE;
}

static void
devd_network_listener_publish (DevdNetworkListener *self,
                               GSocket             *socket)
{
  g_autoptr(GSocketAddress) address = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *service_name = NULL;
  g_autofree gchar *kind_str = NULL;
  GaEntryGroupService *service;
  const gchar *machine_id;
  guint16 port;

  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_LISTENER (self));
  g_assert (G_IS_SOCKET (socket));

  g_debug ("publishing network listener");

  /* Get the address for our side of the socket */
  if (!(address = g_socket_get_local_address (socket, &error)))
    {
      g_warning ("Failed to advertise, could not get local address: %s", error->message);
      DEVD_EXIT;
    }

  /* We require a GInetSocketAddress to broadcast */
  if (!G_IS_INET_SOCKET_ADDRESS (address))
    {
      g_debug ("%s is not an GInetSocketAddress, cannot publish",
               G_OBJECT_TYPE_NAME (address));
      DEVD_EXIT;
    }

  /* We also need a port number to advertise on mDNS */
  if (!(port = g_inet_socket_address_get_port (G_INET_SOCKET_ADDRESS (address))))
    {
      g_warning ("Failed to get port number for socket address");
      DEVD_EXIT;
    }

  /* Setup our AvahiClient if necessary */
  if (self->avahi_client == NULL)
    {
      self->avahi_client = ga_client_new (GA_CLIENT_FLAG_NO_FLAGS);

      if (!ga_client_start (self->avahi_client, &error))
        {
          g_clear_object (&self->avahi_client);
          g_warning ("Failed to connect to Avahi daemon: %s", error->message);
          DEVD_EXIT;
        }

      g_debug ("avahi client listening");
    }

  g_assert (self->avahi_client != NULL);
  g_assert (IS_GA_CLIENT (self->avahi_client));

  /* Setup our mDNS entry group if necessary */
  if (self->avahi_entry_group == NULL)
    {
      self->avahi_entry_group = ga_entry_group_new ();

      if (!ga_entry_group_attach (self->avahi_entry_group, self->avahi_client, &error))
        {
          g_clear_object (&self->avahi_entry_group);
          g_warning ("Failed to create avahi entry group: %s", error->message);
          DEVD_EXIT;
        }

      g_debug ("attached to avahi entry group");
    }

  /* Publish this socket info to the entry group. It would be nice if we
   * could limit to the interface the socket address is listening on, but
   * to do that on Linux we need to setup a Netlink interface to track the
   * index changes of the routing table entries. Not fun (nor even possible
   * without priveliges, likely).
   */

  service_name = g_strdup_printf (_("%s's %s (%s)"),
                                  devd_get_device_owner (),
                                  devd_get_device_name (),
                                  g_get_host_name ());

  g_debug ("publishing service %s", service_name);

  if (!(service = ga_entry_group_add_service (self->avahi_entry_group,
                                              service_name,
                                              "_deviced._tcp",
                                              port,
                                              &error,
                                              NULL)))
    {
      g_warning ("Failed to add service to Avahi entry group: %s", error->message);
      DEVD_EXIT;
    }

  machine_id = devd_get_machine_id ();
  kind_str = devd_get_device_kind ();

  ga_entry_group_service_freeze (service);
  if (!ga_entry_group_service_set (service, "machine-id", machine_id, &error) ||
      !ga_entry_group_service_set (service, "device-kind", kind_str, &error) ||
      !ga_entry_group_service_thaw (service, &error))
    {
      g_warning ("Failed to register TXT info: %s", error->message);
      DEVD_EXIT;
    }

  g_debug ("registered machine-id=%s device-kind=%s", machine_id, kind_str);

  if (!ga_entry_group_commit (self->avahi_entry_group, &error))
    {
      g_warning ("Failed to commit Avahi entry group: %s", error->message);
      DEVD_EXIT;
    }

  g_debug ("avahi entry committed");

  DEVD_EXIT;
}

static void
devd_network_listener_event (GSocketListener      *listener,
                             GSocketListenerEvent  event,
                             GSocket              *socket)
{
  DevdNetworkListener *self = (DevdNetworkListener *)listener;

  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_LISTENER (self));
  g_assert (G_IS_SOCKET (socket));

  /*
   * Once we've reached listening state, we can advertise about our service
   * on the local network using mDNS (Avahi).
   */
  if (event == G_SOCKET_LISTENER_LISTENING)
    devd_network_listener_publish (self, socket);

  DEVD_EXIT;
}

static gboolean
devd_network_listener_accept_certificate_cb (DevdNetworkListener  *self,
                                             GTlsCertificate      *certificate,
                                             GTlsCertificateFlags  flags,
                                             GTlsConnection       *connection)
{
  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_LISTENER (self));
  g_assert (G_IS_TLS_CERTIFICATE (certificate));
  g_assert (G_IS_TLS_CONNECTION (connection));

  /*
   * What type of filtering do we want to do here on the server
   * based on the client certificate?
   */

  /* TODO: Use peer information to query user vis GNotification to
   *       allow the connection? Save the peer certificate to avoid
   *       that in the future?
   *
   *       The client sets a GTlsCertificate as well, so we can save some
   *       details about it for follow up connections to avoid re(auth).
   */

  DEVD_RETURN (TRUE);
}

static void
devd_network_listener_tls_handshake_cb (GObject      *source,
                                        GAsyncResult *result,
                                        gpointer      user_data)
{
  g_autoptr(DevdNetworkListener) self = DEVD_NETWORK_LISTENER (user_data);
  GTlsConnection *conn = G_TLS_CONNECTION (source);
  g_autoptr(GError) error = NULL;
  g_autofree char *cert_hash = NULL;
  g_autofree char *our_cert_hash = NULL;
  g_auto(GStrv) trusted_certs = NULL;

  DEVD_ENTRY;

  if (!g_tls_connection_handshake_finish (conn, result, &error))
    {
      g_warning ("%s", error->message);
      g_clear_object (&self->awaiting_connection);
      DEVD_EXIT;
    }

  our_cert_hash = devd_tls_certificate_get_hash (self->certificate);
  cert_hash = devd_tls_certificate_get_hash (g_tls_connection_get_peer_certificate (conn));
  trusted_certs = g_settings_get_strv (self->settings, "trusted-clients");

  if (g_strcmp0 (cert_hash, our_cert_hash) == 0 ||
      g_strv_contains ((const char * const *)trusted_certs, cert_hash))
    {
      g_autoptr(GIOStream) awaiting = g_steal_pointer (&self->awaiting_connection);

      g_info ("Already trusted cert %s", cert_hash);

      jsonrpc_server_accept_io_stream (self->server, awaiting);
    }
  else
    {
      g_autoptr(GNotification) notification = NULL;

      notification = g_notification_new (_("Accept device connection?"));
      g_notification_set_body (notification, cert_hash);
      g_notification_set_default_action (notification, "app.decline-device");
      g_notification_add_button (notification, _("Accept"), "app.accept-device");
      g_notification_add_button (notification, _("Decline"), "app.decline-device");

      g_application_send_notification (g_application_get_default (),
                                       "accept-request", notification);
    }

  DEVD_EXIT;
}

static gboolean
devd_network_listener_incoming (GSocketService    *service,
                                GSocketConnection *connection,
                                GObject           *source_object)
{
  DevdNetworkListener *self = (DevdNetworkListener *)service;
  g_autoptr(GIOStream) stream = NULL;
  g_autoptr(GError) error = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_LISTENER (self));
  g_assert (G_IS_SOCKET_CONNECTION (connection));
  g_assert (source_object == NULL);
  g_assert (G_IS_TLS_CERTIFICATE (self->certificate));

  stream = g_tls_server_connection_new (G_IO_STREAM (connection),
                                        self->certificate,
                                        &error);

  if (stream == NULL)
    {
      g_warning ("Failed to create TLS server connection: %s", error->message);
      DEVD_GOTO (failure);
    }

  g_object_set (stream,
                "authentication-mode", G_TLS_AUTHENTICATION_REQUIRED,
                NULL);

  g_signal_connect_object (stream,
                           "accept-certificate",
                           G_CALLBACK (devd_network_listener_accept_certificate_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_tls_connection_handshake_async (G_TLS_CONNECTION(stream),
                                    G_PRIORITY_DEFAULT,
                                    self->cancellable,
                                    devd_network_listener_tls_handshake_cb,
                                    g_object_ref (self));

  if (self->awaiting_connection != NULL)
    {
      g_clear_object (&self->awaiting_connection);
      g_application_withdraw_notification (g_application_get_default (), "accept-request");
    }

  self->awaiting_connection = g_object_ref (stream);

failure:
  DEVD_RETURN (TRUE);
}


static void
devd_network_listener_dispose (GObject *object)
{
  DevdNetworkListener *self = (DevdNetworkListener *)object;

  DEVD_ENTRY;

  /* Remove our client request handlers */
  for (guint i = self->handlers->len; i > 0; i--)
    {
      guint handler_id = g_array_index (self->handlers, guint, i - 1);
      jsonrpc_server_remove_handler (self->server, handler_id);
      g_array_remove_index (self->handlers, i - 1);
    }

  /* Lose our AvahiClient connection */
  g_clear_object (&self->avahi_entry_group);
  g_clear_object (&self->avahi_client);

  /* Cancel anything inflight */
  g_cancellable_cancel (self->cancellable);

  G_OBJECT_CLASS (devd_network_listener_parent_class)->dispose (object);

  DEVD_EXIT;
}

static void
devd_network_listener_accept_device (GSimpleAction *action,
                                     GVariant      *param,
                                     gpointer       user_data)
{
  DevdNetworkListener *self = user_data;
  g_autoptr(GIOStream) awaiting = NULL;
  g_auto(GStrv) trusted_certs = NULL;
  g_autofree gchar *cert_hash = NULL;
  GTlsCertificate *certificate;

  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_LISTENER (self));
  g_assert (JSONRPC_IS_SERVER (self->server));

  awaiting = g_steal_pointer (&self->awaiting_connection);

  if (awaiting != NULL)
    jsonrpc_server_accept_io_stream (self->server, awaiting);

  certificate = g_tls_connection_get_peer_certificate (G_TLS_CONNECTION(awaiting));
  cert_hash = devd_tls_certificate_get_hash (certificate);
  trusted_certs = g_settings_get_strv (self->settings, "trusted-clients");
  trusted_certs = devd_strv_append (trusted_certs, cert_hash);
  g_settings_set_strv (self->settings, "trusted-clients", (const char * const *)trusted_certs);

  g_info ("Added certificate to trusted list: %s", cert_hash);

  DEVD_EXIT;
}

static void
devd_network_listener_decline_device (GSimpleAction *action,
                                      GVariant      *param,
                                      gpointer       data)
{
  DevdNetworkListener *self = data;

  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_LISTENER (self));

  g_clear_object (&self->awaiting_connection);

  DEVD_EXIT;
}

static void
foreach_client_cb (JsonrpcClient *client,
                   const gchar   *address_string)
{
  g_autofree gchar *client_address = NULL;

  g_assert (JSONRPC_IS_CLIENT (client));
  g_assert (address_string != NULL);

  client_address = g_strdup_printf ("%p", client);

  if (g_strcmp0 (client_address, address_string) == 0)
    {
      jsonrpc_client_close_async (client, NULL, NULL, NULL);
      g_info ("Closing client connection");
    }
}

static void
devd_network_listener_disconnect_device (GSimpleAction *action,
                                         GVariant      *param,
                                         gpointer       data)
{
  DevdNetworkListener *self = data;

  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_LISTENER (self));

  jsonrpc_server_foreach (self->server,
                          (GFunc)foreach_client_cb,
                          (gchar *)g_variant_get_string (param, NULL));

  DEVD_EXIT;
}


static void
devd_network_listener_client_destroyed (gpointer  data,
                                        GObject  *object)
{
  GApplication *app = g_application_get_default ();
  g_autofree gchar *notification_id = NULL;

  DEVD_ENTRY;

  g_assert (app != NULL);

  notification_id = g_strdup_printf ("connected-client-%p", object);

  g_application_withdraw_notification (app, notification_id);

  DEVD_EXIT;
}

static void
devd_network_listener_client_accepted (JsonrpcServer *server,
                                       JsonrpcClient *client,
                                       gpointer       user_data)
{
  GApplication *app = g_application_get_default ();
  g_autofree gchar *notification_id = NULL;
  g_autofree gchar *notification_action = NULL;
  g_autoptr(GNotification) notification = NULL;

  DEVD_ENTRY;

  g_assert (app != NULL);

  notification_action = g_strdup_printf ("app.disconnect-client('%p')", client);

  /* TODO: Set a human readable name for clients */
  notification = g_notification_new (_("Debugging client connected"));
  g_notification_set_body (notification, _("Click to disconnect client"));
  g_notification_set_default_action (notification, notification_action);

  notification_id = g_strdup_printf ("connected-client-%p", client);
  g_application_send_notification (app, notification_id, notification);

  g_object_weak_ref (G_OBJECT (client), devd_network_listener_client_destroyed, NULL);

  DEVD_EXIT;
}

static GActionEntry actions[] = {
  { .name = "accept-device",
    .activate = devd_network_listener_accept_device },
  { .name = "decline-device",
    .activate = devd_network_listener_decline_device },
  { .name = "disconnect-client",
    .parameter_type = "s",
    .activate = devd_network_listener_disconnect_device },
};

static void
devd_network_listener_finalize (GObject *object)
{
  DevdNetworkListener *self = (DevdNetworkListener *)object;

  DEVD_ENTRY;

  g_clear_object (&self->cancellable);
  g_clear_object (&self->server);
  g_clear_pointer (&self->handlers, g_array_unref);
  g_clear_object (&self->awaiting_connection);
  g_clear_object (&self->settings);

  G_OBJECT_CLASS (devd_network_listener_parent_class)->finalize (object);

  DEVD_EXIT;
}

static void
devd_network_listener_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  DevdNetworkListener *self = DEVD_NETWORK_LISTENER (object);

  switch (prop_id)
    {
    case PROP_CERTIFICATE:
      g_value_set_object (value, devd_network_listener_get_certificate (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
devd_network_listener_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  DevdNetworkListener *self = DEVD_NETWORK_LISTENER (object);

  switch (prop_id)
    {
    case PROP_CERTIFICATE:
      devd_network_listener_set_certificate (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
devd_network_listener_class_init (DevdNetworkListenerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GSocketListenerClass *listener_class = G_SOCKET_LISTENER_CLASS (klass);
  GSocketServiceClass *socket_service_class = G_SOCKET_SERVICE_CLASS (klass);

  object_class->dispose = devd_network_listener_dispose;
  object_class->finalize = devd_network_listener_finalize;
  object_class->get_property = devd_network_listener_get_property;
  object_class->set_property = devd_network_listener_set_property;

  listener_class->event = devd_network_listener_event;

  socket_service_class->incoming = devd_network_listener_incoming;

  /**
   * DevdNetworkListener:certificate:
   *
   * The "certificate" property is the #GTlsCertificate to use for
   * the server-side of a TLS connection. Peers will validate (or not)
   * this certificate when connecting to us.
   */
  properties [PROP_CERTIFICATE] =
    g_param_spec_object ("certificate",
                         "Certificate",
                         "The GTlsCertificate to use for the server-side of TLS",
                         G_TYPE_TLS_CERTIFICATE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
devd_network_listener_init (DevdNetworkListener *self)
{
  DevdApplication *app;

  DEVD_ENTRY;

  self->cancellable = g_cancellable_new ();
  self->server = jsonrpc_server_new ();
  self->handlers = g_array_new (FALSE, FALSE, sizeof (guint));
  self->settings = g_settings_new ("org.gnome.deviced");

  g_signal_connect_object (self->server,
                           "client-accepted",
                           G_CALLBACK (devd_network_listener_client_accepted),
                           self,
                           0);

  g_signal_connect_object (self->server,
                           "handle-call",
                           G_CALLBACK (devd_network_listener_handle_call),
                           self,
                           0);

  /* Bit out of place but all of the actions apply here even if app wide. */
  app = DEVD_APPLICATION_DEFAULT;
  if (!g_action_map_lookup_action (G_ACTION_MAP (app), "accept-device"))
    g_action_map_add_action_entries (G_ACTION_MAP (app), actions, G_N_ELEMENTS(actions), self);

  DEVD_EXIT;
}

/**
 * devd_network_listener_get_certificate:
 * @self: a #DevdNetworkListener
 *
 * Gets the #GTlsCertificate that is used for the server side of
 * connections with peers.
 *
 * Returns: (transfer none): a #GTlsCertificate or %NULL
 */
GTlsCertificate *
devd_network_listener_get_certificate (DevdNetworkListener *self)
{
  g_return_val_if_fail (DEVD_IS_NETWORK_LISTENER (self), NULL);

  return self->certificate;
}

/**
 * devd_network_listener_set_certificate:
 * @self: a #DevdNetworkListener
 * @certificate: (nullable): a #GTlsCertificate or %NULL
 *
 * Sets the #GTlsCertificate to use for the server side of connections.
 * This will only affect new incoming connections.
 *
 * If @certificate is %NULL, then no encryption will be performed
 * so the connection will be in cleartext. That is generally only
 * useful when troubleshooting the deviced program or clients.
 */
void
devd_network_listener_set_certificate (DevdNetworkListener *self,
                                       GTlsCertificate     *certificate)
{
  g_return_if_fail (DEVD_IS_NETWORK_LISTENER (self));
  g_return_if_fail (!certificate || G_IS_TLS_CERTIFICATE (certificate));

  if (g_set_object (&self->certificate, certificate))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CERTIFICATE]);
}

DevdNetworkListener *
devd_network_listener_new (NMActiveConnection  *connection,
                           GTlsCertificate     *certificate,
                           GCancellable        *cancellable,
                           GError             **error)
{
  DevdNetworkListener *self;
  NMIPConfig *configs[2];

  DEVD_ENTRY;

  g_return_val_if_fail (NM_IS_ACTIVE_CONNECTION (connection), NULL);
  g_return_val_if_fail (G_IS_TLS_CERTIFICATE (certificate), NULL);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), NULL);

  self = g_object_new (DEVD_TYPE_NETWORK_LISTENER,
                       "certificate", certificate,
                       NULL);

  configs[0] = nm_active_connection_get_ip4_config (connection);
  configs[1] = nm_active_connection_get_ip6_config (connection);

  for (guint i = 0; i < G_N_ELEMENTS (configs); i++)
    {
      GPtrArray *addrs;

      if (configs[i] == NULL)
        continue;

      if (!(addrs = nm_ip_config_get_addresses (configs[i])))
        continue;

      for (guint j = 0; j < addrs->len; j++)
        {
          NMIPAddress *addr = g_ptr_array_index (addrs, j);
          const gchar *str = nm_ip_address_get_address (addr);
          g_autoptr(GInetAddress) iaddr = g_inet_address_new_from_string (str);
          g_autoptr(GSocketAddress) saddr = g_inet_socket_address_new (iaddr, 0);
          g_autoptr(GSocketAddress) effective = NULL;

          if (g_socket_listener_add_address (G_SOCKET_LISTENER (self),
                                             saddr,
                                             G_SOCKET_TYPE_STREAM,
                                             G_SOCKET_PROTOCOL_TCP,
                                             NULL,
                                             &effective,
                                             NULL))
            break;
        }
    }

  DEVD_RETURN (self);
}
