/* devd-app-provider.h
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

#include "devd-app-info.h"
#include "devd-pty.h"

G_BEGIN_DECLS

#define DEVD_TYPE_APP_PROVIDER (devd_app_provider_get_type())

G_DECLARE_INTERFACE (DevdAppProvider, devd_app_provider, DEVD, APP_PROVIDER, GObject)

struct _DevdAppProviderInterface
{
  GTypeInterface parent_interface;

  void       (*list_apps_async)  (DevdAppProvider      *self,
                                  gboolean              list_runtimes,
                                  GCancellable         *cancellable,
                                  GAsyncReadyCallback   callback,
                                  gpointer              user_data);
  GPtrArray *(*list_apps_finish) (DevdAppProvider      *self,
                                  GAsyncResult         *result,
                                  GError              **error);
  void       (*run_app_async)    (DevdAppProvider      *self,
                                  const gchar          *app_id,
                                  DevdPty              *pty,
                                  GCancellable         *cancellable,
                                  GAsyncReadyCallback   callback,
                                  gpointer              user_data);
  gchar     *(*run_app_finish)   (DevdAppProvider      *self,
                                  GAsyncResult         *result,
                                  GError              **error);
};

void       devd_app_provider_list_apps_async  (DevdAppProvider      *self,
                                               gboolean              list_runtimes,
                                               GCancellable         *cancellable,
                                               GAsyncReadyCallback   callback,
                                               gpointer              user_data);
GPtrArray *devd_app_provider_list_apps_finish (DevdAppProvider      *self,
                                               GAsyncResult         *result,
                                               GError              **error);
void       devd_app_provider_run_app_async    (DevdAppProvider      *self,
                                               const gchar          *app_id,
                                               DevdPty              *pty,
                                               GCancellable         *cancellable,
                                               GAsyncReadyCallback   callback,
                                               gpointer              user_data);
gchar     *devd_app_provider_run_app_finish   (DevdAppProvider      *self,
                                               GAsyncResult         *result,
                                               GError              **error);

G_END_DECLS
