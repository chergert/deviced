/* devd-process-service.h
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "devd-pty.h"
#include "devd-service.h"

G_BEGIN_DECLS

#define DEVD_PROCESS_SERVICE_NAME "org.gnome.deviced.process"
#define DEVD_TYPE_PROCESS_SERVICE (devd_process_service_get_type())

G_DECLARE_FINAL_TYPE (DevdProcessService, devd_process_service, DEVD, PROCESS_SERVICE, DevdService)

DevdProcessService *devd_process_service_new                (void);
void                devd_process_service_force_exit         (DevdProcessService   *self,
                                                             const gchar          *identifier);
void                devd_process_service_send_signal        (DevdProcessService   *self,
                                                             const gchar          *identifier,
                                                             gint                  signum);
gchar              *devd_process_service_create_pty         (DevdProcessService   *self,
                                                             GError              **error);
gboolean            devd_process_service_destroy_pty        (DevdProcessService   *self,
                                                             const gchar          *pty_id,
                                                             GError              **error);
DevdPty            *devd_process_service_get_pty_by_id      (DevdProcessService   *self,
                                                             const gchar          *pty_id);
void                devd_process_service_monitor_subprocess (DevdProcessService   *self,
                                                             GSubprocess          *subprocess);
void                devd_process_service_spawn_async        (DevdProcessService   *self,
                                                             const gchar          *pty,
                                                             const gchar * const  *argv,
                                                             const gchar * const  *env,
                                                             GCancellable         *cancellable,
                                                             GAsyncReadyCallback   callback,
                                                             gpointer              user_data);
gchar              *devd_process_service_spawn_finish       (DevdProcessService   *self,
                                                             GAsyncResult         *result,
                                                             GError              **error);

G_END_DECLS
