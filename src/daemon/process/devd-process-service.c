/* devd-process-service.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "devd-process-service"

#include <errno.h>
#include <jsonrpc-glib.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "devd-application.h"
#include "devd-macros.h"
#include "devd-trace.h"
#include "devd-process-service.h"

struct _DevdProcessService
{
  DevdService parent_instance;

  /*
   * A hashtable of known pids so that we can limit the client to only sending
   * force-exit/send-signal requests to these processes.
   */
  GHashTable *known_identifiers;

  /*
   * A hashtable of DevdPty instances that were created upon request of the
   * client. The key for the hashtable is the uuid that was generated to
   * represent the PTY instance.
   */
  GHashTable *pty_by_uuid;
};

G_DEFINE_TYPE (DevdProcessService, devd_process_service, DEVD_TYPE_SERVICE)

static void
devd_process_service_monitor_wait_cb (GObject      *object,
                                      GAsyncResult *result,
                                      gpointer      user_data)
{
  GSubprocess *subprocess = (GSubprocess *)object;
  g_autoptr(DevdProcessService) self = user_data;
  g_autoptr(GVariant) params = NULL;
  g_autoptr(GError) error = NULL;
  const gchar *identifier;

  DEVD_ENTRY;

  g_assert (G_IS_SUBPROCESS (subprocess));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (DEVD_IS_PROCESS_SERVICE (self));

  /* We need to use our own copy of identifier, since we may have lost
   * the value GSubprocess keeps when the process exited.
   */
  identifier = g_object_get_data (G_OBJECT (subprocess), "IDENTIFIER");
  g_assert (identifier != NULL);

  g_hash_table_remove (self->known_identifiers, identifier);

  if (!g_subprocess_wait_finish (subprocess, result, &error))
    g_warning ("%s", error->message);

  if (g_subprocess_get_if_exited (subprocess))
    {
      gint exit_status = g_subprocess_get_exit_status (subprocess);

      params = JSONRPC_MESSAGE_NEW (
        "identifier", JSONRPC_MESSAGE_PUT_STRING (identifier),
        "exit-status", JSONRPC_MESSAGE_PUT_INT32 (exit_status)
      );

      devd_service_emit_notification (DEVD_SERVICE (self),
                                      DEVD_PROCESS_SERVICE_NAME".process-exited",
                                      params);
    }
  else if (g_subprocess_get_if_signaled (subprocess))
    {
      gint term_sig = g_subprocess_get_term_sig (subprocess);

      params = JSONRPC_MESSAGE_NEW (
        "identifier", JSONRPC_MESSAGE_PUT_STRING (identifier),
        "term-sig", JSONRPC_MESSAGE_PUT_INT32 (term_sig)
      );

      devd_service_emit_notification (DEVD_SERVICE (self),
                                      DEVD_PROCESS_SERVICE_NAME".process-signaled",
                                      params);
    }

  DEVD_EXIT;
}

void
devd_process_service_monitor_subprocess (DevdProcessService *self,
                                         GSubprocess        *subprocess)
{
  const gchar *identifier;

  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_PROCESS_SERVICE (self));
  g_return_if_fail (G_IS_SUBPROCESS (subprocess));

  /*
   * We'll lose the "identifier" after the process exits, so we need to keep a
   * copy of it so that we can notify the peer.
   */

  identifier = g_subprocess_get_identifier (subprocess);
  g_object_set_data_full (G_OBJECT (subprocess), "IDENTIFIER", g_strdup (identifier), g_free);

  g_hash_table_insert (self->known_identifiers,
                       g_strdup (identifier),
                       g_object_ref (subprocess));

  g_subprocess_wait_async (subprocess,
                           NULL,
                           devd_process_service_monitor_wait_cb,
                           g_object_ref (self));

  DEVD_EXIT;
}

/**
 * devd_process_service_force_exit:
 * @self: a #DevdProcessService
 * @identifier: the identifier for the process
 *
 * Forces the process matching @identifier to exit.
 *
 * @identifier must be a process that was previously spawned by the daemon.
 *
 * Since: 3.28
 */
void
devd_process_service_force_exit (DevdProcessService *self,
                                 const gchar        *identifier)
{
  GSubprocess *subprocess;

  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_PROCESS_SERVICE (self));
  g_return_if_fail (identifier != NULL);

  /*
   * Instead of using g_subprocess_force_exit(), we want to send a signal
   * to the progress group to help ensure that everything dies. Otherwise
   * we might only kill a parent process and not the children. It's still
   * not guaranteed that all children will die with the parent, but it
   * improves our chances.
   */

  if ((subprocess = g_hash_table_lookup (self->known_identifiers, identifier)))
    {
      GPid pid = atoi (identifier);
      kill (-pid, SIGKILL);
    }

  DEVD_EXIT;
}

/**
 * devd_process_service_send_signal:
 * @self: a #DevdProcessService
 * @identifier: the identifier for the process
 * @signum: the signal number
 *
 * Sends signal @signum to the process known by @identifier.
 *
 * @identifier must be a process that was previously spawned by the daemon.
 *
 * Since: 3.28
 */
void
devd_process_service_send_signal (DevdProcessService *self,
                                  const gchar        *identifier,
                                  gint                signum)
{
  GSubprocess *subprocess;

  g_return_if_fail (DEVD_IS_PROCESS_SERVICE (self));
  g_return_if_fail (identifier != NULL);

  if ((subprocess = g_hash_table_lookup (self->known_identifiers, identifier)))
    g_subprocess_send_signal (subprocess, signum);
}

gchar *
devd_process_service_create_pty (DevdProcessService  *self,
                                 GError             **error)
{
  g_autofree gchar *uuid = NULL;
  g_autoptr(DevdPty) pty = NULL;
  gint fd;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_PROCESS_SERVICE (self), NULL);

  uuid = g_uuid_string_random ();
  pty = devd_pty_new ();
  fd = devd_pty_get_fd (pty);

  if (fd == -1)
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_FAILED,
                   "Failed to create new PTY device");
      DEVD_RETURN (NULL);
    }

  g_hash_table_insert (self->pty_by_uuid, g_strdup (uuid), g_steal_pointer (&pty));

  DEVD_RETURN (g_steal_pointer (&uuid));
}

gboolean
devd_process_service_destroy_pty (DevdProcessService  *self,
                                  const gchar         *pty_id,
                                  GError             **error)
{
  DevdPty *pty;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_PROCESS_SERVICE (self), FALSE);

  if (pty_id == NULL || !(pty = g_hash_table_lookup (self->pty_by_uuid, pty_id)))
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_FAILED,
                   "No such pty device");
      DEVD_RETURN (FALSE);
    }

  devd_pty_close (pty);
  g_hash_table_remove (self->pty_by_uuid, pty_id);

  DEVD_RETURN (TRUE);
}

DevdPty *
devd_process_service_get_pty_by_id (DevdProcessService *self,
                                    const gchar        *pty_id)
{
  g_return_val_if_fail (DEVD_IS_PROCESS_SERVICE (self), NULL);
  g_return_val_if_fail (pty_id != NULL, NULL);

  return g_hash_table_lookup (self->pty_by_uuid, pty_id);
}


static void
devd_process_service_spawn_child_setup (gpointer user_data)
{
  setsid ();
  setpgid (0, 0);

  if (isatty (STDIN_FILENO))
    {
      if (ioctl (STDIN_FILENO, TIOCSCTTY, 0) != 0)
        g_warning ("Failed to setup TIOCSCTTY on stdin: %s",
                   g_strerror (errno));
    }
}

void
devd_process_service_spawn_async (DevdProcessService  *self,
                                  const gchar         *pty,
                                  const gchar * const *argv,
                                  const gchar * const *env,
                                  GCancellable        *cancellable,
                                  GAsyncReadyCallback  callback,
                                  gpointer             user_data)
{
  static const gchar *default_argv[] = { "/bin/bash", NULL };
  g_autoptr(GSubprocessLauncher) launcher = NULL;
  g_autoptr(GSubprocess) subprocess = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = NULL;
  g_autofree gchar *identifier = NULL;
  DevdPty *pty_obj;
  gint pty_fd;

  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_PROCESS_SERVICE (self));
  g_return_if_fail (pty != NULL);
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  if (argv == NULL || argv[0] == NULL)
    argv = default_argv;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_process_service_spawn_async);

  if (!(pty_obj = g_hash_table_lookup (self->pty_by_uuid, pty)))
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_NOT_FOUND,
                               "No such PTY %s", pty);
      DEVD_EXIT;
    }

  if ((pty_fd = devd_pty_create_slave (pty_obj, TRUE, TRUE, &error)) == -1)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  launcher = g_subprocess_launcher_new (0);

  if (env != NULL && env[0] != NULL)
    g_subprocess_launcher_set_environ (launcher, (gchar **)env);

  g_subprocess_launcher_take_stdin_fd (launcher, dup (pty_fd));
  g_subprocess_launcher_take_stderr_fd (launcher, dup (pty_fd));
  g_subprocess_launcher_take_stdout_fd (launcher, pty_fd);

  g_subprocess_launcher_set_child_setup (launcher,
                                         devd_process_service_spawn_child_setup,
                                         NULL, NULL);

  if (!(subprocess = g_subprocess_launcher_spawnv (launcher, argv, &error)))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  identifier = g_strdup (g_subprocess_get_identifier (subprocess));
  devd_process_service_monitor_subprocess (self, subprocess);
  g_task_return_pointer (task, g_steal_pointer (&identifier), g_free);

  DEVD_EXIT;
}

gchar *
devd_process_service_spawn_finish (DevdProcessService  *self,
                                   GAsyncResult        *result,
                                   GError             **error)
{
  gchar *ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_PROCESS_SERVICE (self), NULL);
  g_return_val_if_fail (G_IS_TASK (result), NULL);

  ret = g_task_propagate_pointer (G_TASK (result), error);

  DEVD_RETURN (ret);
}

static void
on_pty_handle_data (DevdPty            *pty,
                    GBytes             *bytes,
                    DevdProcessService *self)
{
  g_autoptr(GVariant) params = NULL;
  g_autofree gchar *encoded = NULL;
  const gchar *pty_id;
  const guint8 *data;
  gsize len;

  g_assert (DEVD_IS_PTY (pty));
  g_assert (bytes != NULL);
  g_assert (DEVD_IS_PROCESS_SERVICE (self));

  pty_id = g_object_get_data (G_OBJECT (pty), "PTY_ID");
  data = g_bytes_get_data (bytes, &len);
  encoded = g_base64_encode (data, len);

  params = JSONRPC_MESSAGE_NEW (
    "pty", JSONRPC_MESSAGE_PUT_STRING (pty_id),
    "data", JSONRPC_MESSAGE_PUT_STRING (encoded)
  );

  devd_service_emit_notification (DEVD_SERVICE (self),
                                  DEVD_PROCESS_SERVICE_NAME".pty-data",
                                  params);
}

static void
on_pty_invalidated (gpointer  data,
                    GClosure *closure)
{
  const gchar *pty_id = data;
  DevdService *self;

  DEVD_ENTRY;

  g_assert (pty_id != NULL);

  if ((self = devd_application_get_service (DEVD_APPLICATION_DEFAULT, DEVD_PROCESS_SERVICE_NAME)))
    devd_process_service_destroy_pty (DEVD_PROCESS_SERVICE (self), pty_id, NULL);

  DEVD_EXIT;
}

static void
handle_create_pty (DevdService         *service,
                   const gchar         *method,
                   GVariant            *params,
                   GCancellable        *cancellable,
                   GAsyncReadyCallback  callback,
                   gpointer             user_data)
{
  DevdProcessService *self = (DevdProcessService *)service;
  g_autofree gchar *pty = NULL;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GTask) task = NULL;
  g_autoptr(GError) error = NULL;
  DevdPty *pty_obj;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (method != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, handle_create_pty);

  if (!(pty = devd_process_service_create_pty (self, &error)))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  /*
   * Connect to the ::handle-data signal of the DevdPty, but ensure that
   * we our closure id destroyed when either object id destroyed (such as
   * the client being finalized after diconnecting). This allows us to also
   * add a closure invalidation notifier that will destroy the PTY when the
   * client has disconnected.
   */
  if ((pty_obj = devd_process_service_get_pty_by_id (self, pty)))
    {
      GClosure *closure;

      g_object_set_data_full (G_OBJECT (pty_obj), "PTY_ID", g_strdup (pty), g_free);
      closure = g_cclosure_new_object (G_CALLBACK (on_pty_handle_data), G_OBJECT (self));
      g_closure_add_invalidate_notifier (closure, g_strdup (pty), on_pty_invalidated);
      g_signal_connect_closure (pty_obj, "handle-data", closure, FALSE);
    }

  reply = JSONRPC_MESSAGE_NEW ("pty", JSONRPC_MESSAGE_PUT_STRING (pty));

  g_task_return_pointer (task,
                         g_steal_pointer (&reply),
                         (GDestroyNotify)g_variant_unref);

  DEVD_EXIT;
}

static void
handle_destroy_pty (DevdService         *service,
                    const gchar         *method,
                    GVariant            *params,
                    GCancellable        *cancellable,
                    GAsyncReadyCallback  callback,
                    gpointer             user_data)
{
  DevdProcessService *self = (DevdProcessService *)service;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = NULL;
  const gchar *pty;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (method != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, handle_destroy_pty);

  if (!params ||
      !JSONRPC_MESSAGE_PARSE (params, "pty", JSONRPC_MESSAGE_GET_STRING (&pty)) ||
      !devd_process_service_destroy_pty (self, pty, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_pointer (task,
                           g_variant_take_ref (g_variant_new_boolean (TRUE)),
                           (GDestroyNotify)g_variant_unref);

  DEVD_EXIT;
}

static void
handle_write_pty (DevdService         *service,
                  const gchar         *method,
                  GVariant            *params,
                  GCancellable        *cancellable,
                  GAsyncReadyCallback  callback,
                  gpointer             user_data)
{
  DevdProcessService *self = (DevdProcessService *)service;
  g_autoptr(GBytes) bytes = NULL;
  g_autoptr(GTask) task = NULL;
  g_autofree guchar *decoded = NULL;
  const gchar *pty;
  const gchar *data;
  DevdPty *pty_obj;
  gsize len = 0;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (method != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, handle_write_pty);

  if (params == NULL)
    {
      g_task_return_new_error (task, G_IO_ERROR, G_IO_ERROR_INVAL, "Invalid parameters");
      DEVD_EXIT;
    }

  if (!JSONRPC_MESSAGE_PARSE (params,
                              "pty", JSONRPC_MESSAGE_GET_STRING (&pty),
                              "data", JSONRPC_MESSAGE_GET_STRING (&data)) ||
      pty == NULL || data == NULL)
    {
      g_task_return_new_error (task, G_IO_ERROR, G_IO_ERROR_INVAL, "Invalid parameters");
      DEVD_EXIT;
    }

  if (!(pty_obj = devd_process_service_get_pty_by_id (self, pty)))
    {
      g_task_return_new_error (task, G_IO_ERROR, G_IO_ERROR_INVAL, "No such pty");
      DEVD_EXIT;
    }

  if (data[0] == 0)
    {
      devd_pty_close (pty_obj);
      DEVD_GOTO (reply);
    }

  if (!(decoded = g_base64_decode (data, &len)))
    {
      g_task_return_new_error (task, G_IO_ERROR, G_IO_ERROR_INVAL, "Failed to deocde base64");
      DEVD_EXIT;
    }

  bytes = g_bytes_new_take (g_steal_pointer (&decoded), len);
  devd_pty_write (pty_obj, bytes);

reply:
  g_task_return_pointer (task,
                         g_variant_take_ref (g_variant_new_boolean (TRUE)),
                         (GDestroyNotify)g_variant_unref);

  DEVD_EXIT;
}

static void
handle_spawn_cb (GObject      *object,
                 GAsyncResult *result,
                 gpointer      user_data)
{
  DevdProcessService *self = (DevdProcessService *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  g_autofree gchar *identifier = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!(identifier = devd_process_service_spawn_finish (self, result, &error)))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_pointer (task,
                           g_variant_take_ref (g_variant_new_string (identifier)),
                           (GDestroyNotify)g_variant_unref);

  DEVD_EXIT;
}

static void
handle_spawn (DevdService         *service,
              const gchar         *method,
              GVariant            *params,
              GCancellable        *cancellable,
              GAsyncReadyCallback  callback,
              gpointer             user_data)
{
  DevdProcessService *self = (DevdProcessService *)service;
  g_autoptr(GVariantIter) aiter = NULL;
  g_autoptr(GVariantIter) eiter = NULL;
  g_autoptr(GPtrArray) argv = NULL;
  g_autoptr(GPtrArray) env = NULL;
  g_autoptr(GTask) task = NULL;
  const gchar *pty = NULL;
  GVariant *v;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (method != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, handle_spawn);

  if (!params ||
      !JSONRPC_MESSAGE_PARSE (params,
                              "pty", JSONRPC_MESSAGE_GET_STRING (&pty),
                              "argv", JSONRPC_MESSAGE_GET_ITER (&aiter),
                              "env", JSONRPC_MESSAGE_GET_ITER (&eiter)))
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_INVAL,
                               "Invalid parameters");
      DEVD_EXIT;
    }

  argv = g_ptr_array_new_with_free_func (g_free);
  while (g_variant_iter_loop (aiter, "v", &v))
    if (g_variant_is_of_type (v, G_VARIANT_TYPE_STRING))
      g_ptr_array_add (argv, g_variant_dup_string (v, NULL));
  g_ptr_array_add (argv, NULL);

  env = g_ptr_array_new ();
  while (g_variant_iter_next (eiter, "v", &v))
    if (g_variant_is_of_type (v, G_VARIANT_TYPE_STRING))
      g_ptr_array_add (env, g_variant_dup_string (v, NULL));
  g_ptr_array_add (env, NULL);

  devd_process_service_spawn_async (self,
                                    pty,
                                    (const gchar * const *)argv->pdata,
                                    (const gchar * const *)env->pdata,
                                    cancellable,
                                    handle_spawn_cb,
                                    g_steal_pointer (&task));

  DEVD_EXIT;
}

static gboolean
handle_force_exit (DevdService   *service,
                   const gchar   *method,
                   GVariant      *params,
                   GCancellable  *cancellable,
                   GVariant     **reply,
                   GError       **error)
{
  DevdProcessService *self = (DevdProcessService *)service;
  const gchar *identifier = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (method != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  JSONRPC_MESSAGE_PARSE (params, "identifier", JSONRPC_MESSAGE_GET_STRING (&identifier));

  if (identifier != NULL)
    devd_process_service_force_exit (self, identifier);

  *reply = g_variant_new_boolean (TRUE);

  DEVD_RETURN (TRUE);
}

static gboolean
handle_send_signal (DevdService   *service,
                    const gchar   *method,
                    GVariant      *params,
                    GCancellable  *cancellable,
                    GVariant     **reply,
                    GError       **error)
{
  DevdProcessService *self = (DevdProcessService *)service;
  const gchar *identifier = NULL;
  gint64 signum = 0;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (method != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));
  g_assert (reply != NULL);

  if (!JSONRPC_MESSAGE_PARSE (params,
                              "identifier", JSONRPC_MESSAGE_GET_STRING (&identifier),
                              "signal", JSONRPC_MESSAGE_GET_INT64 (&signum)) ||
      identifier == NULL || signum == 0)
    {
      g_set_error_literal (error,
                           JSONRPC_CLIENT_ERROR,
                           JSONRPC_CLIENT_ERROR_INVALID_PARAMS,
                           "identifier and signal params required");
      DEVD_RETURN (FALSE);
    }

  devd_process_service_send_signal (self, identifier, signum);

  *reply = g_variant_new_boolean (TRUE);

  DEVD_RETURN (TRUE);
}

static DevdServiceMethods methods[] = {
  { "create-pty",  handle_create_pty },
  { "destroy-pty", handle_destroy_pty },
  { "force-exit",  NULL, handle_force_exit },
  { "send-signal", NULL, handle_send_signal },
  { "spawn",       handle_spawn },
  { "write-pty",   handle_write_pty },
};

static void
devd_process_service_finalize (GObject *object)
{
  DevdProcessService *self = (DevdProcessService *)object;

  DEVD_ENTRY;

  g_clear_pointer (&self->known_identifiers, g_hash_table_unref);
  g_clear_pointer (&self->pty_by_uuid, g_hash_table_unref);

  G_OBJECT_CLASS (devd_process_service_parent_class)->finalize (object);

  DEVD_EXIT;
}

static void
devd_process_service_class_init (DevdProcessServiceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = devd_process_service_finalize;
}

static void
devd_process_service_init (DevdProcessService *self)
{
  DEVD_ENTRY;

  self->known_identifiers = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);
  self->pty_by_uuid = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);

  devd_service_add_methods (DEVD_SERVICE (self), methods, G_N_ELEMENTS (methods));

  DEVD_EXIT;
}

DevdProcessService *
devd_process_service_new (void)
{
  return g_object_new (DEVD_TYPE_PROCESS_SERVICE, NULL);
}
