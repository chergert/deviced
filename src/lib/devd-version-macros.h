/* devd-version-macros.h
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEVD_VERSION_MACROS_H
#define DEVD_VERSION_MACROS_H

#if !defined(DEVICED_INSIDE) && !defined(DEVICED_COMPILATION)
# error "Only <libdeviced.h> can be included directly."
#endif

#include <glib.h>

#include "devd-version.h"

#ifndef _DEVD_EXTERN
# define _DEVD_EXTERN extern
#endif

#ifdef DEVD_DISABLE_DEPRECATION_WARNINGS
#define DEVD_DEPRECATED _DEVD_EXTERN
#define DEVD_DEPRECATED_FOR(f) _DEVD_EXTERN
#define DEVD_UNAVAILABLE(maj,min) _DEVD_EXTERN
#else
#define DEVD_DEPRECATED G_DEPRECATED _DEVD_EXTERN
#define DEVD_DEPRECATED_FOR(f) G_DEPRECATED_FOR(f) _DEVD_EXTERN
#define DEVD_UNAVAILABLE(maj,min) G_UNAVAILABLE(maj,min) _DEVD_EXTERN
#endif

#define DEVD_VERSION_3_28 (G_ENCODE_VERSION (3, 28))
#define DEVD_VERSION_3_30 (G_ENCODE_VERSION (3, 30))

#if (DEVD_MINOR_VERSION == 99)
# define DEVD_VERSION_CUR_STABLE (G_ENCODE_VERSION (DEVD_MAJOR_VERSION + 1, 0))
#elif (DEVD_MINOR_VERSION % 2)
# define DEVD_VERSION_CUR_STABLE (G_ENCODE_VERSION (DEVD_MAJOR_VERSION, DEVD_MINOR_VERSION + 1))
#else
# define DEVD_VERSION_CUR_STABLE (G_ENCODE_VERSION (DEVD_MAJOR_VERSION, DEVD_MINOR_VERSION))
#endif

#if (DEVD_MINOR_VERSION == 99)
# define DEVD_VERSION_PREV_STABLE (G_ENCODE_VERSION (DEVD_MAJOR_VERSION + 1, 0))
#elif (DEVD_MINOR_VERSION % 2)
# define DEVD_VERSION_PREV_STABLE (G_ENCODE_VERSION (DEVD_MAJOR_VERSION, DEVD_MINOR_VERSION - 1))
#else
# define DEVD_VERSION_PREV_STABLE (G_ENCODE_VERSION (DEVD_MAJOR_VERSION, DEVD_MINOR_VERSION - 2))
#endif

/**
 * DEVD_VERSION_MIN_REQUIRED:
 *
 * A macro that should be defined by the user prior to including
 * the devd.h header.
 *
 * The definition should be one of the predefined DEVD version
 * macros: %DEVD_VERSION_3_28, ...
 *
 * This macro defines the lower bound for the Builder API to use.
 *
 * If a function has been deprecated in a newer version of Builder,
 * it is possible to use this symbol to avoid the compiler warnings
 * without disabling warning for every deprecated function.
 *
 * Since: 3.28
 */
#ifndef DEVD_VERSION_MIN_REQUIRED
# define DEVD_VERSION_MIN_REQUIRED (DEVD_VERSION_CUR_STABLE)
#endif

/**
 * DEVD_VERSION_MAX_ALLOWED:
 *
 * A macro that should be defined by the user prior to including
 * the devd.h header.

 * The definition should be one of the predefined Builder version
 * macros: %DEVD_VERSION_1_0, %DEVD_VERSION_1_2,...
 *
 * This macro defines the upper bound for the DEVD API to use.
 *
 * If a function has been introduced in a newer version of Builder,
 * it is possible to use this symbol to get compiler warnings when
 * trying to use that function.
 *
 * Since: 3.28
 */
#ifndef DEVD_VERSION_MAX_ALLOWED
# if DEVD_VERSION_MIN_REQUIRED > DEVD_VERSION_PREV_STABLE
#  define DEVD_VERSION_MAX_ALLOWED (DEVD_VERSION_MIN_REQUIRED)
# else
#  define DEVD_VERSION_MAX_ALLOWED (DEVD_VERSION_CUR_STABLE)
# endif
#endif

#if DEVD_VERSION_MAX_ALLOWED < DEVD_VERSION_MIN_REQUIRED
# error "DEVD_VERSION_MAX_ALLOWED must be >= DEVD_VERSION_MIN_REQUIRED"
#endif
#if DEVD_VERSION_MIN_REQUIRED < DEVD_VERSION_3_28
# error "DEVD_VERSION_MIN_REQUIRED must be >= DEVD_VERSION_3_28"
#endif

#define DEVD_AVAILABLE_IN_ALL                   _DEVD_EXTERN

#if DEVD_VERSION_MIN_REQUIRED >= DEVD_VERSION_3_28
# define DEVD_DEPRECATED_IN_3_28                DEVD_DEPRECATED
# define DEVD_DEPRECATED_IN_3_28_FOR(f)         DEVD_DEPRECATED_FOR(f)
#else
# define DEVD_DEPRECATED_IN_3_28                _DEVD_EXTERN
# define DEVD_DEPRECATED_IN_3_28_FOR(f)         _DEVD_EXTERN
#endif

#if DEVD_VERSION_MAX_ALLOWED < DEVD_VERSION_3_28
# define DEVD_AVAILABLE_IN_3_28                 DEVD_UNAVAILABLE(3, 28)
#else
# define DEVD_AVAILABLE_IN_3_28                 _DEVD_EXTERN
#endif

#if DEVD_VERSION_MIN_REQUIRED >= DEVD_VERSION_3_30
# define DEVD_DEPRECATED_IN_3_30                DEVD_DEPRECATED
# define DEVD_DEPRECATED_IN_3_30_FOR(f)         DEVD_DEPRECATED_FOR(f)
#else
# define DEVD_DEPRECATED_IN_3_30                _DEVD_EXTERN
# define DEVD_DEPRECATED_IN_3_30_FOR(f)         _DEVD_EXTERN
#endif

#if DEVD_VERSION_MAX_ALLOWED < DEVD_VERSION_3_30
# define DEVD_AVAILABLE_IN_3_30                 DEVD_UNAVAILABLE(3, 30)
#else
# define DEVD_AVAILABLE_IN_3_30                 _DEVD_EXTERN
#endif

#endif /* DEVD_VERSION_MACROS_H */
