/* devd-network-client.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "devd-network-client"

#include "config.h"

#include <jsonrpc-glib.h>

#include "devd-app-info.h"
#include "devd-pty.h"
#include "devd-macros.h"
#include "devd-network-client.h"
#include "devd-trace.h"
#include "devd-util.h"

/**
 * SECTION:devd-network-client
 * @title: DevdNetworkClient
 * @short_description: a client to communicate with network nodes
 *
 * This is a #DevdClient implementation that communicates with a network peer.
 *
 * Since: 3.28
 */

struct _DevdNetworkClient
{
  DevdClient parent_instance;

  /*
   * The certificate we should use for the connection so that the server
   * can use the client certificate to avoid having to annoy the user by
   * asking if the client is authorized.
   */
  GTlsCertificate *certificate;

  /*
   * This is the address (GSocketConnectable) we use to connect to the peer.
   * Since we deal with network address resolution at a higher level, this
   * client is only ever concerned with connecting to a specific peer address.
   */
  GInetSocketAddress *address;

  /*
   * This is our client to communicate over JSON-RPC to the peer. After we
   * establish our TLS connection to the peer, we initialize the protocol
   * and extract the capabilities supported by the device.
   */
  JsonrpcClient *rpc_client;

  /*
   * If we got a capabilties reply back from the peer, we store the result
   * so that we can answer questions locally such as "does this device support
   * feature X".
   */
  GVariant *capabilities;

  /*
   * The host name from the peer, if they provided it during initialization.
   */
  gchar *name;

  /*
   * If we successfully communicated with the peer, and they provded the
   * host information, we should have enough information for a "target
   * triplet" in autoconf parlance. This is the "cpu-vendor-os" bit.
   */
  DevdTriplet *triplet;
};


G_DEFINE_TYPE (DevdNetworkClient, devd_network_client, DEVD_TYPE_CLIENT)

enum {
  PROP_0,
  PROP_ADDRESS,
  PROP_CERTIFICATE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
devd_network_client_initialize_cb (GObject      *object,
                                   GAsyncResult *result,
                                   gpointer      user_data)
{
  JsonrpcClient *rpc_client = (JsonrpcClient *)object;
  DevdNetworkClient *self;
  g_autoptr(GVariantDict) dict = NULL;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  const gchar *name;
  const gchar *arch;
  const gchar *kernel;
  const gchar *system;
  gboolean r;

  DEVD_ENTRY;

  g_assert (JSONRPC_IS_CLIENT (rpc_client));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!jsonrpc_client_call_finish (rpc_client, result, &reply, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  self = g_task_get_source_object (task);
  g_assert (DEVD_IS_NETWORK_CLIENT (self));

  /* Get the device name if we can */
  if (JSONRPC_MESSAGE_PARSE (reply, "name", JSONRPC_MESSAGE_GET_STRING (&name)))
    {
      g_clear_pointer (&self->name, g_free);
      self->name = g_strdup (name);
      g_object_notify (G_OBJECT (self), "name");
    }

  /* Stash the capabilities for future reference if the daemon replied */
  if (JSONRPC_MESSAGE_PARSE (reply, "capabilities", JSONRPC_MESSAGE_GET_DICT (&dict)))
    {
      g_clear_pointer (&self->capabilities, g_variant_unref);
      self->capabilities = g_variant_dict_end (dict);
    }

  /* Parse the target triplet if available */
  r = JSONRPC_MESSAGE_PARSE (reply,
    "host", "{",
      "arch", JSONRPC_MESSAGE_GET_STRING (&arch),
      "kernel", JSONRPC_MESSAGE_GET_STRING (&kernel),
      "system", JSONRPC_MESSAGE_GET_STRING (&system),
    "}"
  );

  if (r)
    {
      g_clear_pointer (&self->triplet, devd_triplet_unref);
      self->triplet = devd_triplet_new_with_triplet (arch, kernel, system);

      g_object_notify (G_OBJECT (self), "triplet");
      g_object_notify (G_OBJECT (self), "arch");
      g_object_notify (G_OBJECT (self), "kernel");
      g_object_notify (G_OBJECT (self), "system");
    }

  g_set_object (&self->rpc_client, rpc_client);

  g_task_return_boolean (task, TRUE);

  DEVD_EXIT;
}

static gboolean
devd_network_client_accept_certificate_cb (DevdNetworkClient    *self,
                                           GTlsCertificate      *certificate,
                                           GTlsCertificateFlags  flags,
                                           GTlsConnection       *connection)
{
  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_CLIENT (self));
  g_assert (G_IS_TLS_CERTIFICATE (certificate));
  g_assert (G_IS_TLS_CONNECTION (connection));

  /* TODO: Peer certificate validation */

  DEVD_RETURN (TRUE);
}

static void
devd_network_client_socket_client_handshake_cb (DevdNetworkClient  *self,
                                                GSocketClientEvent  event,
                                                GSocketConnectable *connectable,
                                                GIOStream          *connection,
                                                GSocketClient      *client)
{
  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_CLIENT (self));
  g_assert (G_IS_INET_SOCKET_ADDRESS (connectable));
  g_assert (!connection || G_IS_IO_STREAM (connection));
  g_assert (G_IS_SOCKET_CLIENT (client));
  g_assert (!self->certificate || G_IS_TLS_CERTIFICATE (self->certificate));
  g_assert (G_IS_TLS_CLIENT_CONNECTION (connection) || event != G_SOCKET_CLIENT_TLS_HANDSHAKING);

  switch (event)
    {
    case G_SOCKET_CLIENT_TLS_HANDSHAKING:
      if (self->certificate != NULL)
        {
          g_tls_connection_set_certificate (G_TLS_CONNECTION (connection), self->certificate);
          g_signal_connect_object (connection,
                                   "accept-certificate",
                                   G_CALLBACK (devd_network_client_accept_certificate_cb),
                                   self,
                                   G_CONNECT_SWAPPED);
        }
      break;

    case G_SOCKET_CLIENT_RESOLVING:
    case G_SOCKET_CLIENT_RESOLVED:
    case G_SOCKET_CLIENT_CONNECTING:
    case G_SOCKET_CLIENT_CONNECTED:
    case G_SOCKET_CLIENT_PROXY_NEGOTIATING:
    case G_SOCKET_CLIENT_PROXY_NEGOTIATED:
    case G_SOCKET_CLIENT_TLS_HANDSHAKED:
    case G_SOCKET_CLIENT_COMPLETE:
    default:
      break;
    }

  DEVD_EXIT;
}

static void
devd_network_client_notification_cb (DevdNetworkClient *self,
                                     const gchar       *method,
                                     GVariant          *params,
                                     JsonrpcClient     *client)
{
  g_assert (DEVD_IS_NETWORK_CLIENT (self));
  g_assert (JSONRPC_IS_CLIENT (client));

  devd_client_emit_notification (DEVD_CLIENT (self), method, params);
}

static void
devd_network_client_service_added_cb (DevdNetworkClient *self,
                                      const gchar       *method,
                                      GVariant          *params,
                                      JsonrpcClient     *client)
{
  g_assert (DEVD_IS_NETWORK_CLIENT (self));
  g_assert (g_str_equal (method, "service-added"));
  g_assert (JSONRPC_IS_CLIENT (client));

  if (params != NULL && g_variant_is_of_type (params, G_VARIANT_TYPE_STRING))
    devd_client_emit_service_added (DEVD_CLIENT (self),
                                    g_variant_get_string (params, NULL));
}

static void
devd_network_client_socket_connect_cb (GObject      *object,
                                       GAsyncResult *result,
                                       gpointer      user_data)
{
  GSocketClient *socket_client = (GSocketClient *)object;
  g_autoptr(GSocketConnection) connection = NULL;
  g_autoptr(JsonrpcClient) rpc_client = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  DevdNetworkClient *self;
  GCancellable *cancellable;

  DEVD_ENTRY;

  g_assert (G_IS_SOCKET_CLIENT (socket_client));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!(connection = g_socket_client_connect_finish (socket_client, result, &error)))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  cancellable = g_task_get_cancellable (task);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  rpc_client = jsonrpc_client_new (G_IO_STREAM (connection));
  g_assert (JSONRPC_IS_CLIENT (rpc_client));

  self = g_task_get_source_object (task);
  g_assert (DEVD_IS_NETWORK_CLIENT (self));

  g_signal_connect_object (rpc_client,
                           "notification::service-added",
                           G_CALLBACK (devd_network_client_service_added_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_signal_connect_object (rpc_client,
                           "notification",
                           G_CALLBACK (devd_network_client_notification_cb),
                           self,
                           G_CONNECT_SWAPPED);

  jsonrpc_client_call_async (rpc_client,
                             "initialize",
                             NULL,
                             cancellable,
                             devd_network_client_initialize_cb,
                             g_steal_pointer (&task));

  DEVD_EXIT;
}

static void
devd_network_client_connect_async (DevdClient          *client,
                                   GCancellable        *cancellable,
                                   GAsyncReadyCallback  callback,
                                   gpointer             user_data)
{
  DevdNetworkClient *self = (DevdNetworkClient *)client;
  g_autoptr(GSocketClient) socket_client = NULL;
  g_autoptr(GTask) task = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_CLIENT (client));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_network_client_connect_async);

  if (self->address == NULL)
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_INVAL,
                               "No address set to connect to");
      DEVD_EXIT;
    }

  socket_client = g_socket_client_new ();
  g_socket_client_set_tls (socket_client, TRUE);

  g_signal_connect_object (socket_client,
                           "event",
                           G_CALLBACK (devd_network_client_socket_client_handshake_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_socket_client_connect_async (socket_client,
                                 G_SOCKET_CONNECTABLE (self->address),
                                 cancellable,
                                 devd_network_client_socket_connect_cb,
                                 g_steal_pointer (&task));

  DEVD_EXIT;
}

static gboolean
devd_network_client_connect_finish (DevdClient    *client,
                                    GAsyncResult  *result,
                                    GError       **error)
{
  gboolean ret;

  DEVD_ENTRY;

  g_assert (DEVD_IS_CLIENT (client));
  g_assert (G_IS_ASYNC_RESULT (result));

  ret = g_task_propagate_boolean (G_TASK (result), error);

  DEVD_RETURN (ret);
}

static void
devd_network_client_disconnect_cb (GObject      *object,
                                   GAsyncResult *result,
                                   gpointer      user_data)
{
  JsonrpcClient *rpc_client = (JsonrpcClient *)object;
  DevdNetworkClient *self;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;

  DEVD_ENTRY;

  g_assert (JSONRPC_IS_CLIENT (rpc_client));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!jsonrpc_client_close_finish (rpc_client, result, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);

  self = g_task_get_source_object (task);

  if (self->rpc_client == rpc_client)
    g_clear_object (&self->rpc_client);

  DEVD_EXIT;
}

static void
devd_network_client_disconnect_async (DevdClient          *client,
                                      GCancellable        *cancellable,
                                      GAsyncReadyCallback  callback,
                                      gpointer             user_data)
{
  DevdNetworkClient *self = (DevdNetworkClient *)client;
  g_autoptr(GTask) task = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_CLIENT (client));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_network_client_disconnect_async);

  if (self->rpc_client == NULL)
    g_task_return_boolean (task, TRUE);
  else
    jsonrpc_client_close_async (self->rpc_client,
                                cancellable,
                                devd_network_client_disconnect_cb,
                                g_steal_pointer (&task));

  DEVD_EXIT;
}

static gboolean
devd_network_client_disconnect_finish (DevdClient    *client,
                                       GAsyncResult  *result,
                                       GError       **error)
{
  gboolean ret;

  DEVD_ENTRY;

  g_assert (DEVD_IS_CLIENT (client));
  g_assert (G_IS_ASYNC_RESULT (result));

  ret = g_task_propagate_boolean (G_TASK (result), error);

  DEVD_RETURN (ret);
}

static void
devd_network_client_call_cb (GObject      *object,
                             GAsyncResult *result,
                             gpointer      user_data)
{
  JsonrpcClient *rpc_client = (JsonrpcClient *)object;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;

  DEVD_ENTRY;

  g_assert (JSONRPC_IS_CLIENT (rpc_client));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!jsonrpc_client_call_finish (rpc_client, result, &reply, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_pointer (task,
                           g_steal_pointer (&reply),
                           (GDestroyNotify)g_variant_unref);

  DEVD_EXIT;
}

static void
devd_network_client_call_async (DevdClient          *client,
                                const gchar         *method,
                                GVariant            *params,
                                GCancellable        *cancellable,
                                GAsyncReadyCallback  callback,
                                gpointer             user_data)
{
  DevdNetworkClient *self = (DevdNetworkClient *)client;
  g_autoptr(GTask) task = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_CLIENT (client));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_network_client_call_async);

  jsonrpc_client_call_async (self->rpc_client,
                             method,
                             params,
                             cancellable,
                             devd_network_client_call_cb,
                             g_steal_pointer (&task));

  DEVD_EXIT;
}

static gboolean
devd_network_client_call_finish (DevdClient    *client,
                                 GAsyncResult  *result,
                                 GVariant     **reply,
                                 GError       **error)
{
  g_autoptr(GVariant) local_reply = NULL;
  g_autoptr(GError) local_error = NULL;
  gboolean ret;

  DEVD_ENTRY;

  g_assert (DEVD_IS_CLIENT (client));
  g_assert (G_IS_TASK (result));

  local_reply = g_task_propagate_pointer (G_TASK (result), &local_error);
  ret = local_error == NULL;
  if (reply != NULL)
    *reply = g_steal_pointer (&local_reply);
  if (local_error != NULL)
    g_propagate_error (error, g_steal_pointer (&local_error));

  DEVD_RETURN (ret);
}

static gchar *
devd_network_client_get_arch (DevdClient *client)
{
  DevdNetworkClient *self = (DevdNetworkClient *)client;

  g_assert (DEVD_IS_NETWORK_CLIENT (self));

  if (self->triplet != NULL)
    return g_strdup (devd_triplet_get_arch (self->triplet));

  return NULL;
}

static gchar *
devd_network_client_get_kernel (DevdClient *client)
{
  DevdNetworkClient *self = (DevdNetworkClient *)client;

  g_assert (DEVD_IS_NETWORK_CLIENT (self));

  if (self->triplet != NULL)
    return g_strdup (devd_triplet_get_kernel (self->triplet));

  return NULL;
}

static gchar *
devd_network_client_get_name (DevdClient *client)
{
  return g_strdup (DEVD_NETWORK_CLIENT (client)->name);
}

static gchar *
devd_network_client_get_system (DevdClient *client)
{
  DevdNetworkClient *self = (DevdNetworkClient *)client;

  g_assert (DEVD_IS_NETWORK_CLIENT (self));

  if (self->triplet != NULL)
    return g_strdup (devd_triplet_get_operating_system (self->triplet));

  return NULL;
}

static DevdTriplet *
devd_network_client_get_triplet (DevdClient *client)
{
  DevdTriplet *triplet = DEVD_NETWORK_CLIENT (client)->triplet;

  if (triplet != NULL)
    return devd_triplet_ref (triplet);

  return NULL;
}

static DevdAppInfo *
create_app_info (GVariant *doc)
{
  g_assert (doc != NULL);

  if (!g_variant_is_of_type (doc, G_VARIANT_TYPE_VARDICT))
    return NULL;

  return devd_from_gvariant (DEVD_TYPE_APP_INFO, doc);
}

static void
devd_network_client_list_apps_runtimes_cb (GObject      *object,
                                           GAsyncResult *result,
                                           gpointer      user_data)
{
  JsonrpcClient *rpc_client = (JsonrpcClient *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GPtrArray) app_infos = NULL;
  GVariantIter iter;

  DEVD_ENTRY;

  g_assert (JSONRPC_IS_CLIENT (rpc_client));
  g_assert (G_IS_ASYNC_RESULT(result));
  g_assert (G_IS_TASK (task));

  if (!jsonrpc_client_call_finish (rpc_client, result, &reply, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  app_infos = g_ptr_array_new_with_free_func (g_object_unref);

  if (!g_variant_is_of_type (reply, G_VARIANT_TYPE ("av")))
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_INVALID_DATA,
                               "Received incorrect data from RPC peer");
      DEVD_EXIT;
    }

  if (g_variant_iter_init (&iter, reply) > 0)
    {
      GVariant *doc;

      while (g_variant_iter_loop (&iter, "v", &doc))
        {
          g_autoptr(DevdAppInfo) app_info = create_app_info (doc);

          if (app_info != NULL)
            g_ptr_array_add (app_infos, g_steal_pointer (&app_info));
        }
    }

  g_task_return_pointer (task,
                         g_steal_pointer (&app_infos),
                         (GDestroyNotify)g_ptr_array_unref);

  DEVD_EXIT;
}

static void
devd_network_client_list_apps_async (DevdClient          *client,
                                     GCancellable        *cancellable,
                                     GAsyncReadyCallback  callback,
                                     gpointer             user_data)
{
  DevdNetworkClient *self = (DevdNetworkClient *)client;
  g_autoptr(GTask) task = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_CLIENT (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_network_client_list_apps_async);

  jsonrpc_client_call_async (self->rpc_client,
                             "list-apps",
                             NULL,
                             cancellable,
                             devd_network_client_list_apps_runtimes_cb,
                             g_steal_pointer (&task));

  DEVD_EXIT;
}

static void
devd_network_client_list_runtimes_async (DevdClient          *client,
                                         GCancellable        *cancellable,
                                         GAsyncReadyCallback  callback,
                                         gpointer             user_data)
{
  DevdNetworkClient *self = (DevdNetworkClient *)client;
  g_autoptr(GTask) task = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_CLIENT (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_network_client_list_runtimes_async);

  jsonrpc_client_call_async (self->rpc_client,
                             "list-runtimes",
                             NULL,
                             cancellable,
                             devd_network_client_list_apps_runtimes_cb,
                             g_steal_pointer (&task));

  DEVD_EXIT;
}

static GPtrArray *
devd_network_client_list_runtimes_finish (DevdClient    *client,
                                          GAsyncResult  *result,
                                          GError       **error)
{
  g_autoptr(GPtrArray) ret = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_CLIENT (client));
  g_assert (G_IS_TASK (result));

  ret = g_task_propagate_pointer (G_TASK (result), error);

  DEVD_RETURN (g_steal_pointer (&ret));
}

static void
devd_network_client_list_files_cb (GObject      *object,
                                   GAsyncResult *result,
                                   gpointer      user_data)
{
  DevdClient *client = (DevdClient *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GPtrArray) file_infos = NULL;
  GVariantIter iter;

  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_CLIENT (client));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!devd_client_call_finish (client, result, &reply, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  file_infos = g_ptr_array_new_with_free_func (g_object_unref);

  if (!g_variant_is_of_type (reply, G_VARIANT_TYPE ("av")))
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_INVALID_DATA,
                               "Received incorrect data from RPC peer");
      DEVD_EXIT;
    }

  if (g_variant_iter_init (&iter, reply) > 0)
    {
      GVariant *doc;

      while (g_variant_iter_loop (&iter, "v", &doc))
        {
          g_autoptr(GFileInfo) file_info = devd_file_info_from_gvariant (doc);

          if (file_info != NULL)
            g_ptr_array_add (file_infos, g_steal_pointer (&file_info));
        }
    }

  g_task_return_pointer (task,
                         g_steal_pointer (&file_infos),
                         (GDestroyNotify)g_ptr_array_unref);

  DEVD_EXIT;
}

static void
devd_network_client_list_files_async (DevdClient          *client,
                                      const gchar         *path,
                                      const gchar         *attributes,
                                      GCancellable        *cancellable,
                                      GAsyncReadyCallback  callback,
                                      gpointer             user_data)
{
  DevdNetworkClient *self = (DevdNetworkClient *)client;
  g_autoptr(GTask) task = NULL;
  g_autoptr(GVariant) params = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_CLIENT (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_network_client_list_files_async);

  params = JSONRPC_MESSAGE_NEW (
    "path", JSONRPC_MESSAGE_PUT_STRING (path),
    "attributes", JSONRPC_MESSAGE_PUT_STRING (attributes)
  );

  devd_client_call_async (DEVD_CLIENT (self),
                          "list-files",
                          params,
                          cancellable,
                          devd_network_client_list_files_cb,
                          g_steal_pointer (&task));

  DEVD_EXIT;
}

static GPtrArray *
devd_network_client_list_apps_finish (DevdClient    *client,
                                      GAsyncResult  *result,
                                      GError       **error)
{
  g_autoptr(GPtrArray) ret = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_CLIENT (client));
  g_assert (G_IS_TASK (result));

  ret = g_task_propagate_pointer (G_TASK (result), error);

  DEVD_RETURN (g_steal_pointer (&ret));
}

static GPtrArray *
devd_network_client_list_files_finish (DevdClient    *client,
                                       GAsyncResult  *result,
                                       GError       **error)
{
  g_autoptr(GPtrArray) ret = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_CLIENT (client));
  g_assert (G_IS_TASK (result));

  ret = g_task_propagate_pointer (G_TASK (result), error);

  DEVD_RETURN (g_steal_pointer (&ret));
}

static void
devd_network_client_run_app_cb (GObject      *object,
                                GAsyncResult *result,
                                gpointer      user_data)
{
  JsonrpcClient *rpc_client = (JsonrpcClient *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_autoptr(GVariant) reply = NULL;

  DEVD_ENTRY;

  g_assert (JSONRPC_IS_CLIENT (rpc_client));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!jsonrpc_client_call_finish (rpc_client, result, &reply, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  if (!g_variant_is_of_type (reply, G_VARIANT_TYPE_STRING))
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_INVALID_DATA,
                               "Received incorrect data from RPC peer");
      DEVD_EXIT;
    }

  g_task_return_pointer (task,
                         g_strdup (g_variant_get_string (reply, NULL)),
                         g_free);

  DEVD_EXIT;
}

static void
devd_network_client_run_app_async (DevdClient          *client,
                                   const gchar         *provider,
                                   const gchar         *app_id,
                                   const gchar         *pty,
                                   GCancellable        *cancellable,
                                   GAsyncReadyCallback  callback,
                                   gpointer             user_data)
{
  DevdNetworkClient *self = (DevdNetworkClient *)client;
  g_autoptr(GTask) task = NULL;
  g_autoptr(GVariant) params = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_CLIENT (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_network_client_run_app_async);

  params = JSONRPC_MESSAGE_NEW (
    "provider", JSONRPC_MESSAGE_PUT_STRING (provider),
    "app-id", JSONRPC_MESSAGE_PUT_STRING (app_id),
    "pty", JSONRPC_MESSAGE_PUT_STRING (pty)
  );

  jsonrpc_client_call_async (self->rpc_client,
                             "run-app",
                             params,
                             cancellable,
                             devd_network_client_run_app_cb,
                             g_steal_pointer (&task));

  DEVD_EXIT;
}

static gchar *
devd_network_client_run_app_finish (DevdClient    *client,
                                    GAsyncResult  *result,
                                    GError       **error)
{
  gchar *ret;

  DEVD_ENTRY;

  g_assert (DEVD_IS_CLIENT (client));
  g_assert (G_IS_TASK (result));

  ret = g_task_propagate_pointer (G_TASK (result), error);

  DEVD_RETURN (ret);
}

static void
devd_network_client_syncfs_cb (GObject      *object,
                               GAsyncResult *result,
                               gpointer      user_data)
{
  JsonrpcClient *rpc_client = (JsonrpcClient *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_autoptr(GVariant) reply = NULL;

  DEVD_ENTRY;

  g_assert (JSONRPC_IS_CLIENT (rpc_client));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!jsonrpc_client_call_finish (rpc_client, result, &reply, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else if (!g_variant_is_of_type (reply, G_VARIANT_TYPE_BOOLEAN))
    g_task_return_new_error (task,
                             G_IO_ERROR,
                             G_IO_ERROR_INVALID_DATA,
                             "Received incorrect data from RPC peer");
  else
    g_task_return_boolean (task, g_variant_get_boolean (reply));

  DEVD_EXIT;
}

static void
devd_network_client_syncfs_async (DevdClient          *client,
                                  const gchar         *devices,
                                  GCancellable        *cancellable,
                                  GAsyncReadyCallback  callback,
                                  gpointer             user_data)
{
  DevdNetworkClient *self = (DevdNetworkClient *)client;
  g_autoptr(GTask) task = NULL;
  g_autoptr(GVariant) params = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_CLIENT (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_network_client_syncfs_async);

  params = JSONRPC_MESSAGE_NEW (
    "devices", JSONRPC_MESSAGE_PUT_STRING (devices ?: "")
  );

  jsonrpc_client_call_async (self->rpc_client,
                             "syncfs",
                             params,
                             cancellable,
                             devd_network_client_syncfs_cb,
                             g_steal_pointer (&task));

  DEVD_EXIT;
}

static gboolean
devd_network_client_syncfs_finish (DevdClient    *client,
                                   GAsyncResult  *result,
                                   GError       **error)
{
  gboolean ret;

  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_CLIENT (client));
  g_assert (G_IS_TASK (result));

  ret = g_task_propagate_boolean (G_TASK (result), error);

  DEVD_RETURN (ret);
}

static void
devd_network_client_finalize (GObject *object)
{
  DevdNetworkClient *self = (DevdNetworkClient *)object;

  DEVD_ENTRY;

  g_clear_object (&self->rpc_client);
  g_clear_object (&self->address);
  g_clear_object (&self->certificate);
  g_clear_pointer (&self->capabilities, g_variant_unref);

  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->triplet, devd_triplet_unref);

  G_OBJECT_CLASS (devd_network_client_parent_class)->finalize (object);

  DEVD_EXIT;
}

static void
devd_network_client_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  DevdNetworkClient *self = DEVD_NETWORK_CLIENT (object);

  switch (prop_id)
    {
    case PROP_ADDRESS:
      g_value_set_object (value, devd_network_client_get_address (self));
      break;

    case PROP_CERTIFICATE:
      g_value_set_object (value, devd_network_client_get_certificate (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
devd_network_client_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  DevdNetworkClient *self = DEVD_NETWORK_CLIENT (object);

  switch (prop_id)
    {
    case PROP_ADDRESS:
      self->address = g_value_dup_object (value);
      break;

    case PROP_CERTIFICATE:
      self->certificate = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
devd_network_client_class_init (DevdNetworkClientClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  DevdClientClass *client_class = DEVD_CLIENT_CLASS (klass);

  object_class->finalize = devd_network_client_finalize;
  object_class->get_property = devd_network_client_get_property;
  object_class->set_property = devd_network_client_set_property;

  client_class->get_arch = devd_network_client_get_arch;
  client_class->get_kernel = devd_network_client_get_kernel;
  client_class->get_name = devd_network_client_get_name;
  client_class->get_system = devd_network_client_get_system;
  client_class->get_triplet = devd_network_client_get_triplet;
  client_class->connect_async = devd_network_client_connect_async;
  client_class->connect_finish = devd_network_client_connect_finish;
  client_class->disconnect_async = devd_network_client_disconnect_async;
  client_class->disconnect_finish = devd_network_client_disconnect_finish;
  client_class->call_async = devd_network_client_call_async;
  client_class->call_finish = devd_network_client_call_finish;
  client_class->list_apps_async = devd_network_client_list_apps_async;
  client_class->list_apps_finish = devd_network_client_list_apps_finish;
  client_class->list_runtimes_async = devd_network_client_list_runtimes_async;
  client_class->list_runtimes_finish = devd_network_client_list_runtimes_finish;
  client_class->list_files_async = devd_network_client_list_files_async;
  client_class->list_files_finish = devd_network_client_list_files_finish;
  client_class->run_app_async = devd_network_client_run_app_async;
  client_class->run_app_finish = devd_network_client_run_app_finish;
  client_class->syncfs_async = devd_network_client_syncfs_async;
  client_class->syncfs_finish = devd_network_client_syncfs_finish;

  /**
   * DevdNetworkClient:address:
   *
   * The "address" is the destination that we will attept to connect to
   * using this client, when devd_client_connect_async() is called.
   */
  properties [PROP_ADDRESS] =
    g_param_spec_object ("address",
                         "Address",
                         "The inet socket address to connect to",
                         G_TYPE_INET_SOCKET_ADDRESS,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  properties [PROP_CERTIFICATE] =
    g_param_spec_object ("certificate",
                         "Certificate",
                         "The client TLS certificate to use for communication",
                         G_TYPE_TLS_CERTIFICATE,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
devd_network_client_init (DevdNetworkClient *self)
{
}

/**
 * devd_network_client_new:
 * @address: a #GInetSocketAddress
 * @certificate: (nullable): a #GTlsCertificate or %NULL
 *
 * Creates a new #DevdNetworkClient.
 *
 * You can use this instead of devd_device_create_client() if you know
 * who you want to communicate with already and the certificate to use.
 *
 * Generally, you should wait for devices to show up using #DevdBrowser
 * and then call devd_device_create_client() to create a client for that
 * device.
 *
 * Returns: (transfer full): a #DevdNetworkClient
 *
 * Since: 3.28
 */
DevdNetworkClient *
devd_network_client_new (GInetSocketAddress *address,
                         GTlsCertificate    *certificate)
{
  g_return_val_if_fail (G_IS_INET_SOCKET_ADDRESS (address), NULL);
  g_return_val_if_fail (!certificate || G_IS_TLS_CERTIFICATE (certificate), NULL);

  return g_object_new (DEVD_TYPE_NETWORK_CLIENT,
                       "address", address,
                       "certificate", certificate,
                       NULL);
}

/**
 * devd_network_client_get_address:
 * @self: a #DevdNetworkClient
 *
 * Gets the address that is to be used to connect to the device.
 *
 * Returns: (transfer none): a #GInetSocketAddress
 *
 * Since: 3.28
 */
GInetSocketAddress *
devd_network_client_get_address (DevdNetworkClient *self)
{
  g_return_val_if_fail (DEVD_IS_NETWORK_CLIENT (self), NULL);

  return self->address;
}

/**
 * devd_network_client_get_certificate:
 * @self: a #DevdNetworkClient
 *
 * Gets the certificate for the network client.
 *
 * Returns: (transfer none): a #GTlsCertificate
 *
 * Since: 3.28
 */
GTlsCertificate *
devd_network_client_get_certificate (DevdNetworkClient *self)
{
  g_return_val_if_fail (DEVD_IS_NETWORK_CLIENT (self), NULL);

  return self->certificate;
}
