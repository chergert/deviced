/* devd-triplet.c
 *
 * Copyright (C) 2018 Corentin Noël <corentin.noel@collabora.com>
 * Copyright (C) 2018 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gio/gio.h>
#include <glib-object.h>

#include "devd-version-macros.h"

G_BEGIN_DECLS

#define DEVD_TYPE_TRIPLET (devd_triplet_get_type())

typedef struct _DevdTriplet DevdTriplet;

DEVD_AVAILABLE_IN_3_30
GType         devd_triplet_get_type             (void);
DEVD_AVAILABLE_IN_3_30
DevdTriplet  *devd_triplet_new                  (const gchar  *full_name);
DEVD_AVAILABLE_IN_3_30
DevdTriplet  *devd_triplet_new_with_triplet     (const gchar  *arch,
                                                 const gchar  *kernel,
                                                 const gchar  *operating_system);
DEVD_AVAILABLE_IN_3_30
DevdTriplet  *devd_triplet_new_with_quadruplet  (const gchar  *arch,
                                                 const gchar  *vendor,
                                                 const gchar  *kernel,
                                                 const gchar  *operating_system);
DEVD_AVAILABLE_IN_3_30
DevdTriplet  *devd_triplet_ref                  (DevdTriplet   *self);
DEVD_AVAILABLE_IN_3_30
void          devd_triplet_unref                (DevdTriplet   *self);
DEVD_AVAILABLE_IN_3_30
const gchar  *devd_triplet_get_full_name        (DevdTriplet   *self);
DEVD_AVAILABLE_IN_3_30
const gchar  *devd_triplet_get_arch             (DevdTriplet   *self);
DEVD_AVAILABLE_IN_3_30
const gchar  *devd_triplet_get_vendor           (DevdTriplet   *self);
DEVD_AVAILABLE_IN_3_30
const gchar  *devd_triplet_get_kernel           (DevdTriplet   *self);
DEVD_AVAILABLE_IN_3_30
const gchar  *devd_triplet_get_operating_system (DevdTriplet   *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (DevdTriplet, devd_triplet_unref)

G_END_DECLS
