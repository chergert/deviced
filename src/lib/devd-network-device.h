/* devd-network-device.h
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(DEVICED_INSIDE) && !defined(DEVICED_COMPILATION)
# error "Only <libdeviced.h> can be included directly."
#endif

#include "devd-device.h"
#include "devd-version-macros.h"

G_BEGIN_DECLS

#define DEVD_TYPE_NETWORK_DEVICE (devd_network_device_get_type())

DEVD_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (DevdNetworkDevice, devd_network_device, DEVD, NETWORK_DEVICE, DevdDevice)

DEVD_AVAILABLE_IN_ALL
GInetSocketAddress *devd_network_device_get_address     (DevdNetworkDevice *self);
DEVD_AVAILABLE_IN_ALL
GTlsCertificate    *devd_network_device_get_certificate (DevdNetworkDevice *self);

G_END_DECLS
