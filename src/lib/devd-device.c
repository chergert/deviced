/* devd-device.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "devd-device"

#include "config.h"

#include "devd-device.h"
#include "devd-enums.h"
#include "devd-trace.h"

/**
 * SECTION:devd-device
 * @title: DevdDevice
 * @short_description: represents a discovered Deviced device
 *
 * The #DevdDevice represents a Deviced device that has been discovered.
 *
 * Since: 3.28
 */

typedef struct
{
  gchar *icon_name;
  gchar *id;
  gchar *machine_id;
  gchar *name;
  DevdDeviceKind kind;
} DevdDevicePrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (DevdDevice, devd_device, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_ICON_NAME,
  PROP_ID,
  PROP_MACHINE_ID,
  PROP_KIND,
  PROP_NAME,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
devd_device_finalize (GObject *object)
{
  DevdDevice *self = (DevdDevice *)object;
  DevdDevicePrivate *priv = devd_device_get_instance_private (self);

  g_clear_pointer (&priv->icon_name, g_free);
  g_clear_pointer (&priv->id, g_free);
  g_clear_pointer (&priv->machine_id, g_free);
  g_clear_pointer (&priv->name, g_free);

  G_OBJECT_CLASS (devd_device_parent_class)->finalize (object);
}

static void
devd_device_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  DevdDevice *self = DEVD_DEVICE (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, devd_device_get_id (self));
      break;

    case PROP_KIND:
      g_value_set_enum (value, devd_device_get_kind (self));
      break;

    case PROP_MACHINE_ID:
      g_value_set_string (value, devd_device_get_machine_id (self));
      break;

    case PROP_NAME:
      g_value_set_string (value, devd_device_get_name (self));
      break;

    case PROP_ICON_NAME:
      g_value_set_string (value, devd_device_get_icon_name (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
devd_device_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  DevdDevice *self = DEVD_DEVICE (object);
  DevdDevicePrivate *priv = devd_device_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_ID:
      priv->id = g_value_dup_string (value);
      break;

    case PROP_KIND:
      devd_device_set_kind (self, g_value_get_enum (value));
      break;

    case PROP_MACHINE_ID:
      devd_device_set_machine_id (self, g_value_get_string (value));
      break;

    case PROP_NAME:
      devd_device_set_name (self, g_value_get_string (value));
      break;

    case PROP_ICON_NAME:
      devd_device_set_icon_name (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
devd_device_class_init (DevdDeviceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = devd_device_finalize;
  object_class->get_property = devd_device_get_property;
  object_class->set_property = devd_device_set_property;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "Id",
                         "The identifier for the device",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  properties [PROP_KIND] =
    g_param_spec_enum ("kind",
                       "Kind",
                       "The device kind",
                       DEVD_TYPE_DEVICE_KIND,
                       DEVD_DEVICE_KIND_COMPUTER,
                       G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  properties [PROP_MACHINE_ID] =
    g_param_spec_string ("machine-id",
                         "Machine ID",
                         "The machine-id of the device",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the device",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  properties [PROP_ICON_NAME] =
    g_param_spec_string ("icon-name",
                         "Icon Name",
                         "The icon-name for the device",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
devd_device_init (DevdDevice *self)
{
  DEVD_ENTRY;
  DEVD_EXIT;
}

/**
 * devd_device_get_machine_id:
 * @self: a #DevdDevice
 *
 * Gets the machine-id property, if specified
 *
 * Returns: (nullable): a machine-id, or %NULL
 *
 * Since: 3.28
 */
const gchar *
devd_device_get_machine_id (DevdDevice *self)
{
  DevdDevicePrivate *priv = devd_device_get_instance_private (self);

  g_return_val_if_fail (DEVD_IS_DEVICE (self), NULL);

  return priv->machine_id;
}

/**
 * devd_device_set_machine_id:
 * @self: a #DevdDevice
 * @machine_id: the identifier of the machine, if known
 *
 * Sets the #DevdDevice:machine-id for the local object instance.
 *
 * Since: 3.28
 */
void
devd_device_set_machine_id (DevdDevice  *self,
                            const gchar *machine_id)
{
  DevdDevicePrivate *priv = devd_device_get_instance_private (self);

  g_return_if_fail (DEVD_IS_DEVICE (self));

  if (g_strcmp0 (machine_id, priv->machine_id) != 0)
    {
      g_free (priv->machine_id);
      priv->machine_id = g_strdup (machine_id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_MACHINE_ID]);
    }
}

/**
 * devd_device_get_icon_name:
 * @self: a #DevdDevice
 *
 * Gets the #DevdDevice:icon-name property.
 *
 * Returns: (nullable): the icon-name, or %NULL
 *
 * Since: 3.28
 */
const gchar *
devd_device_get_icon_name (DevdDevice *self)
{
  DevdDevicePrivate *priv = devd_device_get_instance_private (self);

  g_return_val_if_fail (DEVD_IS_DEVICE (self), NULL);

  if (priv->icon_name != NULL)
    return priv->icon_name;

  /*
   * These are fallbacks, so they don't need to be perfect. Ideally,
   * we'll be able to provide "support files" for specific hardware
   * that will give appropriate icons for a specific device kind.
   *
   * The code that can set the icon-name will be the browser, so it
   * needs to learn how to get specific device information.
   */

  switch (priv->kind)
    {
    case DEVD_DEVICE_KIND_COMPUTER:
      return "computer-symbolic";

    case DEVD_DEVICE_KIND_TABLET:
    case DEVD_DEVICE_KIND_PHONE:
    case DEVD_DEVICE_KIND_MICRO_CONTROLLER:
      return "computer-apple-ipad-symbolic";

    default:
      return NULL;
    }
}

/**
 * devd_device_set_icon_name:
 * @self: a #DevdDevice
 * @icon_name: the icon name for the device
 *
 * This set the icon-name for the local #DevdDevice instance. It does not
 * change the icon on the remove device. To interact with the remove device
 * use devd_device_create_client().
 *
 * Since: 3.28
 */
void
devd_device_set_icon_name (DevdDevice  *self,
                           const gchar *icon_name)
{
  DevdDevicePrivate *priv = devd_device_get_instance_private (self);

  g_return_if_fail (DEVD_IS_DEVICE (self));

  if (g_strcmp0 (icon_name, priv->icon_name) != 0)
    {
      g_free (priv->icon_name);
      priv->icon_name = g_strdup (icon_name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ICON_NAME]);
    }
}

/**
 * devd_device_get_name:
 * @self: a #DevdDevice
 *
 * Gets the "name" property of the device, if there is one.
 *
 * Returns: the device name
 *
 * Since: 3.28
 */
const gchar *
devd_device_get_name (DevdDevice *self)
{
  DevdDevicePrivate *priv = devd_device_get_instance_private (self);

  g_return_val_if_fail (DEVD_IS_DEVICE (self), NULL);

  return priv->name;
}

/**
 * devd_device_set_name:
 * @self: a #DevdDevice
 * @name: the name for the device
 *
 * Sets the name of the device.
 *
 * This only changes the local representation of the device name. It does
 * not change the name on the remove device. To interact with the remote
 * device, use devd_device_create_client() to create a #DevdClient.
 *
 * Since: 3.28
 */
void
devd_device_set_name (DevdDevice  *self,
                      const gchar *name)
{
  DevdDevicePrivate *priv = devd_device_get_instance_private (self);

  g_return_if_fail (DEVD_IS_DEVICE (self));

  if (g_strcmp0 (name, priv->name) != 0)
    {
      g_free (priv->name);
      priv->name = g_strdup (name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

/**
 * devd_device_create_client:
 * @self: a #DevdDevice
 *
 * Creates a new #DevdClient that can be used to connect to the device
 * in question.
 *
 * Not all devices are guaranteed to support multiple connections. For
 * example, a USB device connection over serial may require exclusive
 * access to the serial port and therefore, fail to connect if you try
 * to use multiple client simultaneously.
 *
 * See devd_client_connect_async() to establish a connection.
 *
 * Returns: (transfer full): a newly created #DevdClient
 *
 * Since: 3.28
 */
DevdClient *
devd_device_create_client (DevdDevice *self)
{
  DevdClient *ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_DEVICE (self), NULL);

  ret = DEVD_DEVICE_GET_CLASS (self)->create_client (self);

  DEVD_RETURN (ret);
}

/**
 * devd_device_get_id:
 * @self: a #DevdDevice
 *
 * Gets the identifier for the device.
 *
 * Returns: a string identifying the device
 *
 * Since: 3.28
 */
const gchar *
devd_device_get_id (DevdDevice *self)
{
  DevdDevicePrivate *priv = devd_device_get_instance_private (self);

  g_return_val_if_fail (DEVD_IS_DEVICE (self), NULL);

  return priv->id;
}

DevdDeviceKind
devd_device_get_kind (DevdDevice *self)
{
  DevdDevicePrivate *priv = devd_device_get_instance_private (self);

  g_return_val_if_fail (DEVD_IS_DEVICE (self), 0);

  return priv->kind;
}

void
devd_device_set_kind (DevdDevice     *self,
                      DevdDeviceKind  kind)
{
  DevdDevicePrivate *priv = devd_device_get_instance_private (self);

  g_return_if_fail (DEVD_IS_DEVICE (self));

  if (priv->kind != kind)
    {
      priv->kind = kind;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_KIND]);
    }
}
