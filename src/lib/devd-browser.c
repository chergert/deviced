/* devd-browser.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "devd-browser"

#include "config.h"

#include <avahi-gobject/ga-client.h>
#include <avahi-gobject/ga-service-browser.h>
#include <avahi-gobject/ga-service-resolver.h>
#include <gio/gio.h>

#include "devd-browser.h"
#include "devd-device.h"
#include "devd-enums.h"
#include "devd-macros.h"
#include "devd-network-device.h"
#include "devd-tls-certificate.h"
#include "devd-trace.h"

/**
 * SECTION:devd-browser
 * @title: DevdBrowser
 * @short_description: A browser to discover deviced nodes
 *
 * The #DevdBrowser is the toplevel object used by consumers of the
 * libdeviced library. It scan scan the local network segments for
 * nodes implementing the deviced protocol.
 *
 * After creating a #DevdBrowser, use devd_browser_load_async() to
 * discover available nodes. #DevdBrowser::device-added and
 * #DevdBrowser::device-removed will be emitted in reaction to network
 * and device changes.
 *
 * Since: 3.28
 */

typedef struct
{
  /*
   * We use the avahi client to listen for mDNS changes on the local
   * network segments. When we see a _deviced._tcp service appear, that
   * is a deviced peer that we can pontentially connect to. We convert
   * that to into a DevdDevice that the consumer can use to connect.
   *
   * The avahi_service_browser is the persistent device monitor using
   * avahi_client (or connection to the Avahi daemon).
   */
  GaClient *avahi_client;
  GaServiceBrowser *avahi_service_browser;

  /*
   * An array of ResolverInfo that we are actively tracking for the resolved
   * IP Address and port, or failure (when we lose them).
   */
  GArray *resolvers;

  /*
   * The TLS certificate we should be using for peer communication. This is
   * (re)loaded or generated in devd_browser_load_async(). The peer will use
   * this to allow us followup connections without having to re-authorize on
   * the device.
   */
  GTlsCertificate *certificate;

  /*
   * Protection to ensure we only have load_async() called once. We
   * will raise a critical if they do.
   */
  guint loaded : 1;

  /*
   * Allow enable/disable what we accept as acceptable peers.
   */
  guint enable_ipv4 : 1;
  guint enable_ipv6 : 1;
} DevdBrowserPrivate;

typedef struct
{
  GaServiceResolver *resolver;
  DevdNetworkDevice *device;
} ResolverInfo;

G_DEFINE_TYPE_WITH_PRIVATE (DevdBrowser, devd_browser, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_CERTIFICATE,
  PROP_ENABLE_IPV4,
  PROP_ENABLE_IPV6,
  N_PROPS
};

enum {
  DEVICE_ADDED,
  DEVICE_REMOVED,
  N_SIGNALS
};

static GParamSpec *properties [N_PROPS];
static guint signals [N_SIGNALS];

static void
clear_resolver_info (gpointer data)
{
  ResolverInfo *info = data;

  g_clear_object (&info->resolver);
  g_clear_object (&info->device);
}

static DevdDeviceKind
parse_device_kind_string (const gchar *str)
{
  GEnumClass *klass = g_type_class_ref (DEVD_TYPE_DEVICE_KIND);
  GEnumValue *value = g_enum_get_value_by_nick (klass, str);
  DevdDeviceKind ret = DEVD_DEVICE_KIND_COMPUTER;

  if (value != NULL)
    ret = value->value;

  g_type_class_unref (klass);

  return ret;
}

static void
devd_browser_finalize (GObject *object)
{
  DevdBrowser *self = (DevdBrowser *)object;
  DevdBrowserPrivate *priv = devd_browser_get_instance_private (self);

  DEVD_ENTRY;

  g_clear_pointer (&priv->resolvers, g_array_unref);
  g_clear_object (&priv->avahi_client);
  g_clear_object (&priv->avahi_service_browser);
  g_clear_object (&priv->certificate);

  G_OBJECT_CLASS (devd_browser_parent_class)->finalize (object);

  DEVD_EXIT;
}

static void
devd_browser_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  DevdBrowser *self = DEVD_BROWSER (object);

  switch (prop_id)
    {
    case PROP_CERTIFICATE:
      g_value_set_object (value, devd_browser_get_certificate (self));
      break;

    case PROP_ENABLE_IPV4:
      g_value_set_boolean (value, devd_browser_get_enable_ipv4 (self));
      break;

    case PROP_ENABLE_IPV6:
      g_value_set_boolean (value, devd_browser_get_enable_ipv6 (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
devd_browser_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  DevdBrowser *self = DEVD_BROWSER (object);

  switch (prop_id)
    {
    case PROP_CERTIFICATE:
      devd_browser_set_certificate (self, g_value_get_object (value));
      break;

    case PROP_ENABLE_IPV4:
      devd_browser_set_enable_ipv4 (self, g_value_get_boolean (value));
      break;

    case PROP_ENABLE_IPV6:
      devd_browser_set_enable_ipv6 (self, g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
devd_browser_class_init (DevdBrowserClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = devd_browser_finalize;
  object_class->get_property = devd_browser_get_property;
  object_class->set_property = devd_browser_set_property;

  /**
   * DevdBrowser:certificate:
   *
   * The tls certificate to use when communicating with peers.
   *
   * Since: 3.28
   */
  properties [PROP_CERTIFICATE] =
    g_param_spec_object ("certificate",
                         "Certificate",
                         "The TLS client certifiate to use in communication",
                         G_TYPE_TLS_CERTIFICATE,
                         G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  properties [PROP_ENABLE_IPV4] =
    g_param_spec_boolean ("enable-ipv4",
                         "Enable IPv4",
                         "Discover peers from IPv4 addresses",
                         TRUE,
                         G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  properties [PROP_ENABLE_IPV6] =
    g_param_spec_boolean ("enable-ipv6",
                         "Enable IPv6",
                         "Discover peers from IPv6 addresses",
                         TRUE,
                         G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  /**
   * DevdBrowser::device-added:
   * @self: a #DevdBrowser
   * @device: a #DevdDevice
   *
   * The "device-added" signal is emitted when a device has been discovered.
   *
   * Since: 3.28
   */
  signals [DEVICE_ADDED] =
    g_signal_new ("device-added",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DevdBrowserClass, device_added),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__OBJECT,
                  G_TYPE_NONE, 1, DEVD_TYPE_DEVICE);
  g_signal_set_va_marshaller (signals [DEVICE_ADDED],
                              G_TYPE_FROM_CLASS (klass),
                              g_cclosure_marshal_VOID__OBJECTv);

  /**
   * DevdBrowser::device-removed:
   * @self: a #DevdBrowser
   * @device: a #DevdDevice
   *
   * The "device-removed" signal is emitted when a device is no longer
   * known to be available.
   *
   * Since: 3.28
   */
  signals [DEVICE_REMOVED] =
    g_signal_new ("device-removed",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DevdBrowserClass, device_removed),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__OBJECT,
                  G_TYPE_NONE, 1, DEVD_TYPE_DEVICE);
  g_signal_set_va_marshaller (signals [DEVICE_REMOVED],
                              G_TYPE_FROM_CLASS (klass),
                              g_cclosure_marshal_VOID__OBJECTv);
}

static void
devd_browser_init (DevdBrowser *self)
{
  DevdBrowserPrivate *priv = devd_browser_get_instance_private (self);

  DEVD_ENTRY;

  priv->enable_ipv4 = TRUE;
  priv->enable_ipv6 = FALSE;

  priv->resolvers = g_array_new (FALSE, FALSE, sizeof (ResolverInfo));
  g_array_set_clear_func (priv->resolvers, clear_resolver_info);

  DEVD_EXIT;
}

static void
devd_browser_load_certificate_worker (GTask        *task,
                                      gpointer      source_object,
                                      gpointer      task_data,
                                      GCancellable *cancellable)
{
  g_autofree gchar *public_path = NULL;
  g_autofree gchar *private_path = NULL;
  g_autoptr(GTlsCertificate) certificate = NULL;
  g_autoptr(GError) error = NULL;

  DEVD_ENTRY;

  g_assert (G_IS_TASK (task));
  g_assert (DEVD_IS_BROWSER (source_object));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  public_path = g_build_filename (g_get_user_config_dir (), "deviced", "public.pem", NULL);
  private_path = g_build_filename (g_get_user_config_dir (), "deviced", "private.pem", NULL);

  if (g_file_test (public_path, G_FILE_TEST_IS_REGULAR) &&
      g_file_test (private_path, G_FILE_TEST_IS_REGULAR))
    certificate = g_tls_certificate_new_from_files (public_path, private_path, &error);
  else
    certificate = devd_tls_certificate_new_generate (public_path, private_path, "None", "deviced", cancellable, &error);

  if (certificate == NULL)
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_pointer (task, g_steal_pointer (&certificate), g_object_unref);

  DEVD_EXIT;
}

static void
devd_browser_load_certificate_async (DevdBrowser         *self,
                                     GCancellable        *cancellable,
                                     GAsyncReadyCallback  callback,
                                     gpointer             user_data)
{
  DevdBrowserPrivate *priv = devd_browser_get_instance_private (self);
  g_autoptr(GTask) task = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_BROWSER (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_browser_load_certificate_async);

  if (priv->certificate != NULL)
    g_task_return_pointer (task, g_object_ref (priv->certificate), g_object_unref);
  else
    g_task_run_in_thread (task, devd_browser_load_certificate_worker);

  DEVD_EXIT;
}

static gboolean
devd_browser_load_certificate_finish (DevdBrowser   *self,
                                      GAsyncResult  *result,
                                      GError       **error)
{
  DevdBrowserPrivate *priv = devd_browser_get_instance_private (self);
  g_autoptr(GTlsCertificate) ret = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_BROWSER (self));
  g_assert (G_IS_TASK (result));

  ret = g_task_propagate_pointer (G_TASK (result), error);

  if (priv->certificate == NULL && ret != NULL)
    priv->certificate = g_object_ref (ret);

  DEVD_RETURN (ret != NULL);
}

static void
devd_browser_resolver_found_cb (DevdBrowser         *self,
                                AvahiIfIndex         iface,
                                GaProtocol           protocol,
                                const gchar         *name,
                                const gchar         *type,
                                const gchar         *domain,
                                const gchar         *host_name,
                                const AvahiAddress  *address,
                                gint                 port,
                                AvahiStringList     *txt,
                                GaLookupResultFlags  flags,
                                GaServiceResolver   *resolver)
{
  DevdBrowserPrivate *priv = devd_browser_get_instance_private (self);
  g_autoptr(DevdNetworkDevice) added = NULL;
  g_autoptr(DevdNetworkDevice) removed = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_BROWSER (self));
  g_assert (type != NULL);
  g_assert (g_str_equal (type, "_deviced._tcp"));
  g_assert (address != NULL);
  g_assert (port > 0);
  g_assert (IS_GA_SERVICE_RESOLVER (resolver));

  for (guint i = 0; i < priv->resolvers->len; i++)
    {
      ResolverInfo *info = &g_array_index (priv->resolvers, ResolverInfo, i);

      if (info->resolver == resolver)
        {
          g_autoptr(GInetSocketAddress) isockaddr = NULL;
          g_autoptr(GInetAddress) iaddr = NULL;
          g_autofree gchar *idstr = NULL;
          DevdDeviceKind kind = DEVD_DEVICE_KIND_COMPUTER;

          for (AvahiStringList *iter = txt; iter != NULL; iter = iter->next)
            {
              if (g_utf8_validate ((const gchar *)&iter->text[0], iter->size, NULL))
                {
                  g_autofree gchar *entry = g_strndup ((const gchar *)&iter->text[0], iter->size);
                  g_auto(GStrv) parts = g_strsplit (entry, "=", 2);

                  if (devd_str_equal0 (parts[0], "device-kind"))
                    kind = parse_device_kind_string (parts[1]);
                }
            }

          switch (protocol)
            {
            case AVAHI_PROTO_INET:
              if (priv->enable_ipv4)
                iaddr = g_inet_address_new_from_bytes ((const guint8 *)&address->data.ipv4.address,
                                                       G_SOCKET_FAMILY_IPV4);
              break;

            case AVAHI_PROTO_INET6:
              if (priv->enable_ipv6)
                iaddr = g_inet_address_new_from_bytes (address->data.ipv6.address,
                                                       G_SOCKET_FAMILY_IPV6);
              break;

            case AVAHI_PROTO_UNSPEC:
            default:
              break;
            }

          if (iaddr == NULL)
            continue;

          isockaddr = g_object_new (G_TYPE_INET_SOCKET_ADDRESS,
                                    "address", iaddr,
                                    "port", port,
                                    NULL);

          if (info->device != NULL)
            removed = g_steal_pointer (&info->device);

          idstr = g_socket_connectable_to_string (G_SOCKET_CONNECTABLE (isockaddr));

          info->device = g_object_new (DEVD_TYPE_NETWORK_DEVICE,
                                       "id", idstr,
                                       "address", isockaddr,
                                       "certificate", priv->certificate,
                                       "kind", kind,
                                       "name", name,
                                       NULL);
          added = g_object_ref (info->device);

          break;
        }
    }

  if (removed != NULL)
    g_signal_emit (self, signals [DEVICE_REMOVED], 0, removed);

  if (added != NULL)
    g_signal_emit (self, signals [DEVICE_ADDED], 0, added);

  DEVD_EXIT;
}

static void
devd_browser_resolver_failure_cb (DevdBrowser       *self,
                                  const GError      *error,
                                  GaServiceResolver *resolver)
{
  DevdBrowserPrivate *priv = devd_browser_get_instance_private (self);

  g_assert (DEVD_IS_BROWSER (self));
  g_assert (error != NULL);
  g_assert (IS_GA_SERVICE_RESOLVER (resolver));

  for (guint i = 0; i < priv->resolvers->len; i++)
    {
      ResolverInfo *info = &g_array_index (priv->resolvers, ResolverInfo, i);

      if (info->resolver == resolver)
        {
          g_autoptr(DevdNetworkDevice) device = g_steal_pointer (&info->device);

          g_array_remove_index (priv->resolvers, i);

          if (device != NULL)
            g_signal_emit (self, signals [DEVICE_REMOVED], 0, device);

          break;
        }
    }
}

static void
devd_browser_new_service_cb (DevdBrowser         *self,
                             gint                 interface,
                             GaProtocol           protocol,
                             const gchar         *name,
                             const gchar         *type,
                             const gchar         *domain,
                             GaLookupResultFlags  flags,
                             GaServiceBrowser    *browser)
{
  DevdBrowserPrivate *priv = devd_browser_get_instance_private (self);
  GaServiceResolver *resolver;
  g_autoptr(GError) error = NULL;
  ResolverInfo info = {0};

  DEVD_ENTRY;

  g_assert (DEVD_IS_BROWSER (self));
  g_assert (IS_GA_SERVICE_BROWSER (browser));
  g_assert (type != NULL);
  g_assert (g_str_equal (type, "_deviced._tcp"));

  DEVD_TRACE_MSG ("interface=%d protocol=%d name=%s type=%s domain=%s flags=%d",
                  interface, protocol, name, type, domain, flags);

  /* Maybe we just disposed */
  if (priv->avahi_client == NULL)
    DEVD_EXIT;

  /*
   * When we get notified of a new service, we still don't have all the
   * information we need. We still need to resolve the TXT record for
   * the machine-id, and the address/port number.
   *
   * To do this, we use a GAServiceResolver. We may resolve the same
   * node multiple times, but we can't know that for sure until we get
   * the machine-id from the TXT record.
   */

  resolver = ga_service_resolver_new (interface, protocol, name, type, domain,
                                      GA_PROTOCOL_UNSPEC, GA_LOOKUP_NO_FLAGS);

  if (!ga_service_resolver_attach (resolver, priv->avahi_client, &error))
    {
      g_warning ("Failed to attach service resolver to client: %s", error->message);
      g_clear_object (&resolver);
      DEVD_EXIT;
    }

  g_signal_connect_object (resolver,
                           "found",
                           G_CALLBACK (devd_browser_resolver_found_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_signal_connect_object (resolver,
                           "failure",
                           G_CALLBACK (devd_browser_resolver_failure_cb),
                           self,
                           G_CONNECT_SWAPPED);

  info.resolver = g_steal_pointer (&resolver);
  info.device = NULL;

  g_array_append_val (priv->resolvers, info);

  DEVD_EXIT;
}

static void
devd_browser_removed_service_cb (DevdBrowser         *self,
                                 gint                 interface,
                                 GaProtocol           protocol,
                                 const gchar         *name,
                                 const gchar         *type,
                                 const gchar         *domain,
                                 GaLookupResultFlags  flags,
                                 GaServiceBrowser    *browser)
{
  DEVD_ENTRY;

  g_assert (DEVD_IS_BROWSER (self));
  g_assert (IS_GA_SERVICE_BROWSER (browser));
  g_assert (type != NULL);
  g_assert (g_str_equal (type, "_deviced._tcp"));

  DEVD_TRACE_MSG ("interface=%d protocol=%d name=%s type=%s domain=%s flags=%d",
                  interface, protocol, name, type, domain, flags);

  DEVD_EXIT;
}

static void
devd_browser_load_cb (GObject      *object,
                      GAsyncResult *result,
                      gpointer      user_data)
{
  DevdBrowser *self = (DevdBrowser *)object;
  DevdBrowserPrivate *priv = devd_browser_get_instance_private (self);
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_BROWSER (self));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!devd_browser_load_certificate_finish (self, result, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  priv->avahi_client = ga_client_new (GA_CLIENT_FLAG_NO_FLAGS);

  if (!ga_client_start (priv->avahi_client, &error))
    {
      g_clear_object (&priv->avahi_client);
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  priv->avahi_service_browser = ga_service_browser_new ("_deviced._tcp");

  if (!ga_service_browser_attach (priv->avahi_service_browser, priv->avahi_client, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  g_signal_connect_object (priv->avahi_service_browser,
                           "new-service",
                           G_CALLBACK (devd_browser_new_service_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_signal_connect_object (priv->avahi_service_browser,
                           "removed-service",
                           G_CALLBACK (devd_browser_removed_service_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_task_return_boolean (task, TRUE);

  DEVD_EXIT;
}

/**
 * devd_browser_load_async:
 * @self: a #DevdBrowser
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: (nullable): a #GAsyncReadyCallback to execute upon completion
 *   of the asynchronous task.
 * @user_data: closure data for @callback
 *
 * Asynchronously loads data needed by the browser and then scans the
 * local network segments for Deviced nodes. The #DevdBrowser::device-added
 * and #DevdBrowser::device-removed signals will be emitted in reaction to
 * network changes.
 *
 * Call devd_browser_load_finish() to get the result of this operation.
 *
 * If you want to control the #GTlsCertificate to use, then call
 * devd_browser_set_certificate() before calling this function.
 *
 * Since: 3.28
 */
void
devd_browser_load_async (DevdBrowser         *self,
                         GCancellable        *cancellable,
                         GAsyncReadyCallback  callback,
                         gpointer             user_data)
{
  DevdBrowserPrivate *priv = devd_browser_get_instance_private (self);
  g_autoptr(GTask) task = NULL;

  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_BROWSER (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));
  g_return_if_fail (priv->loaded == FALSE);

  priv->loaded = TRUE;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_browser_load_async);

  devd_browser_load_certificate_async (self,
                                       cancellable,
                                       devd_browser_load_cb,
                                       g_steal_pointer (&task));

  DEVD_EXIT;
}

/**
 * devd_browser_load_finish:
 * @self: a #DevdBrowser
 * @result: a #GAsyncResult
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous load of the browser. This includes loading the
 * TLS certificate (or generating one) and then scanning the local network
 * segments for mDNS (Avahi) peers advertising Deviced nodes.
 *
 * Returns: %TRUE if successful; otherwise %FALSE and @error is set.
 *
 * Since: 3.28
 */
gboolean
devd_browser_load_finish (DevdBrowser   *self,
                          GAsyncResult  *result,
                          GError       **error)
{
  gboolean ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_BROWSER (self), FALSE);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), FALSE);

  ret = g_task_propagate_boolean (G_TASK (result), error);

  DEVD_RETURN (ret);
}

/**
 * devd_browser_new:
 *
 * Creates a new #DevdBrowser
 *
 * Returns: (transfer full): a #DevdBrowser
 *
 * Since: 3.28
 */
DevdBrowser *
devd_browser_new (void)
{
  return g_object_new (DEVD_TYPE_BROWSER, NULL);
}

/**
 * devd_browser_get_certificate:
 * @self: a #DevdBrowser
 *
 * Gets the #DevdBrowser:certificate property. This is used as the
 * client TLS certificate when connecting to peers.
 *
 * If unset, a certificate will be reloaded or generated when
 * devd_browser_load_async() is called.
 *
 * Returns: (transfer none) (nullable): a #GTlsCertificate or %NULL
 *
 * Since: 3.28
 */
GTlsCertificate *
devd_browser_get_certificate (DevdBrowser *self)
{
  DevdBrowserPrivate *priv = devd_browser_get_instance_private (self);

  g_return_val_if_fail (DEVD_IS_BROWSER (self), NULL);

  return priv->certificate;
}

/**
 * devd_browser_get_certificate_hash:
 * @self: a #DevdBrowser
 *
 * Gets a SHA256 hash for the #DevdBrowser:certificate property.
 *
 * Returns: (nullable): the hash or %NULL
 *
 * Since: 3.28
 */
gchar *
devd_browser_get_certificate_hash (DevdBrowser *self)
{
  DevdBrowserPrivate *priv = devd_browser_get_instance_private (self);

  g_return_val_if_fail (DEVD_IS_BROWSER (self), NULL);

  if (priv->certificate == NULL)
    return NULL;

  return devd_tls_certificate_get_hash (priv->certificate);
}


/**
 * devd_browser_set_certificate:
 * @self: a #DevdBrowser
 * @certificate: a #GTlsCertificate
 *
 * Sets the #GTlsCertificate to use for client communication.
 *
 * To ensure this is being used on all new connnections, you should set this
 * property before calling devd_browser_load_async().
 *
 * Since: 3.28
 */
void
devd_browser_set_certificate (DevdBrowser     *self,
                              GTlsCertificate *certificate)
{
  DevdBrowserPrivate *priv = devd_browser_get_instance_private (self);

  g_return_if_fail (DEVD_IS_BROWSER (self));
  g_return_if_fail (!certificate || G_IS_TLS_CERTIFICATE (certificate));

  if (g_set_object (&priv->certificate, certificate))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CERTIFICATE]);
}

/**
 * devd_browser_get_devices:
 * @self: a #DevdBrowser
 *
 * Gets a #GPtrArray of #DevdDevice that have been discovered.
 *
 * Returns: (transfer container) (element-type Deviced.Device): a #GPtrArray
 *   of #DevdDevice.
 *
 * Since: 3.28
 */
GPtrArray *
devd_browser_get_devices (DevdBrowser *self)
{
  DevdBrowserPrivate *priv = devd_browser_get_instance_private (self);
  g_autoptr(GPtrArray) ret = NULL;

  g_return_val_if_fail (DEVD_IS_BROWSER (self), NULL);

  ret = g_ptr_array_new_with_free_func (g_object_unref);

  for (guint i = 0; i < priv->resolvers->len; i++)
    {
      const ResolverInfo *info = &g_array_index (priv->resolvers, ResolverInfo, i);

      if (info->device != NULL)
        g_ptr_array_add (ret, g_object_ref (info->device));
    }

  return g_steal_pointer (&ret);
}

gboolean
devd_browser_get_enable_ipv6 (DevdBrowser *self)
{
  DevdBrowserPrivate *priv = devd_browser_get_instance_private (self);

  g_return_val_if_fail (DEVD_IS_BROWSER (self), FALSE);

  return priv->enable_ipv6;
}

void
devd_browser_set_enable_ipv4 (DevdBrowser *self,
                              gboolean     enable_ipv4)
{
  DevdBrowserPrivate *priv = devd_browser_get_instance_private (self);

  g_return_if_fail (DEVD_IS_BROWSER (self));

  enable_ipv4 = !!enable_ipv4;

  if (priv->enable_ipv4 != enable_ipv4)
    {
      priv->enable_ipv4 = enable_ipv4;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ENABLE_IPV4]);
    }
}

gboolean
devd_browser_get_enable_ipv4 (DevdBrowser *self)
{
  DevdBrowserPrivate *priv = devd_browser_get_instance_private (self);

  g_return_val_if_fail (DEVD_IS_BROWSER (self), FALSE);

  return priv->enable_ipv4;
}

void
devd_browser_set_enable_ipv6 (DevdBrowser *self,
                              gboolean     enable_ipv6)
{
  DevdBrowserPrivate *priv = devd_browser_get_instance_private (self);

  g_return_if_fail (DEVD_IS_BROWSER (self));

  enable_ipv6 = !!enable_ipv6;

  if (priv->enable_ipv6 != enable_ipv6)
    {
      priv->enable_ipv6 = enable_ipv6;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ENABLE_IPV6]);
    }
}
