/* devd-service.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "devd-service"

#include "config.h"

#include "devd-service.h"

typedef struct
{
  DevdClient *client;
} DevdServicePrivate;

enum {
  PROP_0,
  PROP_CLIENT,
  N_PROPS
};

enum {
  NOTIFICATION,
  N_SIGNALS
};

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (DevdService, devd_service, G_TYPE_OBJECT)

static GParamSpec *properties [N_PROPS];
static guint signals [N_SIGNALS];

static void
devd_service_client_notification_cb (DevdService *self,
                                     const gchar *method,
                                     GVariant    *params,
                                     DevdClient  *client)
{
  GQuark detail;

  g_assert (DEVD_IS_SERVICE (self));
  g_assert (method != NULL);
  g_assert (DEVD_IS_CLIENT (client));

  detail = g_quark_try_string (method);

  g_signal_emit (self, signals [NOTIFICATION], detail, method, params);
}

static void
devd_service_set_client (DevdService *self,
                         DevdClient  *client)
{
  DevdServicePrivate *priv = devd_service_get_instance_private (self);

  g_assert (DEVD_IS_SERVICE (self));
  g_assert (!client || DEVD_IS_CLIENT (client));

  if (client == NULL)
    {
      g_critical ("%s created without a service",
                  G_OBJECT_TYPE_NAME (self));
      return;
    }

  priv->client = g_object_ref (client);

  g_signal_connect_object (priv->client,
                           "notification",
                           G_CALLBACK (devd_service_client_notification_cb),
                           self,
                           G_CONNECT_SWAPPED);
}

static void
devd_service_finalize (GObject *object)
{
  DevdService *self = (DevdService *)object;
  DevdServicePrivate *priv = devd_service_get_instance_private (self);

  g_clear_object (&priv->client);

  G_OBJECT_CLASS (devd_service_parent_class)->finalize (object);
}

static void
devd_service_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  DevdService *self = DEVD_SERVICE (object);

  switch (prop_id)
    {
    case PROP_CLIENT:
      g_value_set_object (value, devd_service_get_client (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
devd_service_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  DevdService *self = DEVD_SERVICE (object);

  switch (prop_id)
    {
    case PROP_CLIENT:
      devd_service_set_client (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
devd_service_class_init (DevdServiceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = devd_service_finalize;
  object_class->get_property = devd_service_get_property;
  object_class->set_property = devd_service_set_property;

  properties [PROP_CLIENT] =
    g_param_spec_object ("client",
                         "Client",
                         "The client for the service to communicate over",
                         DEVD_TYPE_CLIENT,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  signals [NOTIFICATION] =
    g_signal_new ("notification",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED,
                  G_STRUCT_OFFSET (DevdServiceClass, notification),
                  NULL, NULL, NULL,
                  G_TYPE_NONE,
                  2,
                  G_TYPE_STRING | G_SIGNAL_TYPE_STATIC_SCOPE,
                  G_TYPE_VARIANT);
}

static void
devd_service_init (DevdService *self)
{
}

/**
 * devd_service_get_client:
 * @self: a #DevdService
 *
 * Gets the client that the service was initialized with.
 *
 * Returns: (transfer none): a #DevdClient
 *
 * Since: 3.28
 */
DevdClient *
devd_service_get_client (DevdService *self)
{
  DevdServicePrivate *priv = devd_service_get_instance_private (self);

  g_return_val_if_fail (DEVD_IS_SERVICE (self), NULL);

  return priv->client;
}

static void
devd_service_call_cb (GObject      *object,
                      GAsyncResult *result,
                      gpointer      user_data)
{
  DevdClient *client = (DevdClient *)object;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;

  g_assert (DEVD_IS_CLIENT (client));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!devd_client_call_finish (client, result, &reply, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_pointer (task,
                           g_steal_pointer (&reply),
                           (GDestroyNotify)g_variant_unref);
}

/**
 * devd_service_call_async:
 * @self: a #DevdService
 * @method: the RPC method name
 * @params: (transfer none) (nullable): the parameters for the method
 * @cancellable: (nullable): a #GCancellable or %NULL
 * @callback: a callback to execute upon completion
 * @user_data: closure data for @callback
 *
 * This is a wrapper around getting the client and calling
 * devd_client_call_async(). It's purpose is to allow service implementations
 * to have a bit less code by just calling this.
 *
 * Call devd_service_call_finish() to get the result.
 *
 * Since: 3.28
 */
void
devd_service_call_async (DevdService         *self,
                         const gchar         *method,
                         GVariant            *params,
                         GCancellable        *cancellable,
                         GAsyncReadyCallback  callback,
                         gpointer             user_data)
{
  DevdServicePrivate *priv = devd_service_get_instance_private (self);
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (DEVD_IS_SERVICE (self));
  g_return_if_fail (method != NULL);

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_service_call_async);

  if (priv->client == NULL)
    g_task_return_new_error (task,
                             G_IO_ERROR,
                             G_IO_ERROR_NOT_CONNECTED,
                             "No connection to client");
  else
    devd_client_call_async (priv->client,
                            method,
                            params,
                            cancellable,
                            devd_service_call_cb,
                            g_steal_pointer (&task));
}


/**
 * devd_service_call_finish:
 * @self: a #DevdService
 * @result: a #GAsyncResult provided to callback
 * @reply: (optional) (out): a location for a #GVariant
 * @error: a location for a #GError, or %NULL
 *
 * Gets the result of the RPC call.
 *
 * Returns: %TRUE if successful; otherwise %FALSE and @error is set.
 *   @reply is set to the reply from the peer, of provided.
 */
gboolean
devd_service_call_finish (DevdService   *self,
                          GAsyncResult  *result,
                          GVariant     **reply,
                          GError       **error)
{
  g_autoptr(GVariant) local_reply = NULL;
  g_autoptr(GError) local_error = NULL;
  gboolean ret;

  g_return_val_if_fail (DEVD_IS_SERVICE (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  local_reply = g_task_propagate_pointer (G_TASK (result), &local_error);
  ret = local_error == NULL;
  if (reply != NULL)
    *reply = g_steal_pointer (&local_reply);
  if (local_error != NULL)
    g_propagate_error (error, g_steal_pointer (&local_error));

  return ret;
}

/**
 * devd_service_new:
 * @service_type: a #GType of a DevdService
 * @service_name: the name of the service such as "org.gnome.foo"
 * @client: the #DevdClient to use for communicating
 * @error: a location for a #GError, or %NULL
 *
 * Creates a new #DevdService. @client will be checked to ensure it supports
 * @service_name, or @error will be set.
 *
 * Returns: (transfer full) (type Deviced.Service): a newly created service if
 *   supported by the client, otherwise %NULL and @error is set.
 */
gpointer
devd_service_new (GType         service_type,
                  const gchar  *service_name,
                  DevdClient   *client,
                  GError      **error)
{
  g_return_val_if_fail (service_name != NULL, NULL);
  g_return_val_if_fail (!client || DEVD_IS_CLIENT (client), NULL);

  if (!g_type_is_a (service_type, DEVD_TYPE_SERVICE) ||
      !G_TYPE_IS_INSTANTIATABLE (service_type))
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_INVAL,
                   "service_type %s is not a DevdService",
                   g_type_name (service_type));
      return NULL;
    }

  if (client == NULL)
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_NOT_CONNECTED,
                   "No client connection");
      return NULL;
    }

  if (!devd_client_has_service (client, service_name))
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_NOT_SUPPORTED,
                   "Client does not currently support service %s",
                   service_name);
      return NULL;
    }

  return g_object_new (service_type, "client", client, NULL);
}
