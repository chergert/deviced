/* devd-process-service.h
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "devd-service.h"
#include "devd-version-macros.h"

G_BEGIN_DECLS

#define DEVD_PROCESS_SERVICE_NAME "org.gnome.deviced.process"
#define DEVD_TYPE_PROCESS_SERVICE (devd_process_service_get_type())

DEVD_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (DevdProcessService, devd_process_service, DEVD, PROCESS_SERVICE, DevdService)

DEVD_AVAILABLE_IN_ALL
DevdProcessService *devd_process_service_new                     (DevdClient             *client,
                                                                  GError                **error);
DEVD_AVAILABLE_IN_ALL
void                devd_process_service_force_exit              (DevdProcessService     *self,
                                                                  const gchar            *identifier);
DEVD_AVAILABLE_IN_ALL
void                devd_process_service_send_signal             (DevdProcessService     *self,
                                                                  const gchar            *identifier,
                                                                  gint                    signum);
DEVD_AVAILABLE_IN_ALL
void                devd_process_service_wait_for_process_async  (DevdProcessService     *self,
                                                                  const gchar            *identifier,
                                                                  GCancellable           *cancellable,
                                                                  GAsyncReadyCallback     callback,
                                                                  gpointer                user_data);
DEVD_AVAILABLE_IN_ALL
gboolean            devd_process_service_wait_for_process_finish (DevdProcessService     *self,
                                                                  GAsyncResult           *result,
                                                                  gboolean               *exited,
                                                                  gint                   *exit_code,
                                                                  gint                   *term_sig,
                                                                  GError                **error);
DEVD_AVAILABLE_IN_ALL
void                devd_process_service_spawn_async             (DevdProcessService     *self,
                                                                  const gchar            *pty_id,
                                                                  const gchar * const    *argv,
                                                                  const gchar * const    *env,
                                                                  GCancellable           *cancellable,
                                                                  GAsyncReadyCallback     callback,
                                                                  gpointer                user_data);
DEVD_AVAILABLE_IN_ALL
gchar              *devd_process_service_spawn_finish            (DevdProcessService     *self,
                                                                  GAsyncResult           *result,
                                                                  GError                **error);
DEVD_AVAILABLE_IN_ALL
void                devd_process_service_destroy_pty_async       (DevdProcessService     *self,
                                                                  const gchar            *pty_id,
                                                                  GCancellable           *cancellable,
                                                                  GAsyncReadyCallback     callback,
                                                                  gpointer                user_data);
DEVD_AVAILABLE_IN_ALL
gboolean            devd_process_service_destroy_pty_finish      (DevdProcessService     *self,
                                                                  GAsyncResult           *result,
                                                                  GError                **error);
DEVD_AVAILABLE_IN_ALL
void                devd_process_service_create_pty_async        (DevdProcessService     *self,
                                                                  gint                    local_pty_fd,
                                                                  GCancellable           *cancellable,
                                                                  GAsyncReadyCallback     callback,
                                                                  gpointer                user_data);
DEVD_AVAILABLE_IN_ALL
gchar              *devd_process_service_create_pty_finish       (DevdProcessService     *self,
                                                                  GAsyncResult           *result,
                                                                  GError                **error);

G_END_DECLS
