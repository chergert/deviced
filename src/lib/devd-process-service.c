/* devd-process-service.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "devd-process-service"

#include "config.h"

#include <jsonrpc-glib.h>

#include "devd-macros.h"
#include "devd-process-service.h"
#include "devd-pty.h"
#include "devd-trace.h"

struct _DevdProcessService
{
  DevdService parent_instance;

  /*
   * This is a queue of WaitForProcess instances. We keep track of them
   * while we wait for a signal from the peer about the process exiting.
   */
  GQueue wait_for_process;

  /*
   * pty_map is a map of PTY identifier (as provided by the peer) to a local
   * PTY (DevdPty). When we receive PTY data from the peer we proxy that
   * data into the local PTY. When we notice data available on the local PTY,
   * we get that data (via a gsignal) and send it to the device.
   *
   * The key is the pty-id, the value is a DevdPty.
   */
  GHashTable *pty_map;
};

typedef struct
{
  gchar *identifier;
  gboolean exited;
  gint exit_code;
  gint term_sig;
} WaitForProces;

typedef struct
{
  gint local_pty_fd;
} CreatePtyTaskData;

G_DEFINE_TYPE (DevdProcessService, devd_process_service, DEVD_TYPE_SERVICE)

enum {
  PROCESS_EXITED,
  PROCESS_SIGNALED,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

static void
create_pty_task_data_free (CreatePtyTaskData *data)
{
  if (data->local_pty_fd != -1)
    {
      close (data->local_pty_fd);
      data->local_pty_fd = -1;
    }
  g_slice_free (CreatePtyTaskData, data);
}

static void
wait_for_process_free (gpointer data)
{
  WaitForProces *state = data;

  g_free (state->identifier);
  g_slice_free (WaitForProces, state);
}

static void
complete_waiters (DevdProcessService *self,
                  const gchar        *identifier,
                  gboolean            exited,
                  gint                exit_code,
                  gint                term_sig)
{
  GSList *matched = NULL;
  GList *iter;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (identifier != NULL);

  iter = self->wait_for_process.head;

  while (iter != NULL)
    {
      GTask *task = iter->data;
      WaitForProces *state = g_task_get_task_data (task);

      if (g_strcmp0 (state->identifier, identifier) == 0)
        {
          GList *tmp;

          state->exited = exited;
          state->exit_code = exit_code;
          state->term_sig = term_sig;

          matched = g_slist_prepend (matched, g_steal_pointer (&iter->data));

          tmp = iter;
          iter = iter->next;

          g_queue_delete_link (&self->wait_for_process, tmp);

          continue;
        }

      iter = iter->next;
    }

  for (const GSList *match = matched; match; match = match->next)
    {
      g_autoptr(GTask) task = match->data;

      g_task_return_boolean (task, TRUE);
    }

  g_slist_free (matched);

  DEVD_EXIT;
}

static void
cancel_waiters (DevdProcessService *self)
{
  GList *waiters;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));

  waiters = g_steal_pointer (&self->wait_for_process.head);
  self->wait_for_process.tail = NULL;
  self->wait_for_process.length = 0;

  for (const GList *iter = waiters; iter != NULL; iter = iter->next)
    {
      g_autoptr(GTask) task = iter->data;

      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_CANCELLED,
                               "The client was shut down");
    }

  g_list_free (waiters);

  DEVD_EXIT;
}

static void
devd_process_service_real_process_exited (DevdProcessService *self,
                                          const gchar        *identifier,
                                          gint                exit_code)
{
  complete_waiters (self, identifier, TRUE, exit_code, -1);
}

static void
devd_process_service_real_process_signaled (DevdProcessService *self,
                                            const gchar        *identifier,
                                            gint                term_sig)
{
  complete_waiters (self, identifier, FALSE, -1, term_sig);
}

static void
devd_process_service_process_exited_cb (DevdProcessService *self,
                                        const gchar        *method,
                                        GVariant           *params)
{
  const gchar *identifier;
  gint32 exit_code = 0;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (g_str_has_suffix (method, ".process-exited"));

  if (params == NULL)
    DEVD_EXIT;

  JSONRPC_MESSAGE_PARSE (params,
    "identifier", JSONRPC_MESSAGE_GET_STRING (&identifier),
    "exit-code", JSONRPC_MESSAGE_GET_INT32 (&exit_code)
  );

  if (identifier != NULL)
    g_signal_emit (self, signals [PROCESS_EXITED], 0, identifier, exit_code);

  DEVD_EXIT;
}

static void
devd_process_service_process_signaled_cb (DevdProcessService *self,
                                          const gchar        *method,
                                          GVariant           *params)
{
  const gchar *identifier;
  gint32 term_sig = 0;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (g_str_has_suffix (method, ".process-signaled"));

  if (params == NULL)
    DEVD_EXIT;

  JSONRPC_MESSAGE_PARSE (params,
    "identifier", JSONRPC_MESSAGE_GET_STRING (&identifier),
    "term-sig", JSONRPC_MESSAGE_GET_INT32 (&term_sig)
  );

  if (identifier != NULL)
    g_signal_emit (self, signals [PROCESS_SIGNALED], 0, identifier, term_sig);

  DEVD_EXIT;
}

static void
devd_process_service_pty_data_cb (DevdProcessService *self,
                                  const gchar        *method,
                                  GVariant           *params)
{
  g_autofree guchar *decoded = NULL;
  g_autoptr(GBytes) bytes = NULL;
  const gchar *pty = NULL;
  const gchar *data = NULL;
  DevdPty *pty_obj;
  gsize len = 0;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (g_str_has_suffix (method, ".pty-data"));

  if (params == NULL)
    DEVD_EXIT;

  JSONRPC_MESSAGE_PARSE (params,
    "pty", JSONRPC_MESSAGE_GET_STRING (&pty),
    "data", JSONRPC_MESSAGE_GET_STRING (&data)
  );

  if (!pty || !data || !(pty_obj = g_hash_table_lookup (self->pty_map, pty)))
    DEVD_EXIT;

  decoded = g_base64_decode (data, &len);
  bytes = g_bytes_new (g_steal_pointer (&decoded), len);

  devd_pty_write (pty_obj, bytes);

  DEVD_EXIT;
}

static void
devd_process_service_pty_handle_data_cb (DevdProcessService *self,
                                         GBytes            *bytes,
                                         DevdPty           *pty)
{
  g_autoptr(GVariant) params = NULL;
  g_autofree gchar *encoded = NULL;
  const gchar *pty_id;
  const guint8 *data;
  gsize len;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (bytes != NULL);
  g_assert (DEVD_IS_PTY (pty));

  pty_id = g_object_get_data (G_OBJECT (pty), "PTY_ID");

  data = g_bytes_get_data (bytes, &len);
  if (len > 0)
    encoded = g_base64_encode (data, len);

  params = JSONRPC_MESSAGE_NEW (
    "pty", JSONRPC_MESSAGE_PUT_STRING (pty_id),
    "data", JSONRPC_MESSAGE_PUT_STRING (encoded ?: "")
  );

  devd_service_call_async (DEVD_SERVICE (self),
                           "org.gnome.deviced.process.write-pty",
                           params,
                           NULL, NULL, NULL);
}

static void
devd_process_service_dispose (GObject *object)
{
  DevdProcessService *self = (DevdProcessService *)object;

  DEVD_ENTRY;

  cancel_waiters (self);

  g_clear_pointer (&self->pty_map, g_hash_table_unref);

  DEVD_EXIT;
}

static void
devd_process_service_class_init (DevdProcessServiceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = devd_process_service_dispose;

  /**
   * DevdProcessService::process-exited:
   * @self: a #DevdProcessService
   * @identifier: the process identifier
   * @exit_code: the exit code for the process
   *
   * This signal is emitted when the peer has notified us that a process which
   * was spawned by a client to the device has exited.
   *
   * You may be notified of processes not spawned by your client, as all clients
   * are notified.
   *
   * Since: 3.28
   */
  signals [PROCESS_EXITED] =
    g_signal_new_class_handler ("process-exited",
                                G_TYPE_FROM_CLASS (klass),
                                G_SIGNAL_RUN_LAST,
                                G_CALLBACK (devd_process_service_real_process_exited),
                                NULL, NULL, NULL,
                                G_TYPE_NONE, 2, G_TYPE_STRING, G_TYPE_INT);

  /**
   * DevdProcessService::process-signaled:
   * @self: a #DevdProcessService
   * @identifier: the process identifier
   * @term_sig: the signal that terminated the process
   *
   * This signal is emitted when the peer has notified us that a process which
   * was spawned by a client to the device has terminated by signal.
   *
   * You may be notified of processes not spawned by your client, as all clients
   * are notified.
   *
   * Since: 3.28
   */
  signals [PROCESS_SIGNALED] =
    g_signal_new_class_handler ("process-signaled",
                                G_TYPE_FROM_CLASS (klass),
                                G_SIGNAL_RUN_LAST,
                                G_CALLBACK (devd_process_service_real_process_signaled),
                                NULL, NULL, NULL,
                                G_TYPE_NONE, 2, G_TYPE_STRING, G_TYPE_INT);
}

static void
devd_process_service_init (DevdProcessService *self)
{
  DEVD_ENTRY;

  self->pty_map = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);

  g_queue_init (&self->wait_for_process);

  g_signal_connect (self,
                    "notification::"DEVD_PROCESS_SERVICE_NAME".process-exited",
                    G_CALLBACK (devd_process_service_process_exited_cb),
                    NULL);

  g_signal_connect (self,
                    "notification::"DEVD_PROCESS_SERVICE_NAME".process-signaled",
                    G_CALLBACK (devd_process_service_process_signaled_cb),
                    NULL);

  g_signal_connect (self,
                    "notification::"DEVD_PROCESS_SERVICE_NAME".pty-data",
                    G_CALLBACK (devd_process_service_pty_data_cb),
                    NULL);

  DEVD_EXIT;
}

/**
 * devd_process_service_new:
 * @client: a #DevdClient
 * @error: a location for a #GError or %NULL
 *
 * Creates a new #DevdProcessService.
 *
 * If @client does not support the process service, %NULL is returned
 * and @error is set.
 *
 * Returns: (transfer full): a #DevdProcessService or %NULL
 */
DevdProcessService *
devd_process_service_new (DevdClient  *client,
                          GError     **error)
{
  return devd_service_new (DEVD_TYPE_PROCESS_SERVICE,
                           DEVD_PROCESS_SERVICE_NAME,
                           client,
                           error);
}

void
devd_process_service_wait_for_process_async (DevdProcessService  *self,
                                             const gchar         *identifier,
                                             GCancellable        *cancellable,
                                             GAsyncReadyCallback  callback,
                                             gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  WaitForProces *task_data;

  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_PROCESS_SERVICE (self));
  g_return_if_fail (identifier != NULL);

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_process_service_wait_for_process_async);

  task_data = g_slice_new0 (WaitForProces);
  task_data->identifier = g_strdup (identifier);
  task_data->exit_code = -1;
  task_data->term_sig = -1;
  g_task_set_task_data (task, task_data, wait_for_process_free);

  g_queue_push_tail (&self->wait_for_process, g_steal_pointer (&task));

  DEVD_EXIT;
}

gboolean
devd_process_service_wait_for_process_finish (DevdProcessService  *self,
                                              GAsyncResult        *result,
                                              gboolean            *exited,
                                              gint                *exit_code,
                                              gint                *term_sig,
                                              GError             **error)
{
  WaitForProces *task_data;
  gboolean ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_PROCESS_SERVICE (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);
  g_return_val_if_fail (g_task_is_valid (G_TASK (result), self), FALSE);

  ret = g_task_propagate_boolean (G_TASK (result), error);
  task_data = g_task_get_task_data (G_TASK (result));

  if (exited != NULL)
    *exited = task_data->exited;

  if (exit_code != NULL)
    *exit_code = task_data->exit_code;

  if (term_sig != NULL)
    *term_sig = task_data->term_sig;

  DEVD_RETURN (ret);
}

static void
devd_process_service_monitor_pty (DevdProcessService *self,
                                 const gchar        *pty_id,
                                 DevdPty            *pty)
{
  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (pty_id != NULL);
  g_assert (DEVD_IS_PTY (pty));

  g_signal_connect_object (pty,
                           "handle-data",
                           G_CALLBACK (devd_process_service_pty_handle_data_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_object_set_data_full (G_OBJECT (pty), "PTY_ID", g_strdup (pty_id), g_free);
  g_hash_table_insert (self->pty_map, g_strdup (pty_id), g_object_ref (pty));

  DEVD_EXIT;
}

static void
devd_process_service_create_pty_cb (GObject      *object,
                                   GAsyncResult *result,
                                   gpointer      user_data)
{
  DevdProcessService *self = (DevdProcessService *)object;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(DevdPty) pty_obj = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  CreatePtyTaskData *task_data;
  const gchar *pty = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!devd_service_call_finish (DEVD_SERVICE (self), result, &reply, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  if (!JSONRPC_MESSAGE_PARSE (reply, "pty", JSONRPC_MESSAGE_GET_STRING (&pty)))
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_FAILED,
                               "Incorrect result from device");
      DEVD_EXIT;
    }

  self = g_task_get_source_object (task);
  task_data = g_task_get_task_data (task);

  pty_obj = devd_pty_new_for_foreign (task_data->local_pty_fd);
  task_data->local_pty_fd = -1;

  devd_process_service_monitor_pty (self, pty, pty_obj);

  g_task_return_pointer (task, g_strdup (pty), g_free);

  DEVD_EXIT;
}

/**
 * devd_process_service_create_pty_async:
 * @self: a #DevdProcessService
 * @local_pty_fd: a PTY slave fd to proxy PTY data to
 * @cancellable: (nullable): a #GCancellable or %NULL
 * @callback: a calllback to execute upon completion
 * @user_data: closure data for @calllback
 *
 * Creates a new PTY on the device and proxies PTY data to @local_pty_fd.
 *
 * You may want to use the result of this asynchronous operation to pass the
 * PTY token to other client operations. That will result in the PTY data being
 * delivered to @local_pty_fd.
 *
 * Since: 3.28
 */
void
devd_process_service_create_pty_async (DevdProcessService  *self,
                                       gint                 local_pty_fd,
                                       GCancellable        *cancellable,
                                       GAsyncReadyCallback  callback,
                                       gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  CreatePtyTaskData *task_data;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_process_service_create_pty_async);

  task_data = g_slice_new (CreatePtyTaskData);
  task_data->local_pty_fd = dup (local_pty_fd);
  g_task_set_task_data (task,
                        task_data,
                        (GDestroyNotify)create_pty_task_data_free);

  if (task_data->local_pty_fd == -1)
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_FAILED,
                               "Failed to dup PTY fd");
      DEVD_EXIT;
    }

  devd_service_call_async (DEVD_SERVICE (self),
                             "org.gnome.deviced.process.create-pty",
                             NULL,
                             cancellable,
                             devd_process_service_create_pty_cb,
                             g_steal_pointer (&task));

  DEVD_EXIT;
}

/**
 * devd_process_service_create_pty_finish:
 * @self: a #DevdProcessService
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Completes a request to create a new PTY on the remote device.
 *
 * The resulting PTY identifier is a unique string representing the PTY.
 * Data will be proxied to the @local_pty_fd provided in
 * devd_process_service_create_pty_async(), however you may need this PTY token
 * to pass to other operations to reference the specific PTY instance.
 *
 * Returns: (transfer full): the pty identifier or %NULL
 */
gchar *
devd_process_service_create_pty_finish (DevdProcessService  *self,
                                        GAsyncResult        *result,
                                        GError             **error)
{
  gchar *ret;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (G_IS_TASK (result));

  ret = g_task_propagate_pointer (G_TASK (result), error);

  DEVD_RETURN (ret);
}

static void
devd_process_service_destroy_pty_cb (GObject      *object,
                                     GAsyncResult *result,
                                     gpointer      user_data)
{
  DevdProcessService *self = (DevdProcessService *)object;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  const gchar *pty_id;
  DevdPty *pty;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!devd_service_call_finish (DEVD_SERVICE (self), result, &reply, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);

  self = g_task_get_source_object (task);
  pty_id = g_task_get_task_data (task);

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (pty_id != NULL);

  pty = g_hash_table_lookup (self->pty_map, pty_id);
  if (pty != NULL)
    devd_pty_close (pty);

  g_hash_table_remove (self->pty_map, pty_id);

  DEVD_EXIT;
}

/**
 * devd_process_service_destroy_pty_async:
 * @self: a #DevdProcessService
 * @pty_id: the identifier of a previously created PTY
 * @cancellable: (nullable): a #GCancellable or %NULL
 * @callback: (nullable): a callback to execute upon completion
 * @user_data: closure data for @callback
 *
 * Requests that the device close a PTY device that was opened. This will
 * stop PTY data being proxied to the FD that was provided when creating the
 * PTY device.
 *
 * Since: 3.28
 */
void
devd_process_service_destroy_pty_async (DevdProcessService  *self,
                                        const gchar         *pty_id,
                                        GCancellable        *cancellable,
                                        GAsyncReadyCallback  callback,
                                        gpointer             user_data)
{
  g_autoptr(GVariant) params = NULL;
  g_autoptr(GTask) task = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (pty_id != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_process_service_destroy_pty_async);
  g_task_set_task_data (task, g_strdup (pty_id), g_free);

  params = JSONRPC_MESSAGE_NEW ("pty", JSONRPC_MESSAGE_PUT_STRING (pty_id));

  devd_service_call_async (DEVD_SERVICE (self),
                             "org.gnome.deviced.process.destroy-pty",
                             params,
                             cancellable,
                             devd_process_service_destroy_pty_cb,
                             g_steal_pointer (&task));

  DEVD_EXIT;
}

/**
 * devd_process_service_destroy_pty_finish:
 * @self: a #DevdProcessService
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous request to destroy a PTY device.
 *
 * Returns: %TRUE if successful; otherwise %FALSE and @error is set.
 *
 * Since: 3.28
 */
gboolean
devd_process_service_destroy_pty_finish (DevdProcessService  *self,
                                         GAsyncResult        *result,
                                         GError             **error)
{
  gboolean ret;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (G_IS_TASK (result));

  ret = g_task_propagate_boolean (G_TASK (result), error);

  DEVD_RETURN (ret);
}

static void
devd_process_service_spawn_cb (GObject      *object,
                               GAsyncResult *result,
                               gpointer      user_data)
{
  DevdProcessService *self = (DevdProcessService *)object;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!devd_service_call_finish (DEVD_SERVICE (self), result, &reply, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  if (!g_variant_is_of_type (reply, G_VARIANT_TYPE_STRING))
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_INVAL,
                               "Invalid reply from peer");
      DEVD_EXIT;
    }

  g_task_return_pointer (task,
                         g_strdup (g_variant_get_string (reply, NULL)),
                         g_free);

  DEVD_EXIT;
}

void
devd_process_service_spawn_async (DevdProcessService  *self,
                                  const gchar         *pty_id,
                                  const gchar * const *argv,
                                  const gchar * const *env,
                                  GCancellable        *cancellable,
                                  GAsyncReadyCallback  callback,
                                  gpointer             user_data)
{
  static const gchar *empty[] = { NULL };
  g_autoptr(GTask) task = NULL;
  GVariantDict dict;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (pty_id != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  if (argv == NULL)
    argv = empty;

  if (env == NULL)
    env = empty;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_process_service_spawn_async);

  g_variant_dict_init (&dict, NULL);
  g_variant_dict_insert (&dict, "pty", "s", pty_id);
  g_variant_dict_insert (&dict, "argv", "^as", argv);
  g_variant_dict_insert (&dict, "env", "^as", env);

  devd_service_call_async (DEVD_SERVICE (self),
                           "org.gnome.deviced.process.spawn",
                           g_variant_dict_end (&dict),
                           cancellable,
                           devd_process_service_spawn_cb,
                           g_steal_pointer (&task));

  DEVD_EXIT;
}

gchar *
devd_process_service_spawn_finish (DevdProcessService  *self,
                                   GAsyncResult        *result,
                                   GError             **error)
{
  gchar *ret;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (G_IS_TASK (result));

  ret = g_task_propagate_pointer (G_TASK (result), error);

  DEVD_RETURN (ret);
}

/**
 * devd_process_service_force_exit:
 * @self: a #DevdProcessService
 * @identifier: the process identifier
 *
 * Requests that the device terminate process identified by @identifier.
 *
 * @identifier should be a process that the client was previously notified of.
 *
 * As these requests are inherently racey, as signals are involved, no async
 * pair is provided for this. Calling this function only ensures an attempt
 * to force exit the program.
 *
 * Since: 3.28
 */
void
devd_process_service_force_exit (DevdProcessService *self,
                                 const gchar        *identifier)
{
  g_autoptr(GVariant) params = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (identifier != NULL);

  params = JSONRPC_MESSAGE_NEW ("identifier", JSONRPC_MESSAGE_PUT_STRING (identifier));

  devd_service_call_async (DEVD_SERVICE (self),
                           "org.gnome.deviced.process.force-exit",
                           params,
                           NULL, NULL, NULL);

  DEVD_EXIT;
}

/**
 * devd_process_service_send_signal:
 * @self: a #DevdProcessService
 * @identifier: the process identifier
 * @signum: the signal number
 *
 * Requests that the device send signal @signum to the process known
 * by @identifier.
 *
 * @identifier should be a process that the client was previously notified of.
 *
 * As these requests are inherently racey, as signals are involved, no async
 * pair is provided for this. Calling this function only ensures an attempt
 * to send the signal to the program.
 *
 * Since: 3.28
 */
void
devd_process_service_send_signal (DevdProcessService *self,
                                  const gchar        *identifier,
                                  gint                signum)
{
  g_autoptr(GVariant) params = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_PROCESS_SERVICE (self));
  g_assert (identifier != NULL);

  params = JSONRPC_MESSAGE_NEW (
    "identifier", JSONRPC_MESSAGE_PUT_STRING (identifier),
    "signal", JSONRPC_MESSAGE_PUT_INT32 (signum)
  );

  devd_service_call_async (DEVD_SERVICE (self),
                             "org.gnome.deviced.process.send-signal",
                             params,
                             NULL, NULL, NULL);

  DEVD_EXIT;
}
