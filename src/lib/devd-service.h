/* devd-service.h
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#if !defined(DEVICED_INSIDE) && !defined(DEVICED_COMPILATION)
# error "Only <libdeviced.h> can be included directly."
#endif

#include <gio/gio.h>

#include "devd-client.h"
#include "devd-version-macros.h"

G_BEGIN_DECLS

#define DEVD_TYPE_SERVICE (devd_service_get_type())

DEVD_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (DevdService, devd_service, DEVD, SERVICE, GObject)

struct _DevdServiceClass
{
  GObjectClass parent_class;

  void (*notification) (DevdService *self,
                        const gchar *method,
                        GVariant    *params);

  /*< private >*/
  gpointer _reserved[16];
};

DEVD_AVAILABLE_IN_ALL
gpointer    devd_service_new         (GType                 service_type,
                                      const gchar          *service_name,
                                      DevdClient           *client,
                                      GError              **error);
DEVD_AVAILABLE_IN_ALL
DevdClient *devd_service_get_client  (DevdService          *self);
DEVD_AVAILABLE_IN_ALL
void        devd_service_call_async  (DevdService          *self,
                                      const gchar          *method,
                                      GVariant             *params,
                                      GCancellable         *cancellable,
                                      GAsyncReadyCallback   callback,
                                      gpointer              user_data);
DEVD_AVAILABLE_IN_ALL
gboolean    devd_service_call_finish (DevdService          *self,
                                      GAsyncResult         *result,
                                      GVariant            **reply,
                                      GError              **error);

G_END_DECLS
