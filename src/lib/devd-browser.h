/* devd-browser.h
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(DEVICED_INSIDE) && !defined(DEVICED_COMPILATION)
# error "Only <libdeviced.h> can be included directly."
#endif

#include <gio/gio.h>

#include "devd-device.h"
#include "devd-version-macros.h"

G_BEGIN_DECLS

#define DEVD_TYPE_BROWSER (devd_browser_get_type())

DEVD_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (DevdBrowser, devd_browser, DEVD, BROWSER, GObject)

struct _DevdBrowserClass
{
  GObjectClass parent;

  void (*device_added)   (DevdBrowser *self,
                          DevdDevice  *device);
  void (*device_removed) (DevdBrowser *self,
                          DevdDevice  *device);

  /*< private >*/
  gpointer _reserved[16];
};

DEVD_AVAILABLE_IN_ALL
DevdBrowser     *devd_browser_new                  (void);
DEVD_AVAILABLE_IN_ALL
gboolean         devd_browser_get_enable_ipv4      (DevdBrowser          *self);
DEVD_AVAILABLE_IN_ALL
void             devd_browser_set_enable_ipv4      (DevdBrowser          *self,
                                                    gboolean              enable_ipv4);
DEVD_AVAILABLE_IN_ALL
gboolean         devd_browser_get_enable_ipv6      (DevdBrowser          *self);
DEVD_AVAILABLE_IN_ALL
void             devd_browser_set_enable_ipv6      (DevdBrowser          *self,
                                                    gboolean              enable_ipv6);
DEVD_AVAILABLE_IN_ALL
GTlsCertificate *devd_browser_get_certificate      (DevdBrowser          *self);
DEVD_AVAILABLE_IN_ALL
gchar           *devd_browser_get_certificate_hash (DevdBrowser          *self);
DEVD_AVAILABLE_IN_ALL
void             devd_browser_set_certificate      (DevdBrowser          *self,
                                                    GTlsCertificate      *certificate);
DEVD_AVAILABLE_IN_ALL
void             devd_browser_load_async           (DevdBrowser          *self,
                                                    GCancellable         *cancellable,
                                                    GAsyncReadyCallback   callback,
                                                    gpointer              user_data);
DEVD_AVAILABLE_IN_ALL
gboolean         devd_browser_load_finish          (DevdBrowser          *self,
                                                    GAsyncResult         *result,
                                                    GError              **error);
DEVD_AVAILABLE_IN_ALL
GPtrArray       *devd_browser_get_devices          (DevdBrowser          *self);

G_END_DECLS
