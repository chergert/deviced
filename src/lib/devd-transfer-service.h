/* devd-transfer-service.h
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "devd-service.h"
#include "devd-version-macros.h"

G_BEGIN_DECLS

#define DEVD_TRANSFER_SERVICE_NAME "org.gnome.deviced.transfers"
#define DEVD_TYPE_TRANSFER_SERVICE (devd_transfer_service_get_type())

DEVD_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (DevdTransferService, devd_transfer_service, DEVD, TRANSFER_SERVICE, DevdService)

DEVD_AVAILABLE_IN_ALL
DevdTransferService *devd_transfer_service_new                   (DevdClient             *client,
                                                                  GError                **error);
DEVD_AVAILABLE_IN_ALL
void                 devd_transfer_service_put_file_async        (DevdTransferService    *self,
                                                                  GFile                  *file,
                                                                  const gchar            *path,
                                                                  GFileProgressCallback   progress,
                                                                  gpointer                progress_data,
                                                                  GDestroyNotify          progress_data_destroy,
                                                                  GCancellable           *cancellable,
                                                                  GAsyncReadyCallback     callback,
                                                                  gpointer                user_data);
DEVD_AVAILABLE_IN_ALL
gboolean             devd_transfer_service_put_file_finish       (DevdTransferService    *self,
                                                                  GAsyncResult           *result,
                                                                  GError                **error);
DEVD_AVAILABLE_IN_ALL
void                 devd_transfer_service_get_file_async        (DevdTransferService    *self,
                                                                  const gchar            *path,
                                                                  GFile                  *file,
                                                                  GFileProgressCallback   progress,
                                                                  gpointer                progress_data,
                                                                  GDestroyNotify          progress_data_destroy,
                                                                  GCancellable           *cancellable,
                                                                  GAsyncReadyCallback     callback,
                                                                  gpointer                user_data);
DEVD_AVAILABLE_IN_ALL
gboolean             devd_transfer_service_get_file_finish       (DevdTransferService    *self,
                                                                  GAsyncResult           *result,
                                                                  GError                **error);

G_END_DECLS
