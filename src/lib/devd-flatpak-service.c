/* devd-flatpak-service.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "devd-flatpak-service"

#include "config.h"

#include <jsonrpc-glib.h>

#include "devd-flatpak-service.h"
#include "devd-macros.h"
#include "devd-trace.h"

void
devd_flatpak_service_install_bundle_cb (GObject      *object,
                                        GAsyncResult *result,
                                        gpointer      user_data)
{
  DevdFlatpakService *self = (DevdFlatpakService *)object;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;

  DEVD_ENTRY;

  g_assert (DEVD_IS_FLATPAK_SERVICE (self));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!devd_service_call_finish (DEVD_SERVICE (self), result, &reply, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else if (!reply || !g_variant_is_of_type (reply, G_VARIANT_TYPE_BOOLEAN))
    g_task_return_new_error (task,
                             G_IO_ERROR,
                             G_IO_ERROR_INVALID_DATA,
                             "Received incorrect data from RPC peer");
  else
    g_task_return_boolean (task, TRUE);

  DEVD_EXIT;
}

void
devd_flatpak_service_install_bundle_async (DevdFlatpakService *self,
                                           const gchar         *path,
                                           GCancellable        *cancellable,
                                           GAsyncReadyCallback  callback,
                                           gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  g_autoptr(GVariant) params = NULL;

  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_FLATPAK_SERVICE (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_flatpak_service_install_bundle_async);

  params = JSONRPC_MESSAGE_NEW ("path", JSONRPC_MESSAGE_PUT_STRING (path));

  devd_service_call_async (DEVD_SERVICE (self),
                           "org.gnome.deviced.flatpak.install-bundle",
                           params,
                           cancellable,
                           devd_flatpak_service_install_bundle_cb,
                           g_steal_pointer (&task));

  DEVD_EXIT;
}

gboolean
devd_flatpak_service_install_bundle_finish (DevdFlatpakService  *self,
                                            GAsyncResult        *result,
                                            GError             **error)
{
  gboolean ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_FLATPAK_SERVICE (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  ret = g_task_propagate_boolean (G_TASK (result), error);

  DEVD_RETURN (ret);
}

DevdFlatpakService *
devd_flatpak_service_new (DevdClient  *client,
                          GError     **error)
{
  return devd_service_new (DEVD_TYPE_FLATPAK_SERVICE,
                           "org.gnome.deviced.flatpak",
                           client,
                           error);
}

struct _DevdFlatpakService { DevdService parent_instance; };
G_DEFINE_TYPE (DevdFlatpakService, devd_flatpak_service, DEVD_TYPE_SERVICE)
static void devd_flatpak_service_class_init (DevdFlatpakServiceClass *klass) { }
static void devd_flatpak_service_init (DevdFlatpakService *self) { }
