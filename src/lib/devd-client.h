/* devd-client.h
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(DEVICED_INSIDE) && !defined(DEVICED_COMPILATION)
# error "Only <libdeviced.h> can be included directly."
#endif

#include <gio/gio.h>

#include "devd-version-macros.h"
#include "devd-triplet.h"

G_BEGIN_DECLS

#define DEVD_TYPE_CLIENT (devd_client_get_type())

DEVD_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (DevdClient, devd_client, DEVD, CLIENT, GObject)

struct _DevdClientClass
{
  GObjectClass parent_instance;

  void         (*notification)          (DevdClient             *self,
                                         const gchar            *method,
                                         GVariant               *params);
  void         (*service_added)         (DevdClient             *self,
                                         const gchar            *service);
  void         (*connect_async)         (DevdClient             *self,
                                         GCancellable           *cancellable,
                                         GAsyncReadyCallback     callback,
                                         gpointer                user_data);
  gboolean     (*connect_finish)        (DevdClient             *self,
                                         GAsyncResult           *result,
                                         GError                **error);
  void         (*disconnect_async)      (DevdClient             *self,
                                         GCancellable           *cancellable,
                                         GAsyncReadyCallback     callback,
                                         gpointer                user_data);
  gboolean     (*disconnect_finish)     (DevdClient             *self,
                                         GAsyncResult           *result,
                                         GError                **error);
  gchar       *(*get_name)              (DevdClient             *self);
  gchar       *(*get_arch)              (DevdClient             *self);
  gchar       *(*get_kernel)            (DevdClient             *self);
  gchar       *(*get_system)            (DevdClient             *self);
  DevdTriplet *(*get_triplet)           (DevdClient             *self);
  void         (*call_async)            (DevdClient             *self,
                                         const gchar            *method,
                                         GVariant               *params,
                                         GCancellable           *cancellable,
                                         GAsyncReadyCallback     callback,
                                         gpointer                user_data);
  gboolean     (*call_finish)           (DevdClient             *self,
                                         GAsyncResult           *result,
                                         GVariant              **reply,
                                         GError                **error);
  void         (*list_apps_async)       (DevdClient             *self,
                                         GCancellable           *cancellable,
                                         GAsyncReadyCallback     callback,
                                         gpointer                user_data);
  GPtrArray   *(*list_runtimes_finish)  (DevdClient             *self,
                                         GAsyncResult           *result,
                                         GError                **error);
  void         (*list_runtimes_async)   (DevdClient             *self,
                                         GCancellable           *cancellable,
                                         GAsyncReadyCallback     callback,
                                         gpointer                user_data);
  GPtrArray   *(*list_apps_finish)      (DevdClient             *self,
                                         GAsyncResult           *result,
                                         GError                **error);
  void         (*list_files_async)      (DevdClient             *self,
                                         const gchar            *path,
                                         const gchar            *attributes,
                                         GCancellable           *cancellable,
                                         GAsyncReadyCallback     callback,
                                         gpointer                user_data);
  GPtrArray   *(*list_files_finish)     (DevdClient             *self,
                                         GAsyncResult           *result,
                                         GError                **error);
  void         (*run_app_async)         (DevdClient             *self,
                                         const gchar            *provider,
                                         const gchar            *app_id,
                                         const gchar            *pty,
                                         GCancellable           *cancellable,
                                         GAsyncReadyCallback     callback,
                                         gpointer                user_data);
  gchar       *(*run_app_finish)        (DevdClient             *self,
                                         GAsyncResult           *result,
                                         GError                **error);
  void         (*syncfs_async)          (DevdClient             *self,
                                         const gchar            *devices,
                                         GCancellable           *cancellable,
                                         GAsyncReadyCallback     callback,
                                         gpointer                user_data);
  gboolean     (*syncfs_finish)         (DevdClient             *self,
                                         GAsyncResult           *result,
                                         GError                **error);


  /*< private >*/
  gpointer _reserved[32];
};

DEVD_AVAILABLE_IN_ALL
void         devd_client_emit_notification     (DevdClient             *self,
                                                const gchar            *method,
                                                GVariant               *params);
DEVD_AVAILABLE_IN_ALL
void         devd_client_emit_service_added    (DevdClient             *self,
                                                const gchar            *service);
DEVD_AVAILABLE_IN_ALL
gchar      **devd_client_get_services          (DevdClient             *self);
DEVD_AVAILABLE_IN_ALL
gboolean     devd_client_has_service           (DevdClient             *self,
                                                const gchar            *name);
DEVD_AVAILABLE_IN_ALL
guint        devd_client_get_timeout           (DevdClient             *self);
DEVD_AVAILABLE_IN_ALL
void         devd_client_set_timeout           (DevdClient             *self,
                                                guint                   timeout);
DEVD_AVAILABLE_IN_ALL
gchar       *devd_client_get_name              (DevdClient             *self);
DEVD_DEPRECATED_IN_3_30_FOR(devd_client_get_triplet)
gchar       *devd_client_get_arch              (DevdClient             *self);
DEVD_DEPRECATED_IN_3_30_FOR(devd_client_get_triplet)
gchar       *devd_client_get_kernel            (DevdClient             *self);
DEVD_DEPRECATED_IN_3_30_FOR(devd_client_get_triplet)
gchar       *devd_client_get_system            (DevdClient             *self);
DEVD_AVAILABLE_IN_3_30
DevdTriplet *devd_client_get_triplet           (DevdClient             *self);
DEVD_AVAILABLE_IN_ALL
void         devd_client_connect_async         (DevdClient             *self,
                                                GCancellable           *cancellable,
                                                GAsyncReadyCallback     callback,
                                                gpointer                user_data);
DEVD_AVAILABLE_IN_ALL
gboolean     devd_client_connect_finish        (DevdClient             *self,
                                                GAsyncResult           *result,
                                                GError                **error);
DEVD_AVAILABLE_IN_ALL
void         devd_client_disconnect_async      (DevdClient             *self,
                                                GCancellable           *cancellable,
                                                GAsyncReadyCallback     callback,
                                                gpointer                user_data);
DEVD_AVAILABLE_IN_ALL
gboolean     devd_client_disconnect_finish     (DevdClient             *self,
                                                GAsyncResult           *result,
                                                GError                **error);
DEVD_AVAILABLE_IN_ALL
void         devd_client_call_async            (DevdClient             *self,
                                                const gchar            *method,
                                                GVariant               *params,
                                                GCancellable           *cancellable,
                                                GAsyncReadyCallback     callback,
                                                gpointer                user_data);
DEVD_AVAILABLE_IN_ALL
gboolean     devd_client_call_finish           (DevdClient             *self,
                                                GAsyncResult           *result,
                                                GVariant              **reply,
                                                GError                **error);
DEVD_AVAILABLE_IN_ALL
void         devd_client_list_apps_async       (DevdClient             *self,
                                                GCancellable           *cancellable,
                                                GAsyncReadyCallback     callback,
                                                gpointer                user_data);
DEVD_AVAILABLE_IN_ALL
GPtrArray   *devd_client_list_apps_finish      (DevdClient             *self,
                                                GAsyncResult           *result,
                                                GError                **error);
DEVD_AVAILABLE_IN_ALL
void         devd_client_list_runtimes_async   (DevdClient            *self,
                                                GCancellable          *cancellable,
                                                GAsyncReadyCallback    callback,
                                                gpointer               user_data);
DEVD_AVAILABLE_IN_ALL
GPtrArray   *devd_client_list_runtimes_finish  (DevdClient             *self,
                                                GAsyncResult           *result,
                                                GError                **error);
DEVD_AVAILABLE_IN_ALL
void         devd_client_list_files_async      (DevdClient             *self,
                                                const gchar            *path,
                                                const gchar            *attributes,
                                                GCancellable           *cancellable,
                                                GAsyncReadyCallback     callback,
                                                gpointer                user_data);
DEVD_AVAILABLE_IN_ALL
GPtrArray   *devd_client_list_files_finish     (DevdClient             *self,
                                                GAsyncResult           *result,
                                                GError                **error);
DEVD_AVAILABLE_IN_ALL
void         devd_client_run_app_async         (DevdClient             *self,
                                                const gchar            *provider,
                                                const gchar            *app_id,
                                                const gchar            *pty,
                                                GCancellable           *cancellable,
                                                GAsyncReadyCallback     callback,
                                                gpointer                user_data);
DEVD_AVAILABLE_IN_ALL
gchar       *devd_client_run_app_finish        (DevdClient             *self,
                                                GAsyncResult           *result,
                                                GError                **error);
DEVD_AVAILABLE_IN_ALL
void         devd_client_syncfs_async          (DevdClient             *self,
                                                const gchar            *devices,
                                                GCancellable           *cancellable,
                                                GAsyncReadyCallback     callback,
                                                gpointer                user_data);
DEVD_AVAILABLE_IN_ALL
gboolean     devd_client_syncfs_finish         (DevdClient             *self,
                                                GAsyncResult           *result,
                                                GError                **error);

G_END_DECLS
