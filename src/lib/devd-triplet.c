/* devd-triplet.c
 *
 * Copyright (C) 2018 Corentin Noël <corentin.noel@collabora.com>
 * Copyright (C) 2018 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "devd-triplet"

#include "config.h"
#include "devd-triplet.h"

G_DEFINE_BOXED_TYPE (DevdTriplet, devd_triplet, devd_triplet_ref, devd_triplet_unref)

struct _DevdTriplet
{
  volatile gint ref_count;

  gchar *full_name;
  gchar *arch;
  gchar *vendor;
  gchar *kernel;
  gchar *operating_system;
};

static DevdTriplet *
_devd_triplet_construct (void)
{
  DevdTriplet *self;

  self = g_slice_new0 (DevdTriplet);
  self->ref_count = 1;
  self->full_name = NULL;
  self->arch = NULL;
  self->vendor = NULL;
  self->kernel = NULL;
  self->operating_system = NULL;

  return self;
}

/**
 * devd_triplet_new:
 * @full_name: The complete identifier of the machine
 *
 * Creates a new #DevdTriplet from a given identifier. This identifier
 * can be a simple architecture name, a duet of "arch-kernel" (like "m68k-coff"), a triplet
 * of "arch-kernel-os" (like "x86_64-linux-gnu") or a quadriplet of "arch-vendor-kernel-os"
 * (like "i686-pc-linux-gnu")
 *
 * Returns: (transfer full): An #DevdTriplet.
 *
 * Since: 3.30
 */
DevdTriplet *
devd_triplet_new (const gchar *full_name)
{
  DevdTriplet *self;
  g_auto (GStrv) parts = NULL;
  guint parts_length = 0;

  g_return_val_if_fail (full_name != NULL, NULL);

  self = _devd_triplet_construct ();
  self->full_name = g_strdup (full_name);

  parts = g_strsplit (full_name, "-", 4);
  parts_length = g_strv_length (parts);
  /* Currently they can't have more than 4 parts */
  if (parts_length >= 4)
    {
      self->arch = g_strdup (parts[0]);
      self->vendor = g_strdup (parts[1]);
      self->kernel = g_strdup (parts[2]);
      self->operating_system = g_strdup (parts[3]);
    }
  else if (parts_length == 3)
    {
      self->arch = g_strdup (parts[0]);
      self->kernel = g_strdup (parts[1]);
      self->operating_system = g_strdup (parts[2]);
    }
  else if (parts_length == 2)
    {
      self->arch = g_strdup (parts[0]);
      self->kernel = g_strdup (parts[1]);
    }
  else if (parts_length == 1)
    self->arch = g_strdup (parts[0]);

  return self;
}

/**
 * devd_triplet_new_with_triplet:
 * @arch: The name of the architecture of the machine (like "x86_64")
 * @kernel: (nullable): The name of the kernel of the machine (like "linux")
 * @operating_system: (nullable): The name of the os of the machine
 * (like "gnuabi64")
 *
 * Creates a new #DevdTriplet from a given triplet of "arch-kernel-os"
 * (like "x86_64-linux-gnu")
 *
 * Returns: (transfer full): An #DevdTriplet.
 *
 * Since: 3.30
 */
DevdTriplet *
devd_triplet_new_with_triplet (const gchar *arch,
                              const gchar *kernel,
                              const gchar *operating_system)
{
  DevdTriplet *self;
  g_autofree gchar *full_name = NULL;

  g_return_val_if_fail (arch != NULL, NULL);

  self = _devd_triplet_construct ();
  self->arch = g_strdup (arch);
  self->kernel = g_strdup (kernel);
  self->operating_system = g_strdup (operating_system);

  full_name = g_strdup (arch);
  if (kernel != NULL)
    {
      g_autofree gchar *start_full_name = full_name;
      full_name = g_strdup_printf ("%s-%s", start_full_name, kernel);
    }

  if (operating_system != NULL)
    {
      g_autofree gchar *start_full_name = full_name;
      full_name = g_strdup_printf ("%s-%s", start_full_name, operating_system);
    }

  self->full_name = g_steal_pointer (&full_name);

  return self;
}

/**
 * devd_triplet_new_with_quadruplet:
 * @arch: The name of the architecture of the machine (like "x86_64")
 * @vendor: (nullable): The name of the vendor of the machine (like "pc")
 * @kernel: (nullable): The name of the kernel of the machine (like "linux")
 * @operating_system: (nullable): The name of the os of the machine (like "gnuabi64")
 *
 * Creates a new #DevdTriplet from a given quadruplet of 
 * "arch-vendor-kernel-os" (like "i686-pc-linux-gnu")
 *
 * Returns: (transfer full): An #DevdTriplet.
 *
 * Since: 3.30
 */
DevdTriplet *
devd_triplet_new_with_quadruplet (const gchar *arch,
                                 const gchar *vendor,
                                 const gchar *kernel,
                                 const gchar *operating_system)
{
  DevdTriplet *self;
  g_autofree gchar *full_name = NULL;

  g_return_val_if_fail (arch != NULL, NULL);

  if (vendor == NULL)
    return devd_triplet_new_with_triplet (arch, kernel, operating_system);

  self = _devd_triplet_construct ();
  self->arch = g_strdup (arch);
  self->vendor = g_strdup (vendor);
  self->kernel = g_strdup (kernel);
  self->operating_system = g_strdup (operating_system);

  full_name = g_strdup_printf ("%s-%s", arch, vendor);
  if (kernel != NULL)
    {
      g_autofree gchar *start_full_name = full_name;
      full_name = g_strdup_printf ("%s-%s", start_full_name, kernel);
    }

  if (operating_system != NULL)
    {
      g_autofree gchar *start_full_name = full_name;
      full_name = g_strdup_printf ("%s-%s", start_full_name, operating_system);
    }

  self->full_name = g_steal_pointer (&full_name);

  return self;
}

static void
devd_triplet_finalize (DevdTriplet *self)
{
  g_free (self->full_name);
  g_free (self->arch);
  g_free (self->vendor);
  g_free (self->kernel);
  g_free (self->operating_system);
  g_slice_free (DevdTriplet, self);
}

/**
 * devd_triplet_ref:
 * @self: An #DevdTriplet
 *
 * Increases the reference count of @self
 *
 * Returns: (transfer none): An #DevdTriplet.
 *
 * Since: 3.30
 */
DevdTriplet *
devd_triplet_ref (DevdTriplet *self)
{
  g_return_val_if_fail (self, NULL);
  g_return_val_if_fail (self->ref_count > 0, NULL);

  g_atomic_int_inc (&self->ref_count);

  return self;
}

/**
 * devd_triplet_unref:
 * @self: An #DevdTriplet
 *
 * Decreases the reference count of @self
 * Once the reference count reaches 0, the object is freed.
 *
 * Since: 3.30
 */
void
devd_triplet_unref (DevdTriplet *self)
{
  g_return_if_fail (self);
  g_return_if_fail (self->ref_count > 0);

  if (g_atomic_int_dec_and_test (&self->ref_count))
    devd_triplet_finalize (self);
}

/**
 * devd_triplet_get_full_name:
 * @self: An #DevdTriplet
 *
 * Gets the full name of the machine configuration name (can be an architecture name,
 * a duet, a triplet or a quadruplet).
 *
 * Returns: (transfer none): The full name of the machine configuration name
 *
 * Since: 3.30
 */
const gchar *
devd_triplet_get_full_name (DevdTriplet *self)
{
  g_return_val_if_fail (self, NULL);

  return self->full_name;
}

/**
 * devd_triplet_get_arch:
 * @self: An #DevdTriplet
 *
 * Gets the architecture name of the machine
 *
 * Returns: (transfer none): The architecture name of the machine
 */
const gchar *
devd_triplet_get_arch (DevdTriplet *self)
{
  g_return_val_if_fail (self, NULL);

  return self->arch;
}

/**
 * devd_triplet_get_vendor:
 * @self: An #DevdTriplet
 *
 * Gets the vendor name of the machine
 *
 * Returns: (transfer none) (nullable): The vendor name of the machine
 *
 * Since: 3.30
 */
const gchar *
devd_triplet_get_vendor (DevdTriplet *self)
{
  g_return_val_if_fail (self, NULL);

  return self->vendor;
}

/**
 * devd_triplet_get_kernel:
 * @self: An #DevdTriplet
 *
 * Gets name of the kernel of the machine
 *
 * Returns: (transfer none) (nullable): The name of the kernel of the machine
 *
 * Since: 3.30
 */
const gchar *
devd_triplet_get_kernel (DevdTriplet *self)
{
  g_return_val_if_fail (self, NULL);

  return self->kernel;
}

/**
 * devd_triplet_get_operating_system:
 * @self: An #DevdTriplet
 *
 * Gets name of the operating system of the machine
 *
 * Returns: (transfer none) (nullable): The name of the operating system of the machine
 *
 * Since: 3.30
 */
const gchar *
devd_triplet_get_operating_system (DevdTriplet *self)
{
  g_return_val_if_fail (self, NULL);

  return self->operating_system;
}
