/* devd-network-device.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "devd-network-device"

#include "config.h"

#include "devd-network-client.h"
#include "devd-network-device.h"
#include "devd-trace.h"

/**
 * SECTION:devd-network-device
 * @title: DevdNetworkDevice
 * @short_description: represetnts an available network device
 *
 * #DevdNetworkDevice is a #DevdDevice implementation that represents a
 * discovered device on a local network segment.
 *
 * Use devd_device_create_client() to create a client to communicate with
 * the network device.
 *
 * Since: 3.28
 */

struct _DevdNetworkDevice
{
  DevdDevice parent_instance;

  /*
   * This is the resolved address (ip address + port) that will connect
   * to a discovered _deviced._tcp service.
   */
  GInetSocketAddress *address;

  /*
   * The TLS client certificate to use for communication. The peer will
   * use this to auto-authorize followup connections.
   */
  GTlsCertificate *certificate;
};

G_DEFINE_TYPE (DevdNetworkDevice, devd_network_device, DEVD_TYPE_DEVICE)

enum {
  PROP_0,
  PROP_ADDRESS,
  PROP_CERTIFICATE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static DevdClient *
devd_network_device_create_client (DevdDevice *device)
{
  DevdNetworkDevice *self = (DevdNetworkDevice *)device;
  DevdNetworkClient *client;

  DEVD_ENTRY;

  g_assert (DEVD_IS_NETWORK_DEVICE (self));

  client = devd_network_client_new (self->address, self->certificate);

  DEVD_RETURN (DEVD_CLIENT (client));
}

static void
devd_network_device_finalize (GObject *object)
{
  DevdNetworkDevice *self = (DevdNetworkDevice *)object;

  DEVD_ENTRY;

  g_clear_object (&self->address);

  G_OBJECT_CLASS (devd_network_device_parent_class)->finalize (object);

  DEVD_EXIT;
}

static void
devd_network_device_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  DevdNetworkDevice *self = DEVD_NETWORK_DEVICE (object);

  switch (prop_id)
    {
    case PROP_ADDRESS:
      g_value_set_object (value, devd_network_device_get_address (self));
      break;

    case PROP_CERTIFICATE:
      g_value_set_object (value, devd_network_device_get_certificate (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
devd_network_device_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  DevdNetworkDevice *self = DEVD_NETWORK_DEVICE (object);

  switch (prop_id)
    {
    case PROP_ADDRESS:
      self->address = g_value_dup_object (value);
      break;

    case PROP_CERTIFICATE:
      self->certificate = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
devd_network_device_class_init (DevdNetworkDeviceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  DevdDeviceClass *device_class = DEVD_DEVICE_CLASS (klass);

  object_class->finalize = devd_network_device_finalize;
  object_class->get_property = devd_network_device_get_property;
  object_class->set_property = devd_network_device_set_property;

  device_class->create_client = devd_network_device_create_client;

  properties [PROP_ADDRESS] =
    g_param_spec_object ("address",
                         "Address",
                         "The inet socket address to connect to",
                         G_TYPE_INET_SOCKET_ADDRESS,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  properties [PROP_CERTIFICATE] =
    g_param_spec_object ("certificate",
                         "Certificate",
                         "The client certificate to use in communication",
                         G_TYPE_TLS_CERTIFICATE,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
devd_network_device_init (DevdNetworkDevice *self)
{
}

/**
 * devd_network_device_get_address:
 * @self: a #DevdNetworkDevice
 *
 * Gets the #GInetSocketAddress that can be used to connect to the
 * device daemon.
 *
 * Returns: (transfer none): a #GInetSocketAddress or %NULL
 *
 * Since: 3.28
 */
GInetSocketAddress *
devd_network_device_get_address (DevdNetworkDevice *self)
{
  g_return_val_if_fail (DEVD_IS_NETWORK_DEVICE (self), NULL);

  return self->address;
}

/**
 * devd_network_device_get_certificate:
 * @self: a #DevdNetworkDevice
 *
 * Gets the #GTlsCertificate that will be used as the client-side certificate
 * when communicating with this network device.
 *
 * Returns: (nullable) (transfer none): a #GTlsCertificate or %NULL
 *
 * Since: 3.28
 */
GTlsCertificate *
devd_network_device_get_certificate (DevdNetworkDevice *self)
{
  g_return_val_if_fail (DEVD_IS_NETWORK_DEVICE (self), NULL);

  return self->certificate;
}
