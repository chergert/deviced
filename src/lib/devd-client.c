/* devd-client.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "devd-client"

#include "config.h"

#include "devd-client.h"
#include "devd-trace.h"

/**
 * SECTION:devd-client
 * @title: DevdClient
 * @short_description: base class for communicating with devices
 *
 * The #DevdClient is a base-class that is used to communicate with
 * devices. Network based devices (or USB) subclass this to provide
 * a common API, despite different transport mediums.
 *
 * Since: 3.28
 */

typedef struct
{
  GHashTable *services;
  guint timeout;
} DevdClientPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (DevdClient, devd_client, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_NAME,
  PROP_ARCH,
  PROP_KERNEL,
  PROP_SYSTEM,
  PROP_TRIPLET,
  PROP_TIMEOUT,
  N_PROPS
};

enum {
  NOTIFICATION,
  SERVICE_ADDED,
  N_SIGNALS
};

static GParamSpec *properties [N_PROPS];
static guint signals [N_SIGNALS];

static void
devd_client_real_list_apps_async (DevdClient          *self,
                                  GCancellable        *cancellable,
                                  GAsyncReadyCallback  callback,
                                  gpointer             user_data)
{
  g_task_report_new_error (self, callback, user_data,
                           devd_client_real_list_apps_async,
                           G_IO_ERROR,
                           G_IO_ERROR_NOT_SUPPORTED,
                           "Listing applications is not supported on this client");
}

static GPtrArray *
devd_client_real_list_apps_finish (DevdClient    *self,
                                   GAsyncResult  *result,
                                   GError       **error)
{
  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
devd_client_real_list_runtimes_async (DevdClient          *self,
                                      GCancellable        *cancellable,
                                      GAsyncReadyCallback  callback,
                                      gpointer             user_data)
{
  g_task_report_new_error (self, callback, user_data,
                           devd_client_real_list_runtimes_async,
                           G_IO_ERROR,
                           G_IO_ERROR_NOT_SUPPORTED,
                           "Listing runtimes is not supported on this client");
}

static GPtrArray *
devd_client_real_list_runtimes_finish (DevdClient    *self,
                                       GAsyncResult  *result,
                                       GError       **error)
{
  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
devd_client_real_list_files_async (DevdClient          *self,
                                   const gchar         *path,
                                   const gchar         *attributes,
                                   GCancellable        *cancellable,
                                   GAsyncReadyCallback  callback,
                                   gpointer             user_data)
{
  g_task_report_new_error (self, callback, user_data,
                           devd_client_real_list_apps_async,
                           G_IO_ERROR,
                           G_IO_ERROR_NOT_SUPPORTED,
                           "Listing files is not supported on this client");
}

static GPtrArray *
devd_client_real_list_files_finish (DevdClient    *self,
                                    GAsyncResult  *result,
                                    GError       **error)
{
  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
devd_client_real_syncfs_async (DevdClient          *self,
                               const gchar         *devices,
                               GCancellable        *cancellable,
                               GAsyncReadyCallback  callback,
                               gpointer             user_data)
{
  g_task_report_new_error (self, callback, user_data,
                           devd_client_real_syncfs_async,
                           G_IO_ERROR,
                           G_IO_ERROR_NOT_SUPPORTED,
                           "Syncing file-systems is not supported on this client");
}

static gboolean
devd_client_real_syncfs_finish (DevdClient    *self,
                                GAsyncResult  *result,
                                GError       **error)
{
  return g_task_propagate_boolean (G_TASK (result), error);
}


static void
devd_client_real_call_async (DevdClient          *self,
                             const gchar         *method,
                             GVariant            *params,
                             GCancellable        *cancellable,
                             GAsyncReadyCallback  callback,
                             gpointer             user_data)
{
  g_task_report_new_error (self, callback, user_data,
                           devd_client_real_call_async,
                           G_IO_ERROR,
                           G_IO_ERROR_NOT_SUPPORTED,
                           "Client does not support generic call interface");
}

static gboolean
devd_client_real_call_finish (DevdClient    *self,
                              GAsyncResult  *result,
                              GVariant     **reply,
                              GError       **error)
{
  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
devd_client_dispose (GObject *object)
{
  DevdClient *self = (DevdClient *)object;
  DevdClientPrivate *priv = devd_client_get_instance_private (self);

  DEVD_ENTRY;

  g_clear_pointer (&priv->services, g_hash_table_unref);

  G_OBJECT_CLASS (devd_client_parent_class)->dispose (object);

  DEVD_EXIT;
}

static void
devd_client_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  DevdClient *self = DEVD_CLIENT (object);

  switch (prop_id)
    {
    case PROP_ARCH:
      g_value_take_string (value, devd_client_get_arch (self));
      break;

    case PROP_KERNEL:
      g_value_take_string (value, devd_client_get_kernel (self));
      break;

    case PROP_NAME:
      g_value_take_string (value, devd_client_get_name (self));
      break;

    case PROP_SYSTEM:
      g_value_take_string (value, devd_client_get_system (self));
      break;

    case PROP_TRIPLET:
      g_value_take_boxed (value, devd_client_get_triplet (self));
      break;

    case PROP_TIMEOUT:
      g_value_set_uint (value, devd_client_get_timeout (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
devd_client_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  DevdClient *self = DEVD_CLIENT (object);

  switch (prop_id)
    {
    case PROP_TIMEOUT:
      devd_client_set_timeout (self, g_value_get_uint (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
devd_client_class_init (DevdClientClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = devd_client_dispose;
  object_class->get_property = devd_client_get_property;
  object_class->set_property = devd_client_set_property;

  klass->list_apps_async = devd_client_real_list_apps_async;
  klass->list_apps_finish = devd_client_real_list_apps_finish;
  klass->list_runtimes_async = devd_client_real_list_runtimes_async;
  klass->list_runtimes_finish = devd_client_real_list_runtimes_finish;
  klass->list_files_async = devd_client_real_list_files_async;
  klass->list_files_finish = devd_client_real_list_files_finish;
  klass->syncfs_async = devd_client_real_syncfs_async;
  klass->syncfs_finish = devd_client_real_syncfs_finish;
  klass->call_async = devd_client_real_call_async;
  klass->call_finish = devd_client_real_call_finish;

  properties [PROP_ARCH] =
    g_param_spec_string ("arch",
                         "Arch",
                         "The architecture of the device, if provided",
                         NULL,
                         G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  properties [PROP_KERNEL] =
    g_param_spec_string ("kernel",
                         "Kernel",
                         "The kernel of the device, if provided",
                         NULL,
                         G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the device, if any",
                         NULL,
                         G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  properties [PROP_SYSTEM] =
    g_param_spec_string ("system",
                         "System",
                         "The operating system of the device, if provided",
                         NULL,
                         G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  properties [PROP_TRIPLET] =
    g_param_spec_string ("triplet",
                         "Triplet",
                         "The architecture triplet, if provided",
                         NULL,
                         G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  properties [PROP_TIMEOUT] =
    g_param_spec_uint ("timeout",
                       "Timeout",
                       "The timeout for communication in seconds",
                       0,
                       G_MAXUINT,
                       0,
                       G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  /**
   * DevdClient::notification:
   * @self: a #DevdClient
   * @method: the method name
   * @params: the parameters for the notification
   *
   * The "notification" signal is emitted when a client has received a
   * notification from the peer.
   *
   * Not all clients will support this, so it's specific for communicating
   * with some clients and services.
   *
   * Since: 3.28
   */
  signals [NOTIFICATION] =
    g_signal_new ("notification",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED,
                  G_STRUCT_OFFSET (DevdClientClass, notification),
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 2, G_TYPE_STRING, G_TYPE_VARIANT);

  /**
   * DevdClient::service-added:
   * @self: a #DevdClient
   * @service: the name of the service
   *
   * The "service-added" signal is emitted when a new service is advertised by
   * the client. This can happen when the device has enabled a new feature
   * while you're connected.
   *
   * If integrating with third-party libraries, this can be used to advertise
   * that additional services are enabled on the connection.
   *
   * Since: 3.28
   */
  signals [SERVICE_ADDED] =
    g_signal_new ("service-added",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DevdClientClass, service_added),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__STRING,
                  G_TYPE_NONE, 1, G_TYPE_STRING);
  g_signal_set_va_marshaller (signals [SERVICE_ADDED],
                              G_TYPE_FROM_CLASS (klass),
                              g_cclosure_marshal_VOID__STRINGv);
}

static void
devd_client_init (DevdClient *self)
{
  DevdClientPrivate *priv = devd_client_get_instance_private (self);

  priv->services = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);
}

void
devd_client_emit_service_added (DevdClient  *self,
                                const gchar *service)
{
  DevdClientPrivate *priv = devd_client_get_instance_private (self);

  g_return_if_fail (DEVD_IS_CLIENT (self));
  g_return_if_fail (service != NULL);

  g_hash_table_insert (priv->services, g_strdup (service), NULL);
  g_signal_emit (self, signals [SERVICE_ADDED], 0, service);
}

void
devd_client_emit_notification (DevdClient  *self,
                               const gchar *method,
                               GVariant    *params)
{
  g_return_if_fail (DEVD_IS_CLIENT (self));
  g_return_if_fail (method != NULL);

  g_signal_emit (self, signals [NOTIFICATION], 0, method, params);
}

gboolean
devd_client_has_service (DevdClient  *self,
                         const gchar *method)
{
  DevdClientPrivate *priv = devd_client_get_instance_private (self);

  g_return_val_if_fail (DEVD_IS_CLIENT (self), FALSE);
  g_return_val_if_fail (method != NULL, FALSE);

  return g_hash_table_contains (priv->services, method);
}

/**
 * devd_client_get_services:
 * @self: a #DevdClient
 *
 * Gets the names of services currently supported by the client.
 *
 * Returns: (transfer full): a #GStrv of service names
 *
 * Since: 3.28
 */
gchar **
devd_client_get_services (DevdClient *self)
{
  DevdClientPrivate *priv = devd_client_get_instance_private (self);
  GHashTableIter iter;
  const gchar *key;
  GPtrArray *services;

  g_return_val_if_fail (DEVD_IS_CLIENT (self), NULL);

  services = g_ptr_array_new ();
  g_hash_table_iter_init (&iter, priv->services);
  while (g_hash_table_iter_next (&iter, (gpointer *)&key, NULL))
    g_ptr_array_add (services, g_strdup (key));
  g_ptr_array_add (services, NULL);

  return (gchar **)g_ptr_array_free (services, FALSE);
}

guint
devd_client_get_timeout (DevdClient *self)
{
  DevdClientPrivate *priv = devd_client_get_instance_private (self);

  g_return_val_if_fail (DEVD_IS_CLIENT (self), 0);

  return priv->timeout;
}

void
devd_client_set_timeout (DevdClient *self,
                         guint       timeout)
{
  DevdClientPrivate *priv = devd_client_get_instance_private (self);

  g_return_if_fail (DEVD_IS_CLIENT (self));

  if (priv->timeout != timeout)
    {
      priv->timeout = timeout;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TIMEOUT]);
    }
}

/**
 * devd_client_connect_async:
 * @self: a #DevdClient
 * @cancellable: (nullable): a #GCancellable or %NULL
 * @callback: a #GAsyncReadyCallback, or %NULL
 * @user_data: closure data for @callback
 *
 * Asynchronously connects to the device for which the client was created.
 *
 * @callback should complete the operation by calling devd_client_connect_finish().
 *
 * Since: 3.28
 */
void
devd_client_connect_async  (DevdClient          *self,
                            GCancellable        *cancellable,
                            GAsyncReadyCallback  callback,
                            gpointer             user_data)
{
  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_CLIENT (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  DEVD_CLIENT_GET_CLASS (self)->connect_async (self, cancellable, callback, user_data);

  DEVD_EXIT;
}

/**
 * devd_client_connect_finish:
 * @self: a #DevdClient
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError or %NULL
 *
 * Completes an asynchronous connection to the device, initiated with
 * devd_client_connect_async().
 *
 * Returns: %TRUE if successful; otherwise %FALSE and @error is set.
 *
 * Since: 3.28
 */
gboolean
devd_client_connect_finish (DevdClient    *self,
                            GAsyncResult  *result,
                            GError       **error)
{
  gboolean ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_CLIENT (self), FALSE);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), FALSE);

  ret = DEVD_CLIENT_GET_CLASS (self)->connect_finish (self, result, error);

  DEVD_RETURN (ret);
}

void
devd_client_disconnect_async (DevdClient          *self,
                              GCancellable        *cancellable,
                              GAsyncReadyCallback  callback,
                              gpointer             user_data)
{
  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_CLIENT (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  DEVD_CLIENT_GET_CLASS (self)->disconnect_async (self, cancellable, callback, user_data);

  DEVD_EXIT;
}

gboolean
devd_client_disconnect_finish (DevdClient    *self,
                               GAsyncResult  *result,
                               GError       **error)
{
  gboolean ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_CLIENT (self), FALSE);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), FALSE);

  ret = DEVD_CLIENT_GET_CLASS (self)->disconnect_finish (self, result, error);

  DEVD_RETURN (ret);
}

/**
 * devd_client_get_name:
 * @self: a #DevdClient
 *
 * Gets the name of the peer that this client is connected to.
 *
 * This value is discovered when initializing a connection with the peer.
 *
 * Returns: (transfer full) (nullable): a newly allocated string or %NULL
 *
 * Since: 3.28
 */
gchar *
devd_client_get_name (DevdClient *self)
{
  gchar *ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_CLIENT (self), NULL);

  ret = DEVD_CLIENT_GET_CLASS (self)->get_name (self);

  DEVD_RETURN (ret);
}

/**
 * devd_client_get_arch:
 * @self: a #DevdClient
 *
 * Gets the architecture of the device, such as "x86_64". Generally,
 * this is the machine value from uname().
 *
 * Returns: (transfer full): a string representing the architecture.
 *
 * Since: 3.28
 * Deprecated: 3.30: Use devd_client_get_triplet() instead.
 */
gchar *
devd_client_get_arch (DevdClient *self)
{
  gchar *ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_CLIENT (self), NULL);

  ret = DEVD_CLIENT_GET_CLASS (self)->get_arch (self);

  DEVD_RETURN (ret);
}

/**
 * devd_client_get_kernel:
 * @self: a #DevdClient
 *
 * Gets the kernel of the device, such as "linux". This is the kernel
 * component of the familiar "target triplet".
 *
 * Returns: (transfer full): a string representing the kernel.
 *
 * Since: 3.28
 * Deprecated: 3.30: Use devd_client_get_triplet() instead.
 */
gchar *
devd_client_get_kernel (DevdClient *self)
{
  gchar *ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_CLIENT (self), NULL);

  ret = DEVD_CLIENT_GET_CLASS (self)->get_kernel (self);

  DEVD_RETURN (ret);
}

/**
 * devd_client_get_system:
 * @self: a #DevdClient
 *
 * Gets the system of the device, such as "gnu". This is the system
 * component of the familiar "target triplet".
 *
 * Returns: (transfer full): a string representing the system.
 *
 * Since: 3.28
 * Deprecated: 3.30: Use devd_client_get_triplet() instead.
 */
gchar *
devd_client_get_system (DevdClient *self)
{
  gchar *ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_CLIENT (self), NULL);

  ret = DEVD_CLIENT_GET_CLASS (self)->get_system (self);

  DEVD_RETURN (ret);
}

/**
 * devd_client_get_triplet:
 * @self: a #DevdClient
 *
 * Gets the #DevdTriplet object holding information about the architecture triplet
 *
 * Returns: (transfer full): a #DevdTriplet representing the architecture triplet.
 *
 * Since: 3.30
 */
DevdTriplet *
devd_client_get_triplet (DevdClient *self)
{
  DevdTriplet *ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_CLIENT (self), NULL);

  ret = DEVD_CLIENT_GET_CLASS (self)->get_triplet (self);

  DEVD_RETURN (ret);
}

/**
 * devd_client_list_apps_async:
 * @self: a #DevdClient
 * @cancellable: (nullable): a #GCancellable or %NULL
 * @callback: a #GAsyncReadyCallback
 * @user_data: closure data for @callback
 *
 * Asynchronously requests a list of applications that are available to
 * the peer that the #DevdClient is connected to.
 *
 * To complete the asynchronous request, @callback must call
 * devd_client_list_apps_finish() to obtain the result.
 *
 * Since: 3.28
 */
void
devd_client_list_apps_async (DevdClient          *self,
                             GCancellable        *cancellable,
                             GAsyncReadyCallback  callback,
                             gpointer             user_data)
{
  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_CLIENT (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  DEVD_CLIENT_GET_CLASS (self)->list_apps_async (self, cancellable, callback, user_data);

  DEVD_EXIT;
}

/**
 * devd_client_list_apps_finish:
 * @self: a #DevdClient
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous request to devd_client_list_apps_async().
 *
 * Returns: (transfer container) (element-type Deviced.AppInfo): An array
 *   of #DevdClientAppInfo if successful; otherwise %NULL and @error is set.
 *
 * Since: 3.28
 */
GPtrArray *
devd_client_list_apps_finish (DevdClient    *self,
                              GAsyncResult  *result,
                              GError       **error)
{
  GPtrArray *ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_CLIENT (self), NULL);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), NULL);

  ret = DEVD_CLIENT_GET_CLASS (self)->list_apps_finish (self, result, error);

  DEVD_RETURN (ret);
}

/**
 * devd_client_list_runtimes_async:
 * @self: a #DevdClient
 * @cancellable: (nullable): a #GCancellable or %NULL
 * @callback: a #GAsyncReadyCallback
 * @user_data: closure data for @callback
 *
 * Asynchronously requests a list of runtimes that are available to
 * the peer that the #DevdClient is connected to.
 *
 * To complete the asynchronous request, @callback must call
 * devd_client_list_runtimes_finish() to obtain the result.
 *
 * Since: 3.28
 */
void
devd_client_list_runtimes_async (DevdClient          *self,
                                 GCancellable        *cancellable,
                                 GAsyncReadyCallback  callback,
                                 gpointer             user_data)
{
  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_CLIENT (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  DEVD_CLIENT_GET_CLASS (self)->list_runtimes_async (self, cancellable, callback, user_data);

  DEVD_EXIT;
}

/**
 * devd_client_list_runtimes_finish:
 * @self: a #DevdClient
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous request to devd_client_list_runtimes_async().
 *
 * Returns: (transfer container) (element-type Deviced.AppInfo): An array
 *   of #DevdClientAppInfo if successful; otherwise %NULL and @error is set.
 *
 * Since: 3.28
 */
GPtrArray *
devd_client_list_runtimes_finish (DevdClient    *self,
                                  GAsyncResult  *result,
                                  GError       **error)
{
  GPtrArray *ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_CLIENT (self), NULL);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), NULL);

  ret = DEVD_CLIENT_GET_CLASS (self)->list_runtimes_finish (self, result, error);

  DEVD_RETURN (ret);
}

/**
 * devd_client_list_files_async:
 * @self: a #DevdClient
 * @path: the path on the device
 * @attributes: file attributes
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: a callback to execute up on completion
 * @user_data: closure data for @callback
 *
 * Lists the files in a given path on the device.
 *
 * Use @attributes to specify the file attributes to retrieve. These
 * follow the same convention as g_file_enumerate_children().
 *
 * Since: 3.28
 */
void
devd_client_list_files_async (DevdClient          *self,
                              const gchar         *path,
                              const gchar         *attributes,
                              GCancellable        *cancellable,
                              GAsyncReadyCallback  callback,
                              gpointer             user_data)
{
  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_CLIENT (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  DEVD_CLIENT_GET_CLASS (self)->list_files_async (self, path, attributes, cancellable, callback, user_data);

  DEVD_EXIT;
}

/**
 * devd_client_list_files_finish:
 * @self: a #DevdClient
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous request to list files on the device.
 *
 * Returns: (transfer container) (element-type Gio.FileInfo): a #GPtrArray of
 *   #GFileInfo retrieved from the device.
 *
 * Since: 3.28
 */
GPtrArray *
devd_client_list_files_finish (DevdClient    *self,
                               GAsyncResult  *result,
                               GError       **error)
{
  GPtrArray *ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_CLIENT (self), NULL);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), NULL);

  ret = DEVD_CLIENT_GET_CLASS (self)->list_files_finish (self, result, error);

  DEVD_RETURN (ret);
}

/**
 * devd_client_run_app_async:
 * @self: a #DevdClient
 * @provider: the app provider
 * @app_id: the ID of the app to run
 * @pty: (nullable): a pty ID
 * @cancellable: (nullable): a #GCancellable or %NULL
 * @callback: a #GAsyncReadyCallback
 * @user_data: closure data for @callback
 *
 * Asynchronously runs an application on the connected #DevdClient.
 *
 * To complete the asynchronous request, @callback must call
 * devd_client_run_app_finish() to obtain the result.
 *
 * Since: 3.28
 */
void
devd_client_run_app_async (DevdClient          *self,
                           const gchar         *provider,
                           const gchar         *app_id,
                           const gchar         *pty,
                           GCancellable        *cancellable,
                           GAsyncReadyCallback  callback,
                           gpointer             user_data)
{
  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_CLIENT (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));
  g_return_if_fail (provider != NULL);
  g_return_if_fail (app_id != NULL);

  DEVD_CLIENT_GET_CLASS (self)->run_app_async (self, provider, app_id, pty, cancellable, callback, user_data);

  DEVD_EXIT;
}

/**
 * devd_client_run_app_finish:
 * @self: a #DevdClient
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous request to devd_client_run_app_async().
 *
 * Returns: the identifier for the process, or %NULL and @error is set.
 *
 * Since: 3.28
 */
gchar *
devd_client_run_app_finish (DevdClient    *self,
                            GAsyncResult  *result,
                            GError       **error)
{
  gchar *ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_CLIENT (self), FALSE);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), FALSE);

  ret = DEVD_CLIENT_GET_CLASS (self)->run_app_finish (self, result, error);

  DEVD_RETURN (ret);
}

/**
 * devd_client_syncfs_async:
 * @self: a #DevdClient
 * @devices: (nullable): a comma-separated list of devices, or %NULL
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: a callback to execute upon completion
 * @user_data: closure data for @callback
 *
 * Requests that the device sync changes to the file-system to the underlying
 * storage devices specified, or all devices if @devices is %NULL.
 *
 * Note, currently, only syncing all file-systems is supported.
 *
 * Since: 3.28
 */
void
devd_client_syncfs_async (DevdClient          *self,
                          const gchar         *devices,
                          GCancellable        *cancellable,
                          GAsyncReadyCallback  callback,
                          gpointer             user_data)
{
  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_CLIENT (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  DEVD_CLIENT_GET_CLASS (self)->syncfs_async (self, devices, cancellable, callback, user_data);

  DEVD_EXIT;
}

/**
 * devd_client_syncfs_finish:
 * @self: a #DevdClient
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous request to sync file-system changes to
 * their underlying storage devices.
 *
 * Returns: %TRUE if successful; otherwise %FALSE and @error is set.
 *
 * Since: 3.28
 */
gboolean
devd_client_syncfs_finish (DevdClient    *self,
                           GAsyncResult  *result,
                           GError       **error)
{
  gboolean ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_CLIENT (self), FALSE);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), FALSE);

  ret = DEVD_CLIENT_GET_CLASS (self)->syncfs_finish (self, result, error);

  DEVD_RETURN (ret);
}

/**
 * devd_client_call_async:
 * @self: a #DevdClient
 * @method: the RPC method name
 * @params: (transfer none) (nullable): the parameters for the method
 * @cancellable: (nullable): a #GCancellable or %NULL
 * @callback: a callback to execute upon completion
 * @user_data: closure data for @callback
 *
 * Asynchronously calls an RPC on the device, if available.
 *
 * Not all #DevdClient implementations are guaranteed to support this
 * interface, and therefore may fail.
 *
 * Since: 3.28
 */
void
devd_client_call_async (DevdClient          *self,
                        const gchar         *method,
                        GVariant            *params,
                        GCancellable        *cancellable,
                        GAsyncReadyCallback  callback,
                        gpointer             user_data)
{
  DEVD_ENTRY;

  g_return_if_fail (DEVD_IS_CLIENT (self));
  g_return_if_fail (method != NULL);
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  DEVD_CLIENT_GET_CLASS (self)->call_async (self, method, params, cancellable, callback, user_data);

  DEVD_EXIT;
}

/**
 * devd_client_call_finish:
 * @self: a #DevdClient
 * @result: a #GAsyncResult provided to callback
 * @reply: (optional) (out): a location for a #GVariant
 * @error: a location for a #GError, or %NULL
 *
 * Gets the result of the RPC call.
 *
 * Returns: %TRUE if successful; otherwise %FALSE and @error is set.
 *   @reply is set to the reply from the peer, of provided.
 */
gboolean
devd_client_call_finish (DevdClient    *self,
                         GAsyncResult  *result,
                         GVariant     **reply,
                         GError       **error)
{
  gboolean ret;

  DEVD_ENTRY;

  g_return_val_if_fail (DEVD_IS_CLIENT (self), FALSE);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), FALSE);

  if (reply != NULL)
    *reply = NULL;

  ret = DEVD_CLIENT_GET_CLASS (self)->call_finish (self, result, reply, error);

  DEVD_RETURN (ret);
}
