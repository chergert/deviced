/* devd-device.h
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(DEVICED_INSIDE) && !defined(DEVICED_COMPILATION)
# error "Only <libdeviced.h> can be included directly."
#endif

#include <gio/gio.h>

#include "devd-client.h"
#include "devd-version-macros.h"

G_BEGIN_DECLS

typedef enum
{
  DEVD_DEVICE_KIND_COMPUTER,
  DEVD_DEVICE_KIND_TABLET,
  DEVD_DEVICE_KIND_PHONE,
  DEVD_DEVICE_KIND_MICRO_CONTROLLER,
} DevdDeviceKind;

#define DEVD_TYPE_DEVICE (devd_device_get_type())

DEVD_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (DevdDevice, devd_device, DEVD, DEVICE, GObject)

struct _DevdDeviceClass
{
  GObjectClass parent_class;

  DevdClient *(*create_client) (DevdDevice *self);

  /*< private >*/
  gpointer _reserved[16];
};

DEVD_AVAILABLE_IN_ALL
const gchar    *devd_device_get_id         (DevdDevice     *self);
DEVD_AVAILABLE_IN_ALL
DevdClient     *devd_device_create_client  (DevdDevice     *self);
DEVD_AVAILABLE_IN_ALL
const gchar    *devd_device_get_icon_name  (DevdDevice     *self);
DEVD_AVAILABLE_IN_ALL
void            devd_device_set_icon_name  (DevdDevice     *self,
                                            const gchar    *icon_name);
DEVD_AVAILABLE_IN_ALL
DevdDeviceKind  devd_device_get_kind       (DevdDevice     *self);
DEVD_AVAILABLE_IN_ALL
void            devd_device_set_kind       (DevdDevice     *self,
                                            DevdDeviceKind  kind);
DEVD_AVAILABLE_IN_ALL
const gchar    *devd_device_get_machine_id (DevdDevice     *self);
DEVD_AVAILABLE_IN_ALL
void            devd_device_set_machine_id (DevdDevice     *self,
                                            const gchar    *machine_id);
DEVD_AVAILABLE_IN_ALL
const gchar    *devd_device_get_name       (DevdDevice     *self);
DEVD_AVAILABLE_IN_ALL
void            devd_device_set_name       (DevdDevice     *self,
                                            const gchar    *name);

G_END_DECLS
