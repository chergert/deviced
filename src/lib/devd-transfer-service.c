/* devd-transfer-service.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "devd-transfer-service"

#include "config.h"

#include <jsonrpc-glib.h>

#include "devd-macros.h"
#include "devd-trace.h"
#include "devd-transfer-service.h"
#include "devd-util.h"

#define READ_BUFFER_SIZE (4096*16)

struct _DevdTransferService
{
  DevdService parent_instance;
};

typedef struct
{
  GFile                 *src_file;
  gchar                 *dst_path;
  GInputStream          *stream;
  gchar                 *token;
  goffset                offset;
  goffset                total;
  GFileProgressCallback  progress;
  gpointer               progress_data;
  GDestroyNotify         progress_data_destroy;
} PutFileTaskData;

typedef struct
{
  GFile                 *dst_file;
  gchar                 *src_path;
  GOutputStream         *stream;
  gchar                 *token;
  GFile                 *tmp_file;
  GBytes                *bytes;
  goffset                current;
  gsize                  size;
  GFileProgressCallback  progress;
  gpointer               progress_data;
  GDestroyNotify         progress_data_destroy;
} GetFileTaskData;

static void devd_transfer_service_put_file_read_cb (GObject      *object,
                                                    GAsyncResult *result,
                                                    gpointer      user_data);
static void devd_transfer_service_get_file_data_cb (GObject      *object,
                                                    GAsyncResult *result,
                                                    gpointer      user_data);

G_DEFINE_TYPE (DevdTransferService, devd_transfer_service, DEVD_TYPE_SERVICE)

static void
put_file_task_data_free (PutFileTaskData *data)
{
  g_clear_object (&data->src_file);
  g_clear_pointer (&data->dst_path, g_free);
  g_clear_object (&data->stream);
  g_clear_pointer (&data->token, g_free);
  if (data->progress_data_destroy != NULL)
    g_clear_pointer (&data->progress_data, data->progress_data_destroy);
  g_slice_free (PutFileTaskData, data);
}

static void
get_file_task_data_free (GetFileTaskData *data)
{
  g_clear_object (&data->dst_file);
  g_clear_pointer (&data->src_path, g_free);
  g_clear_object (&data->stream);
  g_clear_pointer (&data->token, g_free);
  if (data->progress_data_destroy != NULL)
    g_clear_pointer (&data->progress_data, data->progress_data_destroy);
  g_slice_free (GetFileTaskData, data);
}

static void
devd_transfer_service_class_init (DevdTransferServiceClass *klass)
{
}

static void
devd_transfer_service_init (DevdTransferService *self)
{
}

/**
 * devd_transfer_service_new:
 * @client: a #DevdClient
 * @error: a location for a #GError
 *
 * Creates a new #DevdTransferService that communicates to deviced
 * using @client. If the service is not supported on the peer, then
 * %NULL is returned and @error is set.
 *
 * Returns: (transfer full): a new #DevdTransferService or %NULL and @error is set.
 *
 * Since: 3.28
 */
DevdTransferService *
devd_transfer_service_new (DevdClient  *client,
                           GError     **error)
{
  return devd_service_new (DEVD_TYPE_TRANSFER_SERVICE,
                           DEVD_TRANSFER_SERVICE_NAME,
                           client,
                           error);
}


static void
devd_transfer_service_put_file_finish_cb (GObject      *object,
                                          GAsyncResult *result,
                                          gpointer      user_data)
{
  DevdTransferService *self = (DevdTransferService *)object;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;

  DEVD_ENTRY;

  g_assert (DEVD_IS_TRANSFER_SERVICE (self));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (result));

  if (!devd_service_call_finish (DEVD_SERVICE (self), result, &reply, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);

  DEVD_EXIT;
}

static void
devd_transfer_service_put_file_data_cb (GObject      *object,
                                        GAsyncResult *result,
                                        gpointer      user_data)
{
  DevdTransferService *self = (DevdTransferService *)object;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  PutFileTaskData *task_data;
  GCancellable *cancellable;

  DEVD_ENTRY;

  g_assert (DEVD_IS_TRANSFER_SERVICE (self));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (result));

  task_data = g_task_get_task_data (task);
  cancellable = g_task_get_cancellable (task);

  g_assert (task_data != NULL);
  g_assert (G_IS_INPUT_STREAM (task_data->stream));
  g_assert (G_IS_FILE (task_data->src_file));

  if (!devd_service_call_finish (DEVD_SERVICE (self), result, &reply, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  if (task_data->progress != NULL)
    task_data->progress (task_data->offset,
                         task_data->total,
                         task_data->progress_data);

  g_input_stream_read_bytes_async (task_data->stream,
                                   READ_BUFFER_SIZE,
                                   G_PRIORITY_DEFAULT,
                                   cancellable,
                                   devd_transfer_service_put_file_read_cb,
                                   g_steal_pointer (&task));

  DEVD_EXIT;
}

static void
devd_transfer_service_put_file_read_cb (GObject      *object,
                                        GAsyncResult *result,
                                        gpointer      user_data)
{
  GInputStream *stream = (GInputStream *)object;
  g_autoptr(GVariant) params = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GBytes) bytes = NULL;
  g_autoptr(GTask) task = user_data;
  DevdTransferService *self;
  PutFileTaskData *task_data;
  GCancellable *cancellable;
  g_autofree gchar *out_data = NULL;
  const guchar *in_data;
  gsize len;

  DEVD_ENTRY;

  g_assert (G_IS_INPUT_STREAM (stream));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  cancellable = g_task_get_cancellable (task);
  self = g_task_get_source_object (task);
  task_data = g_task_get_task_data (task);

  g_assert (G_IS_FILE (task_data->src_file));
  g_assert (task_data->token != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));
  g_assert (DEVD_IS_TRANSFER_SERVICE (self));

  bytes = g_input_stream_read_bytes_finish (stream, result, &error);

  if (bytes == NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  in_data = g_bytes_get_data (bytes, &len);

  if (in_data == NULL || len == 0)
    {
      params = JSONRPC_MESSAGE_NEW (
        "token", JSONRPC_MESSAGE_PUT_STRING (task_data->token)
      );

      devd_service_call_async (DEVD_SERVICE (self),
                               DEVD_TRANSFER_SERVICE_NAME".put-file-finish",
                               params,
                               cancellable,
                               devd_transfer_service_put_file_finish_cb,
                               g_steal_pointer (&task));

      DEVD_EXIT;
    }

  out_data = g_base64_encode (in_data, len);

  params = JSONRPC_MESSAGE_NEW (
    "token", JSONRPC_MESSAGE_PUT_STRING (task_data->token),
    "offset", JSONRPC_MESSAGE_PUT_INT64 (task_data->offset),
    "data", JSONRPC_MESSAGE_PUT_STRING (out_data)
  );

  task_data->offset += len;

  devd_service_call_async (DEVD_SERVICE (self),
                           DEVD_TRANSFER_SERVICE_NAME".put-file-data",
                           params,
                           cancellable,
                           devd_transfer_service_put_file_data_cb,
                           g_steal_pointer (&task));

  DEVD_EXIT;
}

static void
devd_transfer_service_put_file_begin_cb (GObject      *object,
                                         GAsyncResult *result,
                                         gpointer      user_data)
{
  DevdTransferService *self = (DevdTransferService *)object;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  PutFileTaskData *task_data;
  GCancellable *cancellable;
  const gchar *token = NULL;

  DEVD_ENTRY;

  g_assert (DEVD_IS_TRANSFER_SERVICE (self));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  cancellable = g_task_get_cancellable (task);
  task_data = g_task_get_task_data (task);

  if (!devd_service_call_finish (DEVD_SERVICE (self), result, &reply, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  if (!reply || !g_variant_is_of_type (reply, G_VARIANT_TYPE_VARDICT))
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_INVAL,
                               "Invalid reply from peer");
      DEVD_EXIT;
    }

  if (!JSONRPC_MESSAGE_PARSE (reply, "token", JSONRPC_MESSAGE_GET_STRING (&token)))
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_INVAL,
                               "Reply did not include write token");
      DEVD_EXIT;
    }

  task_data->token = g_strdup (token);

  g_input_stream_read_bytes_async (task_data->stream,
                                   READ_BUFFER_SIZE,
                                   G_PRIORITY_DEFAULT,
                                   cancellable,
                                   devd_transfer_service_put_file_read_cb,
                                   g_steal_pointer (&task));

  DEVD_EXIT;
}

static void
devd_transfer_service_put_file_open_cb (GObject      *object,
                                        GAsyncResult *result,
                                        gpointer      user_data)
{
  GFile *file = (GFile *)object;
  g_autoptr(GInputStream) stream = NULL;
  g_autoptr(GFileInfo) info = NULL;
  g_autoptr(GVariant) params = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  DevdTransferService *self;
  PutFileTaskData *task_data;
  GCancellable *cancellable;
  goffset size;

  DEVD_ENTRY;

  g_assert (G_IS_FILE (file));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  self = g_task_get_source_object (task);
  g_assert (DEVD_IS_TRANSFER_SERVICE (self));

  task_data = g_task_get_task_data (task);
  g_assert (G_IS_FILE (task_data->src_file));
  g_assert (g_file_equal (task_data->src_file, file));
  g_assert (task_data->stream == NULL);

  cancellable = g_task_get_cancellable (task);

  stream = devd_file_read_with_info_finish (file, result, &info, &error);

  if (stream == NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  task_data->stream = g_steal_pointer (&stream);

  g_assert (info != NULL);
  g_assert (G_IS_FILE_INFO (info));

  size = g_file_info_get_size (info);

  params = JSONRPC_MESSAGE_NEW (
    "path", JSONRPC_MESSAGE_PUT_STRING (task_data->dst_path),
    "size", JSONRPC_MESSAGE_PUT_INT64 (size)
  );

  task_data->total = size;

  devd_service_call_async (DEVD_SERVICE (self),
                           DEVD_TRANSFER_SERVICE_NAME".put-file-begin",
                           params,
                           cancellable,
                           devd_transfer_service_put_file_begin_cb,
                           g_steal_pointer (&task));

  DEVD_EXIT;
}

/**
 * devd_transfer_service_put_file_async:
 * @self: a #DevdTransferService
 * @file: a #GFile to copy
 * @path: the path on the remote device
 * @progress: (nullable) (scope notified) (closure progress_data) (destroy progress_data_destroy):
 *   A progress callback or %NULL to execute when transfer progress is updated.
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: (closure user_data) (scope async): a callback to execute upon completion
 * @user_data: closure data for @callback
 *
 * Asynchronously copies the contents of @file to the remote device at @path.
 *
 * @progress is called periodically with updates to the progress of the copy.
 *
 * Since: 3.28
 */
void
devd_transfer_service_put_file_async (DevdTransferService   *self,
                                      GFile                 *file,
                                      const gchar           *path,
                                      GFileProgressCallback  progress,
                                      gpointer               progress_data,
                                      GDestroyNotify         progress_data_destroy,
                                      GCancellable          *cancellable,
                                      GAsyncReadyCallback    callback,
                                      gpointer               user_data)
{
  g_autoptr(GTask) task = NULL;
  PutFileTaskData *task_data;

  DEVD_ENTRY;

  g_assert (DEVD_IS_TRANSFER_SERVICE (self));
  g_assert (G_IS_FILE (file));
  g_assert (path != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  /*
   * To upload a file using our JSON-RPC protocol, we need to submit
   * at least three requests.
   *
   * - The first request contains information about the file we are
   *   creating. The daemon will use this to setup a local temporary
   *   target for us to fill with data (and ensure there is space for
   *   the file before we copy data).
   *
   *   This RPC will also return us a token that we must use on follow
   *   up requests to add data to the file.
   *
   * - Then we proceed with a series of N writes to add contents to the
   *   temporary file on the daemon. We specify the offset for the bytes
   *   to write. The bytes are base64 encoding so that we can put them
   *   into JSON. Not super ideal, but it allows us to use JSON-RPC which
   *   is good for external tooling.
   *
   * - Finally, we submit a request to close the temporary file and move
   *   it into the final target position.
   */

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_transfer_service_put_file_async);

  task_data = g_slice_new0 (PutFileTaskData);
  task_data->src_file = g_object_ref (file);
  task_data->dst_path = g_strdup (path);
  task_data->progress = progress;
  task_data->progress_data = progress_data;
  task_data->progress_data_destroy = progress_data_destroy;
  g_task_set_task_data (task, task_data, (GDestroyNotify)put_file_task_data_free);

  devd_file_read_with_info_async (file,
                                  G_FILE_ATTRIBUTE_STANDARD_NAME","
                                  G_FILE_ATTRIBUTE_STANDARD_SIZE,
                                  cancellable,
                                  devd_transfer_service_put_file_open_cb,
                                  g_steal_pointer (&task));

  DEVD_EXIT;
}

/**
 * devd_transfer_service_put_file_finish:
 * @self: a #DevdTransferService
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous request to upload a file to the device.
 *
 * Clients are required to use rename() properly to ensure that the file is
 * either replaced or created if successful so you get either the previous
 * version or new version of the file.
 *
 * Returns: %TRUE if successful; otherwise %FALSE and @error is set.
 *
 * Since: 3.28
 */
gboolean
devd_transfer_service_put_file_finish (DevdTransferService  *self,
                                       GAsyncResult         *result,
                                       GError              **error)
{
  gboolean ret;

  DEVD_ENTRY;

  g_assert (DEVD_IS_TRANSFER_SERVICE (self));
  g_assert (G_IS_TASK (result));

  ret = g_task_propagate_boolean (G_TASK (result), error);

  DEVD_RETURN (ret);
}

static void
devd_transfer_service_get_file_close_cb (GObject      *object,
                                         GAsyncResult *result,
                                         gpointer      user_data)
{
  GOutputStream *stream = (GOutputStream *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  GetFileTaskData *task_data;

  DEVD_ENTRY;

  g_assert (G_IS_OUTPUT_STREAM (stream));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!g_output_stream_close_finish (stream, result, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  task_data = g_task_get_task_data (task);
  g_assert (task_data != NULL);
  g_assert (G_IS_FILE (task_data->tmp_file));
  g_assert (G_IS_FILE (task_data->dst_file));

  /*
   * This is synchronous, but since we created it in the directory of our
   * destination, it should in practice be a rename(). Of course, that will
   * be much more painful for remote files, but we will deal with that when
   * we decide it is a use case we care about.
   *
   * To ensure that would fail when rename() isn't supported, we disable
   * fallbacks for move.
   */

  if (!g_file_move (task_data->tmp_file,
                    task_data->dst_file,
                    (G_FILE_COPY_OVERWRITE |
                     G_FILE_COPY_BACKUP |
                     G_FILE_COPY_ALL_METADATA |
                     G_FILE_COPY_NO_FALLBACK_FOR_MOVE),
                    NULL, NULL, NULL, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);

  DEVD_EXIT;
}

static void
devd_transfer_service_get_file_write_cb (GObject      *object,
                                         GAsyncResult *result,
                                         gpointer      user_data)
{
  GOutputStream *stream = (GOutputStream *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GVariant) params = NULL;
  DevdTransferService *self;
  GetFileTaskData *task_data;
  GCancellable *cancellable;
  gssize n_written;
  gsize expected;

  DEVD_ENTRY;

  g_assert (G_IS_OUTPUT_STREAM (stream));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  n_written = g_output_stream_write_bytes_finish (stream, result, &error);

  if (error != NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  task_data = g_task_get_task_data (task);
  g_assert (task_data != NULL);
  g_assert (G_IS_FILE (task_data->tmp_file));
  g_assert (G_IS_FILE (task_data->dst_file));
  g_assert (task_data->bytes != NULL);

  cancellable = g_task_get_cancellable (task);
  self = g_task_get_source_object (task);

  expected = g_bytes_get_size (task_data->bytes);

  if (n_written < expected)
    {
      g_autoptr(GBytes) next = g_bytes_new_from_bytes (task_data->bytes,
                                                       n_written,
                                                       expected - n_written);

      g_clear_pointer (&task_data->bytes, g_bytes_unref);
      task_data->bytes = g_steal_pointer (&next);

      g_output_stream_write_bytes_async (task_data->stream,
                                         task_data->bytes,
                                         G_PRIORITY_DEFAULT,
                                         cancellable,
                                         devd_transfer_service_get_file_write_cb,
                                         g_steal_pointer (&task));

      DEVD_EXIT;
    }

  g_clear_pointer (&task_data->bytes, g_bytes_unref);

  params = JSONRPC_MESSAGE_NEW (
    "token", JSONRPC_MESSAGE_PUT_STRING (task_data->token)
  );

  devd_service_call_async (DEVD_SERVICE (self),
                           DEVD_TRANSFER_SERVICE_NAME".get-file-data",
                           params,
                           cancellable,
                           devd_transfer_service_get_file_data_cb,
                           g_steal_pointer (&task));

  DEVD_EXIT;
}

static void
devd_transfer_service_get_file_data_cb (GObject      *object,
                                        GAsyncResult *result,
                                        gpointer      user_data)
{
  DevdTransferService *self = (DevdTransferService *)object;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  g_autofree guchar *decoded = NULL;
  const gchar *encoded = NULL;
  GetFileTaskData *task_data;
  GCancellable *cancellable;
  gsize len = 0;

  DEVD_ENTRY;

  g_assert (DEVD_IS_TRANSFER_SERVICE (self));
  g_assert (G_IS_TASK (task));

  if (!devd_service_call_finish (DEVD_SERVICE (self), result, &reply, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  if (!JSONRPC_MESSAGE_PARSE (reply, "data", JSONRPC_MESSAGE_GET_STRING (&encoded)) ||
      !(decoded = g_base64_decode (encoded, &len)))
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_INVALID_DATA,
                               "Invalid reply from peer");
      DEVD_EXIT;
    }

  task_data = g_task_get_task_data (task);
  g_assert (task_data != NULL);
  g_assert (G_IS_OUTPUT_STREAM (task_data->stream));
  g_assert (task_data->bytes == NULL);

  cancellable = g_task_get_cancellable (task);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  if (len == 0)
    {
      g_output_stream_close_async (task_data->stream,
                                   G_PRIORITY_DEFAULT,
                                   cancellable,
                                   devd_transfer_service_get_file_close_cb,
                                   g_steal_pointer (&task));
      DEVD_EXIT;
    }

  task_data->bytes = g_bytes_new_take (g_steal_pointer (&decoded), len);
  task_data->current += len;

  g_output_stream_write_bytes_async (task_data->stream,
                                     task_data->bytes,
                                     G_PRIORITY_DEFAULT,
                                     cancellable,
                                     devd_transfer_service_get_file_write_cb,
                                     g_steal_pointer (&task));

  if (task_data->progress != NULL)
    task_data->progress (task_data->current, task_data->size, task_data->progress_data);

  DEVD_EXIT;
}

static void
devd_transfer_service_get_file_mktemp_cb (GObject      *object,
                                        GAsyncResult *result,
                                        gpointer      user_data)
{
  GFile *file = (GFile *)object;
  g_autoptr(GFileOutputStream) stream = NULL;
  g_autoptr(GVariant) params = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GFile) tmp_file = NULL;
  DevdTransferService *self;
  GetFileTaskData *task_data;
  GCancellable *cancellable;

  DEVD_ENTRY;

  g_assert (G_IS_FILE (file));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  tmp_file = devd_create_tmp_in_dir_finish (file, result, &stream, &error);

  if (tmp_file == NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  g_assert (stream != NULL);
  g_assert (G_IS_FILE_OUTPUT_STREAM (stream));

  task_data = g_task_get_task_data (task);
  task_data->stream = G_OUTPUT_STREAM (g_steal_pointer (&stream));
  task_data->tmp_file = g_steal_pointer (&tmp_file);

  /*
   * Now that we have our temp file in place, we can start receiving data
   * from the peer. Request the first chunk.
   */

  self = g_task_get_source_object (task);
  cancellable = g_task_get_cancellable (task);

  params = JSONRPC_MESSAGE_NEW (
    "token", JSONRPC_MESSAGE_PUT_STRING (task_data->token)
  );

  devd_service_call_async (DEVD_SERVICE (self),
                           DEVD_TRANSFER_SERVICE_NAME".get-file-data",
                           params,
                           cancellable,
                           devd_transfer_service_get_file_data_cb,
                           g_steal_pointer (&task));

  DEVD_EXIT;
}

static void
devd_transfer_service_get_file_begin_cb (GObject      *object,
                                       GAsyncResult *result,
                                       gpointer      user_data)
{
  DevdTransferService *self = (DevdTransferService *)object;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GFile) directory = NULL;
  GetFileTaskData *task_data;
  GCancellable *cancellable;
  const gchar *token = NULL;
  gint64 size = 0;
  gboolean r;

  DEVD_ENTRY;

  g_assert (DEVD_IS_TRANSFER_SERVICE (self));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!devd_service_call_finish (DEVD_SERVICE (self), result, &reply, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      DEVD_EXIT;
    }

  r = JSONRPC_MESSAGE_PARSE (reply,
    "size", JSONRPC_MESSAGE_GET_INT64 (&size),
    "token", JSONRPC_MESSAGE_GET_STRING (&token)
  );

  if (!r)
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_INVALID_DATA,
                               "Invalid get-file-begin response from peer");
      DEVD_EXIT;
    }

  task_data = g_task_get_task_data (task);
  task_data->size = size;
  task_data->token = g_strdup (token);

  directory = g_file_get_parent (task_data->dst_file);
  cancellable = g_task_get_cancellable (task);
  devd_create_tmp_in_dir_async (directory,
                                cancellable,
                                devd_transfer_service_get_file_mktemp_cb,
                                g_steal_pointer (&task));

  DEVD_EXIT;
}

/**
 * devd_transfer_service_get_file_async:
 * @self: a #DevdTransferService
 * @path: the path on the remote device
 * @file: a #GFile to write to
 * @progress: (nullable) (scope notified) (closure progress_data) (destroy progress_data_destroy):
 *   A progress callback or %NULL to execute when transfer progress is updated.
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: (closure user_data) (scope async): a callback to execute upon completion
 * @user_data: closure data for @callback
 *
 * Asynchronously copies the contents of @path on the remote device to @file.
 *
 * @progress is called periodically with updates to the progress of the copy.
 *
 * Since: 3.28
 */
void
devd_transfer_service_get_file_async (DevdTransferService   *self,
                                      const gchar           *path,
                                      GFile                 *file,
                                      GFileProgressCallback  progress,
                                      gpointer               progress_data,
                                      GDestroyNotify         progress_data_destroy,
                                      GCancellable          *cancellable,
                                      GAsyncReadyCallback    callback,
                                      gpointer               user_data)
{
  g_autoptr(GTask) task = NULL;
  g_autoptr(GVariant) params = NULL;
  GetFileTaskData *task_data;

  DEVD_ENTRY;

  g_assert (DEVD_IS_TRANSFER_SERVICE (self));
  g_assert (G_IS_FILE (file));
  g_assert (path != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_transfer_service_get_file_async);

  params = JSONRPC_MESSAGE_NEW (
    "path", JSONRPC_MESSAGE_PUT_STRING (path)
  );

  task_data = g_slice_new0 (GetFileTaskData);
  task_data->src_path = g_strdup (path);
  task_data->dst_file = g_object_ref (file);
  task_data->progress = progress;
  task_data->progress_data = progress_data;
  task_data->progress_data_destroy = progress_data_destroy;
  g_task_set_task_data (task, task_data, (GDestroyNotify)get_file_task_data_free);

  devd_service_call_async (DEVD_SERVICE (self),
                           DEVD_TRANSFER_SERVICE_NAME".get-file-begin",
                           params,
                           cancellable,
                           devd_transfer_service_get_file_begin_cb,
                           g_steal_pointer (&task));

  DEVD_EXIT;
}

/**
 * devd_transfer_service_get_file_finish:
 * @self: a #DevdTransferService
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous request to download a file from the device.
 *
 * Returns: %TRUE if successful; otherwise %FALSE and @error is set.
 *
 * Since: 3.28
 */
gboolean
devd_transfer_service_get_file_finish (DevdTransferService  *self,
                                       GAsyncResult         *result,
                                       GError              **error)
{
  gboolean ret;

  DEVD_ENTRY;

  g_assert (DEVD_IS_TRANSFER_SERVICE (self));
  g_assert (G_IS_TASK (result));

  ret = g_task_propagate_boolean (G_TASK (result), error);

  DEVD_RETURN (ret);
}
