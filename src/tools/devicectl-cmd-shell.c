/* devicectl-cmd-shell.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>
#include <glib-unix.h>

#include "devicectl.h"

static gchar *saved_pty_id;
static gchar *saved_identifier;

static void
on_sigwinch (int signum)
{
  write (STDOUT_FILENO, "TODO: Notify of sigwinch\n", 25);
}

static void
wait_for_process_cb (GObject      *object,
                     GAsyncResult *result,
                     gpointer      user_data)
{
  DevdProcessService *service = (DevdProcessService *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  const gchar *identifier;
  gboolean exited;
  gint exit_code;
  gint term_sig;

  g_assert (DEVD_IS_PROCESS_SERVICE (service));

  if (!devd_process_service_wait_for_process_finish (service, result, &exited, &exit_code, &term_sig, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  identifier = g_task_get_task_data (task);

  if (!exited)
    g_printerr ("%s exited with signal %d\n", identifier, term_sig);
  else if (!exit_code)
    g_printerr ("%s exited with exit code %d\n", identifier, exit_code);

  g_clear_pointer (&saved_identifier, g_free);

  g_task_return_boolean (task, TRUE);
}

static void
spawn_shell_cb (GObject      *object,
                GAsyncResult *result,
                gpointer      user_data)
{
  DevdProcessService *service = (DevdProcessService *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *identifier = NULL;

  g_assert (DEVD_IS_PROCESS_SERVICE (service));

  if (!(identifier = devd_process_service_spawn_finish (service, result, &error)))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    devd_process_service_wait_for_process_async (service,
                                                 identifier,
                                                 g_task_get_cancellable (task),
                                                 wait_for_process_cb,
                                                 g_object_ref (task));

  saved_identifier = g_strdup (identifier);

  g_task_set_task_data (task, g_steal_pointer (&identifier), g_free);
}

static void
create_pty_cb (GObject      *object,
               GAsyncResult *result,
               gpointer      user_data)
{
  static const gchar * argv[] = { "/bin/bash", NULL };
  DevdProcessService *service = (DevdProcessService *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *pty_id = NULL;

  if (!(pty_id = devd_process_service_create_pty_finish (service, result, &error)))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    devd_process_service_spawn_async (service,
                                      pty_id,
                                      argv,
                                      NULL,
                                      g_task_get_cancellable (task),
                                      spawn_shell_cb,
                                      g_object_ref (task));

  saved_pty_id = g_steal_pointer (&pty_id);
}

static gboolean
forward_sigint_handler (gpointer data)
{
  DevdProcessService *service = data;

  if (saved_identifier != NULL)
    devd_process_service_send_signal (service, saved_identifier, SIGINT);

  return G_SOURCE_CONTINUE;
}

gboolean
devicectl_cmd_shell (gint                  argc,
                     const gchar * const  *argv,
                     GCancellable         *cancellable,
                     GError              **error)
{
  static gboolean did_sigwinch;
  g_autoptr(DevdProcessService) service = NULL;
  g_autoptr(GTask) task = NULL;
  DevdClient *client;
  gboolean ret;
  guint forward_sigint;

  if (!(client = devicectl_get_client (error)))
    return FALSE;

  /* We totally can support this for scripting, just not yet */
  if (!isatty (STDIN_FILENO))
    return devicectl_arg_error (error, "stdin is not a PTY, cannot setup shell");

  if (!did_sigwinch)
    {
      did_sigwinch = TRUE;
      signal (SIGWINCH, on_sigwinch);
    }

  if (!(service = devd_process_service_new (client, error)))
    return FALSE;

  task = g_task_new (NULL, cancellable, NULL, NULL);

  /* We need to create a new PTY proxy on the peer before we can spawn a shell.
   * That way, we get proxied PTY data to our PTY (stdin/out/err in this case).
   */
  devd_process_service_create_pty_async (service,
                                         STDIN_FILENO,
                                         cancellable,
                                         create_pty_cb,
                                         g_object_ref (task));

  devicectl_disable_sigint ();

  forward_sigint = g_unix_signal_add (SIGINT, forward_sigint_handler, client);

  ret = devicectl_wait_for_task (task, error);

  if (saved_pty_id != NULL)
    devd_process_service_destroy_pty_async (service, saved_pty_id, NULL, NULL, NULL);
  g_clear_pointer (&saved_pty_id, g_free);

  g_source_remove (forward_sigint);

  devicectl_enable_sigint ();

  return ret;
}
