/* devicectl-cmd-run-app.c
 *
 * Copyright 2018 Patrick Griffis <tingping@tingping.se>
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>
#include <glib-unix.h>

#include "devicectl.h"

typedef struct {
  DevdClient *client;
  DevdProcessService *process_service;
  char *app_id;
  char *pid;
  char *pty_id;
} RunAppData;

static void
run_app_data_free (gpointer ptr)
{
  RunAppData *data = ptr;
  g_clear_object (&data->client);
  g_clear_object (&data->process_service);
  g_clear_pointer (&data->app_id, g_free);
  g_clear_pointer (&data->pid, g_free);
  g_clear_pointer (&data->pty_id, g_free);
}

static void
run_app_data_release (gpointer ptr)
{
  g_atomic_rc_box_release_full (ptr, (GDestroyNotify) run_app_data_free);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (RunAppData, run_app_data_release)

static void
devicectl_wait_for_process_cb (GObject      *object,
                               GAsyncResult *result,
                               gpointer      user_data)
{
  DevdProcessService *process_service = (DevdProcessService *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;

  g_assert (DEVD_IS_PROCESS_SERVICE (process_service));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  devd_process_service_wait_for_process_finish (process_service,
                                                result,
                                                NULL, NULL, NULL,
                                                &error);

  if (error != NULL)
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);
}

static void
devicectl_run_app_cb (GObject      *object,
                      GAsyncResult *result,
                      gpointer      user_data)
{
  DevdClient *client = (DevdClient *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  RunAppData *data;

  g_assert (DEVD_IS_CLIENT (client));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  data = g_task_get_task_data (task);
  data->pid = devd_client_run_app_finish (client, result, &error);

  if (error != NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_print ("Started %s with PID %s\n", data->app_id, data->pid);

  devd_process_service_wait_for_process_async (data->process_service,
                                               data->pid,
                                               g_task_get_cancellable (task),
                                               devicectl_wait_for_process_cb,
                                               g_object_ref (task));
}

static gboolean
forward_sigint_handler (gpointer user_data)
{
  RunAppData *data = user_data;

  if (data->pid != NULL)
    devd_process_service_send_signal (data->process_service, data->pid, SIGINT);

  return G_SOURCE_CONTINUE;
}

static void
devicectl_create_pty_cb (GObject      *object,
                         GAsyncResult *result,
                         gpointer      user_data)
{
  DevdProcessService *process_service = (DevdProcessService *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  const gchar *provider = "flatpak"; // Only one for now
  RunAppData *data;

  g_assert (DEVD_IS_PROCESS_SERVICE (process_service));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  data = g_task_get_task_data (task);
  data->pty_id = devd_process_service_create_pty_finish (process_service, result, &error);

  if (error != NULL)
    g_task_return_error (task, g_steal_pointer (&error));
  else
    devd_client_run_app_async (data->client,
                               provider,
                               data->app_id,
                               data->pty_id,
                               g_task_get_cancellable (task),
                               devicectl_run_app_cb,
                               g_object_ref (task));
}

gboolean
devicectl_cmd_run_app (gint                  argc,
                       const gchar * const  *argv,
                       GCancellable         *cancellable,
                       GError              **error)
{
  DevdClient *client;
  g_autoptr(GTask) task = NULL;
  const gchar *app_id = NULL;
  g_autoptr(RunAppData) data = NULL;
  gboolean ret;
  guint forward_sigint;

  g_return_val_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable), FALSE);

  if (!(client = devicectl_get_client (error)))
    return FALSE;

  if (argc < 2)
    return devicectl_arg_error (error, _("Must pass an APP_ID to run"));

  app_id = argv[1];

  task = g_task_new (NULL, NULL, NULL, NULL);

  data = g_atomic_rc_box_new0 (RunAppData);

  data->process_service = devd_process_service_new (client, error);
  if (data->process_service == NULL)
    return FALSE;

  data->client = g_object_ref (client);
  data->app_id = g_strdup (app_id);
  g_task_set_task_data (task, g_atomic_rc_box_acquire (data), run_app_data_release);

  devicectl_disable_sigint ();
  forward_sigint = g_unix_signal_add (SIGINT, forward_sigint_handler, data);

  devd_process_service_create_pty_async (data->process_service,
                                         STDIN_FILENO,
                                         cancellable,
                                         devicectl_create_pty_cb,
                                         g_object_ref (task));

  ret = devicectl_wait_for_task (task, error);

  if (data->pty_id != NULL)
    devd_process_service_destroy_pty_async (data->process_service, data->pty_id, NULL, NULL, NULL);

  g_source_remove (forward_sigint);
  devicectl_enable_sigint ();

  return ret;
}
