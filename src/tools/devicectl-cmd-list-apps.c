/* devicectl-cmd-list-apps.c
 *
 * Copyright 2018 Patrick Griffis <tingping@tingping.se>
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "devicectl.h"

static void
devicectl_list_apps_cb (GObject      *object,
                        GAsyncResult *result,
                        gpointer      user_data)
{
  DevdClient *client = (DevdClient *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_autoptr(GPtrArray) app_infos = NULL;
  g_auto(DevdTable) table = {0};

  if (!(app_infos = devd_client_list_apps_finish (client, result, &error)))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  if (app_infos->len == 0)
    {
      g_printerr ("No applications where found.\n");
      goto finish;
    }

  devd_table_init (&table, 5, TRUE);
  devd_table_add (&table,
                  0, _("ID"),
                  1, _("Name"),
                  2, _("Installed Size"),
                  3, _("Provider"),
                  4, _("Commit ID"),
                  -1);

  for (guint i = 0; i < app_infos->len; i++)
    {
      DevdAppInfo *app_info = g_ptr_array_index (app_infos, i);
      gsize size = devd_app_info_get_installed_size (app_info);
      g_autofree gchar *size_format = g_format_size (size);

      devd_table_add (&table,
                      0, devd_app_info_get_id (app_info),
                      1, devd_app_info_get_name (app_info),
                      2, size_format,
                      3, devd_app_info_get_provider (app_info),
                      4, devd_app_info_get_commit_id (app_info),
                      -1);
    }

  devd_table_print (&table);

finish:
  g_task_return_boolean (task, TRUE);
}

gboolean
devicectl_cmd_list_apps (gint                  argc,
                         const gchar * const  *argv,
                         GCancellable         *cancellable,
                         GError              **error)
{
  DevdClient *client;
  g_autoptr(GTask) task = NULL;

  if (!(client = devicectl_get_client (error)))
    return FALSE;

  task = g_task_new (NULL, NULL, NULL, NULL);

  devd_client_list_apps_async (client,
                               cancellable,
                               devicectl_list_apps_cb,
                               g_object_ref (task));

  return devicectl_wait_for_task (task, error);
}
