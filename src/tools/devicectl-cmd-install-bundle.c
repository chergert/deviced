/* devicectl-cmd-install-bundle.c
 *
 * Copyright 2018 Patrick Griffis <tingping@tingping.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "devicectl.h"

static void
devicectl_install_bundle_cb (GObject      *object,
                             GAsyncResult *result,
                             gpointer      user_data)
{
  DevdFlatpakService *service = (DevdFlatpakService *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;

  if (!devd_flatpak_service_install_bundle_finish (service, result, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);
}

gboolean
devicectl_cmd_install_bundle (gint                  argc,
                              const gchar * const  *argv,
                              GCancellable         *cancellable,
                              GError              **error)
{
  g_autoptr(DevdFlatpakService) service = NULL;
  g_autoptr(GTask) task = NULL;
  const gchar *path;
  DevdClient *client;

  if (!(client = devicectl_get_client (error)))
    return FALSE;

  if (argc < 2)
    return devicectl_arg_error (error, _("Must pass a path to a bundle to install"));

  path = argv[1];

  if (!(service = devd_flatpak_service_new (client, error)))
    return FALSE;

  task = g_task_new (NULL, NULL, NULL, NULL);

  devd_flatpak_service_install_bundle_async (service,
                                             path,
                                             cancellable,
                                             devicectl_install_bundle_cb,
                                             g_object_ref (task));

  return devicectl_wait_for_task (task, error);
}
