/* devicectl-cmd-help.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "devicectl.h"

gboolean
devicectl_cmd_help (gint                  argc,
                    const gchar * const  *argv,
                    GCancellable         *cancellable,
                    GError              **error)
{
  g_auto(DevdTable) table = {0};

  devd_table_init (&table, 3, TRUE);
  devd_table_add (&table,
                  0, _("Command"),
                  1, "",
                  2, _("Description"),
                  -1);

#define DEVICECTL_COMMAND(func, cmd, help, usage, req_client) \
  devd_table_add (&table, \
                  0, cmd, \
                  1, req_client ? "*" : "", \
                  2, help, \
                  -1);
# include "devicectl-commands.defs"
#undef DEVICECTL_COMMAND

  devd_table_print (&table);

  g_print ("\n");
  g_print (_("* denotes that command requires an active client connection."));
  g_print ("\n");

  return TRUE;
}
