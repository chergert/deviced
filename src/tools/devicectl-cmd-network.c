/* devicectl-cmd-network.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>
#include <NetworkManager.h>

#include "devicectl.h"

static GSettings *
lookup_settings (void)
{
  GSettingsSchemaSource *source;
  g_autoptr(GSettingsSchema) schema = NULL;

  if (!(source = g_settings_schema_source_get_default ()) ||
      !(schema = g_settings_schema_source_lookup (source, "org.gnome.deviced", TRUE)))
    return NULL;

  return g_settings_new_full (schema, NULL, NULL);
}

static GPtrArray *
get_networks (GSettings *settings)
{
  gchar **networks = g_settings_get_strv (settings, "enabled-connections");
  GPtrArray *ar = g_ptr_array_new_with_free_func (g_free);

  for (guint i = 0; networks[i]; i++)
    g_ptr_array_add (ar, g_steal_pointer (&networks[i]));
  g_free (networks);

  return ar;
}

static gboolean
ptr_array_contains (GPtrArray   *ar,
                    const gchar *key)
{
  for (guint i = 0; i < ar->len; i++)
    {
      const gchar *value = g_ptr_array_index (ar, i);

      if (g_strcmp0 (value, key) == 0)
        return TRUE;
    }

  return FALSE;
}

static void
ptr_array_remove (GPtrArray   *ar,
                  const gchar *key)
{
  for (guint i = 0; i < ar->len; i++)
    {
      const gchar *value = g_ptr_array_index (ar, i);

      if (g_strcmp0 (value, key) == 0)
        {
          g_ptr_array_remove_index (ar, i);
          return;
        }
    }
}

gboolean
devicectl_cmd_enable_network (gint                  argc,
                              const gchar * const  *argv,
                              GCancellable         *cancellable,
                              GError              **error)
{
  g_autoptr(GSettings) settings = NULL;
  g_autoptr(GPtrArray) networks = NULL;

  if (!(settings = lookup_settings ()))
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_NOT_SUPPORTED,
                   "Deviced does not seem to be installed, cannot alter settings");
      return FALSE;
    }

  g_settings_set_boolean (settings, "enable-network", TRUE);

  if (argc == 1)
    return TRUE;

  networks = get_networks (settings);

  for (guint i = 1; i < argc; i++)
    {
      if (!g_uuid_string_is_valid (argv[i]))
        {
          g_set_error (error,
                       G_IO_ERROR,
                       G_IO_ERROR_INVAL,
                       "%s is not an valid connection uuid",
                       argv[i]);
          return FALSE;
        }

      if (!ptr_array_contains (networks, argv[i]))
        g_ptr_array_add (networks, g_strdup (argv[i]));
    }
  g_ptr_array_add (networks, NULL);

  g_settings_set_strv (settings, "enabled-connections", (const gchar * const *)networks->pdata);

  return TRUE;
}

gboolean
devicectl_cmd_show_available_networks (gint                  argc,
                                       const gchar * const  *argv,
                                       GCancellable         *cancellable,
                                       GError              **error)
{
  const GPtrArray *active_connections;
  NMClient *nm_client;

  if (!(nm_client = nm_client_new (NULL, error)))
    return FALSE;

  active_connections = nm_client_get_active_connections (nm_client);

  g_print ("%u available connections\n", active_connections->len);
  g_print ("You may use the UUID below like 'enable-network UUID' to allow connections on that network\n\n");

  for (guint i = 0; i < active_connections->len; i++)
    {
      NMActiveConnection *conn = g_ptr_array_index (active_connections, i);

      g_print (" %s = %s\n",
               nm_active_connection_get_uuid (conn),
               nm_active_connection_get_id (conn));
    }

  g_clear_object (&nm_client);

  return TRUE;
}

gboolean
devicectl_cmd_disable_network (gint                  argc,
                               const gchar * const  *argv,
                               GCancellable         *cancellable,
                               GError              **error)
{
  g_autoptr(GSettings) settings = NULL;
  g_autoptr(GPtrArray) networks = NULL;

  if (!(settings = lookup_settings ()))
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_NOT_SUPPORTED,
                   "Deviced does not seem to be installed, cannot alter settings");
      return FALSE;
    }

  if (argc == 1)
    {
      g_settings_set_boolean (settings, "enable-network", FALSE);
      return TRUE;
    }

  networks = get_networks (settings);

  for (guint i = 1; i < argc; i++)
    ptr_array_remove (networks, argv[i]);
  g_ptr_array_add (networks, NULL);

  g_settings_set_strv (settings, "enabled-connections", (const gchar * const *)networks->pdata);

  if (networks->len == 1)
    g_settings_set_boolean (settings, "enable-network", FALSE);

  return TRUE;
}
