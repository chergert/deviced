/* devicectl-cmd-list-files.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "devicectl.h"

static void
print_info (DevdTable *table,
            GFileInfo *info)
{
  g_autofree gchar *sizestr = NULL;
  goffset size;

  g_assert (G_IS_FILE_INFO (info));

  size = g_file_info_get_size (info);
  sizestr = g_format_size (size);

  devd_table_add (table,
                  0, g_file_info_get_name (info),
                  1, sizestr,
                  2, g_file_info_get_content_type (info),
                  -1);
}

static void
devicectl_list_files_cb (GObject      *object,
                         GAsyncResult *result,
                         gpointer      user_data)
{
  DevdClient *client = (DevdClient *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GPtrArray) files = NULL;
  g_autoptr(GError) error = NULL;
  g_auto(DevdTable) table = {0};

  g_assert (DEVD_IS_CLIENT (client));

  if (!(files = devd_client_list_files_finish (client, result, &error)))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  devd_table_init (&table, 3, TRUE);
  devd_table_add (&table,
                  0, _("Name"),
                  1, _("Size"),
                  2, _("Content Type"),
                  -1);

  for (guint i = 0; i < files->len; i++)
    {
      GFileInfo *info = g_ptr_array_index (files, i);
      print_info (&table, info);
    }

  devd_table_print (&table);

  g_task_return_boolean (task, TRUE);
}

gboolean
devicectl_cmd_list_files (gint                  argc,
                          const gchar * const  *argv,
                          GCancellable         *cancellable,
                          GError              **error)
{
  g_autoptr(GTask) task = NULL;
  DevdClient *client;

  if (!(client = devicectl_get_client (error)))
    return FALSE;

  if (argc != 2)
    return devicectl_arg_error (error, "list-files takes a single parameter, PATH");

  task = g_task_new (NULL, NULL, NULL, NULL);

  devd_client_list_files_async (client,
                                argv[1],
                                NULL,
                                cancellable,
                                devicectl_list_files_cb,
                                g_object_ref (task));

  return devicectl_wait_for_task (task, error);
}
