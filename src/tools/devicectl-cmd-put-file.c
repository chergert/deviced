/* devicectl-put-file.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>
#include <glib/gstdio.h>

#include "devicectl.h"

static void
progress_cb (goffset  current_num_bytes,
             goffset  total_num_bytes,
             gpointer user_data)
{
  const gchar *filename = user_data;

  devicectl_progress ((gdouble)current_num_bytes / (gdouble)total_num_bytes * 100.0,
                      filename);
}

static void
devicectl_cmd_put_file_cb (GObject      *object,
                           GAsyncResult *result,
                           gpointer      user_data)
{
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;

  if (!devd_transfer_service_put_file_finish (DEVD_TRANSFER_SERVICE (object), result, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);
}

gboolean
devicectl_cmd_put_file (gint                  argc,
                        const gchar * const  *argv,
                        GCancellable         *cancellable,
                        GError              **error)
{
  g_autoptr(GFile) file = NULL;
  g_autoptr(GTask) task = NULL;
  g_autoptr(DevdTransferService) service = NULL;
  g_autofree gchar *format = NULL;
  DevdClient *client;
  const gchar *dst_path;
  const gchar *src_path;
  GFileType file_type;
  gboolean ret;
  GStatBuf st;
  gint64 started = g_get_monotonic_time ();
  gint64 now;

  if (!(client = devicectl_get_client (error)))
    return FALSE;

  if (argc != 3)
    return devicectl_arg_error (error, "usage: put-file HOST_PATH DEVICE_PATH");

  if (!(service = devd_transfer_service_new (client, error)))
    return FALSE;

  src_path = argv[1];
  dst_path = argv[2];

  file = g_file_new_for_commandline_arg (src_path);
  if (!g_file_query_exists (file, cancellable))
    return devicectl_arg_error (error, "The file \"%s\" does not eixst.", src_path);

  g_stat (src_path, &st);

  file_type = g_file_query_file_type (file, 0, cancellable);
  if (file_type != G_FILE_TYPE_REGULAR)
    return devicectl_arg_error (error, "Can only upload regular files.");

  task = g_task_new (NULL, NULL, NULL, NULL);

  devd_transfer_service_put_file_async (service,
                                        file,
                                        dst_path,
                                        progress_cb,
                                        g_path_get_basename (src_path),
                                        g_free,
                                        cancellable,
                                        devicectl_cmd_put_file_cb,
                                        g_object_ref (task));

  ret = devicectl_wait_for_task (task, error);

  /* End our progress. */
  g_print ("\n");

  if (ret)
    {
      now = g_get_monotonic_time ();
      format = g_format_size (st.st_size);
      g_print ("Copied %s in %0.2lf seconds.\n",
               format, (now - started) / (gdouble)G_TIME_SPAN_SECOND);
    }

  return ret;
}
