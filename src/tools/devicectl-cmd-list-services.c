/* devicectl-cmd-list-services.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "devicectl.h"

gboolean
devicectl_cmd_list_services (gint                  argc,
                             const gchar * const  *argv,
                             GCancellable         *cancellable,
                             GError              **error)
{
  g_auto(DevdTable) table = {0};
  g_auto(GStrv) services = NULL;
  DevdClient *client;

  if (!(client = devicectl_get_client (error)))
    return FALSE;

  services = devd_client_get_services (client);

  devd_table_init (&table, 1, TRUE);
  devd_table_add (&table,
                  0, _("Name"),
                  -1);
  for (guint i = 0; services[i]; i++)
    devd_table_add (&table, 0, services[i], -1);
  devd_table_print (&table);

  return TRUE;
}
