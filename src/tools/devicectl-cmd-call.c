/* devicectl-cmd-call.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "devicectl.h"

static void
devicectl_cmd_call_cb (GObject      *object,
                       GAsyncResult *result,
                       gpointer      user_data)
{
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GError) error = NULL;
  GTask *task = user_data;

  if (!devd_client_call_finish (DEVD_CLIENT (object), result, &reply, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  if (reply == NULL)
    g_print ("Ok\n");
  else
    {
      g_autofree gchar *str = g_variant_print (reply, TRUE);
      g_print ("%s\n", str);
    }

  g_task_return_boolean (task, TRUE);
}

gboolean
devicectl_cmd_call (gint                  argc,
                    const gchar * const  *argv,
                    GCancellable         *cancellable,
                    GError              **error)
{
  g_autoptr(GVariant) params = NULL;
  g_autoptr(GTask) task = NULL;
  DevdClient *client;

  if (!(client = devicectl_get_client (error)))
    return FALSE;

  if (argc < 2)
    return devicectl_arg_error (error, "usage: method params");

  if (argc > 2)
    {
      if (!(params = g_variant_parse (NULL, argv[2], NULL, NULL, error)))
        return FALSE;
    }

  task = g_task_new (NULL, cancellable, NULL, NULL);
  devd_client_call_async (client, argv[1], params, cancellable, devicectl_cmd_call_cb, task);
  return devicectl_wait_for_task (task, error);
}
