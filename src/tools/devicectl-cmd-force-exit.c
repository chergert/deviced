/* devicectl-cmd-force-exit.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "devicectl.h"

gboolean
devicectl_cmd_force_exit (gint                  argc,
                          const gchar * const  *argv,
                          GCancellable         *cancellable,
                          GError              **error)
{
  DevdClient *client;
  g_autoptr(DevdProcessService) service = NULL;

  if (!(client = devicectl_get_client (error)))
    return FALSE;

  if (argc != 2)
    return devicectl_arg_error (error, "usage: force-exit IDENTIFIER");

  if (!(service = devd_process_service_new (client, error)))
    return FALSE;

  devd_process_service_force_exit (service, argv[1]);

  return TRUE;
}
