/* devicectl-cmd-get-file.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <glib/gi18n.h>
#include <glib/gstdio.h>

#include "devicectl.h"

static void
progress_cb (goffset  current_num_bytes,
             goffset  total_num_bytes,
             gpointer user_data)
{
  const gchar *filename = user_data;

  devicectl_progress ((gdouble)current_num_bytes / (gdouble)total_num_bytes * 100.0,
                      filename);
}

static void
devicectl_cmd_get_file_cb (GObject      *object,
                           GAsyncResult *result,
                           gpointer      user_data)
{
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;

  if (!devd_transfer_service_get_file_finish (DEVD_TRANSFER_SERVICE (object), result, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);
}

gboolean
devicectl_cmd_get_file (gint                  argc,
                        const gchar * const  *argv,
                        GCancellable         *cancellable,
                        GError              **error)
{
  g_autoptr(GFile) file = NULL;
  g_autoptr(DevdTransferService) service = NULL;
  g_autoptr(GTask) task = NULL;
  DevdClient *client;
  const gchar *local_path;
  const gchar *device_path;
  gboolean ret;
  gint64 started = g_get_monotonic_time ();
  gint64 now;

  if (!(client = devicectl_get_client (error)))
    return FALSE;

  if (argc != 3)
    return devicectl_arg_error (error, "usage: get-file DEVICE_PATH HOST_PATH");

  if (!(service = devd_transfer_service_new (client, error)))
      return FALSE;

  device_path = argv[1];
  local_path = argv[2];

  file = g_file_new_for_commandline_arg (local_path);

  task = g_task_new (NULL, NULL, NULL, NULL);

  devd_transfer_service_get_file_async (service,
                                        device_path,
                                        file,
                                        progress_cb,
                                        g_path_get_basename (device_path),
                                        g_free,
                                        cancellable,
                                        devicectl_cmd_get_file_cb,
                                        g_object_ref (task));

  ret = devicectl_wait_for_task (task, error);

  /* End our progress. */
  g_print ("\n");

  if (ret)
    {
      now = g_get_monotonic_time ();
      g_print ("Copied in %0.2lf seconds.\n",
               (now - started) / (gdouble)G_TIME_SPAN_SECOND);
    }

  return ret;
}
