/* devicectl.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib-unix.h>
#include <libdeviced.h>
#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <sys/ioctl.h>

#include "devicectl.h"

#include "devd-macros.h"

static struct {
  DevdBrowser        *browser;
  DevdClient         *client;
  DevdProcessService *process;
  GCancellable       *cancellable;
  GMainLoop          *main_loop;
  guint               sigint_source;
  guint               sigint_count;
} devicectl;

DevdBrowser *
devicectl_get_browser (void)
{
  return devicectl.browser;
}

static void
append_with_color (GString     *str,
                   const gchar *color,
                   const gchar *event)
{
  if (event != NULL)
    {
      if (color != NULL)
        g_string_append_printf (str, "\033[%sm%s\033[0m", color, event);
      else
        g_string_append (str, event);
    }
}

void
devicectl_advisory (const gchar *color,
                    const gchar *event,
                    const gchar *format,
                    ...)
{
  g_autoptr(GString) str = g_string_new (NULL);
  va_list args;

  if (event != NULL)
    {
      if (isatty (STDIN_FILENO))
        append_with_color (str, color, event);
      else if (event != NULL)
        g_string_append (str, event);
      g_string_append (str, ": ");
    }

  va_start (args, format);
  g_string_append_vprintf (str, format, args);
  va_end (args);

  rl_save_prompt ();
  rl_message ("%s", str->str);
  rl_crlf ();
  rl_restore_prompt ();
  rl_redisplay ();
}

static void
device_added_cb (DevdBrowser *browser,
                 DevdDevice  *device,
                 gpointer     user_data)
{
  devicectl_advisory (DEVD_COLOR_GREEN, "Device Added", "%s at %s",
                      devd_device_get_name (device),
                      devd_device_get_id (device));
}

static void
device_removed_cb (DevdBrowser *browser,
                   DevdDevice  *device,
                   gpointer     user_data)
{
  devicectl_advisory (DEVD_COLOR_YELLOW, "Device Removed", "%s from %s",
                      devd_device_get_name (device),
                      devd_device_get_id (device));
}

static void
load_cb (GObject      *object,
         GAsyncResult *result,
         gpointer      user_data)
{
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;

  if (!devd_browser_load_finish (DEVD_BROWSER (object), result, &error))
    g_printerr ("%s\n", error->message);

  g_task_return_boolean (task, TRUE);
}

static void
setup_browser (void)
{
  g_autoptr(GTask) task = g_task_new (NULL, NULL, NULL, NULL);

  devicectl.browser = devd_browser_new ();
  g_signal_connect (devicectl.browser, "device-added", G_CALLBACK (device_added_cb), NULL);
  g_signal_connect (devicectl.browser, "device-removed", G_CALLBACK (device_removed_cb), NULL);
  devd_browser_load_async (devicectl.browser,
                           devicectl.cancellable,
                           load_cb,
                           g_object_ref (task));

  devicectl_wait_for_task (task, NULL);
}

#define DEVICECTL_COMMAND(func, cmd, help, usage, req_client) \
gboolean func (gint, const gchar * const  *, GCancellable *, GError **);
# include "devicectl-commands.defs"
#undef DEVICECTL_COMMAND

static gboolean
dispatch_command (gint                  argc,
                  const gchar * const  *argv,
                  GError              **error)
{
  if (argc == 0)
    {
      g_set_error (error, G_IO_ERROR, G_IO_ERROR_FAILED, "No command to run");
      return FALSE;
    }

#define DEVICECTL_COMMAND(func, cmd, help, usage, req_client) \
  if (g_strcmp0 (cmd, argv[0]) == 0) \
    return func(argc, argv, devicectl.cancellable, error);
# include "devicectl-commands.defs"
#undef DEVICECTL_COMMAND

  g_set_error (error,
               G_IO_ERROR,
               G_IO_ERROR_FAILED,
               "No such command \"%s\"",
               argv[0]);

  return FALSE;
}

static void
handle_readline_cb (gchar *line)
{
  g_autoptr(GError) error = NULL;
  g_auto(GStrv) argv = NULL;
  gint argc = 0;

  devicectl.sigint_count = 0;

  if (line == NULL)
    {
      rl_crlf ();
      rl_callback_handler_remove ();
      g_main_loop_quit (devicectl.main_loop);
      return;
    }

  if (*line == 0)
    return;

  if (!g_shell_parse_argv (line, &argc, &argv, &error) ||
      !dispatch_command (argc, (const gchar * const *)argv, &error))
    {
      if (isatty (STDOUT_FILENO))
        g_printerr ("\033["DEVD_COLOR_RED"m%s\033[0m\n", error->message);
      else
        g_printerr ("%s\n", error->message);
    }

  /* Success is not required, only that we parsed the command */
  if (argv != NULL && argc > 0)
    add_history (line);
}

static gboolean
stdin_fd_pollin (gpointer data)
{
  rl_callback_read_char ();
  return G_SOURCE_CONTINUE;
}

static gboolean
handle_sigint (gpointer data)
{
  if (devicectl.sigint_count == 1)
    {
      rl_crlf ();
      rl_callback_handler_remove ();
      g_print ("^C pressed again. Exiting.\n");
      exit (EXIT_FAILURE);
    }

  rl_crlf ();
  g_print ("^C detected. Cancelling active operations. Press again to exit.");
  rl_crlf ();
  rl_on_new_line ();
  rl_replace_line ("", 0);
  rl_redisplay ();

  devicectl.sigint_count++;

  g_cancellable_cancel (devicectl.cancellable);
  g_clear_object (&devicectl.cancellable);
  devicectl.cancellable = g_cancellable_new ();

  return G_SOURCE_CONTINUE;
}

gboolean
devicectl_wait_for_task (GTask   *task,
                         GError **error)
{
  g_assert (G_IS_TASK (task));

  while (!g_task_get_completed (task))
    g_main_context_iteration (NULL, TRUE);
  return g_task_propagate_boolean (task, error);
}

gboolean
devicectl_arg_error (GError      **error,
                     const gchar  *format,
                     ...)
{
  g_autofree gchar *msg = NULL;
  va_list args;

  va_start (args, format);
  msg = g_strdup_vprintf (format, args);
  va_end (args);

  g_set_error (error,
               G_IO_ERROR,
               G_IO_ERROR_INVAL,
               "Invalid arguments: %s",
               msg);

  return FALSE;
}

gboolean
devicectl_parse_uint (guint        *vint,
                      const gchar  *arg,
                      GError      **error)
{
  gchar *end = NULL;
  guint64 v64;

  *vint = 0;
  v64 = g_ascii_strtoull (arg, &end, 10);

  if (arg == end)
    return devicectl_arg_error (error, "argument requires an integer");
  if (v64 == G_MAXUINT64 && errno == ERANGE)
    return devicectl_arg_error (error, "argument overflows integer");
  if (v64 == 0 && errno == EINVAL)
    return devicectl_arg_error (error, "argument out of range");
  if (v64 > G_MAXUINT)
    return devicectl_arg_error (error, "argument out of range");
  *vint = (guint)v64;
  return TRUE;
}

static void
on_client_process_exited (DevdProcessService *service,
                          const gchar        *identifier,
                          gint                exit_code,
                          gpointer            user_data)
{
  g_assert (DEVD_IS_PROCESS_SERVICE (service));
  g_assert (identifier != NULL);

  devicectl_advisory (NULL, NULL, "Process %s exited with code %d", identifier, exit_code);
}

static void
on_client_process_signaled (DevdProcessService *service,
                            const gchar        *identifier,
                            gint                term_sig,
                            gpointer            user_data)
{
  g_assert (DEVD_IS_PROCESS_SERVICE (service));
  g_assert (identifier != NULL);

  devicectl_advisory (NULL, NULL, "Process %s terminated due to signal %d", identifier, term_sig);
}

DevdClient *
devicectl_get_client (GError **error)
{
  if (devicectl.client == NULL)
    g_set_error (error,
                 G_IO_ERROR,
                 G_IO_ERROR_INVAL,
                 "This command requires a connection. Use `connect N` to connect to a device.");

  return devicectl.client;
}

void
devicectl_set_client (DevdClient *client)
{
  if (g_set_object (&devicectl.client, client))
    {
      g_autofree gchar *prompt = NULL;

      g_clear_object (&devicectl.process);

      if (client != NULL)
        {
          g_autofree gchar *name = devd_client_get_name (client);

          if (name != NULL)
            prompt = g_strdup_printf ("devicectl[%s]> ", name);
          else
            prompt = g_strdup ("devicectl[unnamed]> ");

          if ((devicectl.process = devd_process_service_new (client, NULL)))
            {
              g_signal_connect (devicectl.process,
                                "process-exited",
                                G_CALLBACK (on_client_process_exited),
                                NULL);
              g_signal_connect (devicectl.process,
                                "process-signaled",
                                G_CALLBACK (on_client_process_signaled),
                                NULL);
            }
        }

      if (prompt != NULL)
        rl_callback_handler_install (prompt, handle_readline_cb);
      else
        rl_callback_handler_install ("devicectl> ", handle_readline_cb);
    }
}

static char **
devicectl_completion (const char *text,
                      gint        start,
                      gint        end)
{
  g_autofree gchar *prefix = g_strndup (text + start, end);
  g_autoptr(GPtrArray) matches = g_ptr_array_new ();
  char **ret;

  if (start == end)
    return NULL;

#define DEVICECTL_COMMAND(func, cmd, help, usage, req_client) \
  if (g_str_has_prefix (cmd, prefix)) \
    g_ptr_array_add (matches, (gchar *)cmd);
# include "devicectl-commands.defs"
#undef DEVICECTL_COMMAND

  if (matches->len == 0)
    return NULL;

  ret = malloc (sizeof (gchar*) * (matches->len + 1));
  for (guint i = 0; i < matches->len; i++)
    ret[i] = strdup (g_ptr_array_index (matches, i));
  ret[matches->len] = NULL;

  return ret;
}

#define BAR_LENGTH 20
#define BAR_CHARS " -=#"

void
devicectl_progress (gdouble      progress,
                    const gchar *status)
{
  g_autoptr(GString) str = g_string_new ("");
  int i;
  int n_full, remainder, partial;
  int width, padded_width;
  int n_columns = 0;
  int last_width = 0;

    {
      struct winsize w;
      if (ioctl (STDOUT_FILENO, TIOCGWINSZ, &w) == 0)
        n_columns = w.ws_col;
      last_width = 0;
    }

  g_string_append (str, "[");

  n_full = (BAR_LENGTH * progress) / 100;
  remainder = progress - (n_full * 100 / BAR_LENGTH);
  partial = (remainder * strlen(BAR_CHARS) * BAR_LENGTH) / 100;

  for (i = 0; i < n_full; i++)
    g_string_append_c (str, BAR_CHARS[strlen(BAR_CHARS)-1]);

  if (i < BAR_LENGTH)
    {
      g_string_append_c (str, BAR_CHARS[partial]);
      i++;
    }

  for (; i < BAR_LENGTH; i++)
    g_string_append (str, " ");

  g_string_append (str, "] ");
  g_string_append (str, status);

  g_print ("\r");
  width = MIN (strlen (str->str), n_columns);
  padded_width = MAX (last_width, width);
  last_width = width;
  g_print ("%-*.*s", padded_width, padded_width, str->str);
}

int
main (int    argc,
      char **argv)
{
  GSource *source;
  guint source_id;

  devicectl.main_loop = g_main_loop_new (NULL, FALSE);
  devicectl.cancellable = g_cancellable_new ();
  devicectl_enable_sigint ();

  setup_browser ();

  /*
   * If there are command line arguments to devicectl, then we want to process
   * them as if we are in the interactive shell, and then exit.
   *
   * TODO: Allow providing a connection id that we can match with
   *       --device or similar.
   */
  if (argc > 1)
    {
      g_autoptr(GError) error = NULL;

      /* Special case --help */
      if (g_strcmp0 (argv[1], "--help") == 0)
        argv[1] = "help";

#define DEVICECTL_COMMAND(func, cmd, help, usage, req_client) \
      if (g_strcmp0 (cmd, argv[1]) == 0) \
        { \
          if (!func(argc-1, (const gchar * const *)&argv[1], devicectl.cancellable, &error)) \
            { \
              if (isatty (STDOUT_FILENO)) \
                g_printerr ("\033["DEVD_COLOR_RED"%s\033[0m\n", error->message); \
              else \
                g_printerr ("%s\n", error->message); \
              return EXIT_FAILURE; \
            } \
          return EXIT_SUCCESS; \
        }
# include "devicectl-commands.defs"
#undef DEVICECTL_COMMAND
    }

  source = g_unix_fd_source_new (STDIN_FILENO, G_IO_IN);
  g_source_set_name (source, "[readline]");
  g_source_set_priority (source, G_PRIORITY_HIGH);
  g_source_set_callback (source, stdin_fd_pollin, NULL, NULL);
  source_id = g_source_attach (source, NULL);

  rl_attempted_completion_function = devicectl_completion;
  rl_callback_handler_install ("devicectl> ", handle_readline_cb);

  g_main_loop_run (devicectl.main_loop);

  g_source_remove (source_id);

  return 0;
}

void
devicectl_disable_sigint (void)
{
  devd_clear_source (&devicectl.sigint_source);
}

void
devicectl_enable_sigint (void)
{
  if (devicectl.sigint_source == 0)
    devicectl.sigint_source = g_unix_signal_add (SIGINT, handle_sigint, NULL);
}
