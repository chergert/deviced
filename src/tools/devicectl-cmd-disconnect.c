/* devicectl-cmd-disconnect.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "devicectl.h"

static void
devicectl_cmd_disconnect_cb (GObject      *object,
                             GAsyncResult *result,
                             gpointer      user_data)
{
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;

  if (!devd_client_disconnect_finish (DEVD_CLIENT (object), result, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_print ("Disconnected.\n");
  devicectl_set_client (NULL);
  g_task_return_boolean (task, TRUE);
}

gboolean
devicectl_cmd_disconnect (gint                  argc,
                          const gchar * const  *argv,
                          GCancellable         *cancellable,
                          GError              **error)
{
  g_autoptr(GTask) task = NULL;
  DevdClient *client;

  if (!(client = devicectl_get_client (error)))
    return FALSE;

  task = g_task_new (NULL, NULL, NULL, NULL);

  devd_client_disconnect_async (client,
                                cancellable,
                                devicectl_cmd_disconnect_cb,
                                g_object_ref (task));

  return devicectl_wait_for_task (task, error);
}
