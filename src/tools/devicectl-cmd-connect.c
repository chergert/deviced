/* devicectl-cmd-connect.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "devicectl.h"

static void
devicectl_cmd_connect_cb (GObject      *object,
                          GAsyncResult *result,
                          gpointer      user_data)
{
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;

  if (!devd_client_connect_finish (DEVD_CLIENT (object), result, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);
}

gboolean
devicectl_cmd_connect (gint                  argc,
                       const gchar * const  *argv,
                       GCancellable         *cancellable,
                       GError              **error)
{
  DevdBrowser *browser = devicectl_get_browser ();
  g_autoptr(GPtrArray) devices = devd_browser_get_devices (browser);
  g_autoptr(DevdClient) client = NULL;
  g_autoptr(GTask) task = NULL;
  g_autofree gchar *name = NULL;
  g_autofree gchar *arch = NULL;
  g_autofree gchar *kernel = NULL;
  g_autofree gchar *system = NULL;
  g_autofree gchar *cert_hash = NULL;
  g_auto(DevdTable) table = {0};
  DevdDevice *device;
  guint id;

  if (argc < 2)
    return devicectl_arg_error (error, "Connect requires a device ID to connect");

  if (!devicectl_parse_uint (&id, argv[1], error))
    return FALSE;

  if (id >= devices->len)
    return devicectl_arg_error (error, "Argument \"%u\" is out of range", id);

  device = g_ptr_array_index (devices, id);
  g_assert (DEVD_IS_DEVICE (device));

  client = devd_device_create_client (device);
  g_assert (DEVD_IS_CLIENT (client));

  cert_hash = devd_browser_get_certificate_hash (browser);
  g_print (_("Connecting to device %s…\n"), devd_device_get_name (device));
  g_print (_("Verify your certficate matches.\n"));
  g_print ("  %s\n", cert_hash);

  task = g_task_new (NULL, NULL, NULL, NULL);

  devd_client_connect_async (client,
                             cancellable,
                             devicectl_cmd_connect_cb,
                             g_object_ref (task));

  if (!devicectl_wait_for_task (task, error))
    return FALSE;

  g_print ("\n");

  devd_table_init (&table, 4, TRUE);
  devd_table_add (&table,
                  0, _("Arch"),
                  1, _("Kernel"),
                  2, _("System"),
                  3, _("Name"),
                  -1);
  g_object_get (client,
                "arch", &arch,
                "kernel", &kernel,
                "system", &system,
                "name", &name,
                NULL);
  devd_table_add (&table,
                  0, arch,
                  1, kernel,
                  2, system,
                  3, name,
                  -1);

  devd_table_print (&table);

  devicectl_set_client (client);

  return TRUE;
}
