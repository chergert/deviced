/* devicectl-cmd-list-devices.c
 *
 * Copyright © 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "devicectl.h"

static const gchar *
get_kind_string (DevdDeviceKind kind)
{
  GTypeClass *klass = g_type_class_peek (DEVD_TYPE_DEVICE_KIND);
  GEnumValue *value = g_enum_get_value (G_ENUM_CLASS (klass), kind);

  if (value != NULL)
    return value->value_nick;

  return NULL;
}

gboolean
devicectl_cmd_list_devices (gint                  argc,
                            const gchar * const  *argv,
                            GCancellable         *cancellable,
                            GError              **error)
{
  DevdBrowser *browser = devicectl_get_browser ();
  g_autoptr(GPtrArray) devices = devd_browser_get_devices (browser);
  g_auto(DevdTable) table = {0};

  if (devices->len == 0)
    {
      g_printerr (_("No devices available.\n"));
      return TRUE;
    }

  devd_table_init (&table, 4, TRUE);
  devd_table_add (&table,
                  0, _("ID"),
                  1, _("Name"),
                  2, _("Kind"),
                  3, _("Address"),
                  -1);

  for (guint i = 0; i < devices->len; i++)
    {
      DevdDevice *device = g_ptr_array_index (devices, i);
      g_autofree gchar *id = g_strdup_printf ("%u", i);
      DevdDeviceKind kind = devd_device_get_kind (device);
      const gchar *kindstr = get_kind_string (kind);

      devd_table_add (&table,
                      0, id,
                      1, devd_device_get_name (device),
                      2, kindstr,
                      3, devd_device_get_id (device),
                      -1);
    }

  devd_table_print (&table);

  return TRUE;
}
