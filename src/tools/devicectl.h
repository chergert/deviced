/* devicectl.h
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <libdeviced.h>

#include "devd-table.h"

G_BEGIN_DECLS

#define DEVD_COLOR_RED    "0;31"
#define DEVD_COLOR_GREEN  "0;32"
#define DEVD_COLOR_YELLOW "1;33"

void          devicectl_progress        (gdouble       progress,
                                         const gchar  *status);
void          devicectl_disable_sigint  (void);
void          devicectl_enable_sigint   (void);
DevdBrowser  *devicectl_get_browser     (void);
DevdClient   *devicectl_get_client      (GError      **error);
void          devicectl_set_client      (DevdClient   *client);
GCancellable *devicectl_get_cancellable (void);
gboolean      devicectl_parse_uint      (guint        *vint,
                                         const gchar  *arg,
                                         GError      **error);
gboolean      devicectl_arg_error       (GError      **error,
                                         const gchar  *format,
                                         ...) G_GNUC_PRINTF (2, 3);
gboolean      devicectl_wait_for_task   (GTask        *task,
                                         GError      **error);
void          devicectl_advisory        (const gchar  *color,
                                         const gchar  *event,
                                         const gchar  *format,
                                         ...) G_GNUC_PRINTF (3, 4);

G_END_DECLS
