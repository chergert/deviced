/* devd-util.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <errno.h>
#include <gio/gio.h>

#include "devd-util.h"
#include "devd-variant-transforms.h"

typedef enum {
  ATTR_TYPE_STRING,
  ATTR_TYPE_BYTE_ARRAY,
  ATTR_TYPE_UINT32,
  ATTR_TYPE_UINT64,
  ATTR_TYPE_BOOLEAN,
  ATTR_TYPE_ICON,
} AttrType;

static gchar *machine_id;
static gchar *device_owner;

gchar *
devd_get_device_kind (void)
{
  g_autoptr(GSettings) settings = g_settings_new ("org.gnome.deviced");

  return g_settings_get_string (settings, "device-kind");
}

const gchar *
devd_get_machine_id (void)
{
  if (g_once_init_enter (&machine_id))
    {
      g_autofree gchar *gen = NULL;

#ifdef __linux__
        {
          g_autoptr(GChecksum) checksum = g_checksum_new (G_CHECKSUM_SHA256);
          g_autofree gchar *machine_bytes = NULL;
          g_autofree gchar *boot_bytes = NULL;
          gsize len;

          /* Our strategy here is to just take the machine-id and
           * salt it with the unique boot-id. That gets us stable
           * values (well per-boot) without having to expose the
           * machines real machine-id to the network.
           */

          if (g_file_get_contents ("/etc/machine-id", &machine_bytes, &len, NULL))
            g_checksum_update (checksum, (const guchar *)machine_bytes, len);

          if (g_file_get_contents ("/proc/sys/kernel/random/boot_id", &boot_bytes, &len, NULL))
            g_checksum_update (checksum, (const guchar *)boot_bytes, len);

          gen = g_strdup (g_checksum_get_string (checksum));
        }
#else
# error "Please come up with a salted machine-id for your OS"
#endif

      g_once_init_leave (&machine_id, g_steal_pointer (&gen));
    }

  return machine_id;
}

const gchar *
devd_get_device_owner (void)
{
  if (g_once_init_enter (&device_owner))
    {
      g_autofree gchar *name = g_strdup (g_get_real_name ());

      if (name != NULL)
        {
          for (gchar *iter = name; *iter; iter = g_utf8_next_char (iter))
            {
              if (g_unichar_isspace (g_utf8_get_char (iter)))
                {
                  *iter = 0;
                  goto done;
                }
            }
        }

      g_free (name);
      name = g_strdup (g_get_user_name ());

    done:
      g_once_init_leave (&device_owner, g_steal_pointer (&name));
    }

  return device_owner;
}

const gchar *
devd_get_device_name (void)
{
  static gchar *device_name;

  if (device_name == NULL)
    {
      g_autoptr(GSettings) settings = g_settings_new ("org.gnome.deviced");
      device_name = g_settings_get_string (settings, "device-kind");
    }

  return device_name;
}

GVariant *
devd_to_gvariant (GObject *object)
{
  g_autofree GParamSpec **pspecs = NULL;
  GVariantDict dict;
  guint len;

  g_assert (G_IS_OBJECT (object));

  pspecs = g_object_class_list_properties (G_OBJECT_GET_CLASS (object), &len);

  g_variant_dict_init (&dict, NULL);

  for (guint i = 0; i < len; i++)
    {
      g_auto(GValue) value = G_VALUE_INIT;

      if (G_IS_PARAM_SPEC_STRING (pspecs[i]))
        {
          g_value_init (&value, G_TYPE_STRING);
          g_object_get_property (G_OBJECT (object), pspecs[i]->name, &value);
          g_variant_dict_insert_value (&dict, pspecs[i]->name,
                                       g_variant_new_string (g_value_get_string (&value)));
        }
      else if (G_IS_PARAM_SPEC_UINT64 (pspecs[i]))
        {
          g_value_init (&value, G_TYPE_UINT64);
          g_object_get_property (G_OBJECT (object), pspecs[i]->name, &value);
          g_variant_dict_insert_value (&dict, pspecs[i]->name,
                                       g_variant_new_uint64 (g_value_get_uint64 (&value)));
        }
      else
        g_warning ("Unsupported type for property %s", pspecs[i]->name);
    }

  return g_variant_take_ref (g_variant_dict_end (&dict));
}

static void
clear_param (gpointer data)
{
  GParameter *p = data;

  /* name is const, but we copied it in when building */
  g_clear_pointer ((gchar **)&p->name, g_free);
  g_value_unset (&p->value);
}

gpointer
devd_from_gvariant (GType     type,
                    GVariant *doc)
{
  static gboolean did_init;
  g_autoptr(GArray) params = NULL;
  GObjectClass *klass;
  GVariantIter iter;
  GVariant *var;
  gchar *key;
  GObject *ret = NULL;

  g_assert (G_TYPE_IS_OBJECT (type));
  g_assert (doc != NULL);

  if (!g_variant_is_of_type (doc, G_VARIANT_TYPE_VARDICT))
    return NULL;

  if (!(klass = g_type_class_ref (type)))
    return NULL;

  if G_UNLIKELY (!did_init)
    {
      devd_register_variant_transforms ();
      did_init = TRUE;
    }

  params = g_array_new (FALSE, FALSE, sizeof (GParameter));
  g_array_set_clear_func (params, clear_param);

  g_variant_iter_init (&iter, doc);

  while (g_variant_iter_loop (&iter, "{sv}", &key, &var))
    {
      GParamSpec *pspec;
      GParameter param = {0};
      GValue boxed = G_VALUE_INIT;

      g_value_init (&boxed, G_TYPE_VARIANT);
      g_value_set_variant (&boxed, var);

      if (!(pspec = g_object_class_find_property (klass, key)))
        {
          g_critical ("No such property %s in type %s",
                      key, g_type_name (type));
          goto failure;
        }

      param.name = g_strdup (key);
      g_value_init (&param.value, pspec->value_type);
      g_value_transform (&boxed, &param.value);
      g_value_unset (&boxed);

      g_array_append_val (params, param);
    }

  ret = g_object_newv (type,
                       params->len,
                       (GParameter *)(gpointer)params->data);

failure:
  g_clear_pointer (&klass, g_type_class_unref);

  return ret;
}

GVariant *
devd_file_info_to_gvariant (GFileInfo *file_info)
{
  g_auto(GStrv) attrs = NULL;
  GVariantDict dict;

  g_return_val_if_fail (G_IS_FILE_INFO (file_info), NULL);

  attrs = g_file_info_list_attributes (file_info, NULL);

  /*
   * We use strings for all values so that we know they come across
   * on the other side without being mutated through JSON encoding
   * issues. (Such as 64-bit integers dropping to 56-bit).
   */

  g_variant_dict_init (&dict, NULL);

  for (guint i = 0; attrs[i] != NULL; i++)
    {
      const gchar *key = attrs[i];
      g_autofree gchar *val = NULL;
      GFileAttributeType type;

      type = g_file_info_get_attribute_type (file_info, key);

      if (type == G_FILE_ATTRIBUTE_TYPE_OBJECT)
        {
          GObject *obj = g_file_info_get_attribute_object (file_info, key);

          if (G_IS_ICON (obj))
            val = g_icon_to_string (G_ICON (obj));
        }

      if (val == NULL)
        val = g_file_info_get_attribute_as_string (file_info, key);

      g_variant_dict_insert (&dict, key, "s", val);
    }

  return g_variant_take_ref (g_variant_dict_end (&dict));
}

static AttrType
get_attr_type (const gchar *attr)
{
  static GHashTable *attrs;

  if (g_once_init_enter (&attrs))
    {
      GHashTable *_attrs = g_hash_table_new (g_str_hash, g_str_equal);

#define ADD_ATTR(SUFFIX, VAL) \
      g_hash_table_insert (_attrs, G_FILE_ATTRIBUTE_##SUFFIX, GINT_TO_POINTER (VAL))

      ADD_ATTR (STANDARD_NAME, ATTR_TYPE_BYTE_ARRAY);
      ADD_ATTR (STANDARD_TYPE, ATTR_TYPE_UINT32);
      ADD_ATTR (STANDARD_SIZE, ATTR_TYPE_UINT64);
      ADD_ATTR (STANDARD_IS_SYMLINK, ATTR_TYPE_BOOLEAN);
      ADD_ATTR (STANDARD_ICON, ATTR_TYPE_ICON);
      ADD_ATTR (STANDARD_SYMBOLIC_ICON, ATTR_TYPE_ICON);

#undef ADD_ATTR

      g_once_init_leave (&attrs, _attrs);
    }

  return GPOINTER_TO_INT (g_hash_table_lookup (attrs, attr));
}

static void
add_to_file_info (GFileInfo   *file_info,
                  const gchar *key,
                  GVariant    *variant)
{
  const gchar *str;
  AttrType type;
  gsize len;

  g_assert (G_IS_FILE_INFO (file_info));
  g_assert (key != NULL);
  g_assert (variant != NULL);

  if (!g_variant_is_of_type (variant, G_VARIANT_TYPE_STRING))
    return;

  str = g_variant_get_string (variant, &len);

  type = get_attr_type (key);

  switch (type)
    {
    case ATTR_TYPE_STRING:
      g_file_info_set_attribute_string (file_info, key, str);
      break;

    case ATTR_TYPE_BYTE_ARRAY:
      g_file_info_set_attribute_byte_string (file_info, key, str);
      break;

    case ATTR_TYPE_UINT32:
    case ATTR_TYPE_UINT64:
      {
        guint64 v;
        gchar *end;

        v = g_ascii_strtoull (str, &end, 10);
        if (v == G_MAXUINT64 && errno == ERANGE)
          return;
        if (v == 0 && errno == EINVAL)
          return;

        if (type == ATTR_TYPE_UINT32)
          g_file_info_set_attribute_uint32 (file_info, key, v);
        else
          g_file_info_set_attribute_uint64 (file_info, key, v);

        break;
      }
      break;

    case ATTR_TYPE_BOOLEAN:
      g_file_info_set_attribute_boolean (file_info, key, g_strcmp0 (str, "TRUE") == 0);
      break;

    case ATTR_TYPE_ICON:
      {
        g_autoptr(GIcon) icon = g_themed_icon_new (str);
        g_file_info_set_attribute_object (file_info, key, G_OBJECT (icon));
      }
      break;

    default:
      break;
    }
}

GFileInfo *
devd_file_info_from_gvariant (GVariant *doc)
{
  g_autoptr(GFileInfo) file_info = NULL;
  GVariantIter iter;
  GVariant *value;
  gchar *key;

  g_return_val_if_fail (doc != NULL, NULL);
  g_return_val_if_fail (g_variant_is_of_type (doc, G_VARIANT_TYPE_VARDICT), NULL);

  file_info = g_file_info_new ();

  g_variant_iter_init (&iter, doc);
  while (g_variant_iter_loop (&iter, "{sv}", &key, &value))
    add_to_file_info (file_info, key, value);

  return g_steal_pointer (&file_info);
}

static void
devd_file_read_with_info_read_cb (GObject      *object,
                                  GAsyncResult *result,
                                  gpointer      user_data)
{
  GFile *file = (GFile *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_autoptr(GFileInputStream) stream = NULL;

  g_assert (G_IS_FILE (file));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!(stream = g_file_read_finish (file, result, &error)))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_pointer (task, g_steal_pointer (&stream), g_object_unref);
}

static void
devd_file_read_with_info_query_cb (GObject      *object,
                                   GAsyncResult *result,
                                   gpointer      user_data)
{
  GFile *file = (GFile *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_autoptr(GFileInfo) file_info = NULL;
  GCancellable *cancellable;

  g_assert (G_IS_FILE (file));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!(file_info = g_file_query_info_finish (file, result, &error)))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_task_set_task_data (task, g_steal_pointer (&file_info), g_object_unref);

  cancellable = g_task_get_cancellable (task);

  g_file_read_async (file,
                     G_PRIORITY_DEFAULT,
                     cancellable,
                     devd_file_read_with_info_read_cb,
                     g_steal_pointer (&task));
}

void
devd_file_read_with_info_async (GFile               *file,
                                const gchar         *attributes,
                                GCancellable        *cancellable,
                                GAsyncReadyCallback  callback,
                                gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (G_IS_FILE (file));
  g_return_if_fail (attributes != NULL);
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (file, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_file_read_with_info_async);

  g_file_query_info_async (file,
                           attributes,
                           G_FILE_QUERY_INFO_NONE,
                           G_PRIORITY_DEFAULT,
                           cancellable,
                           devd_file_read_with_info_query_cb,
                           g_steal_pointer (&task));
}

GInputStream *
devd_file_read_with_info_finish (GFile         *file,
                                 GAsyncResult  *result,
                                 GFileInfo    **info,
                                 GError       **error)
{
  GInputStream *ret;

  g_return_val_if_fail (G_IS_FILE (file), NULL);
  g_return_val_if_fail (G_IS_TASK (result), NULL);

  ret = g_task_propagate_pointer (G_TASK (result), error);

  if (ret != NULL && info != NULL)
    {
      GFileInfo *task_data = g_task_get_task_data (G_TASK (result));
      g_assert (G_IS_FILE_INFO (task_data));
      *info = g_object_ref (task_data);
    }

  return ret;
}

gchar **
devd_strv_append (gchar      **array,
                  const char  *str)
{
  const gsize len = array ? g_strv_length (array) : 0;
  GStrv new_array = g_realloc_n (array, len + 2, sizeof(char*));
  new_array[len] = g_strdup (str);
  new_array[len + 1] = NULL;
  return new_array;
}

static void
devd_create_tmp_in_dir_worker (GTask        *task,
                               gpointer      source_object,
                               gpointer      task_data,
                               GCancellable *cancellable)
{
  GFile *file = source_object;
  g_autoptr(GFileOutputStream) stream = NULL;
  g_autoptr(GString) name = NULL;
  g_autoptr(GFile) child = NULL;
  g_autoptr(GError) error = NULL;

  g_assert (G_IS_TASK (task));
  g_assert (G_IS_FILE (file));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  name = g_string_new (".devicedoutputstream-");

again:
  for (guint i = 0; i < 6; i++)
    {
      gchar ch = g_random_int_range ('A', 'Z');
      g_string_append_c (name, ch);
    }

  child = g_file_get_child (file, name->str);
  stream = g_file_create (child, G_FILE_CREATE_NONE, cancellable, &error);

  /* If we get G_IO_ERROR_EXISTS, we can retry with another random suffix */
  if (error != NULL && !g_error_matches (error, G_IO_ERROR, G_IO_ERROR_EXISTS))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  if (stream == NULL)
    {
      g_clear_error (&error);
      g_clear_object (&child);
      g_string_truncate (name, name->len - 6);
      goto again;
    }

  g_assert (G_IS_FILE_OUTPUT_STREAM (stream));

  g_task_set_task_data (task, g_steal_pointer (&stream), g_object_unref);
  g_task_return_pointer (task, g_steal_pointer (&child), g_object_unref);
}

void
devd_create_tmp_in_dir_async (GFile               *directory,
                              GCancellable        *cancellable,
                              GAsyncReadyCallback  callback,
                              gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (G_IS_FILE (directory));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (directory, cancellable, callback, user_data);
  g_task_set_source_tag (task, devd_create_tmp_in_dir_async);
  g_task_run_in_thread (task, devd_create_tmp_in_dir_worker);
}

GFile *
devd_create_tmp_in_dir_finish (GFile              *file,
                               GAsyncResult       *result,
                               GFileOutputStream **iostream,
                               GError            **error)
{
  GFileOutputStream *task_data;

  g_return_val_if_fail (G_IS_FILE (file), NULL);
  g_return_val_if_fail (G_IS_TASK (result), NULL);

  task_data = g_task_get_task_data (G_TASK (result));

  if (iostream != NULL)
    *iostream = task_data ? g_object_ref (task_data) : NULL;

  return g_task_propagate_pointer (G_TASK (result), error);
}
