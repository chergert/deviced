/* devd-pty.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "devd-pty"

#include "config.h"

#include <errno.h>
#include <fcntl.h>
#include <glib-unix.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>

#include "devd-macros.h"
#include "devd-pty.h"

typedef int pty_fd_t;

typedef struct
{
  GIOChannel *channel;
  pty_fd_t    pty_fd;
  GQueue      write_queue;
  guint       write_source;
  guint       read_source;
} DevdPtyPrivate;

enum {
  PROP_0,
  PROP_FD,
  N_PROPS
};

enum {
  HANDLE_DATA,
  N_SIGNALS
};

G_DEFINE_TYPE_WITH_PRIVATE (DevdPty, devd_pty, G_TYPE_OBJECT)

static GParamSpec *properties [N_PROPS];
static guint signals [N_SIGNALS];

static inline pty_fd_t
pty_fd_steal (pty_fd_t *fd)
{
  pty_fd_t ret = *fd;
  *fd = -1;
  return ret;
}

static void
pty_fd_clear (pty_fd_t *fd)
{
  if (fd != NULL && *fd != -1)
    {
      int rfd = *fd;
      *fd = -1;
      close (rfd);
    }
}

G_DEFINE_AUTO_CLEANUP_CLEAR_FUNC (pty_fd_t, pty_fd_clear)

static pty_fd_t
devd_pty_create_master (void)
{
  g_auto(pty_fd_t) master_fd = DEVD_PTY_INVALID;

  master_fd = posix_openpt (O_RDWR | O_NOCTTY | O_NONBLOCK | O_CLOEXEC);

#ifndef __linux__
  /* Fallback for operating systems that don't support
   * O_NONBLOCK and O_CLOEXEC when opening.
   */
  if (master_fd == DEVD_PTY_INVALID && errno == EINVAL)
    {
      master_fd = posix_openpt (O_RDWR | O_NOCTTY | O_CLOEXEC);

      if (master_fd == DEVD_PTY_INVALID && errno == EINVAL)
        {
          gint flags;

          master_fd = posix_openpt (O_RDWR | O_NOCTTY);
          if (master_fd == -1)
            return DEVD_PTY_INVALID;

          flags = fcntl (master_fd, F_GETFD, 0);
          if (flags < 0)
            return DEVD_PTY_INVALID;

          if (fcntl (master_fd, F_SETFD, flags | FD_CLOEXEC) < 0)
            return DEVD_PTY_INVALID;
        }

      if (!g_unix_set_fd_nonblocking (master_fd, TRUE, NULL))
        return DEVD_PTY_INVALID;
    }
#endif

  return pty_fd_steal (&master_fd);
}

static void
devd_pty_panic (DevdPty *self)
{
  DevdPtyPrivate *priv = devd_pty_get_instance_private (self);
  g_auto(pty_fd_t) fd = DEVD_PTY_INVALID;

  g_assert (DEVD_IS_PTY (self));

  devd_clear_source (&priv->read_source);
  devd_clear_source (&priv->write_source);
  fd = pty_fd_steal (&priv->pty_fd);
  g_clear_pointer (&priv->channel, g_io_channel_unref);
}

static gboolean
devd_pty_on_readable (GIOChannel   *channel,
                      GIOCondition  condition,
                      gpointer      user_data)
{
  DevdPty *self = user_data;
  g_autoptr(GBytes) bytes = NULL;
  GIOStatus status;
  gchar buf[4096];
  gsize n_read = 0;

  g_assert (channel != NULL);
  g_assert (condition & (G_IO_ERR | G_IO_HUP | G_IO_IN));
  g_assert (DEVD_IS_PTY (self));

  if G_UNLIKELY (condition & (G_IO_ERR | G_IO_HUP))
    {
      devd_pty_panic (self);
      return G_SOURCE_REMOVE;
    }

  status = g_io_channel_read_chars (channel, buf, sizeof buf, &n_read, NULL);

  if (status == G_IO_STATUS_EOF)
    {
      bytes = g_bytes_new ("", 0);
      g_signal_emit (self, signals [HANDLE_DATA], 0, bytes);
      return G_SOURCE_REMOVE;
    }

  if G_UNLIKELY (status != G_IO_STATUS_NORMAL)
    {
      devd_pty_panic (self);
      return G_SOURCE_REMOVE;
    }

  bytes = g_bytes_new (buf, n_read);

  g_signal_emit (self, signals [HANDLE_DATA], 0, bytes);

  return G_SOURCE_CONTINUE;
}

static void
devd_pty_set_fd (DevdPty *self,
                 gint     fd)
{
  DevdPtyPrivate *priv = devd_pty_get_instance_private (self);
  g_autoptr(GError) error = NULL;

  g_assert (DEVD_IS_PTY (self));

  if (fd == DEVD_PTY_INVALID)
    fd = devd_pty_create_master ();

  if (fd == DEVD_PTY_INVALID)
    {
      g_warning ("Failed to initialize PTY master");
      return;
    }

  if (!g_unix_set_fd_nonblocking (fd, TRUE, &error))
    {
      g_warning ("Failed to set PTY to non-blocking mode: %s",
                 error->message);
      return;
    }

  priv->pty_fd = fd;

  priv->channel = g_io_channel_unix_new (fd);
  g_io_channel_set_close_on_unref (priv->channel, FALSE);
  g_io_channel_set_encoding (priv->channel, NULL, NULL);
  g_io_channel_set_buffer_size (priv->channel, 4096 * 4);
  g_io_channel_set_buffered (priv->channel, TRUE);

  priv->read_source =
    g_io_add_watch_full (priv->channel,
                         G_PRIORITY_DEFAULT,
                         G_IO_IN | G_IO_ERR | G_IO_HUP,
                         devd_pty_on_readable, self, NULL);
}

static void
devd_pty_finalize (GObject *object)
{
  DevdPty *self = (DevdPty *)object;
  DevdPtyPrivate *priv = devd_pty_get_instance_private (self);

  g_clear_pointer (&priv->channel, g_io_channel_unref);

  if (priv->pty_fd != -1)
    {
      close (priv->pty_fd);
      priv->pty_fd = -1;
    }

  g_queue_foreach (&priv->write_queue, (GFunc)g_bytes_unref, NULL);
  g_queue_clear (&priv->write_queue);

  devd_clear_source (&priv->read_source);

  G_OBJECT_CLASS (devd_pty_parent_class)->finalize (object);
}

static void
devd_pty_get_property (GObject    *object,
                       guint       prop_id,
                       GValue     *value,
                       GParamSpec *pspec)
{
  DevdPty *self = DEVD_PTY (object);
  DevdPtyPrivate *priv = devd_pty_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_FD:
      g_value_set_int (value, priv->pty_fd);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
devd_pty_set_property (GObject      *object,
                       guint         prop_id,
                       const GValue *value,
                       GParamSpec   *pspec)
{
  DevdPty *self = DEVD_PTY (object);

  switch (prop_id)
    {
    case PROP_FD:
      devd_pty_set_fd (self, g_value_get_int (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
devd_pty_class_init (DevdPtyClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = devd_pty_finalize;
  object_class->get_property = devd_pty_get_property;
  object_class->set_property = devd_pty_set_property;

  properties [PROP_FD] =
    g_param_spec_int ("fd",
                      "FD",
                      "The file-descriptor for the PTY",
                      -1, G_MAXINT, -1,
                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  signals [HANDLE_DATA] =
    g_signal_new ("handle-data",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (DevdPtyClass, handle_data),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__BOXED,
                  G_TYPE_NONE, 1, G_TYPE_BYTES);
  g_signal_set_va_marshaller (signals [HANDLE_DATA],
                              G_TYPE_FROM_CLASS (klass),
                              g_cclosure_marshal_VOID__BOXEDv);
}

static void
devd_pty_init (DevdPty *self)
{
  DevdPtyPrivate *priv = devd_pty_get_instance_private (self);

  priv->pty_fd = -1;
  g_queue_init (&priv->write_queue);
}

DevdPty *
devd_pty_new_for_foreign (gint pty_fd)
{
  return g_object_new (DEVD_TYPE_PTY, "fd", pty_fd, NULL);
}

DevdPty *
devd_pty_new (void)
{
  return devd_pty_new_for_foreign (DEVD_PTY_INVALID);
}

static gboolean
pty_fd_set_raw (pty_fd_t fd)
{
  struct termios t;

  if (tcgetattr (fd, &t) == -1)
    return FALSE;

  t.c_lflag &= ~(ICANON | ISIG | IEXTEN | ECHO);
  t.c_iflag &= ~(BRKINT | ICRNL | IGNBRK | IGNCR | INLCR | INPCK | ISTRIP | IXON | PARMRK);
  t.c_oflag &= ~(OPOST);
  t.c_cc[VMIN] = 1;
  t.c_cc[VTIME] = 0;

  if (tcsetattr (fd, TCSAFLUSH, &t) == -1)
    return FALSE;

  return TRUE;
}


gint
devd_pty_create_slave (DevdPty   *self,
                       gboolean   blocking,
                       gboolean   raw,
                       GError   **error)
{
  DevdPtyPrivate *priv = devd_pty_get_instance_private (self);
  g_auto(pty_fd_t) ret = DEVD_PTY_INVALID;
  pty_fd_t master_fd;
  gint extra = blocking ? 0 : O_NONBLOCK;
#if defined(HAVE_PTSNAME_R) || defined(__FreeBSD__)
  char name[256];
#else
  const char *name;
#endif

  g_return_val_if_fail (DEVD_IS_PTY (self), DEVD_PTY_INVALID);
  g_return_val_if_fail (priv->pty_fd != DEVD_PTY_INVALID, DEVD_PTY_INVALID);

  master_fd = priv->pty_fd;

  if (grantpt (master_fd) != 0)
    return DEVD_PTY_INVALID;

  if (unlockpt (master_fd) != 0)
    return DEVD_PTY_INVALID;

#ifdef HAVE_PTSNAME_R
  if (ptsname_r (master_fd, name, sizeof name - 1) != 0)
    return DEVD_PTY_INVALID;
  name[sizeof name - 1] = '\0';
#elif defined(__FreeBSD__)
  if (fdevname_r (master_fd, name + 5, sizeof name - 6) == NULL)
    return DEVD_PTY_INVALID;
  memcpy (name, "/dev/", 5);
  name[sizeof name - 1] = '\0';
#else
  if (NULL == (name = ptsname (master_fd)))
    return DEVD_PTY_INVALID;
#endif

  ret = open (name, O_RDWR | O_CLOEXEC | extra);

  if (ret == DEVD_PTY_INVALID && errno == EINVAL)
    {
      gint flags;

      ret = open (name, O_RDWR | O_CLOEXEC);
      if (ret == DEVD_PTY_INVALID && errno == EINVAL)
        ret = open (name, O_RDWR);

      if (ret == DEVD_PTY_INVALID)
        return DEVD_PTY_INVALID;

      /* Add FD_CLOEXEC if O_CLOEXEC failed */
      flags = fcntl (ret, F_GETFD, 0);
      if ((flags & FD_CLOEXEC) == 0)
        {
          if (fcntl (ret, F_SETFD, flags | FD_CLOEXEC) < 0)
            return DEVD_PTY_INVALID;
        }

      if (!blocking)
        {
          if (!g_unix_set_fd_nonblocking (ret, TRUE, NULL))
            return DEVD_PTY_INVALID;
        }
    }

  if (ret != DEVD_PTY_INVALID)
    {
      if (raw && !pty_fd_set_raw (ret))
        g_warning ("Failed to put PTY in raw mode");
    }

  return pty_fd_steal (&ret);
}

void
devd_pty_set_size (DevdPty *self,
                   guint    columns,
                   guint    rows)
{
  DevdPtyPrivate *priv = devd_pty_get_instance_private (self);

  g_return_if_fail (DEVD_IS_PTY (self));
  g_return_if_fail (priv->pty_fd != DEVD_PTY_INVALID);
  g_return_if_fail (columns > 0);
  g_return_if_fail (rows > 0);

  if (priv->pty_fd != DEVD_PTY_INVALID)
    {
      struct winsize ws = {0};

      ws.ws_col = columns;
      ws.ws_row = rows;

      if (ioctl (priv->pty_fd, TIOCSWINSZ, &ws) != 0)
        g_warning ("Failed to set PTY to cols=%u rows=%u", columns, rows);
    }
}

gint
devd_pty_get_fd (DevdPty *self)
{
  DevdPtyPrivate *priv = devd_pty_get_instance_private (self);

  g_return_val_if_fail (DEVD_IS_PTY (self), DEVD_PTY_INVALID);

  return priv->pty_fd;
}

static gboolean
devd_pty_on_writable (GIOChannel   *channel,
                      GIOCondition  condition,
                      gpointer      user_data)
{
  DevdPty *self = user_data;
  DevdPtyPrivate *priv = devd_pty_get_instance_private (self);
  g_autoptr(GBytes) bytes = NULL;
  GIOStatus status;
  const gchar *wrbuf;
  gsize n_written = 0;
  gsize len = 0;

  g_assert (channel != NULL);
  g_assert (condition & (G_IO_ERR | G_IO_HUP | G_IO_OUT));
  g_assert (DEVD_IS_PTY (self));

  /* Implausable, but for completeness */
  if (priv->write_queue.length == 0)
    goto cancel_writer;

  if (condition & (G_IO_ERR | G_IO_HUP))
    {
      devd_pty_panic (self);
      return G_SOURCE_REMOVE;
    }

  bytes = g_queue_pop_head (&priv->write_queue);
  wrbuf = g_bytes_get_data (bytes, &len);
  status = g_io_channel_write_chars (priv->channel, wrbuf, len, &n_written, NULL);

  if (status != G_IO_STATUS_NORMAL)
    {
      devd_pty_panic (self);
      return G_SOURCE_REMOVE;
    }

  if (n_written < len)
    g_queue_push_head (&priv->write_queue,
                       g_bytes_new_from_bytes (bytes, n_written, len - n_written));

  g_io_channel_flush (priv->channel, NULL);

  if (priv->write_queue.length == 0)
    goto cancel_writer;

  return G_SOURCE_CONTINUE;

cancel_writer:
  priv->write_source = 0;
  return G_SOURCE_REMOVE;
}

static void
devd_pty_queue_flush (DevdPty *self)
{
  DevdPtyPrivate *priv = devd_pty_get_instance_private (self);

  g_assert (DEVD_PTY (self));

  if (priv->write_queue.length > 0 && priv->write_source == 0)
    priv->write_source = g_io_add_watch_full (priv->channel,
                                              G_PRIORITY_HIGH,
                                              G_IO_OUT | G_IO_ERR | G_IO_HUP,
                                              devd_pty_on_writable,
                                              g_object_ref (self),
                                              g_object_unref);
}

void
devd_pty_write (DevdPty *self,
                GBytes  *bytes)
{
  DevdPtyPrivate *priv = devd_pty_get_instance_private (self);

  g_return_if_fail (DEVD_IS_PTY (self));
  g_return_if_fail (bytes != NULL);

  g_queue_push_tail (&priv->write_queue, g_bytes_ref (bytes));

  devd_pty_queue_flush (self);
}

void
devd_pty_close (DevdPty *self)
{
  DevdPtyPrivate *priv = devd_pty_get_instance_private (self);
  g_auto(pty_fd_t) fd = DEVD_PTY_INVALID;

  g_return_if_fail (DEVD_IS_PTY (self));

  devd_clear_source (&priv->write_source);
  devd_clear_source (&priv->read_source);
  g_clear_pointer (&priv->channel, g_io_channel_unref);
  fd = pty_fd_steal (&priv->pty_fd);
}
