/* devd-util.h
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

gchar         *devd_get_device_kind            (void);
const gchar   *devd_get_machine_id             (void) G_GNUC_CONST;
const gchar   *devd_get_device_owner           (void) G_GNUC_CONST;
const gchar   *devd_get_device_name            (void) G_GNUC_CONST;
gchar        **devd_strv_append                (gchar                **array,
                                                const char            *str);
GVariant      *devd_to_gvariant                (GObject               *object);
gpointer       devd_from_gvariant              (GType                  type_id,
                                                GVariant              *doc);
GVariant      *devd_file_info_to_gvariant      (GFileInfo             *file_info);
GFileInfo     *devd_file_info_from_gvariant    (GVariant              *doc);
void           devd_file_read_with_info_async  (GFile                 *file,
                                                const gchar           *attributes,
                                                GCancellable          *cancellable,
                                                GAsyncReadyCallback    callback,
                                                gpointer               user_data);
GInputStream  *devd_file_read_with_info_finish (GFile                 *file,
                                                GAsyncResult          *result,
                                                GFileInfo            **info,
                                                GError               **error);
void           devd_create_tmp_in_dir_async    (GFile                 *directory,
                                                GCancellable          *cancellable,
                                                GAsyncReadyCallback    callback,
                                                gpointer               user_data);
GFile         *devd_create_tmp_in_dir_finish   (GFile                 *file,
                                                GAsyncResult          *result,
                                                GFileOutputStream    **iostream,
                                                GError               **error);

G_END_DECLS
