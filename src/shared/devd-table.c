/* devd-table.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either rsion 3 of the License, or
 * (at your option) any later rsion.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without en the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should ha receid a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "devd-table"

#include <string.h>
#include <stdio.h>
#include <unistd.h>

#include "devd-table.h"

void
devd_table_init (DevdTable *self,
                 guint      n_columns,
                 gboolean   first_is_title)
{
  g_assert (self != NULL);
  g_assert (n_columns > 0);

  memset (self, 0, sizeof *self);

  self->values = g_ptr_array_new_with_free_func (g_free);
  self->n_columns = n_columns;
  self->first_is_title = !!first_is_title;
}

void
devd_table_add (DevdTable *self,
                gint       first_column,
                ...)
{
  const gchar *value;
  gint column = first_column;
  va_list args;
  guint base;

  g_assert (self != NULL);
  g_assert (self->n_columns > 0);
  g_assert (self->values != NULL);

  base = self->values->len;
  g_ptr_array_set_size (self->values, self->values->len + self->n_columns);

  g_assert (base % self->n_columns == 0);

  va_start (args, first_column);

  while (column >= 0)
    {
      if G_UNLIKELY (column >= self->n_columns)
        {
          g_critical ("Invalid column: %d", column);
          va_end (args);
          return;
        }

      value = va_arg (args, const gchar *);
      g_ptr_array_index (self->values, base + column) = g_strdup (value);
      column = va_arg (args, gint);
    }

  va_end (args);
}

void
devd_table_print (DevdTable *self)
{
  g_autoptr(GString) str = NULL;
  g_autofree guint *longest = NULL;

  g_assert (self != NULL);
  g_assert (self->values != NULL);
  g_assert (self->n_columns > 0);
  g_assert (self->values->len % self->n_columns == 0);

  longest = g_new0 (guint, self->n_columns);

  /* Calculate longest value of each column */

  for (guint i = 0; i < self->values->len; i += self->n_columns)
    {
      for (guint j = 0; j < self->n_columns; j++)
        {
          const gchar *col = g_ptr_array_index (self->values, i + j);
          guint len = col ? strlen (col) : 0;

          if (len > longest[j])
            longest[j] = len;
        }
    }

  /* Add edge spacing */
  for (guint i = 0; i < self->n_columns; i++)
    longest[i] += 2;

  str = g_string_new (NULL);

  for (guint i = 0; i < self->values->len; i += self->n_columns)
    {
      for (guint j = 0; j < self->n_columns; j++)
        {
          gchar *col = g_ptr_array_index (self->values, i + j);
          guint len = col ? strlen (col) : 0;

          if (col != NULL)
            {
              if (i == 0 && self->first_is_title && isatty (STDOUT_FILENO))
                g_string_append_printf (str, "\x1b[1m%s\x1b[22m", col);
              else
                g_string_append (str, col);
            }

          for (guint k = len; k < longest[j]; k++)
            g_string_append_c (str, ' ');
        }

      g_string_append_c (str, '\n');
    }

  g_print ("%s", str->str);
}

void
devd_table_clear (DevdTable *self)
{
  g_clear_pointer (&self->values, g_ptr_array_unref);
  memset (self, 0, sizeof *self);
}
