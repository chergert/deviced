/* devd-pty.h
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

#define DEVD_TYPE_PTY (devd_pty_get_type())
#define DEVD_PTY_INVALID (-1)

G_DECLARE_DERIVABLE_TYPE (DevdPty, devd_pty, DEVD, PTY, GObject)

struct _DevdPtyClass
{
  GObjectClass parent_class;

  void (*handle_data) (DevdPty *self,
                       GBytes  *data);

  /*< private >*/
  gpointer _reserved[8];
};

DevdPty *devd_pty_new             (void);
DevdPty *devd_pty_new_for_foreign (gint       pty_fd);
void     devd_pty_close           (DevdPty   *self);
gint     devd_pty_get_fd          (DevdPty   *self);
gint     devd_pty_create_slave    (DevdPty   *self,
                                   gboolean   blocking,
                                   gboolean   raw,
                                   GError   **error);
void     devd_pty_write           (DevdPty   *self,
                                   GBytes    *bytes);
void     devd_pty_set_size        (DevdPty   *self,
                                   guint      columns,
                                   guint      rows);

G_END_DECLS
