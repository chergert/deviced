# Contributing to Deviced

## Code Style

Make it look like everything else in the file.
In general, gtk+ coding conventions with C11 features like `g_autoptr`.
If in doubt, ask.

## License

The library is licensed under GPLv3+.
Your contributions must be compatible with that.

## Overview

The code is broken into a few groups.

 - The daemon, which runs on the target device.
 - The library, which is used by applications to consume deviced.
 - The shared utility code, which is a static library linked into both the daemon and library.
 - The devicectl utility command line program.

## Abstractions

### Client Library

#### DevdBrowser

This object is used by the library to discover new devices.
They could come from the network, or USB, or some other third-party system we add support for.

#### DevdDevice

The browse discovers devices.
The DevdDevice object represents the minimal information we know about a device before connecting to it.
It also deals with creating a new DevdClient that can connect to the device.
This may be device specific and determined by how we discovered the device (usb, network, etc).

#### DevdClient

The client is used to connect to the device.
It can also perform a number of basic operations.
More advanced features are implemented in services.

#### DevdService

A service is a collection of related methods.
Not all services are supported on a device.
So this is a convenient way for us to enable/disable that code as a group for a particular device.

There are subclasses of this object, which perform specific tasks.
For example, the DevdFlatpakService performs flatpak specific operations, but is only availble when the daemon supports flatpak.

### Daemon

#### DevdAppProvider

So that we can provide a unified interface for basic operations, the AppProvider interface tries to abstract some basic app management features.

#### DevdService

This is the daemon side of the DevdService used in the client library.
It implements various RPCs that can be called.
The subclasses of this are only registered when the device supports that feature (usually, compile time).

#### DevdNetworkManager

This listens to changes in the ntework configuration and updates the listener as necessary.

### DevdNetworkListener

This is an active socket listener, converting JSONRPC messages into operations, such as to the registered services.

