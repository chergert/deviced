# Deviced

Deviced is a daemon that can be run on a development device that will allow IDEs and other tooling to interact with the device.
It comes with a daemon `deviced`, a library `libdeviced`, and a command line tool `devicectl`.

This project is in it's fetal state.
It can not yet do anything.
So just chill out for a bit while we build it.

## Not Yet Secure

The most important thing you should know is that this is not yet secure.
Network communication is performed using TLS with self-signed certificates.
Work still needs to be done around certificate pinning and client-side certificate acceptance.

### Security Status

 * Use TLS with self-signed server / client certificates 🗸
 * Require interactive authorization on device for new client cert 🗸
 * Automatically re-authorize followup connections for authorized cert 🗸
 * Display notification while client is connected to device 🗸
 * Require interactive authorization on client to verify device cert
 * Pin peer certificate with socket address address on client and device

## Assumptions

We currently assume Linux and GNOME-based target devices.
We will happily accept patches to broaden the scope as long as they are reasonable, well written, and maintainable.

## Design Changes and Open Questions

There are a few things that'd I'd like to consider changing, but don't make sense to do now until we have a functional product.

 * Should we switch to libsoup so we can do more advanced TLS/Compression things?
   Or should we simply require LZMA compression on the wire.
 * Should we use raw content-data for file-transfers (requires patching jsonrpc-glib).
 * We can proxy PTY data, but handling all the corner cases could use some work.
 * We might want to have a polkit client in libdeviced so that we can authorize service restarts.
   However, we want this to not use the host polkit agent which is compositor-wide.
   Instead we want something that allows us to attach UI to the widget representing the device in the IDE.

## USB Support

We don't currently have USB connectivity, but it is planned after the network support reaches feature completion.
It is likely we'll be able to reuse a lot of the network listener and client code, but over a virtual USB driver using FunctionFS.
At least, that is our current plan.
The `DevdBrowser` will need to listen for `udev` events and setup the appropriate communication channels.

Some care will need to be taken so that we can consume this from inside sandboxed tooling, such as Flatpak.

## libdeviced

This library is meant to be used from IDEs.
Builder will be the first consumer of this to provide external device integration.

Currently, the only goal for libdeviced is to abstract connections to deviced enabled devices.
Depending on how well the abstractions work, we may consider abstracting other classes of devices to simplify IDE tooling.
It seems pretty likely that we'd also get a SSH-based device at least.

## deviced daemon

This daemon is intended to run in the user session.
It can accept network connections to perform operations on the device.

 - Communication is performed over TLS.
 - There is a GSetting to allow network connections and select for which networks are enabled.
 - Avahi is used to advertise the device to machines on the local network segments.

To enable devices to connect to your deviced, you need to enable networking support and what networks are allowed to connect.

```sh
$ nmcli connection show Your-Network-Name | grep uuid
connection.uuid:                        ba362dfe-1123-1ee1-1111-123412312312
$ devicectl
devicectl> enable-network ba362dfe-1123-1ee1-1111-123412312312
Device Added: Christian's computer (starthink) at 192.168.122.1:41131
devicectl>
```

To disable network connections, use the `disable-network` command.
It optionally takes network UUID as parameters.

```sh
devicectl> disable-network
Device Removed: Christian's computer (starthink) at 192.168.122.1:41131
```

### More Information

GSettings is used behind the scenes to determine what and how things can connect.
The command simply sets these values for you.

```sh
$ gsettings set org.gnome.deviced enable-network true
$ gsettings set org.gnome.deviced enabled-connections "['ba362dfe-1123-1ee1-1111-123412312312']"
```

The expectation is that this will eventually be integrated into gnome-control-center similar to the "Sharing" panel.

## devicectl

This is a readline-based command-line tool to interact with devices.
You use it to discover and connect to devices.
At that point, you'll be able to perform operations on the device.

```
[christian@tinybox build]$ ./src/tools/devicectl
Device Added: Christian's computer (onda) at [fe80::c99:c1d7:7ff4:5b3b]:33141
Device Added: Christian's computer (onda) at 192.168.0.24:33141
Device Added: Christian's computer (flightbox) at 192.168.0.4:37229
devicectl> list-devices
ID  Name                              Address
0   Christian's computer (onda)       [fe80::c99:c1d7:7ff4:5b3b]:33141
1   Christian's computer (onda)       192.168.0.24:33141
2   Christian's computer (flightbox)  192.168.0.4:37229
devicectl> connect 1
Connecting to device Christian's computer (onda)…
Verify your certficate matches.
  0123456789012345678901234567890123456789012345678901234567891105

Arch    Kernel  System  Name
x86_64  Linux   gnu     onda
devicectl[onda]> help 
Command            Description
help               Display this list of help commands and usage
connect            Connect to an available device
disconnect      *  Disconnect from the currently connected device
list-devices       Display this list of known devices
list-files      *  Display this list of help commands and usage
list-apps       *  Display this list of available applications on the device
list-runtimes   *  Display this list of available runtimes on the device
run-app         *  Run an application on the device
put-file        *  Upload a file to device
get-file        *  Download a file from device
install-bundle  *  Install a Flatpak bundle on the device
force-exit      *  Force an application to exit
send-signal     *  Send an application a signal
sync            *  Request that the device sync file-systems
shell           *  Spawn a shell on the device and enter the shell

* denotes that command requires an active client connection.
devicectl[onda]> list-files /home/christian
Name           Size       Content Type
Videos         4.1 kB     inode/directory
Pictures       4.1 kB     inode/directory
Music          4.1 kB     inode/directory
Documents      4.1 kB     inode/directory
Projects       4.1 kB     inode/directory
Downloads      4.1 kB     inode/directory
Desktop        4.1 kB     inode/directory
devicectl[onda]> 
```

## Testing it out

If you want to play with what we have so far (not much) we use the meson build system.

### Dependencies

You'll need a recent `gio-2.0` (part of GLib), `gnutls`, `readline`, and `jsonrpc-glib` and their development headers.

We use `gnutls` for TLS certificate generation due to license compatibility with GPLv3 software.
`readline` is used for shell-like interactive control in devicectl.
The whole thing is built on `GObject` (so gio is a natural choice) and `jsonrpc-glib` for our RPC communication to allow easy integration from other language and tooling.

### Building

```
git clone https://gitlab.gnome.org/chergert/deviced.git
cd deviced
meson build
cd build
ninja
```

You can choose to install it if you like (`ninja install`), since I develop in JHbuild, I use `--prefix=/opt/gnome --libdir=lib` options like `meson build --prefix=/opt/gnome --libdir=lib`.

You can also run it out of tree (shown below).
If you install it, you can just run `deviced` and `devicectl`.

```
# From inside of build/
$ GNOTIFICATION_BACKEND=freedesktop ./src/daemon/deviced
```

If you installed to `/usr` or `/usr/local`, specifying the notification backend should not be needed.

Make sure you allow network connections and the specific network segment you're on.
(See gsetings usage above).
Otherwise you won't get any network advertisements and be able to find the node.

Now run the device controller.

```
# From inside of build
$ ./src/tools/devicectl
```
